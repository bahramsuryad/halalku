﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Giift.SuciHalal.CommonConstants
{
    public class URLConstants
    {
        //public const string SignUp = "https://sandbox.giift.com/api/auth/v1/oauth2/token";
        //public const string SignIn = "https://sandbox.giift.com/api/auth/v1/oauth2/token";
        //public const string ForgotPassword = "https://sandbox.giift.com/api/user/v1/user/forgot_password?email={0}&name_id={1}";
        //public const string SubscribeNewsletter = "https://sandbox.giift.com/api/user/v1/user/item?no_email=true";

        //public const string BuyableList = "https://sandbox.giift.com/api/cart/v2/buyable/list?name_id={0}{1}";

        ////public const string BuyableList = "https://sandbox.giift.com/api/cart/v2/buyable/list?country={0}&type={1}";
        ////public const string BuyableListForCountry = "https://sandbox.giift.com/api/cart/v2/buyable/list?country={0}";
        ////public const string BuyableListForType = "https://sandbox.giift.com/api/cart/v2/buyable/list?type={0}";
        //public const string BuyableDetails = "https://sandbox.giift.com/api/cart/v2/buyable/item/{0}/{1}?currency={2}";
        //public const string RetrieveContent = "https://sandbox.giift.com/api/retailer/v0/content/find";
        //public const string UpdateProfile = "https://sandbox.giift.com/api/user/v1/user/item";
        //public const string GetAllCart = "https://sandbox.giift.com/api/cart/v1/cart/all";
        //public const string NewCart = "https://sandbox.giift.com/api/cart/v1/cart/new";
        //public const string AddCartItem = "https://sandbox.giift.com/api/cart/v1/item/item";
        //public const string RemoveCartItem = "https://sandbox.giift.com/api/cart/v1/item/item/{0}";
        //public const string CartCheckout = "https://sandbox.giift.com/api/cart/v1/cart/order/{0}";
        //public const string OrderDelivery = "https://sandbox.giift.com/api/cart/v1/order/delivery/{0}?create_only={1}";
        //public const string GetDeliveryProvider = "https://sandbox.giift.com/api/cart/v1/orderitem/delivery_providers/{0}";
        //public const string SetDeliveryProduct = "https://sandbox.giift.com/api/cart/v1/orderitem/delivery/{0}";
        //public const string SetDeliveryGiftCard = "https://sandbox.giift.com/api/cart/v1/orderitem/delivery/{0}";
        //public const string CreatePayment = "https://sandbox.giift.com/api/cart/v1/order/payment/{0}";
        //public const string GetPaymentProvider = "https://sandbox.giift.com/api/cart/v1/order/available_payment_providers/{0}";
        //public const string PaymentPay = "https://sandbox.giift.com/api/cart/v1/payment/pay/{0}";
        //public const string GetAllOrders = "https://sandbox.giift.com/api/cart/v1/order/all";
        //public const string GetAllOrdersCopy = "https://sandbox.giift.com/api/cart/v1/order/item/{0}";
        //public const string GetRestaurantLocations = "https://sandbox.giift.com/api/user/v0/retailer/location/find";
        //public const string GetMosqueLocations = "https://sandbox.giift.com/api/user/v0/retailer/location/find";
        //public const string CartDetails = "https://sandbox.giift.com/api/cart/v1/cart/item/{0}";
        //public const string LocationSettings = "https://sandbox.giift.com/api/user/v0/retailer/location/setting/find";

        //public const string ChangePassword = "https://sandbox.giift.com/api/user/v0/user/password";
        //public const string GetUserProfile = "https://sandbox.giift.com//api/user/v1/user/item";

        //public static string GetCategories = "https://sandbox.giift.com/api/cart/v2/buyable/categories-tree?type={0}&name_id={1}&buying_with={2}";
        //public static string GetMetas = "https://sandbox.giift.com/api/cart/v2/buyable/metas?name_id={0}{1}";

        //public const string SetDeliveryEmail = "https://sandbox.giift.com/api/cart/v1/orderitem/delivery/{0}";

        public const string SignUp = "https://www.giift.com/api/auth/v1/oauth2/token";
        public const string SignIn = "https://www.giift.com/api/auth/v1/oauth2/token";
        public const string ForgotPassword = "https://www.giift.com/api/user/v1/user/forgot_password?email={0}&name_id={1}";
        public const string SubscribeNewsletter = "https://www.giift.com/api/user/v1/user/item?no_email=true";

        public const string BuyableList = "https://www.giift.com/api/cart/v2/buyable/list?name_id={0}{1}";

        //public const string BuyableList = "https://www.giift.com/api/cart/v2/buyable/list?country={0}&type={1}";
        //public const string BuyableListForCountry = "https://www.giift.com/api/cart/v2/buyable/list?country={0}";
        //public const string BuyableListForType = "https://www.giift.com/api/cart/v2/buyable/list?type={0}";
        public const string BuyableDetails = "https://www.giift.com/api/cart/v2/buyable/item/{0}/{1}?currency={2}";
        public const string RetrieveContent = "https://www.giift.com/api/retailer/v0/content/find";
        public const string UpdateProfile = "https://www.giift.com/api/user/v1/user/item";
        public const string GetAllCart = "https://www.giift.com/api/cart/v1/cart/all?status={0}";
        public const string NewCart = "https://www.giift.com/api/cart/v1/cart/new";
        public const string AddCartItem = "https://www.giift.com/api/cart/v1/item/item";
        public const string RemoveCartItem = "https://www.giift.com/api/cart/v1/item/item/{0}";
        public const string CartCheckout = "https://www.giift.com/api/cart/v1/cart/order/{0}";
        public const string OrderDelivery = "https://www.giift.com/api/cart/v1/order/delivery/{0}?create_only={1}";
        public const string GetDeliveryProvider = "https://www.giift.com/api/cart/v1/orderitem/delivery_providers/{0}";
        public const string SetDeliveryProduct = "https://www.giift.com/api/cart/v1/orderitem/delivery/{0}";
        public const string SetDeliveryGiftCard = "https://www.giift.com/api/cart/v1/orderitem/delivery/{0}";
        public const string CreatePayment = "https://www.giift.com/api/cart/v1/order/payment/{0}";
        public const string GetPaymentProvider = "https://www.giift.com/api/cart/v1/order/available_payment_providers/{0}";
        public const string PaymentPay = "https://www.giift.com/api/cart/v1/payment/pay/{0}";
        public const string GetAllOrders = "https://www.giift.com/api/cart/v1/order/all";
        public const string GetAllOrdersCopy = "https://www.giift.com/api/cart/v1/order/item/{0}";
        //public const string GetRestaurantLocations = "https://www.giift.com/api/user/v0/retailer/location/find";
        //public const string GetMosqueLocations = "https://www.giift.com/api/user/v0/retailer/location/find";
        public const string GetRestaurantLocations = "https://www.giift.com/api/user/v1/retailer/location/list?latitude={0}&longitude={1}&order_by=distance&retailer_id={2}&page={3}&per_page={4}";
        public const string GetMosqueLocations = "https://www.giift.com/api/user/v1/retailer/location/list?latitude={0}&longitude={1}&order_by=distance&retailer_id={2}&page={3}&per_page={4}";
        public const string CartDetails = "https://www.giift.com/api/cart/v1/cart/item/{0}";
        public const string LocationSettings = "https://www.giift.com/api/user/v0/retailer/location/setting/find";

        public const string ChangePassword = "https://www.giift.com/api/user/v0/user/password";
        public const string GetUserProfile = "https://www.giift.com//api/user/v1/user/item";

        public static string GetCategories = "https://www.giift.com/api/cart/v2/buyable/categories-tree?type={0}&name_id={1}&buying_with={2}";
        public static string GetMetas = "https://www.giift.com/api/cart/v2/buyable/metas?name_id={0}{1}";

        public const string SetDeliveryEmail = "https://www.giift.com/api/cart/v1/orderitem/delivery/{0}";


        public const string ClientId = "visa_halal";
        //public const string ClientSecret = "5bf9e4969c5e56a6372c8b009236813b574b21ec"; //www key
        public const string ClientSecret = "ad1aa268dc748470990f7257e233e38c03d99a43"; //Production Key
        public const string Grant_Signup = "signup";
        public const string Grant_SignIn = "password";
        public const string Scope = "user wallet cart";
        public const string Country = "MY";

        public const string Email_From = "GiftHalal";
        public const string S_Email = "visa_halal_my@giift.com";

        public const string Grant_Guest = "client_credentials";
        public const string Scope_Guest = "wallet cart";


    }
}
