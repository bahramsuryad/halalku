﻿using Framework.EnterpriseLibrary.Adapters;
using Giift.SuciHalal.Entities;
using Giift.SuciHalal.Helper;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SuciHalalWebclient
{
    public partial class Mosque : System.Web.UI.Page
    {
        public static List<MosqueDetails> lobjListOfMosqueDetails = null;
        public static string pstrAccessToken = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        public static MosqueLocations GetNearbyMosque(int pintPageNo, int pintTakeRecords, string pstrLatitude, string pstrLongitude)
        {
            try
            {
                string lstrResponse = string.Empty;
                if (string.IsNullOrEmpty(pstrLatitude) || string.IsNullOrEmpty(pstrLongitude))
                {
                    pstrLatitude = "3.142615";
                    pstrLongitude = "101.691110";
                }

                List<MosqueDetails> lobjListOfMosqueDetails = new List<MosqueDetails>();

                string lstrMosqueRetailerId = Convert.ToString(ConfigurationManager.AppSettings["MosqueRetailerId"]);
                lstrResponse = ApiHelper.GetMosqueLocations(lstrMosqueRetailerId, pstrLatitude, pstrLongitude, pintPageNo, pintTakeRecords);

                RetailerLocations lobjRetailerLocations = new RetailerLocations();
                lobjRetailerLocations = JsonConvert.DeserializeObject<RetailerLocations>(lstrResponse);

                foreach (Data obj in lobjRetailerLocations.data)
                {
                    MosqueDetails lobjMosqueDetails = new MosqueDetails();
                    lobjMosqueDetails.Id = obj.id;
                    lobjMosqueDetails.Name = obj.name;
                    lobjMosqueDetails.Address = obj.address;
                    lobjMosqueDetails.Longitude = obj.longitude;
                    lobjMosqueDetails.Latitude = obj.latitude;
                    lobjMosqueDetails.PhoneNo = obj.phone == null ? "" : Convert.ToString(obj.phone);
                    lobjMosqueDetails.ImageUrl = obj.settings.Exists(lobj => lobj.name.Equals("photo_large")) ? (obj.settings.Find(lobj => lobj.name.Equals("photo_large")).value.Contains("app_default") ? "images/mosque.jpg" : obj.settings.Find(lobj => lobj.name.Equals("photo_large")).value) : "images/mosque.jpg";
                    lobjMosqueDetails.ServiceLanguage = obj.settings.Exists(lobj => lobj.name.Equals("language_of_service")) ? obj.settings.Find(lobj => lobj.name.Equals("language_of_service")).value : "";
                    lobjMosqueDetails.Distance = Math.Round((GetDistance(lobjMosqueDetails.Latitude, lobjMosqueDetails.Longitude, Convert.ToDouble(pstrLatitude), Convert.ToDouble(pstrLongitude), 'K')), 2); //M
                    lobjListOfMosqueDetails.Add(lobjMosqueDetails);
                }

                HttpContext.Current.Session["MosqueDetails"] = lobjListOfMosqueDetails;

                MosqueLocations lobjMosqueLocations = new MosqueLocations();
                lobjMosqueLocations.MDetails = lobjListOfMosqueDetails;
                lobjMosqueLocations.TotalPages = lobjRetailerLocations.metadata.total_pages;

                return lobjMosqueLocations;
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("Mosque.aspx.cs : GetNearbyMosque :" + ex.Message + Environment.NewLine + ex.StackTrace);
                return null;
            }
        }

        private static double GetDistance(double lat1, double lon1, double lat2, double lon2, char unit)
        {
            if ((lat1 == lat2) && (lon1 == lon2))
            {
                return 0;
            }
            else
            {
                double theta = lon1 - lon2;
                double dist = Math.Sin(deg2rad(lat1)) * Math.Sin(deg2rad(lat2)) + Math.Cos(deg2rad(lat1)) * Math.Cos(deg2rad(lat2)) * Math.Cos(deg2rad(theta));
                dist = Math.Acos(dist);
                dist = rad2deg(dist);
                dist = dist * 60 * 1.1515;
                if (unit == 'K')
                {
                    dist = dist * 1.609344;
                }
                else if (unit == 'N')
                {
                    dist = dist * 0.8684;
                }
                return (dist);
            }
        }

        private static double deg2rad(double deg)
        {
            return (deg * Math.PI / 180.0);
        }

        private static double rad2deg(double rad)
        {
            return (rad / Math.PI * 180.0);
        }
    }
}