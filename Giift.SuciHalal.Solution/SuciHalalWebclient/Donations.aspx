﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SuciHalalSite.Master" AutoEventWireup="true" CodeBehind="Donations.aspx.cs" Inherits="SuciHalalWebclient.Donations" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="tools_list" style="background-color: #eee;">
        <div class="banner sub"
            style="background-image: -webkit-gradient(linear, 0% 0%, 0% 100%, from(rgba(0, 0, 0, 0.3)), to(rgba(0, 0, 0, 0.3))), url('https://images.unsplash.com/photo-1506869673173-31eba16560ab?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1050&q=80'); background-size: cover; background-position: center 80%;">
            <div class="container center in-sub w-container">
                <h1 class="sub-heading-title">Donations</h1>
                <div class="breadcrumb">
                    <div class="breadcrumb-content">
                        <a class="bread-link" href="index.aspx">Home</a>
                        <img alt="" src="https://uploads-ssl.webflow.com/5c3ca7fbdc8fab3302b31532/5c41f1941b7ac65ffde90630_right-arrow%20(3).svg" width="18">
                        <div class="bread-link gray-color">Donations</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="section">
            <h3 style="margin-top: 0px; margin-bottom: 10px; text-align: center;">My Halal Stop Donations</h3>
            <h5 style="margin-top: 0px; margin-bottom: 30px; text-align: center;">Making a donation is the ultimate sign of solidarity!</h5>


            <div id="divDonations" class="donation-list-content-container-new" runat="server">
                <%--<a href="./DonationDetail.aspx">
                    <div class="donation-list-card">
                        <img class="donation-list-card-image" src="https://i.ytimg.com/vi/K8I1fmuSOSk/0.jpg" />
                        <div class="donation-list-card-content">
                            <p class="donation-list-card-location">Alexandria, VA</p>
                            <p class="donation-list-card-name">Building Home for Orphans</p>
                        </div>
                        <div class="donation-list-card-content">
                            <p class="donation-list-card-for">Funds go to <b>Penny Appeal USA</b> <i class="fa fa-check" style="color: lawngreen;"></i></p>
                            <p class="donation-list-card-last-donation">Last donation 1m ago</p>
                            <div class="progress-bar progress-bar-behind">
                                <div class="progress-bar progress-bar-fill" style="width: 84%;"></div>
                            </div>
                            <p class="donation-list-card-last-donation-total"><span style="color: #000; font-weight: 800;">$5000</span> of $6500</p>
                        </div>
                        <div class="donation-list-card-footer">
                            <div class="donation-list-card-book-container">
                                <p style="margin: 0px; width: 160px;" class="donation-list-card-book-now">Donate Now</p>
                            </div>
                        </div>
                    </div>
                </a>--%>
            </div>
            <div class="w-pagination-wrapper pagination"></div>
            <div id="divPagination" style="display: none;" class="center">
                <ul id="ulPagingItems" class="pagination"></ul>
            </div>
        </div>
    </div>
    <div id="divLoader" class="divLoader" style="display: none;"></div>
    <script src="js/jquery-3.3.1.min.js"></script>
    <script type="text/javascript">
        var lintPageSize = 15;
        var lintTotalPages = 0;

        $(document).ready(function () {
            $("#ContentPlaceHolder1_divDonations").text("");
            BindDonations();
        });

        function BindDonations() {
            $('#divLoader').show();
            $.ajax({
                type: "POST",
                url: "Donations.aspx/BindDonations",
                data: "{pintSkipRecords:0,pintTakeRecords:" + lintPageSize + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: true,
                cache: false,
                success: function (msg) {
                    $('#divLoader').hide();
                    if (msg.d != null) {
                        lintTotalPages = msg.d.TotalPages;
                        if (lintTotalPages > 0) {
                            $("#ContentPlaceHolder1_divDonations").html("");
                            printDonationsData(msg);
                            $('#divPagination').show();
                            $('html,body').animate({
                                scrollTop: $('#ContentPlaceHolder1_divDonations').offset().top
                            }, 500);
                            $('#ulPagingItems').twbsPagination({
                                totalPages: lintTotalPages,
                                visiblePages: 10,
                                next: 'Next',
                                prev: 'Prev',
                                onPageClick: function (event, page) {
                                    pageData(page);
                                }
                            });
                        }
                        else {
                            $('#divPagination').hide();
                            $("#ContentPlaceHolder1_divDonations").text("No records were found.");
                            $('html,body').animate({
                                scrollTop: $('#ContentPlaceHolder1_divDonations').offset().top
                            }, 500);
                        }
                    }
                    else {
                        $('#divPagination').hide();
                        $("#ContentPlaceHolder1_divDonations").text("");
                        $("#ContentPlaceHolder1_divDonations").text("No records were found.");
                        $('html,body').animate({
                            scrollTop: $('#ContentPlaceHolder1_divDonations').offset().top
                        }, 500);
                    }
                },
                error: function (error) {
                    $('#divLoader').show();
                    $('#divPagination').hide();
                    $("#ContentPlaceHolder1_divDonations").text("");
                    $("#ContentPlaceHolder1_divDonations").text("No records were found.");
                    $('html,body').animate({
                        scrollTop: $('#ContentPlaceHolder1_divDonations').offset().top
                    }, 500);
                }


            });
        }

        function printDonationsData(msg) {
            $('#divLoader').show();
            var products = msg.d.PDetails;
            var html = "";
            $("#ContentPlaceHolder1_divDonations").text(html);
            var count = 0;
            for (var i = 0; i < products.length; i++) {
                //html += '<a href="./DonationDetail.aspx?id=1559119045mpOAA">                     <div class="donation-list-card">                         <img class="donation-list-card-image" src="https://d16yh42raur1q5.cloudfront.net/assets/img/card_tpl/155911899567Eh7.png" />                         <div class="donation-list-card-content">                             <p class="donation-list-card-location"></p>                             <p class="donation-list-card-name">Help A Single Mother Diagnosed With Lung Disease Support Her Family</p>                         </div>                         <div class="donation-list-card-footer">                             <div class="donation-list-card-book-container">                                 <p style="margin: 0px; width: 160px;" class="donation-list-card-book-now">Donate Now</p>                             </div>                         </div>                     </div>                 </a>';

                html += '<a href="./DonationDetail.aspx?id=';
                html += products[i].Id;
                html += '"><div class="donation-list-card-new">';
                html += '<img class="donation-list-card-image-new" src="';
                html += products[i].ImageUrl;
                html += '"/><div class="donation-list-card-content-new">';
                html += '<p class="donation-list-card-name-new">';
                html += products[i].Name;
                html += '</p></div>';
                //html += '<div class="donation-list-card-content"></div>';
                html += '<div class="donation-list-card-footer-new"> <div class="donation-list-card-book-container-new"> <p style="margin: 0px; width: 160px;" class="donation-list-card-book-now-new">Donate Now</p> </div> </div> </div> </a>';


            }
            $("#ContentPlaceHolder1_divDonations").append(html);
            $('#divLoader').hide();
        }

        function pageData(e) {
            $('#divLoader').show();
            var lintSkipRecords = e == 1 ? 0 : (e * lintPageSize) - lintPageSize;

            $.ajax({
                type: "POST",
                url: "Donations.aspx/BindDonations",
                data: "{pintSkipRecords: " + lintSkipRecords + ",pintTakeRecords:" + lintPageSize + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: true,
                cache: false,
                success: function (msg) {
                    $('#divLoader').hide();
                    printDonationsData(msg);
                },
                error: function () {
                    $('#divLoader').hide();
                    $("#ContentPlaceHolder1_divDonations").text("");
                    $("#ContentPlaceHolder1_divDonations").text("No records were found.");
                }
            });
            return false;
        }

    </script>
</asp:Content>
