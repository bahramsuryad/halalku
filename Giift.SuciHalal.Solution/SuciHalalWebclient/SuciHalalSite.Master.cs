﻿using Framework.EnterpriseLibrary.Adapters;
using Giift.SuciHalal.Helper;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SuciHalalWebclient
{
    public partial class SuciHalalSite : System.Web.UI.MasterPage
    {
        string lstrAccessToken = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            if(!Page.IsPostBack)
            {
                divLoginHeader.InnerHtml = "";
                divLoginMobile.InnerHtml = "";

                if (HttpContext.Current.Session["AccessToken"] == null)
                {
                    //lstrAccessToken = Convert.ToString(HttpContext.Current.Session["AccessToken"]);
                    divLoginHeader.InnerHtml += "<a class=\"w-commerce-commercecartopenlink cart-button w-inline-block\" data-node-type=\"commerce-cart-open-link\""
                        + "onclick=\"var retvalue = LoginUser(); event.returnValue = retvalue; return retvalue;\">Login / Register</a>";

                    divUser.InnerHtml = "";
                    //divNewsletter.Visible = false;

                    divLoginMobile.InnerHtml += "<a href=\"./login.aspx\"><li>Login / Register</li></a>";
                }
                else
                {
                    divLoginHeader.InnerHtml += "<a class=\"w-commerce-commercecartopenlink cart-button w-inline-block\" data-node-type=\"commerce-cart-open-link\""
                        + "onclick=\"var retvalue = LogoutUser(); event.returnValue = retvalue; return retvalue;\">Welcome, Logout</a>";
                    //+"<a href = \"Profile.aspx\" class=\"btn go\"><img alt = \"\" class=\"icon\" src=\"icons/user.svg\" width=\"20\"></a>";
                    divUser.InnerHtml = "<a href = \"Profile.aspx\" class=\"btn go\"><img alt = \"\" class=\"icon\" src=\"icons/user.svg\" width=\"20\"></a>";

                    divLoginMobile.InnerHtml += "<a href=\"./index.aspx\" onclick=\"var retvalue = LogoutUser(); event.returnValue = retvalue; return retvalue;\"><li>Logout</li></a>";

                    //divNewsletter.Visible = true;
                }
                BindCategories();
            }
        }

        public void BindCategories()
        {
            try
            {
                string lstrCategoriesResponse = string.Empty;
                lstrCategoriesResponse = ApiHelper.GetCategories("product", Convert.ToString(ConfigurationManager.AppSettings["BuyingWith"]));
                if (!string.IsNullOrEmpty(lstrCategoriesResponse) && lstrCategoriesResponse.Contains("name"))
                {
                    dynamic lobjCategories = JsonConvert.DeserializeObject(lstrCategoriesResponse);
                    string lstrCategoriesHtml = string.Empty;
                    string lstrPopoverCategories = string.Empty;

                    lstrCategoriesHtml += "<div class=\"w-col w-col-2 w-col-stack\">"
                                       + "<div>"
                                       + "<div>"
                                       + "<h4 class=\"footer-title\">Shop</h4>"
                                       + "<div class=\"top-margin\">"
                                       + "<a class=\"shop-button w-inline-block\" data-w-id=\"fba3057a-8780-7c1e-d882-8dca6c731a50\" href=\"Catalogue.aspx\">"
                                       + "<div>All</div>"
                                       + "<div class=\"shop-line black\" style=\"width: 0%;\"></div>"
                                       + "</a>"
                                       + "<div class=\"w-dyn-list\">"
                                       + "<div class=\"w-dyn-items\">";

                    for (int i = 0; i < lobjCategories.Count; i++)
                    {
                        //if(i == 7)
                        //{
                        //    lstrCategoriesHtml += "</div></div></div></div></div></div><div class=\"w-col w-col-2 w-col-stack\"><div><div><h4 class=\"footer-title\"></h4><div class=\"top-margin\"><div class=\"w-dyn-list\" style=\"margin-top: 40px;\"><div class=\"w-dyn-items\">";
                        //}
                        //else
                        //{
                            lstrCategoriesHtml += "<div class=\"w-dyn-item\">"
                                           + "<a class=\"shop-button with-margin w-inline-block\" data-w-id=\"fba3057a-8780-7c1e-d882-8dca6c731a57\" href=\"Catalogue.aspx?Category=" + lobjCategories[i]["name"].Value + "\">"
                                           + "<div>" + lobjCategories[i]["name"].Value + "</div>"
                                           + "<div class=\"shop-line black\" style=\"width: 0%;\"></div>"
                                           + "</a>"
                                           + "</div>";
                        //}

                        lstrPopoverCategories += "<div class=\"w-dyn-item\">"
                                              + "<a class=\"shop-button with-margin w-inline-block\""
                                              + "data-w-id=\"5c7abf2b-aadf-7367-9d60-75ef7c82ab80\" href=\"Catalogue.aspx?Category=" + lobjCategories[i]["name"].Value + "\">"
                                              + "<div style=\"font-size: 13px;\">"
                                              + lobjCategories[i]["name"].Value
                                              + "</div>"
                                              + "<div class=\"shop-line black\" style=\"width: 0%;\"></div>"
                                              + "</a>"
                                              + "</div>";
                    }

                    lstrCategoriesHtml += "</div></div></div></div></div></div>";
                    divFooterCategories.InnerHtml = lstrCategoriesHtml;
                    divPopoverCategory.InnerHtml = lstrPopoverCategories;
                }
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("SuciHalalSite Master.aspx : BindCategories: " + ex.Message + Environment.NewLine + ex.StackTrace);
            }
        }
    }
}