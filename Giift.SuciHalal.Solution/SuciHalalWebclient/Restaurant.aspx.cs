﻿using Framework.EnterpriseLibrary.Adapters;
using Framework.EnterpriseLibrary.Common.SerializationHelper;
using Giift.SuciHalal.Entities;
using Giift.SuciHalal.Helper;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SuciHalalWebclient
{
    public partial class Restaurant : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        [WebMethod]
        public static RestaurantLocations GetNearbyRestaurants(int pintPageNo, int pintTakeRecords, string pstrLatitude, string pstrLongitude)
        {
            try
            {
                string lstrResponse = string.Empty;
                if (string.IsNullOrEmpty(pstrLatitude) || string.IsNullOrEmpty(pstrLongitude))
                {
                    pstrLatitude = "3.142615";
                    pstrLongitude = "101.691110";
                }

                List<RestaurantDetails> lobjListOfRestaurantDetails = new List<RestaurantDetails>();

                string lstrRestaurantRetailerId = Convert.ToString(ConfigurationManager.AppSettings["RestaurantRetailerId"]);
                lstrResponse = ApiHelper.GetRestaurantLocations(lstrRestaurantRetailerId, pstrLatitude, pstrLongitude, pintPageNo, pintTakeRecords);

                RetailerLocations lobjRetailerLocations = new RetailerLocations();
                lobjRetailerLocations = JsonConvert.DeserializeObject<RetailerLocations>(lstrResponse);

                foreach (Data obj in lobjRetailerLocations.data)
                {
                    RestaurantDetails lobjRestaurantDetails = new RestaurantDetails();
                    lobjRestaurantDetails.Id = obj.id;
                    lobjRestaurantDetails.Name = obj.name;
                    lobjRestaurantDetails.Address = obj.address;
                    lobjRestaurantDetails.Longitude = obj.longitude;
                    lobjRestaurantDetails.Latitude = obj.latitude;
                    lobjRestaurantDetails.PhoneNo = obj.phone;
                    lobjRestaurantDetails.ImageUrl = obj.settings.Exists(lobj => lobj.name.Equals("photo_large")) ? (obj.settings.Find(lobj => lobj.name.Equals("photo_large")).value.Contains("app_default_res") ? "images/restaurant.jpg" : obj.settings.Find(lobj => lobj.name.Equals("photo_large")).value) : "images/restaurant.jpg";
                    lobjRestaurantDetails.Description = obj.settings.Exists(lobj => lobj.name.Equals("description")) ? obj.settings.Find(lobj => lobj.name.Equals("description")).value : "";
                    lobjRestaurantDetails.Website = obj.settings.Exists(lobj => lobj.name.Equals("website")) ? obj.settings.Find(lobj => lobj.name.Equals("website")).value : "";
                    lobjRestaurantDetails.Deal = obj.settings.Exists(lobj => lobj.name.Equals("visa_halal_deal")) ? obj.settings.Find(lobj => lobj.name.Equals("visa_halal_deal")).value : "";
                    lobjRestaurantDetails.DealMore = obj.settings.Exists(lobj => lobj.name.Equals("visa_halal_deal_more")) ? obj.settings.Find(lobj => lobj.name.Equals("visa_halal_deal_more")).value : "";
                    lobjRestaurantDetails.Cuisine = obj.settings.Exists(lobj => lobj.name.Equals("cuisine")) ? obj.settings.Find(lobj => lobj.name.Equals("cuisine")).value : "";
                    lobjRestaurantDetails.AveragePrice = obj.settings.Exists(lobj => lobj.name.Equals("price")) ? obj.settings.Find(lobj => lobj.name.Equals("price")).value : "";
                    lobjRestaurantDetails.Distance = Math.Round((GetDistance(lobjRestaurantDetails.Latitude, lobjRestaurantDetails.Longitude, Convert.ToDouble(pstrLatitude), Convert.ToDouble(pstrLongitude), 'K')), 2); //M
                    lobjRestaurantDetails.HalalType = obj.settings.Exists(lobj => lobj.name.Equals("halaltype")) ? obj.settings.Find(lobj => lobj.name.Equals("halaltype")).value : "";
                    lobjListOfRestaurantDetails.Add(lobjRestaurantDetails);
                }
                HttpContext.Current.Session["RestaurantDetails"] = lobjListOfRestaurantDetails;

                RestaurantLocations lobjRestaurantLocations = new RestaurantLocations();
                lobjRestaurantLocations.RDetails = lobjListOfRestaurantDetails;
                lobjRestaurantLocations.TotalPages = lobjRetailerLocations.metadata.total_pages;

                return lobjRestaurantLocations;
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("Restaurant.aspx.cs : GetNearbyRestaurants : " + ex.Message + Environment.NewLine + ex.StackTrace);
                return null;
            }

        }

        private static double GetDistance(double lat1, double lon1, double lat2, double lon2, char unit)
        {
            if ((lat1 == lat2) && (lon1 == lon2))
            {
                return 0;
            }
            else
            {
                double theta = lon1 - lon2;
                double dist = Math.Sin(deg2rad(lat1)) * Math.Sin(deg2rad(lat2)) + Math.Cos(deg2rad(lat1)) * Math.Cos(deg2rad(lat2)) * Math.Cos(deg2rad(theta));
                dist = Math.Acos(dist);
                dist = rad2deg(dist);
                dist = dist * 60 * 1.1515;
                if (unit == 'K')
                {
                    dist = dist * 1.609344;
                }
                else if (unit == 'N')
                {
                    dist = dist * 0.8684;
                }
                return (dist);
            }
        }

        private static double deg2rad(double deg)
        {
            return (deg * Math.PI / 180.0);
        }

        private static double rad2deg(double rad)
        {
            return (rad / Math.PI * 180.0);
        }
    }
}