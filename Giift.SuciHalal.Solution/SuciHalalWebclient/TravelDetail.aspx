﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SuciHalalSite.Master" AutoEventWireup="true" CodeBehind="TravelDetail.aspx.cs" Inherits="SuciHalalWebclient.TravelDetail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="tools_list" style="background-color: #eee;">
        <div class="banner sub"
            style="background-image: -webkit-gradient(linear, 0% 0%, 0% 100%, from(rgba(0, 0, 0, 0.3)), to(rgba(0, 0, 0, 0.3))), url('https://images.unsplash.com/photo-1504609773096-104ff2c73ba4?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1050&q=80'); background-size: cover; background-position: center 80%;">
            <div class="container center in-sub w-container">
                <h1 class="sub-heading-title">Travel Detail</h1>
                <div class="breadcrumb">
                    <div class="breadcrumb-content">
                        <a class="bread-link" href="Index.aspx">Home</a>
                        <img alt="" src="https://uploads-ssl.webflow.com/5c3ca7fbdc8fab3302b31532/5c41f1941b7ac65ffde90630_right-arrow%20(3).svg" width="18">
                        <a class="bread-link" href="Travel.aspx">Travel</a>
                        <img alt="" src="https://uploads-ssl.webflow.com/5c3ca7fbdc8fab3302b31532/5c41f1941b7ac65ffde90630_right-arrow%20(3).svg" width="18">
                        <div class="bread-link gray-color">Travel Detail</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="section">
            <div class="row">
                <div class="travel-detail-card">
                    <div class="travel-detail-card-left">
                        <p class="travel-detail-card-name">
                            <span id="spanProductName" runat="server"></span>
                            <%--Weekend Getaway Adventure to Cairo--%>
                        </p>
                        <div class="travel-detail-card-container-top">
                            <div class="travel-detail-card-price-container">
                                <p class="travel-detail-card-price">

                                    <span id="spanPrice" runat="server"></span>
                                </p>
                                <%-- <p class="travel-detail-card-price-detail">per person</p>--%>
                            </div>
                            <!--                                    <p class="travel-detail-card-duration">-->
                            <!--                                        3 Days 2 Night-->
                            <!--                                    </p>-->
                        </div>

                        <div class="travel-detail-card-container-top-form">
                            <select id="ddlAdults" class="w-commerce-commercecheckoutshippingcountryselector" name="number_of_adult" style="margin-top: 35px;">
                                <option value="0" disabled selected>Number of Adult</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                                <option value="8">8</option>
                                <option value="9">9</option>
                                <option value="10">10</option>
                            </select>
                            <select id="ddlInfants" class="w-commerce-commercecheckoutshippingcountryselector"
                                name="number_of_children" style="margin-top: 35px;">
                                <option value="0" disabled selected>Number of Children</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                                <option value="8">8</option>
                                <option value="9">9</option>
                                <option value="10">10</option>
                            </select>
                            <input id="txtTravelDate" type="text" onfocus="(this.type='date')" placeholder="Tour Start Date" class="w-commerce-commercecheckoutshippingcountryselector" name="tour_start_date" style="margin-top: 35px;" />
                        </div>

                        <div class="travel-detail-card-container-top-form">
                            <%--<input type="text" placeholder="Name" id="txtname" class="w-commerce-commercecheckoutshippingcountryselector" name="txtName" style="margin-top: 35px;" />                           
                            <input type="text" placeholder="Email" id="txtEmail" class="w-commerce-commercecheckoutshippingcountryselector" name="txtEmail" style="margin-top: 35px;" />--%>

                            <div style="margin-top: 20px;" class="travel-detail-card-container-top-form-contact">
                                <p class="travel-detail-card-contact-title">Contact Details</p>
                                <div class="travel-detail-card-container-contact-container">
                                    <input id='txtname' type="text" name="first_name" class="w-commerce-commercecheckoutemailinput text-input-contact" placeholder="First Name">
                                    <input id='txtLastname' type="text" name="last_name" class="w-commerce-commercecheckoutemailinput text-input-contact" placeholder="Last Name">
                                    <input runat="server" id='txtEmail' type="text" name="email" class="w-commerce-commercecheckoutemailinput text-input-contact" placeholder="Email" disabled>
                                    <input id='txtNumber' type="text" name="contact_number" class="w-commerce-commercecheckoutemailinput text-input-contact" placeholder="Contact Number">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="travel-detail-card-right">
                        <img class="travel-detail-card-image" id="imgTravelrightImage" runat="server" alt="" src="https://media.halaltrip.com/package/1200x900_180501112522-citadel-of-salah-el-din.jpg">
                        <div class="travel-detail-card-button-container">
                            <div class="button-book-now" onclick="BookNow();">Book Now</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="section" style="padding-top: 0px !important;">
            <div class="card-white-background">
                <input id="tab1" type="radio" name="tabs" checked>
                <label class="label-tab" for="tab1">Description</label>

                <input id="tab2" type="radio" name="tabs">
                <label class="label-tab" for="tab2">Booking Conditions</label>

                <!--                        <input id="tab3" type="radio" name="tabs">-->
                <!--                        <label class="label-tab" for="tab3">Photos</label>-->

                <section class="tab" style="padding: 15px;" id="content1">
                    <label class="contenttag-Font show-on-medium-and-down">Description</label>
                    <div class="content-spacingbox bottomradius" id="divDiscription" runat="server">
                    </div>
                </section>

                <section class="tab" style="padding: 15px;" id="content2">
                    <div class="packageupdate-pagecontent">
                        <label class="contenttag-Font show-on-medium-and-down">Booking Conditions</label>
                        <div class="content-spacingbox bottomradius" id="divTC" runat="server">
                        </div>
                    </div>
                </section>
            </div>
        </div>

        <asp:HiddenField ID="hfdProductID" runat="server" Value="" />
        <asp:HiddenField ID="hfdValue" runat="server" Value="" />
        <asp:HiddenField ID="hfdPrice" runat="server" Value="" />
        <asp:HiddenField ID="hfdUnit" runat="server" Value="" />
        <asp:HiddenField ID="hfAdults" runat="server" Value="" />
        <asp:HiddenField ID="hfInfants" runat="server" Value="" />

    </div>
    <div id="divLoader" class="divLoader" style="display: none;"></div>
    <script src="js/jquery-3.3.1.min.js"></script>
    <script type="text/javascript">

        //$(document).ready(function () {
        //    $('#ddlAdults').change(function () {
        //        $("#ContentPlaceHolder1_hfAdults").val($(this).val());
        //    });
        //    $('#ddlInfants').change(function () {
        //        $("#ContentPlaceHolder1_hfInfants").val($(this).val());
        //    });
        //});

        //$(document).on('change', "#ddlAdults", function () {
        //    //$("#ContentPlaceHolder1_hfAdults").val($(this).val());
        //    document.getElementById("hfAdults").value = $(this).val();
        //});

        //$(document).on('change', "#ddlInfants", function () {
        //    //$("#ContentPlaceHolder1_hfInfants").val($(this).val());
        //    document.getElementById("ddlInfants").value = $(this).val();
        //});

        function BookNow() {

            var Name = $("#txtname").val();
            var Email = $("#ContentPlaceHolder1_txtEmail").val();

            var pintAdults = $("#ddlAdults option:selected").val();
            var pintInfants = $("#ddlInfants option:selected").val();

            var msg = '';

            if (Name == "") {
                msg += "Please enter name. ";
            }
            if (Email == "") {
                msg += "Please enter email  ";
            }
            else {
                if (validateEmail(Email)) {

                } else {
                    msg += "Please enter valid Email.  ";
                }
            }
            if (pintAdults == 0) {
                 msg += "Please select no of adults.";
            }
            //if (pintInfants == 0) {
            //    msg += "Please select no of infants.";
            //}
            if (msg != "") {
                alert(msg);
            }
            else {

                var pstrProductID = $("#ContentPlaceHolder1_hfdProductID").val();
                var pstrPrice = $("#ContentPlaceHolder1_hfdPrice").val();
                var pstrUnit = $("#ContentPlaceHolder1_hfdUnit").val();
                var pstrValue = $("#ContentPlaceHolder1_hfdValue").val();
                //var pintAdults = $("#ddlAdults option:selected").text();
                //var pintInfants = $("#ddlInfants option:selected").text();

                var pintQty = parseInt(pintAdults) + parseInt(pintInfants);

                pstrPrice = parseFloat(pstrPrice).toFixed(2);

                $.ajax({
                    type: "POST",
                    url: "TravelDetail.aspx/BookNow",
                    data: "{pstrName:'" + Name + "',pstrEmail:'" + Email + "',pstrProductID:'" + pstrProductID + "',pdblPrice:" + pstrPrice + ",pstrUnit:'" + pstrUnit + "',pstrValue:" + pstrValue + ",pintQty:" + pintQty + "}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    async: true,
                    cache: false,
                    success: function (data) {
                        $('#divLoader').hide();


                        var newData = data.d;
                        if ((newData != "") || newData == null) {
                            if (newData == "SUCCESS") {
                                window.location.href = "PostPayment.aspx";
                            } else {
                                //alert("Oops, something went wrong!")
                                swal("Oops!", "Something went wrong.", "error");
                            }
                        }
                        return false;
                    },
                    error: function () {
                        $('#divLoader').hide();
                        //alert("Oops something went wrong,please try again later.");
                        swal("Oops!", "Something went wrong.", "error");
                    }
                });
            }
        }

        function validateEmail(email) {
            var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(email);
        }

    </script>
</asp:Content>
