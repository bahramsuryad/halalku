﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SuciHalalSite.Master" AutoEventWireup="true" CodeBehind="Profile.aspx.cs" Inherits="SuciHalalWebclient.Profile" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="profile">
        <div class="profile-top-container">
            <img src="images/Profile.jpg" class="profile-picture">
            <span class="profile-name" id="spnFullName"></span>
            <br />
            <div style="display: flex; justify-content: space-around; flex-wrap: wrap; width: 50vw;">
                <button onclick="javascript:MakeProfileDataEditable();" type="button" class="ant-btn" style="flex: 1; margin-bottom: 10px;">
                    <i aria-label="icon: edit" class="anticon anticon-edit">
                        <svg viewBox="64 64 896 896" class="" data-icon="edit" width="1em" height="1em" fill="currentColor" aria-hidden="true" focusable="false">
                            <path d="M257.7 752c2 0 4-.2 6-.5L431.9 722c2-.4 3.9-1.3 5.3-2.8l423.9-423.9a9.96 9.96 0 0 0 0-14.1L694.9 114.9c-1.9-1.9-4.4-2.9-7.1-2.9s-5.2 1-7.1 2.9L256.8 538.8c-1.5 1.5-2.4 3.3-2.8 5.3l-29.5 168.2a33.5 33.5 0 0 0 9.4 29.8c6.6 6.4 14.9 9.9 23.8 9.9zm67.4-174.4L687.8 215l73.3 73.3-362.7 362.6-88.9 15.7 15.6-89zM880 836H144c-17.7 0-32 14.3-32 32v36c0 4.4 3.6 8 8 8h784c4.4 0 8-3.6 8-8v-36c0-17.7-14.3-32-32-32z"></path>
                        </svg>
                    </i>
                    <span>Edit Profile</span>
                </button>
                <button onclick="window.location.href = 'ChangePassword.aspx';" type="button" class="ant-btn" style="flex: 1; margin-left: 10px; margin-right: 10px;">
                    <i aria-label="icon: key" class="anticon anticon-key">
                        <svg viewBox="64 64 896 896" class="" data-icon="key" width="1em" height="1em" fill="currentColor" aria-hidden="true" focusable="false">
                            <path d="M608 112c-167.9 0-304 136.1-304 304 0 70.3 23.9 135 63.9 186.5l-41.1 41.1-62.3-62.3a8.15 8.15 0 0 0-11.4 0l-39.8 39.8a8.15 8.15 0 0 0 0 11.4l62.3 62.3-44.9 44.9-62.3-62.3a8.15 8.15 0 0 0-11.4 0l-39.8 39.8a8.15 8.15 0 0 0 0 11.4l62.3 62.3-65.3 65.3a8.03 8.03 0 0 0 0 11.3l42.3 42.3c3.1 3.1 8.2 3.1 11.3 0l253.6-253.6A304.06 304.06 0 0 0 608 720c167.9 0 304-136.1 304-304S775.9 112 608 112zm161.2 465.2C726.2 620.3 668.9 644 608 644c-60.9 0-118.2-23.7-161.2-66.8-43.1-43-66.8-100.3-66.8-161.2 0-60.9 23.7-118.2 66.8-161.2 43-43.1 100.3-66.8 161.2-66.8 60.9 0 118.2 23.7 161.2 66.8 43.1 43 66.8 100.3 66.8 161.2 0 60.9-23.7 118.2-66.8 161.2z"></path>
                        </svg>
                    </i>
                    <span>Change Password</span>
                </button>
                <button onclick="window.location.href = 'OrderHistory.aspx';" type="button" class="ant-btn" style="flex: 1; margin-left: 10px; margin-right: 10px;">
                    <i aria-label="icon: key" class="anticon anticon-key">
                        <svg viewBox="64 64 896 896" class="" data-icon="key" width="1em" height="1em" fill="currentColor" aria-hidden="true" focusable="false">
                            <path d="M608 112c-167.9 0-304 136.1-304 304 0 70.3 23.9 135 63.9 186.5l-41.1 41.1-62.3-62.3a8.15 8.15 0 0 0-11.4 0l-39.8 39.8a8.15 8.15 0 0 0 0 11.4l62.3 62.3-44.9 44.9-62.3-62.3a8.15 8.15 0 0 0-11.4 0l-39.8 39.8a8.15 8.15 0 0 0 0 11.4l62.3 62.3-65.3 65.3a8.03 8.03 0 0 0 0 11.3l42.3 42.3c3.1 3.1 8.2 3.1 11.3 0l253.6-253.6A304.06 304.06 0 0 0 608 720c167.9 0 304-136.1 304-304S775.9 112 608 112zm161.2 465.2C726.2 620.3 668.9 644 608 644c-60.9 0-118.2-23.7-161.2-66.8-43.1-43-66.8-100.3-66.8-161.2 0-60.9 23.7-118.2 66.8-161.2 43-43.1 100.3-66.8 161.2-66.8 60.9 0 118.2 23.7 161.2 66.8 43.1 43 66.8 100.3 66.8 161.2 0 60.9-23.7 118.2-66.8 161.2z"></path>
                        </svg>
                    </i>
                    <span>Order History</span>
                </button>
            </div>
        </div>

        <div class="profile-bottom-container">
            <div class="profile-detail-card-1">
                <div class="profile-detail-card-content">
                    <div class="profile-detail-card-title">Basic Details</div>

                    <div class="profile-detail-card-detail-container">
                        <p class="profile-detail-card-detail-text-title">First Name</p>
                        <%--<p class="profile-detail-card-detail-text-value">John</p>--%>
                        <input id="txtFirstName" class="profile-detail-card-detail-text-value" style="height: 38px;" value="" type="text" disabled />
                    </div>
                    <div class="profile-detail-card-detail-container">
                        <p class="profile-detail-card-detail-text-title">Last Name</p>
                        <%--<p class="profile-detail-card-detail-text-value">Doe</p>--%>
                        <input id="txtLastName" class="profile-detail-card-detail-text-value" style="height: 38px;" value="" type="text" disabled />
                    </div>
                    <div class="profile-detail-card-detail-container">
                        <p class="profile-detail-card-detail-text-title">Phone</p>
                        <%--<p class="profile-detail-card-detail-text-value">+62 822 1234 4321</p>--%>
                        <input id="txtPhoneNo" class="profile-detail-card-detail-text-value" style="height: 38px;" value="" type="tel" disabled />
                    </div>
                    <div class="profile-detail-card-detail-container">
                        <p class="profile-detail-card-detail-text-title">Birth Date</p>
                        <%--<p class="profile-detail-card-detail-text-value">January 14, 2000</p>--%>
                        <input id="txtDOB" class="profile-detail-card-detail-text-value" style="height: 38px;" value="" type="date" disabled />
                    </div>
                    <div class="profile-detail-card-detail-container">
                        <p class="profile-detail-card-detail-text-title">Gender</p>
                        <%--<p class="profile-detail-card-detail-text-value">Male</p>--%>
                        <select id="ddlGender" name="gender" class="profile-detail-card-detail-text-value" style="height: 38px;" disabled>
                            <option value="male">male</option>
                            <option value="female">female</option>
                        </select>
                    </div>
                    <div class="profile-detail-card-detail-container">
                        <p class="profile-detail-card-detail-text-title">Street Address</p>
                        <%--<p class="profile-detail-card-detail-text-value">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>--%>
                        <input id="txtStreetAddress" class="profile-detail-card-detail-text-value" style="height: 38px;" value="" type="text" disabled />
                    </div>
                    <div class="profile-detail-card-detail-container">
                        <p class="profile-detail-card-detail-text-title">Zip Code</p>
                        <%--<p class="profile-detail-card-detail-text-value">14123</p>--%>
                        <input id="txtZipCode" class="profile-detail-card-detail-text-value" style="height: 38px;" value="" type="number" disabled />
                    </div>
                    <div class="profile-detail-card-detail-container">
                        <p class="profile-detail-card-detail-text-title">City</p>
                        <%--<p class="profile-detail-card-detail-text-value">DKI Jakarta</p>--%>
                        <input id="txtCity" class="profile-detail-card-detail-text-value" style="height: 38px;" value="" type="text" disabled />
                    </div>
                    <div class="profile-detail-card-detail-container">
                        <p class="profile-detail-card-detail-text-title">Country</p>
                        <%--<p class="profile-detail-card-detail-text-value">Australia</p>--%>
                        <select id="ddlCountry" name="country" class="profile-detail-card-detail-text-value" style="height: 38px;" disabled>
                            <option value="AF">Afghanistan</option>
                            <option value="AX">Aland Islands</option>
                            <option value="AL">Albania</option>
                            <option value="DZ">Algeria</option>
                            <option value="AS">American Samoa</option>
                            <option value="AD">Andorra</option>
                            <option value="AO">Angola</option>
                            <option value="AI">Anguilla</option>
                            <option value="AG">Antigua and Barbuda</option>
                            <option value="AR">Argentina</option>
                            <option value="AM">Armenia</option>
                            <option value="AW">Aruba</option>
                            <option value="AU">Australia</option>
                            <option value="AT">Austria</option>
                            <option value="AZ">Azerbaijan</option>
                            <option value="BS">Bahamas</option>
                            <option value="BH">Bahrain</option>
                            <option value="BD">Bangladesh</option>
                            <option value="BB">Barbados</option>
                            <option value="BY">Belarus</option>
                            <option value="BE">Belgium</option>
                            <option value="BZ">Belize</option>
                            <option value="BJ">Benin</option>
                            <option value="BM">Bermuda</option>
                            <option value="BT">Bhutan</option>
                            <option value="BO">Bolivia</option>
                            <option value="BQ">Bonaire, Saint Eustatius and Saba</option>
                            <option value="BA">Bosnia and Herzegovina</option>
                            <option value="BW">Botswana</option>
                            <option value="BR">Brazil</option>
                            <option value="IO">British Indian Ocean Territory</option>
                            <option value="VG">British Virgin Islands</option>
                            <option value="BN">Brunei</option>
                            <option value="BG">Bulgaria</option>
                            <option value="BF">Burkina Faso</option>
                            <option value="BI">Burundi</option>
                            <option value="KH">Cambodia</option>
                            <option value="CM">Cameroon</option>
                            <option value="CA">Canada</option>
                            <option value="CV">Cape Verde</option>
                            <option value="KY">Cayman Islands</option>
                            <option value="CF">Central African Republic</option>
                            <option value="TD">Chad</option>
                            <option value="CL">Chile</option>
                            <option value="CN">China</option>
                            <option value="CX">Christmas Island</option>
                            <option value="CC">Cocos Islands</option>
                            <option value="CO">Colombia</option>
                            <option value="KM">Comoros</option>
                            <option value="CK">Cook Islands</option>
                            <option value="CR">Costa Rica</option>
                            <option value="HR">Croatia</option>
                            <option value="CU">Cuba</option>
                            <option value="CW">Curacao</option>
                            <option value="CY">Cyprus</option>
                            <option value="CZ">Czechia</option>
                            <option value="CD">Democratic Republic of the Congo</option>
                            <option value="DK">Denmark</option>
                            <option value="DJ">Djibouti</option>
                            <option value="DM">Dominica</option>
                            <option value="DO">Dominican Republic</option>
                            <option value="EC">Ecuador</option>
                            <option value="EG">Egypt</option>
                            <option value="SV">El Salvador</option>
                            <option value="GQ">Equatorial Guinea</option>
                            <option value="ER">Eritrea</option>
                            <option value="EE">Estonia</option>
                            <option value="ET">Ethiopia</option>
                            <option value="FK">Falkland Islands</option>
                            <option value="FO">Faroe Islands</option>
                            <option value="FJ">Fiji</option>
                            <option value="FI">Finland</option>
                            <option value="FR">France</option>
                            <option value="GF">French Guiana</option>
                            <option value="PF">French Polynesia</option>
                            <option value="TF">French Southern Territories</option>
                            <option value="GA">Gabon</option>
                            <option value="GM">Gambia</option>
                            <option value="GE">Georgia</option>
                            <option value="DE">Germany</option>
                            <option value="GH">Ghana</option>
                            <option value="GI">Gibraltar</option>
                            <option value="GR">Greece</option>
                            <option value="GL">Greenland</option>
                            <option value="GD">Grenada</option>
                            <option value="GP">Guadeloupe</option>
                            <option value="GU">Guam</option>
                            <option value="GT">Guatemala</option>
                            <option value="GG">Guernsey</option>
                            <option value="GN">Guinea</option>
                            <option value="GW">Guinea-Bissau</option>
                            <option value="GY">Guyana</option>
                            <option value="HT">Haiti</option>
                            <option value="HN">Honduras</option>
                            <option value="HK">Hong Kong</option>
                            <option value="HU">Hungary</option>
                            <option value="IS">Iceland</option>
                            <option value="IN">India</option>
                            <option value="ID">Indonesia</option>
                            <option value="IR">Iran</option>
                            <option value="IQ">Iraq</option>
                            <option value="IE">Ireland</option>
                            <option value="IM">Isle of Man</option>
                            <option value="IL">Israel</option>
                            <option value="IT">Italy</option>
                            <option value="CI">Ivory Coast</option>
                            <option value="JM">Jamaica</option>
                            <option value="JP">Japan</option>
                            <option value="JE">Jersey</option>
                            <option value="JO">Jordan</option>
                            <option value="KZ">Kazakhstan</option>
                            <option value="KE">Kenya</option>
                            <option value="KI">Kiribati</option>
                            <option value="XK">Kosovo</option>
                            <option value="KW">Kuwait</option>
                            <option value="KG">Kyrgyzstan</option>
                            <option value="LA">Laos</option>
                            <option value="LV">Latvia</option>
                            <option value="LB">Lebanon</option>
                            <option value="LS">Lesotho</option>
                            <option value="LR">Liberia</option>
                            <option value="LY">Libya</option>
                            <option value="LI">Liechtenstein</option>
                            <option value="LT">Lithuania</option>
                            <option value="LU">Luxembourg</option>
                            <option value="MO">Macao</option>
                            <option value="MK">Macedonia</option>
                            <option value="MG">Madagascar</option>
                            <option value="MW">Malawi</option>
                            <option value="MY">Malaysia</option>
                            <option value="MV">Maldives</option>
                            <option value="ML">Mali</option>
                            <option value="MT">Malta</option>
                            <option value="MH">Marshall Islands</option>
                            <option value="MQ">Martinique</option>
                            <option value="MR">Mauritania</option>
                            <option value="MU">Mauritius</option>
                            <option value="YT">Mayotte</option>
                            <option value="MX">Mexico</option>
                            <option value="FM">Micronesia</option>
                            <option value="MD">Moldova</option>
                            <option value="MC">Monaco</option>
                            <option value="MN">Mongolia</option>
                            <option value="ME">Montenegro</option>
                            <option value="MS">Montserrat</option>
                            <option value="MA">Morocco</option>
                            <option value="MZ">Mozambique</option>
                            <option value="MM">Myanmar</option>
                            <option value="NA">Namibia</option>
                            <option value="NR">Nauru</option>
                            <option value="NP">Nepal</option>
                            <option value="NL">Netherlands</option>
                            <option value="NC">New Caledonia</option>
                            <option value="NZ">New Zealand</option>
                            <option value="NI">Nicaragua</option>
                            <option value="NE">Niger</option>
                            <option value="NG">Nigeria</option>
                            <option value="NU">Niue</option>
                            <option value="NF">Norfolk Island</option>
                            <option value="KP">North Korea</option>
                            <option value="MP">Northern Mariana Islands</option>
                            <option value="NO">Norway</option>
                            <option value="OM">Oman</option>
                            <option value="PK">Pakistan</option>
                            <option value="PW">Palau</option>
                            <option value="PS">Palestinian Territory</option>
                            <option value="PA">Panama</option>
                            <option value="PG">Papua New Guinea</option>
                            <option value="PY">Paraguay</option>
                            <option value="PE">Peru</option>
                            <option value="PH">Philippines</option>
                            <option value="PN">Pitcairn</option>
                            <option value="PL">Poland</option>
                            <option value="PT">Portugal</option>
                            <option value="PR">Puerto Rico</option>
                            <option value="QA">Qatar</option>
                            <option value="CG">Republic of the Congo</option>
                            <option value="RE">Reunion</option>
                            <option value="RO">Romania</option>
                            <option value="RU">Russia</option>
                            <option value="RW">Rwanda</option>
                            <option value="BL">Saint Barthelemy</option>
                            <option value="SH">Saint Helena</option>
                            <option value="KN">Saint Kitts and Nevis</option>
                            <option value="LC">Saint Lucia</option>
                            <option value="MF">Saint Martin</option>
                            <option value="PM">Saint Pierre and Miquelon</option>
                            <option value="VC">Saint Vincent and the Grenadines</option>
                            <option value="WS">Samoa</option>
                            <option value="SM">San Marino</option>
                            <option value="ST">Sao Tome and Principe</option>
                            <option value="SA">Saudi Arabia</option>
                            <option value="SN">Senegal</option>
                            <option value="RS">Serbia</option>
                            <option value="SC">Seychelles</option>
                            <option value="SL">Sierra Leone</option>
                            <option value="SG">Singapore</option>
                            <option value="SX">Sint Maarten</option>
                            <option value="SK">Slovakia</option>
                            <option value="SI">Slovenia</option>
                            <option value="SB">Solomon Islands</option>
                            <option value="SO">Somalia</option>
                            <option value="ZA">South Africa</option>
                            <option value="GS">South Georgia and the South Sandwich Islands</option>
                            <option value="KR">South Korea</option>
                            <option value="SS">South Sudan</option>
                            <option value="ES">Spain</option>
                            <option value="LK">Sri Lanka</option>
                            <option value="SD">Sudan</option>
                            <option value="SR">Suriname</option>
                            <option value="SJ">Svalbard and Jan Mayen</option>
                            <option value="SZ">Swaziland</option>
                            <option value="SE">Sweden</option>
                            <option value="CH">Switzerland</option>
                            <option value="SY">Syria</option>
                            <option value="TW">Taiwan</option>
                            <option value="TJ">Tajikistan</option>
                            <option value="TZ">Tanzania</option>
                            <option value="TH">Thailand</option>
                            <option value="TL">Timor Leste</option>
                            <option value="TG">Togo</option>
                            <option value="TK">Tokelau</option>
                            <option value="TO">Tonga</option>
                            <option value="TT">Trinidad and Tobago</option>
                            <option value="TN">Tunisia</option>
                            <option value="TR">Turkey</option>
                            <option value="TM">Turkmenistan</option>
                            <option value="TC">Turks and Caicos Islands</option>
                            <option value="TV">Tuvalu</option>
                            <option value="VI">U.S. Virgin Islands</option>
                            <option value="UG">Uganda</option>
                            <option value="UA">Ukraine</option>
                            <option value="AE">United Arab Emirates</option>
                            <option value="GB">United Kingdom</option>
                            <option value="US">United States</option>
                            <option value="UM">United States Minor Outlying Islands</option>
                            <option value="UY">Uruguay</option>
                            <option value="UZ">Uzbekistan</option>
                            <option value="VU">Vanuatu</option>
                            <option value="VA">Vatican</option>
                            <option value="VE">Venezuela</option>
                            <option value="VN">Vietnam</option>
                            <option value="WF">Wallis and Futuna</option>
                            <option value="EH">Western Sahara</option>
                            <option value="YE">Yemen</option>
                            <option value="ZM">Zambia</option>
                            <option value="ZW">Zimbabwe</option>
                        </select>
                    </div>
                    <div class="profile-detail-card-detail-container">
                        <p class="profile-detail-card-detail-text-title">Lang</p>
                        <%--<p class="profile-detail-card-detail-text-value">English</p>--%>
                        <select id="ddlLanguage" name="language" class="profile-detail-card-detail-text-value" style="height: 38px;" disabled>
                            <option value="en">English</option>
                        </select>
                    </div>
                    <div id="divEditDetails" class="profile-detail-card-detail-container" style="float: right; margin-top: 10px; width: 250px !important; display: none">
                        <input type="button" class="w-commerce-commercecartcheckoutbutton checkout-button" value="Cancel" onclick="javascript: LockProfileData();" style="width: 45%; margin-right: 10%;" />
                        <input type="button" class="w-commerce-commercecartcheckoutbutton checkout-button" value="Submit" onclick="javascript: UpdateProfile();" style="width: 45%;" />
                    </div>
                </div>
            </div>
        </div>

    </div>
    <div id="divLoader" class="divLoader" style="display: none;"></div>
    <script src="js/jquery-3.3.1.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            GetUserProfile();
        });

        function GetUserProfile() {
            $('#divLoader').show();
            $.ajax({
                url: 'Profile.aspx/GetUserProfile',
                type: 'POST',
                contentType: 'application/json; charset =utf-8',
                data: "",
                dataType: 'json',
                success: function (data) {
                    $('#divLoader').hide();
                    try {
                        var newData = data.d;
                        if (newData.ResponseMessage == "SUCCESS") {
                            $("#txtFirstName").val(newData.UserDetailsList[0].givenName);
                            $("#txtLastName").val(newData.UserDetailsList[0].familyName);
                            $("#txtPhoneNo").val(newData.UserDetailsList[0].tel[0].value);
                            $("#txtDOB").val(newData.UserDetailsList[0].bday);
                            $("#ddlGender").val(newData.UserDetailsList[0].gender);
                            $("#txtStreetAddress").val(newData.UserDetailsList[0].adr[0].streetaddress);
                            $("#txtZipCode").val(newData.UserDetailsList[0].adr[0].postalcode);
                            $("#txtCity").val(newData.UserDetailsList[0].adr[0].locality);
                            $("#ddlCountry").val(newData.UserDetailsList[0].adr[0].countryname);
                            $("#ddlLanguage").val(newData.UserDetailsList[0].lang);
                            $("#spnFullName").text('');
                            if (newData.UserDetailsList[0].givenName != null) {
                                $("#spnFullName").text(newData.UserDetailsList[0].givenName + " " + newData.UserDetailsList[0].familyName);
                            }
                        }
                        else if (newData.ResponseMessage != "") {

                        }
                        else {
                            swal("Oops!", "Something went wrong", "error");
                        }
                    }
                    catch (e) {
                        swal("Oops!", "Something went wrong", "error");
                        return false;
                    }
                    return false;
                },
                error: function (errmsg) {
                    $('#divLoader').hide();
                    swal("Oops!", "Something went wrong", "error");
                }
            });
        }

        function MakeProfileDataEditable() {
            $("#txtFirstName").prop('disabled', false);
            $("#txtLastName").prop('disabled', false);
            $("#txtPhoneNo").prop('disabled', false);
            $("#txtDOB").prop('disabled', false);
            $("#ddlGender").prop('disabled', false);
            $("#txtStreetAddress").prop('disabled', false);
            $("#txtZipCode").prop('disabled', false);
            $("#txtCity").prop('disabled', false);
            $("#ddlCountry").prop('disabled', false);
            $("#ddlLanguage").prop('disabled', false);
            $("#divEditDetails").show();
        }

        function LockProfileData() {
            $("#txtFirstName").prop('disabled', true);
            $("#txtLastName").prop('disabled', true);
            $("#txtPhoneNo").prop('disabled', true);
            $("#txtDOB").prop('disabled', true);
            $("#ddlGender").prop('disabled', true);
            $("#txtStreetAddress").prop('disabled', true);
            $("#txtZipCode").prop('disabled', true);
            $("#txtCity").prop('disabled', true);
            $("#ddlCountry").prop('disabled', true);
            $("#ddlLanguage").prop('disabled', true);

            $("#divEditDetails").hide();
        }

        function UpdateProfile() {
            var lstrFirstName = $("#txtFirstName").val();
            var lstrLastName = $("#txtLastName").val();
            var lstrPhoneNo = $("#txtPhoneNo").val();
            //var lstrDOB = $("#txtDOB").val();

            var year = $("#txtDOB").val().substring(0, 4);
            var month = $("#txtDOB").val().substring(5, 7);
            var day = $("#txtDOB").val().substring(8, 10);


            var lstrDOB = year + month + day;
            var lstrStreetAddress = $("#txtStreetAddress").val();
            var lstrZipCode = $("#txtZipCode").val();
            var lstrCity = $("#txtCity").val();
            var lstrCountry = $("#ddlCountry").children("option:selected").val();
            var lstrGender = $("#ddlGender").children("option:selected").val();
            var lstrLanguage = $("#ddlLanguage").children("option:selected").val();

            $('#divLoader').show();
            $.ajax({
                url: 'Profile.aspx/UpdateProfileData',
                type: 'POST',
                contentType: 'application/json; charset =utf-8',
                //data: "{'pstrFirstName':'" + lstrFirstName + "','pstrLastName':'" + lstrLastName + "','pstrPhoneNo':'" + lstrPhoneNo + "','pstrDOB':'" + lstrDOB + "','pstrStreetAddress':'" + lstrStreetAddress + "','pstrZipCode':'" + lstrZipCode + "','pstrCity':'" + lstrCity + "','pstrCountry':'" + lstrCountry + "','pstrGender':'" + lstrGender + "','pstrLanguage':'" + lstrLanguage + "'}",
                data: '{"pstrFirstName":"' + lstrFirstName + '","pstrLastName":"' + lstrLastName + '","pstrPhoneNo":"' + lstrPhoneNo + '","pstrDOB":"' + lstrDOB + '","pstrStreetAddress":"' + lstrStreetAddress + '","pstrZipCode":"' + lstrZipCode + '","pstrCity":"' + lstrCity + '","pstrCountry":"' + lstrCountry + '","pstrGender":"' + lstrGender + '","pstrLanguage":"' + lstrLanguage + '"}',
                dataType: 'json',
                success: function (data) {
                    $('#divLoader').hide();
                    try {
                        var newData = data.d;
                        if ((newData != "") || newData == null) {
                            if (newData == "SUCCESS") {
                                swal(" ", "Update Successful!", "success");
                                LockProfileData();
                                GetUserProfile();
                            }
                            else if (newData == "unauthorized") {
                                window.location.href = "Login.aspx";
                            }
                            else {
                                swal("Oops!", "Something went wrong", "error");
                            }
                        }
                        else {
                            swal("Oops!", "Something went wrong", "error");
                        }
                    }
                    catch (e) {
                        swal("Oops!", "Something went wrong", "error");
                        return false;
                    }
                    return false;
                },
                error: function (errmsg) {
                    $('#divLoader').hide();
                    swal("Oops!", "Something went wrong", "error");
                }
            });
        }

        function dateFormat(value, pattern) {
            var i = 0,
                date = value.toString();
            return pattern.replace(/#/g, _ => date[i++]);
        }
    </script>
</asp:Content>
