﻿using Framework.EnterpriseLibrary.Adapters;
using Giift.SuciHalal.Entities;
using Giift.SuciHalal.Helper;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SuciHalalWebclient
{
    public partial class Offer : System.Web.UI.Page
    {
        string lstrRestaurantId = string.Empty;
        public static string lstrAccessToken = string.Empty;
        public static List<RestaurantDetails> lobjListOfRestaurantDetail = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                //if (HttpContext.Current.Session["AccessToken"] == null && HttpContext.Current.Session["GuestToken"] == null)
                //{
                //    //Response.Redirect("Login.aspx?Callbackurl=Offer.aspx");
                //    string lstrResponse = ApiHelper.GuestToken();
                //    string lstrErrorMessage = string.Empty;

                //    JToken outer = JToken.Parse(lstrResponse);
                //    if (outer["error_description"] != null)
                //    {
                //        lstrErrorMessage = outer.Value<string>("error_description");
                //    }
                //    else if (outer["access_token"] != null)
                //    {
                //        lstrAccessToken = outer.Value<string>("access_token");
                //        HttpContext.Current.Session["GuestToken"] = lstrAccessToken;
                //    }
                //}
                //else if(HttpContext.Current.Session["AccessToken"] != null)
                //{
                //    lstrAccessToken = Convert.ToString(HttpContext.Current.Session["AccessToken"]);
                //}
                //else
                //{
                //    lstrAccessToken = Convert.ToString(HttpContext.Current.Session["GuestToken"]);
                //}
            }
        }

        [WebMethod]
        public static RestaurantLocations GetRestaurantOffers(int pintSkipRecords, int pintTakeRecords)
        {
            try
            {
                if (pintSkipRecords == 0)
                {
                    lobjListOfRestaurantDetail = null;
                    lobjListOfRestaurantDetail = new List<RestaurantDetails>();

                    //string lstrAccessToken = Convert.ToString(HttpContext.Current.Session["AccessToken"]);
                    string lstrResponse = ApiHelper.GetOffers(lstrAccessToken);
                    dynamic lobjRestaurantOffers = JsonConvert.DeserializeObject(lstrResponse);
                    List<string> lobjListOfLocationId = new List<string>();
                    foreach (var prop in lobjRestaurantOffers.retailer_location_setting)
                    {
                        var obj = prop.Value;
                        lobjListOfLocationId.Add(obj["location"].ToString());
                    }

                    string lstrFinalResponse = string.Empty;
                    if (lobjListOfLocationId != null && lobjListOfLocationId.Count > 0)
                    {
                        lstrFinalResponse = ApiHelper.GetMultipleLocationSettings(lstrAccessToken, lobjListOfLocationId);
                    }
                    dynamic lobjLocationSettings = JsonConvert.DeserializeObject(lstrFinalResponse);

                    for (int i = 0; i < lobjListOfLocationId.Count; i++)
                    {
                        RestaurantDetails lobjRestaurantDetails = new RestaurantDetails();
                        lobjRestaurantDetails.Id = lobjListOfLocationId[i];
                        foreach (var prop in lobjLocationSettings.retailer_location_setting)
                        {
                            var obj = prop.Value;
                            if (obj["location"] == lobjListOfLocationId[i])
                            {
                                foreach (JProperty parsedProperty in obj.Properties())
                                {
                                    string propertyName = parsedProperty.Name;
                                    if (propertyName.Equals("name"))
                                    {
                                        if ((string)parsedProperty.Value == "website")
                                        {
                                            lobjRestaurantDetails.Website = obj["value"].ToString();
                                        }
                                        else if ((string)parsedProperty.Value == "description")
                                        {
                                            lobjRestaurantDetails.Description = obj["value"].ToString();
                                        }
                                        else if ((string)parsedProperty.Value == "visa_halal_deal")
                                        {
                                            lobjRestaurantDetails.Deal = obj["value"].ToString();
                                        }
                                        else if ((string)parsedProperty.Value == "visa_halal_deal_teaser")
                                        {
                                            lobjRestaurantDetails.DealTeaser = obj["value"].ToString();
                                        }
                                        else if ((string)parsedProperty.Value == "visa_halal_deal_more")
                                        {
                                            lobjRestaurantDetails.DealMore = obj["value"].ToString();
                                        }
                                        else if ((string)parsedProperty.Value == "photo_large")
                                        {
                                            string imageUrl = obj["value"].ToString();
                                            if (string.IsNullOrEmpty(imageUrl) || !imageUrl.Contains("/"))
                                            {
                                                lobjRestaurantDetails.ImageUrl = "images/restaurant.jpg";
                                            }
                                            else
                                            {
                                                lobjRestaurantDetails.ImageUrl = imageUrl;
                                            }
                                        }
                                        else if ((string)parsedProperty.Value == "cuisine")
                                        {
                                            lobjRestaurantDetails.Cuisine = obj["value"].ToString();
                                        }
                                        else if ((string)parsedProperty.Value == "price")
                                        {
                                            lobjRestaurantDetails.AveragePrice = obj["value"].ToString();
                                        }
                                        else if ((string)parsedProperty.Value == "name")
                                        {
                                            lobjRestaurantDetails.Name = obj["value"].ToString();
                                        }
                                        else if ((string)parsedProperty.Value == "address")
                                        {
                                            lobjRestaurantDetails.Address = obj["value"].ToString();
                                        }
                                        else if ((string)parsedProperty.Value == "halaltype")
                                        {
                                            lobjRestaurantDetails.HalalType = obj["value"].ToString();
                                        }
                                        else if ((string)parsedProperty.Value == "latitude")
                                        {
                                            lobjRestaurantDetails.Latitude = obj["value"];
                                        }
                                        else if ((string)parsedProperty.Value == "longitude")
                                        {
                                            lobjRestaurantDetails.Longitude = obj["value"];
                                        }

                                    }
                                }
                            }
                        }
                        lobjListOfRestaurantDetail.Add(lobjRestaurantDetails);
                    }

                    if (lobjListOfRestaurantDetail != null && lobjListOfRestaurantDetail.Count > 0)
                    {
                        HttpContext.Current.Session["RestaurantDetails"] = lobjListOfRestaurantDetail;
                    }
                }
                else if (HttpContext.Current.Session["RestaurantDetails"] != null)
                {
                    lobjListOfRestaurantDetail = HttpContext.Current.Session["RestaurantDetails"] as List<RestaurantDetails>;
                }

                RestaurantLocations lobjRestaurantLocations = new RestaurantLocations();
                var query = (from RestaurantDetails in lobjListOfRestaurantDetail
                             select RestaurantDetails)
                                .Skip(pintSkipRecords)
                                .Take(pintTakeRecords)
                                .ToList();


                lobjRestaurantLocations.RDetails = query;
                int lintRestaurantCount = (from p in lobjListOfRestaurantDetail
                                           select p).Count();
                double pageCount = (lintRestaurantCount / 15) + 1; //To be changed

                lobjRestaurantLocations.TotalPages = Math.Ceiling(pageCount);

                return lobjRestaurantLocations;
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("Offer.aspx.cs GetRestaurantOffers : " + ex.Message + Environment.NewLine + ex.StackTrace);
                return null;
            }

        }
    }
}