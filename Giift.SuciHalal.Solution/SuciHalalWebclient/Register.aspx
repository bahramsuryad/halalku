﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SuciHalalSite.Master" AutoEventWireup="true" CodeBehind="Register.aspx.cs" Inherits="SuciHalalWebclient.Register" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="dashboard">
        <div class="w-row" style="margin-top: 200px;">
            <div class="w-col w-col-3"></div>
            <div class="w-col w-col-6">
                <div class="w-row">
                    <div class="w-col w-col-12">
                        <div class="form-margin">
                            <input class="subscribe-field-2 full w-input"
                                id="txtEmail" maxlength="256" name="Email"
                                placeholder="Enter your email" required="" type="text" onblur="validateEmail(this);">
                        </div>
                    </div>
                </div>
                <div class="top-margin-2 w-row">
                    <div class="w-col w-col-12">
                        <div class="form-margin">
                            <input class="subscribe-field-2 full w-input"
                                id="txtPassword" maxlength="256" name="Password"
                                placeholder="Enter your password" required="" type="password">
                        </div>
                    </div>
                </div>
                <div class="top-margin-2 w-row">
                    <div class="w-col w-col-12">
                        <div class="form-margin">
                            <input class="subscribe-field-2 full w-input"
                                id="txtConfirmPassword" maxlength="256" name="Re-type Password"
                                placeholder="Enter your password once again" required="" type="password">
                        </div>
                    </div>
                </div>
                <div class="top-margin-2 medium">
                    <div>
                        <input class="checkout-button full-button w-button" data-wait="Please wait..." type="submit" value="Register" onclick="var retvalue = SignUpUser(); event.returnValue = retvalue; return retvalue;">
                    </div>
                </div>

            </div>
            <div class="w-col w-col-3"></div>
        </div>
    </div>
    <div id="divLoader" class="divLoader" style="display: none;"></div>
    <script src="js/jquery-3.3.1.min.js"></script>
    <script type="text/javascript">
        $("#txtEmail").focusout(function () {
            if ($(this).val() == "") {
                $(this).addClass('w-commerce-commercecheckouterrorstate');
            }
            else {
                $(this).removeClass('w-commerce-commercecheckouterrorstate');
            }

        });

        $("#txtPassword").focusout(function () {
            if ($(this).val() == "") {
                $(this).addClass('w-commerce-commercecheckouterrorstate');
            }
            else {
                $(this).removeClass('w-commerce-commercecheckouterrorstate');
            }
        });

        $("#txtConfirmPassword").focusout(function () {
            if ($(this).val() == "") {
                $(this).addClass('w-commerce-commercecheckouterrorstate');
            }
            else {
                $(this).removeClass('w-commerce-commercecheckouterrorstate');
            }
        });

        function SignUpUser() {
            var regularExpression = /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{6,16}$/;

            var lstrMemberEmail = $("#txtEmail").val();
            var lstrPassword = $("#txtPassword").val();
            var lstrConfirmPassword = $("#txtConfirmPassword").val();

            if (lstrMemberEmail == "") {
                $("#txtEmail").addClass('w-commerce-commercecheckouterrorstate');
            }
            else if (lstrPassword == "") {
                $("#txtPassword").addClass('w-commerce-commercecheckouterrorstate');
            }
            else if (!regularExpression.test(lstrPassword)) {
                swal(" ", "Password should be minimum 6 characters and should contain atleast 1 digit and 1 special character.", "warning");
            }
            else if (lstrConfirmPassword == "") {
                $("#txtConfirmPassword").addClass('w-commerce-commercecheckouterrorstate');
            }
            else {

                $("#txtEmail").removeClass('w-commerce-commercecheckouterrorstate');
                $("#txtPassword").removeClass('w-commerce-commercecheckouterrorstate');
                $("#txtConfirmPassword").removeClass('w-commerce-commercecheckouterrorstate');

                if (lstrPassword == lstrConfirmPassword) {
                    $('#divLoader').show();

                    $.ajax({
                        url: 'Register.aspx/SignUpUser',
                        type: 'POST',
                        contentType: 'application/json; charset =utf-8',
                        data: "{'pstrMemberEmail':'" + lstrMemberEmail.toString() + "'" + "," + "'pstrMemberPassword':'" + lstrConfirmPassword.toString() + "'}",
                        dataType: 'json',
                        success: function (data) {
                            $('#divLoader').hide();
                            try {

                                var newData = data.d;

                                if ((newData != "") || newData == null) {
                                    if (newData == "SUCCESS") {
                                        swal(" ", "Registration Successful!", "success");
                                        window.location.href = "Index.aspx";
                                    }
                                    else {
                                        swal("Oops", "Something went wrong.", "error");
                                    }
                                }
                                else {
                                    swal("Oops", "Something went wrong.", "error");
                                }
                            }
                            catch (e) {
                                swal("Oops", "Something went wrong.", "error");
                                return false;
                            }
                            return false;
                        },
                        error: function (errmsg) {
                            $('#divLoader').hide();
                            swal("Oops", "Something went wrong.", "error");
                        }
                    });
                }
                else {
                    swal(" ", "Password Mismatch!", "warning");
                }
            }
        };

        function validateEmail(emailField) {
            var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
            if (reg.test(emailField.value) == false) {
                swal(" ", "Invalid Email Address", "warning");
                return false;
            }
            return true;

        }
    </script>
</asp:Content>
