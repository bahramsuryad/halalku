﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SuciHalalSite.Master" AutoEventWireup="true" CodeBehind="PaymentSuccessful.aspx.cs" Inherits="SuciHalalWebclient.PaymentSuccessul" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="pages-wrapper">
        <div>
            <img alt="" class="pages-icon" src="https://suci.apps.bangun-kreatif.com/thank_you_black.c32a1221.png" width="230">
            <h1>Thank you</h1>
            <p>
                Thank you for visiting us and making your purchase! We’re glad that you found what you were
                        looking for.
            </p>
            <div class="top-margin">
                <a class="shop-button w-inline-block" href="Index.aspx">
                    <div class="shop-text dark">Back To Homepage</div>
                    <div class="shop-line black" style="width: 0%;"></div>
                </a>
            </div>
        </div>
    </div>
</asp:Content>
