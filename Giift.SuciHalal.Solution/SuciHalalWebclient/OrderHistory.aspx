﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SuciHalalSite.Master" AutoEventWireup="true" CodeBehind="OrderHistory.aspx.cs" Inherits="SuciHalalWebclient.OrderHistory" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="order-history">
        <h3>Orders Overview</h3>
        <div id="divOrderOverview" class="order-overview-container">
            <%--<div class="order-overview-card">
                <img class="order-overview-card-icon" src="assets/icons/payment.png">
                <div class="order-overview-card-text-container">
                    <span class="order-overview-card-text-value">50</span>
                    <span class="order-overview-card-text-title">Waiting Payment</span>
                </div>
            </div>
            <div class="order-overview-card">
                <img class="order-overview-card-icon" src="assets/icons/progress.png">
                <div class="order-overview-card-text-container">
                    <span class="order-overview-card-text-value">0</span>
                    <span class="order-overview-card-text-title">On Progress</span>
                </div>
            </div>
            <div class="order-overview-card">
                <img class="order-overview-card-icon" src="assets/icons/complete.png">
                <div class="order-overview-card-text-container">
                    <span class="order-overview-card-text-value">22</span>
                    <span class="order-overview-card-text-title">Completed</span>
                </div>
            </div>
            <div class="order-overview-card">
                <img class="order-overview-card-icon" src="assets/icons/cancelled.png">
                <div class="order-overview-card-text-container">
                    <span class="order-overview-card-text-value">5</span>
                    <span class="order-overview-card-text-title">Cancelled</span>
                </div>
            </div>--%>
        </div>

        <h3 style="margin-top: 10vh;">Orders</h3>
        <div class="table-responsive-vertical shadow-z-1">
            <!-- Table starts here -->
            <table id="table" class="table table-hover table-mc-light-blue">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Date</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                    <%--<tr>
                        <td data-title="ID">1</td>
                        <td data-title="Name">
                            <a href="./order-detail.html">Dunkin Donuts Special</a>
                        </td>
                        <td data-title="Date">May 10, 2019</td>
                        <td data-title="Status">Completed</td>
                    </tr>--%>
                </tbody>
            </table>
        </div>
    </div>
    <div id="divLoader" class="divLoader" style="display: none;"></div>
    <script src="js/jquery-3.3.1.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            GetOrderHistory();
        });

        function GetOrderHistory() {
            $('#divLoader').show();
            $.ajax({
                url: 'OrderHistory.aspx/GetOrderHistory',
                type: 'POST',
                contentType: 'application/json; charset=utf-8',
                data: "",
                dataType: 'json',
                success: function (data) {

                    try {
                        $("#divOrderOverview").empty();
                        var divHtml = '', tblHtml = '', formattedDate = '', orderStatus = '', orderName = '';
                        var itemNo = 0;
                        var orderOverview = [];
                        var ordersData = data.d;
                        if (ordersData != "" && ordersData != null) {
                            orderOverview = getOrderOverview(ordersData);
                            for (var i = 0; i < orderOverview.length; i++) {
                                divHtml += '<div class="order-overview-card">' +
                                    '<img class="order-overview-card-icon" src="' + orderOverview[i].toString().split("|")[2] + '">' +
                                    '<div class="order-overview-card-text-container">' +
                                    '<span class="order-overview-card-text-value">' + orderOverview[i].toString().split("|")[0] + '</span>' +
                                    '<span class="order-overview-card-text-title">' + orderOverview[i].toString().split("|")[1] + '</span>' +
                                    '</div>' +
                                    '</div>';
                            }
                            for (var i = 0; i < ordersData.length; i++) {
                                if (ordersData[i].CartOrderItems != null && ordersData[i].CartOrderItems.length > 0) {
                                    itemNo += 1;
                                    formattedDate = new Date(parseInt(ordersData[i].CreatedAt.substr(6)));
                                    orderStatus = getOrderStatus(ordersData[i].Status);
                                    if (ordersData[i].CartOrderItems[0].Name != null && ordersData[i].CartOrderItems[0].Name != '' && ordersData[i].CartOrderItems[0].Name.length > 16) {
                                        orderName = ordersData[i].CartOrderItems[0].Name.substring(0, 16);
                                    }
                                    else if (ordersData[i].CartOrderItems[0].Name != null) {
                                        orderName = ordersData[i].CartOrderItems[0].Name;
                                    }
                                    tblHtml += '<tr><td data-title="ID">' + itemNo + '</td>' +
                                        '<td data-title="Name"><a href="OrderDetails.aspx?Id=' + ordersData[i].Id + '">' + orderName + '...' + '</a></td>' +
                                        '<td data-title="Date">' + formattedDate.toDateString() + '</td>' +
                                        '<td data-title="Status">' + orderStatus + '</td></tr>';
                                }
                            }
                            $("#divOrderOverview").append(divHtml);
                            $("#table tbody").append(tblHtml);

                        }
                        else if (ordersData == "NotFound") {
                            swal(" ", "No History Found!", "warning");
                        }
                        $('#divLoader').hide();
                    } catch (e) {
                        $('#divLoader').hide();
                        swal("Oops!", "Something went wrong", "error");
                        return false;
                    }
                    return false;
                },
                error: function (errmsg) {
                    $('#divLoader').hide();
                    swal("Oops!", "Something went wrong", "error");
                }
            });
        }

        function getOrderOverview(strData) {
            try {
                var retValue = [];
                var openCount = 0, progressCount = 0, completedCount = 0, cancelledCount = 0;
                for (var i = 0; i < strData.length; i++) {
                    if (strData[i].Status == 'open') {
                        openCount += 1;
                    }
                    if (strData[i].Status == 'progess' || strData[i].Status == 'in delivery') {
                        progressCount += 1;
                    }
                    if (strData[i].Status == 'complete') {
                        completedCount += 1;
                    }
                    if (strData[i].Status == 'cancelled') {
                        cancelledCount += 1;
                    }
                }
                retValue = [openCount.toString() + '|Waiting Payment|icons/payment.png', progressCount.toString() + '|On Progress / In delivery|icons/progress.png', completedCount.toString() + '|Completed|icons/complete.png', cancelledCount.toString() + '|Cancelled|icons/cancelled.png'];
                return retValue;
            } catch (e) {
                swal("Oops!", "Something went wrong", "error");
            }
        }

        function getOrderStatus(status) {
            try {
                var retValue = '';
                if (status == 'open') {
                    retValue = 'Waiting Payment';
                    return retValue;
                }
                else if (status == 'progess') {
                    return 'On Progess';
                }
                else if (status == 'complete') {
                    return 'Completed';
                }
                else if (status == 'cancelled') {
                    return 'Cancelled';
                }
                else if (status == 'in delivery') {
                    return 'in delivery';
                }
                else {
                    return retValue;
                }
            } catch (e) {
                swal("Oops!", "Something went wrong", "error");
            }
        }
    </script>
</asp:Content>
