﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SuciHalalSite.Master" AutoEventWireup="true" CodeBehind="Catalogue.aspx.cs" Inherits="SuciHalalWebclient.Catalogue" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="store">
        <div class="banner sub banner-store" id="banner-store"
            style="background-image: url( &quot;https://images.unsplash.com/photo-1528356857578-6dc41b9a62e0?ixlib=rb-1.2.1&amp;ixid=eyJhcHBfaWQiOjEyMDd9&amp;auto=format&amp;fit=crop&amp;w=1350&amp;q=80&quot; ) !important; background-size: cover !important;">
            <div class="container center in-sub w-container">
                <h1 class="sub-heading-title">All Products</h1>
                <div class="breadcrumb">
                    <div class="breadcrumb-content">
                        <a class="bread-link" href="Index.aspx">Home</a><img alt=""
                            src="https://uploads-ssl.webflow.com/5c3ca7fbdc8fab3302b31532/5c41f1941b7ac65ffde90630_right-arrow%20(3).svg"
                            width="18">
                        <div class="bread-link gray-color">Store</div>
                    </div>
                </div>
                <span style="margin-top: 25px; width: 22%;" class="ant-input-search ant-input-search-enter-button ant-input-group-wrapper hide-on-med-and-down">
                    <span class="ant-input-wrapper ant-input-group">
                        <input id="txtProductName" placeholder="Search Product" class="ant-input" type="text" value="">
                        <span class="ant-input-group-addon">
                            <button id="btnSearch" type="button" class="ant-btn ant-input-search-button ant-btn-primary" style="background-color: #04629d; border-color: #04629d;">
                                <i aria-label="icon: search" class="anticon anticon-search">
                                    <svg viewBox="64 64 896 896" class="" data-icon="search" width="1em" height="1em" fill="currentColor" aria-hidden="true" focusable="false">
                                        <path d="M909.6 854.5L649.9 594.8C690.2 542.7 712 479 712 412c0-80.2-31.3-155.4-87.9-212.1-56.6-56.7-132-87.9-212.1-87.9s-155.5 31.3-212.1 87.9C143.2 256.5 112 331.8 112 412c0 80.1 31.3 155.5 87.9 212.1C256.5 680.8 331.8 712 412 712c67 0 130.6-21.8 182.7-62l259.7 259.6a8.2 8.2 0 0 0 11.6 0l43.6-43.5a8.2 8.2 0 0 0 0-11.6zM570.4 570.4C528 612.7 471.8 636 412 636s-116-23.3-158.4-65.6C211.3 528 188 471.8 188 412s23.3-116.1 65.6-158.4C296 211.3 352.2 188 412 188s116.1 23.2 158.4 65.6S636 352.2 636 412s-23.3 116.1-65.6 158.4z"></path>
                                    </svg>
                                </i>
                            </button>
                        </span>
                    </span>
                </span>
            </div>
        </div>
        <div class="section">
            <div class="flex-store">
                <div id="filter" class="store-filter">
                    <!--<div style="top: 160px; width: 30vw;">-->
                    <div style="width: 100%;">
                        <div>
                            <h3 class="">Filter</h3>
                            <!--                                        <div class="top-margin">-->
                            <!--                                            <h4 class="footer-title">Sort By</h4>-->
                            <!--                                            <select class="w-commerce-commercecheckoutemailinput" required="">-->
                            <!--                                                <option value="">Select</option>-->
                            <!--                                                <option value="Popular">Popular</option>-->
                            <!--                                                <option value="Price">Price</option>-->
                            <!--                                                <option value="Highest Price">Highest Price</option>-->
                            <!--                                                <option value="Lowest Price">Lowest Price</option>-->
                            <!--                                            </select>-->
                            <!--                                        </div>-->
                        </div>
                        <div class="top-margin"></div>
                        <h4 class="footer-title">Category</h4>
                        <div class="">

                            <asp:DropDownList runat="server" CssClass="w-commerce-commercecheckoutemailinput" id="dropdownListCategory">
                          
                        
                  
                            </asp:DropDownList>

                        </div>

                        <div>
                            <div class="top-margin">
                                <h4 class="footer-title">Price</h4>
                                <asp:DropDownList ID="ddlPrice" runat="server" CssClass="w-commerce-commercecheckoutemailinput">
                                    <asp:ListItem Text="Select Price" Value="0-0" Selected="True"></asp:ListItem>
                                    <asp:ListItem Text="Less than 50" Value="0-50"></asp:ListItem>
                                    <asp:ListItem Text="50-100" Value="50-100"></asp:ListItem>
                                    <asp:ListItem Text="100-200" Value="100-200"></asp:ListItem>
                                    <asp:ListItem Text="200-300" Value="200-300"></asp:ListItem>
                                    <asp:ListItem Text="300-400" Value="300-400"></asp:ListItem>
                                    <asp:ListItem Text="400-500" Value="400-500"></asp:ListItem>
                                    <asp:ListItem Text="Greater than 500" Value="500-0"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div id="divMetas">
                            <%--<div class="top-margin">
                                    <h4 class="footer-title">Size</h4>
                                    <select class="w-commerce-commercecheckoutemailinput" required="">
                                        <option value="">Select Size</option>
                                        <option value="S">S</option>
                                        <option value="M">M</option>
                                        <option value="L">L</option>
                                        <option value="XL">XL</option>
                                    </select>
                                </div>
                                <div class="top-margin">
                                    <h4 class="footer-title">Color</h4>
                                    <select class="w-commerce-commercecheckoutemailinput" required="">
                                        <option value="">Select Color</option>
                                        <option value="Red">Red</option>
                                        <option value="Green">Green</option>
                                        <option value="Yellow">Yellow</option>
                                        <option value="Black">Black</option>
                                    </select>
                                </div>
                                <div class="top-margin">
                                    <button class="w-commerce-commercecartcheckoutbutton checkout-button"
                                        style="width: 100%;">
                                        Search
                                    </button>
                                </div>--%>
                        </div>

                    </div>
                    <div class="top-margin more" style="display: none;">
                        <h4 class="footer-title">Follow Us</h4>
                        <div class="top-margin">
                            <a class="social-wrapper w-inline-block" href="#">
                                <img
                                    alt="" class="social-icon"
                                    src="https://uploads-ssl.webflow.com/5c3ca7fbdc8fab3302b31532/5c45fc19970d27bf64b63577_font-awesome_4-7-0_facebook_100_0_111111_none.png"
                                    width="16">
                                <div class="shop-text dark no-margin">Facebook</div>
                            </a><a class="social-wrapper w-inline-block" href="#">
                                <img alt=""
                                    class="social-icon"
                                    src="https://uploads-ssl.webflow.com/5c3ca7fbdc8fab3302b31532/5c45fc19cdc5be71a2b0cdc4_font-awesome_4-7-0_twitter_100_0_111111_none.png"
                                    width="16">
                                <div class="shop-text dark no-margin">Twitter</div>
                            </a><a class="social-wrapper w-inline-block" href="#">
                                <img alt=""
                                    class="social-icon"
                                    src="https://uploads-ssl.webflow.com/5c3ca7fbdc8fab3302b31532/5c41ea0a8a209f10201c2e31_font-awesome_4-7-0_instagram_100_0_111111_none.png"
                                    width="16">
                                <div class="shop-text dark no-margin">Instagram</div>
                            </a>
                        </div>
                    </div>
                </div>
                <div id="product" class="store-product">
                    <!-- #change# added => style="margin-left:0px !important;" -->
                    <div class="w-dyn-list">
                        <div id="divProducts" runat="server" class="collection-list make-block w-dyn-items w-row">
                        </div>
                         
                        <div class="w-pagination-wrapper pagination"></div>
                        <div id="divPagination" class="center">
                            <ul id="ulPagingItems" class="pagination"></ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="divLoader" class="divLoader" style="display: none;"></div>
    
    <script type="text/javascript">
        const currentLocation = location.href;
        const menuItem = document.querySelectorAll('shop-button');
        const menuLength = menuItem.length;
        for(let i = 0; i<menuLength; i++) {
            if(menuItem[i].href === currentLocation) {
            menuItem[i].className = "active"
         }
        }
    </script>
    <script src="js/jquery-3.3.1.min.js"></script>

    <script type="text/javascript">
        var lintPageSize = 15;
        var lintTotalPages = 0;
        var category = "";

        $("#btnSearch").click(function () {
            var lstrProductName = $("#txtProductName").val();
            if (lstrProductName != "" && lstrProductName.length > 2) {
                BindProducts("all", lstrProductName, 0, 0);
            }
        });

        //$('#btnSearch').keypress(function (event) {
        //    var keycode = (event.keyCode ? event.keyCode : event.which);
        //    if (keycode == '13') {
        //        var lstrProductName = $("#txtProductName").val();
        //        if (lstrProductName != "" && lstrProductName.length > 2) {
        //            BindProducts("all", lstrProductName);
        //        }
        //    }
        //});

        $(document).keypress(function (e) {
            if (e.keyCode === 13) {
                e.preventDefault();
                var lstrProductNameNew = $("#txtProductName").val();
                if (lstrProductNameNew != "" && lstrProductNameNew.length > 2) {
                    BindProducts("all", lstrProductNameNew);
                }
                return false;
            }
        });

        //$("#btnSearch").keyup(function (event) {
        //    if (event.keyCode === 13) {
        //        var lstrProductName = $("#txtProductName").val();
        //        if (lstrProductName != "" && lstrProductName.length > 2) {
        //            BindProducts("all", lstrProductName);
        //        }
        //    }
        //});

        //function isPostBack()
        //{
        //    return document.referrer.indexOf(document.location.href) > -1;
        //}


        $(document).ready(function () {

            $(window).scroll(function () {
                if ($(window).scrollTop() >= 550 && $(window).scrollTop() < 4300) {
                    $('#filter').addClass('filter-fixed').removeClass('filter-bottom');
                    $('#product').addClass('product-fixed');
                } else if ($(window).scrollTop() >= 4300) {
                    $('#filter').removeClass('filter-fixed').addClass('filter-bottom');
                    //$('#product').removeClass('product-fixed');
                } else {
                    $('#filter').removeClass('filter-fixed').removeClass('filter-bottom');
                    $('#product').removeClass('product-fixed');
                }
            });

            $("#ContentPlaceHolder1_divProducts").text("");

            var queryStringValue = GetParameterValues("Category");
            if (queryStringValue != null && queryStringValue != "undefined") {
                category = decodeURIComponent(queryStringValue);
            }

            if (category != null && category != "") {
                BindProducts(category, "all", "0", "0");
            }
            else {
                category = "all";
                BindProducts(category, "all", "0", "0");
            }

            $('#ContentPlaceHolder1_dropdownListCategory').on('change', (e) => BindProducts(e.target.value, "all", "0", "0"))

        });
           
          
        $(document.body).on('change', '#<%=ddlPrice.ClientID %>', function () {
            var priceRange = $("#ContentPlaceHolder1_ddlPrice").val().split("-");
            var lowerVal = priceRange[0];
            var higherVal = priceRange[1];

            if (category != null && category != "") {
                BindProducts(category, "all", lowerVal, higherVal);
            }
            else {
                category = "all";
                BindProducts(category, "all", lowerVal, higherVal);
            }
        });

       

        function BindProducts(lstrCategory, lstrName, startRange, endRange) {
            category = lstrCategory;
            $('#divLoader').show();
            $.ajax({
                type: "POST",
                url: "Catalogue.aspx/BindProducts",
                data: "{pintSkipRecords:0,pintTakeRecords:" + lintPageSize + ",pstrCategory:'" + lstrCategory + "',pstrName:'" + lstrName + "',pintStartRange:" + startRange + ",pintEndRange:" + endRange + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: true,
                cache: false,
                success: function (msg) {
                    $('#divLoader').hide();
                    if (msg.d != null) {
                        lintTotalPages = msg.d.TotalPages;
                        if (msg.d.MetasHtml != "") {
                            $("#divMetas").html(msg.d.MetasHtml);
                        }
                        else {
                            $("#divMetas").html("");
                        }
                        if (lintTotalPages > 0) {
                            $("#ContentPlaceHolder1_divProducts").html("");
                            printProductsData(msg);
                            $('#divPagination').show();
                            $('html,body').animate({
                                scrollTop: $('#ContentPlaceHolder1_divProducts').offset().top
                            }, 500);

                            if ($('#ulPagingItems').data("twbs-pagination")) {
                                $('#ulPagingItems').twbsPagination('destroy');
                            }

                            $('#ulPagingItems').twbsPagination({
                                totalPages: lintTotalPages,
                                visiblePages: 10,
                                next: 'Next',
                                prev: 'Prev',
                                onPageClick: function (event, page) {
                                    pageData(page);
                                }
                            });
                        }
                        else {
                            $('#divPagination').hide();
                            $("#ContentPlaceHolder1_divProducts").text("No records were found.");
                            $('html,body').animate({
                                scrollTop: $('#ContentPlaceHolder1_divProducts').offset().top
                            }, 500);
                        }
                    }
                    else {
                        $('#divPagination').hide();
                        $("#ContentPlaceHolder1_divProducts").text("");
                        $("#ContentPlaceHolder1_divProducts").text("No records were found.");
                        $('html,body').animate({
                            scrollTop: $('#ContentPlaceHolder1_divProducts').offset().top
                        }, 500);
                    }
                },
                error: function () {
                    $('#divLoader').hide();
                    $('#divPagination').hide();
                    $("#ContentPlaceHolder1_divProducts").text("");
                    $("#ContentPlaceHolder1_divProducts").text("No records were found.");
                    $('html,body').animate({
                        scrollTop: $('#ContentPlaceHolder1_divProducts').offset().top
                    }, 500);
                }


            });
        }

        //function FilterProducts(productCategory, priceStart, priceEnd) {
        //    BindProducts(productCategory);
        //}

        // This function accepts a customer object
        // and prints the results to the div element.
        function printProductsData(msg) {
            $('#divLoader').show();
            var products = msg.d.PDetails;
            var html = "";
            $("#ContentPlaceHolder1_divProducts").text(html);
            var count = 0;
            for (var i = 0; i < products.length; i++) {
                html += '<div class="w-dyn-item w-col w-col-6"><a class="prdBox" href="ProductDetail.aspx?id=';
                html += products[i].Id;
                html += '"><div class="product-photo"><img alt="" class="product-image"';
                html += 'data-wf-sku-bindings="%5B%7B%22from%22%3A%22f_main_image_4dr%22%2C%22to%22%3A%22src%22%7D%5D"';
                html += 'sizes="(max-width: 320px) 100vw, (max-width: 767px) 90vw, 33vw" src="';
                html += products[i].ImageUrl;
                html += '"style="opacity: 1;"><div class="sale-badge w-condition-invisible">Sale</div>';
                html += '<div class="featured-badge w-condition-invisible">Featured</div></div>';
                html += '<div class="product-content" style="margin-bottom: 18px;">';
                html += '<div class="product-top-title"><h5 class="product-title">'
                html += products[i].Name;
                html += '</h5><div class="shop-line black" style="width: 0px;"></div></div><div class="compare-price w-condition-invisible "><div class="price compare"';
                html += 'data-wf-sku-bindings="%5B%7B%22from%22%3A%22f_price_%22%2C%22to%22%3A%22innerHTML%22%7D%5D">RM ';
                html += (parseFloat(products[i].Price)).toFixed(2);
                html += '</div><div class="price w-dyn-bind-empty"';
                html += 'data-wf-sku-bindings="%5B%7B%22from%22%3A%22f_compare_at_price_7dr10dr%22%2C%22to%22%3A%22innerHTML%22%7D%5D">';
                html += '</div></div><div><div class="price" data-wf-sku-bindings="%5B%7B%22from%22%3A%22f_price_%22%2C%22to%22%3A%22innerHTML%22%7D%5D">';
                html += products[i].Currency + ' ' + (parseFloat(products[i].Price)).toFixed(2);
               
                html += '</div>'
                
                html += '</div>'
                
                html += '</div>'
                
                html += '</a>'
                html += '<div class="btn-outer-buynow"><a class="btn-buynow" href="ProductDetail.aspx?id=' + products[i].Id + '">Buy Now</a></div>'

                
                html += '</div>'
            }
            $("#ContentPlaceHolder1_divProducts").append(html);
            $('#divLoader').hide();
        }

        function pageData(e) {
            $('#divLoader').show();
            var lintSkipRecords = e == 1 ? 0 : (e * lintPageSize) - lintPageSize;
            var lstrCategoryVal = category;


            var priceRangeAll = $("#ContentPlaceHolder1_ddlPrice").val().split("-");
            var lowerPriceVal = priceRangeAll[0];
            var higherPriceVal = priceRangeAll[1];

            $.ajax({
                type: "POST",
                url: "Catalogue.aspx/BindProducts",
                data: "{pintSkipRecords: " + lintSkipRecords + ",pintTakeRecords:" + lintPageSize + ",pstrCategory:'" + lstrCategoryVal + "',pstrName:'" + "all" + "',pintStartRange:" + lowerPriceVal + ",pintEndRange:" + higherPriceVal + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: true,
                cache: false,
                success: function (msg) {
                    $('#divLoader').hide();
                    printProductsData(msg);
                },
                error: function () {
                    $('#divLoader').hide();
                    $("#ContentPlaceHolder1_divProducts").text("");
                    $("#ContentPlaceHolder1_divProducts").text("No records were found.");
                }
            });
            return false;
        }

        function GetParameterValues(param) {
            var url = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
            for (var i = 0; i < url.length; i++) {
                var urlparam = url[i].split('=');
                if (urlparam[0] == param) {
                    return urlparam[1];
                }
            }
        }

       

        
    </script>
</asp:Content>
