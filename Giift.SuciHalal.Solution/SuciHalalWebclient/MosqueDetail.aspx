﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SuciHalalSite.Master" AutoEventWireup="true" CodeBehind="MosqueDetail.aspx.cs" Inherits="SuciHalalWebclient.MosqueDetail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="tools_list" style="background-color: #eee;">
        <div class="banner sub"
            style="background-image: -webkit-gradient(linear, 0% 0%, 0% 100%, from(rgba(0, 0, 0, 0.3)), to(rgba(0, 0, 0, 0.3))), url('https://images.unsplash.com/photo-1515091943-9d5c0ad475af?ixlib=rb-1.2.1&auto=format&fit=crop&w=375&q=80'); background-size: cover; background-position: center 80%;">
            <div class="container center in-sub w-container">
                <h1 class="sub-heading-title"><span id="spanMosqueNameTitle" runat="server"></span>
                </h1>
                <div class="breadcrumb">
                    <div class="breadcrumb-content">
                        <a class="bread-link" href="Index.aspx">Home</a>
                        <img alt="" src="https://uploads-ssl.webflow.com/5c3ca7fbdc8fab3302b31532/5c41f1941b7ac65ffde90630_right-arrow%20(3).svg" width="18">
                        <a class="bread-link" href="Tools.aspx">Tools</a>
                        <img alt="" src="https://uploads-ssl.webflow.com/5c3ca7fbdc8fab3302b31532/5c41f1941b7ac65ffde90630_right-arrow%20(3).svg" width="18">
                        <a class="bread-link" href="Mosque.aspx">Mosque</a>
                        <img alt="" src="https://uploads-ssl.webflow.com/5c3ca7fbdc8fab3302b31532/5c41f1941b7ac65ffde90630_right-arrow%20(3).svg" width="18">
                        <div class="bread-link gray-color">
                            <span id="spanMosqueNameMain" runat="server"></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="section">
            <div class="row">
                <div class="restaurant-detail-card">
                    <div class="restaurant-detail-card-left">
                        <p class="restaurant-detail-card-certified"><span id="spanMosqueRegion" runat="server"></span></p>
                        <p class="restaurant-detail-card-name"><span id="spanMosqueName" runat="server"></span></p>
                        <p class="restaurant-detail-card-address">
                            <a id="mapAddress" href="#" runat="server" target="_blank"><span id="spanMosqueAddress" runat="server"></span></a>
                        </p>
                        <%--<div class="restaurant-detail-card-detail-container">
                            <img class="restaurant-detail-card-detail-image"
                                src="images/phone-icon.png">
                            <p class="restaurant-detail-card-detail-text">
                                <b> </b>
                            </p>
                        </div>--%>
                        <div id="divPhone" runat="server" class="restaurant-detail-card-detail-container">
                        </div>
                        <div id="divServiceLanguage" runat="server" class="restaurant-detail-card-detail-container">
                        </div>
                        <div id="divQuickFacts" runat="server" class="restaurant-detail-card-detail-container">
                        </div>
                    </div>
                    <div class="restaurant-detail-card-right">
                        <img id="imgMosqueImage" src="" runat="server" class="restaurant-detail-card-image">
                    </div>
                </div>
            </div>
        </div>
        <div class="section" style="padding-top: 0px !important;">
            <div class="card-white-background">

                <input id="tab2" type="radio" name="tabs" checked>
                <label class="label-tab" for="tab2">About</label>

                <%--<input id="tab3" type="radio" name="tabs">
                <label class="label-tab" for="tab3">Photos</label>--%>

                <section style="padding: 15px;" id="content2">
                    <div class="restaurant-about-container">
                        <h3>About <span id="spanAboutMosqueName" runat="server"></span></h3>
                        <p><span id="spanMosqueDescription" runat="server">No description available.</span></p>
                        <%--<p>
                            Bacon ipsum dolor sit amet landjaeger sausage brisket, jerky drumstick fatback boudin.
                        </p>
                        <p>
                            Jerky jowl pork chop tongue, kielbasa shank venison. Capicola shank pig ribeye leberkas filet mignon brisket beef kevin tenderloin porchetta. Capicola fatback venison shank kielbasa, drumstick ribeye landjaeger beef kevin tail meatball pastrami prosciutto pancetta. Tail kevin spare ribs ground round ham ham hock brisket shoulder. Corned beef tri-tip leberkas flank sausage ham hock filet mignon beef ribs pancetta turkey.
                        </p>--%>
                    </div>
                </section>

                <%--<section style="padding: 15px;" id="content3">
                    <h3>Mosque Al-'Alamin Images - Pranakorn, Bangkok</h3>
                    <div class="restaurant-photos-container">
                        <img class="restaurant-photos-image" src="https://images.unsplash.com/photo-1549829975-9dc94a71ab0c?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1448&q=80">
                        <img class="restaurant-photos-image" src="https://images.unsplash.com/photo-1549829975-9dc94a71ab0c?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1448&q=80">
                        <img class="restaurant-photos-image" src="https://images.unsplash.com/photo-1549829975-9dc94a71ab0c?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1448&q=80">
                        <img class="restaurant-photos-image" src="https://images.unsplash.com/photo-1549829975-9dc94a71ab0c?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1448&q=80">
                        <img class="restaurant-photos-image" src="https://images.unsplash.com/photo-1549829975-9dc94a71ab0c?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1448&q=80">
                    </div>
                </section>--%>
            </div>
        </div>
    </div>
</asp:Content>
