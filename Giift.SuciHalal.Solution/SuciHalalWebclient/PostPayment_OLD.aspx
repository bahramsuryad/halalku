﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PostPayment_OLD.aspx.cs" Inherits="SuciHalalWebclient.PostPayment_OLD" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

    <link href="css/slick.min.css" rel="stylesheet" type="text/css">
    <link href="css/slick-theme.min.css" rel="stylesheet" type="text/css">
    <link href="css/antd.min.f5349187.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet" type="text/css">
    <link href="https://fast.fonts.net/cssapi/487b73f1-c2d1-43db-8526-db577e4c822b.css" rel="stylesheet"
        type="text/css">
    <%--<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD9URUN6ziiszOfffaY4rpP4TmVrRXP6FY&amp;libraries=places,drawing,visualization"
        type="text/javascript"></script>--%>
    <script src="js/webfont.js" type="text/javascript"></script>
    <%-- <link href="https://fonts.googleapis.com/css?family=Poppins:100,200,300,regular,500,600,700,800,900%7CIndie+Flower:regular"
        rel="stylesheet">--%>
    <link href="https://uploads-ssl.webflow.com/5c3ca7fbdc8fab3302b31532/5c87d724b4d0caf567e8988a_fa.jpg"
        rel="shortcut icon" type="image/x-icon">
    <link href="https://uploads-ssl.webflow.com/5c3ca7fbdc8fab3302b31532/5c87d729b774e4f7561ff95d_fav.jpg"
        rel="apple-touch-icon">
    <style>
        body,
        html {
            overflow-x: hidden
        }

        .theme-background-color {
            background-color: #04629d !important
        }
    </style>
    <link href="css/src.fc45d0fd.css" rel="stylesheet">
    <%--<script charset="UTF-8" src="https://maps.googleapis.com/maps-api-v3/api/js/36/9/common.js"
        type="text/javascript"></script>
    <script charset="UTF-8" src="https://maps.googleapis.com/maps-api-v3/api/js/36/9/util.js"
        type="text/javascript"></script>--%>
</head>
<body>
    <form action="PaymentStatus.aspx" method="POST">

        <div data-node-type="commerce-checkout-form-container" data-wf-page-link-href-prefix=""
            class="w-commerce-commercecheckoutformcontainer gray checkout">
            <div class="w-commerce-commercelayoutcontainer w-container">
                <div>
                    <a class="brand-logo w-nav-brand w--current" href="home">
                        <div class="logo-text">
                            <img src="https://visa-my.s3-ap-southeast-1.amazonaws.com/assets/img/visa_cart_logo.png" style="height: 100px;">
                        </div>
                    </a>
                </div>
            </div>
            <div class="w-commerce-commercelayoutmain" style="width: 70%; padding-left: 30%">

                <div data-node-type="commerce-checkout-customer-info-wrapper" class="w-commerce-commercecheckoutcustomerinfowrapper customer-info" style="align-content: center;">

                    <div class="w-commerce-commercecheckoutblockheader block-header-2">
                        <h4>Payment Confirmation</h4>
                    </div>
                    <fieldset class="w-commerce-commercecheckoutblockcontent">
                        <label
                            class="w-commerce-commercecheckoutlabel field-label-2">
                            Order Reference</label>
                        <input runat="server"
                            id="txtOrderReference1" readonly type="text" class="w-commerce-commercecheckoutemailinput" />
                        <br />
                        <label
                            class="w-commerce-commercecheckoutlabel field-label-2">
                            Total Charges (<%:Session["CartCurrency"].ToString().ToUpper() %>)</label>
                        <input runat="server"
                            id="txtChargeAmount" readonly type="text" class="w-commerce-commercecheckoutemailinput" />
                    </fieldset>
                    <fieldset class="w-commerce-commercecheckoutblockcontent" style="padding-left: 33%;">
                        <script
                            src="https://checkout.stripe.com/checkout.js" class="stripe-button"
                            data-key="<%=PostPublishableKey %>"
                            data-amount="<%=PostAmount %>"
                            data-name="eGiftHalal"
                            data-description="Enter your payment details"
                            data-locale="auto"
                            data-image="https://visa-my.s3-ap-southeast-1.amazonaws.com/assets/img/visa_stripe.jpg"
                            data-zip-code="true"
                            data-currency="<%:Session["CartCurrency"]%>">
                        </script>

                        Or
                        <a href="index.aspx" style="text-decoration: underline;">Cancel</a>
                    </fieldset>
                </div>
            </div>
        </div>
        <footer class="footer" style="margin-top: -200px;">
            <div>
                <div class="w-row">
                    <div id="divFooterCategories" runat="server">
                    </div>
                    <div class="w-col w-col-2 w-col-stack">
                        <div class="margin-top-device">
                            <div>
                                <h4 class="footer-title">Menu</h4>
                            </div>
                            <div class="top-margin">
                                <a class="shop-button w-inline-block w--current" data-w-id="fba3057a-8780-7c1e-d882-8dca6c731a63" href="Index.aspx">
                                    <div class="shop-text">Home</div>
                                    <div class="shop-line" style="width: 0%;"></div>
                                </a>
                                <div>
                                    <a class="shop-button with-margin w-inline-block" data-w-id="fba3057a-8780-7c1e-d882-8dca6c731a68" href="Travel.aspx">
                                        <div class="shop-text">Travel</div>
                                        <div class="shop-line" style="width: 0%;"></div>
                                    </a>
                                </div>
                                <div>
                                    <a class="shop-button with-margin w-inline-block" data-w-id="fba3057a-8780-7c1e-d882-8dca6c731a68" href="Catalogue.aspx">
                                        <div class="shop-text">Shop</div>
                                        <div class="shop-line" style="width: 0%;"></div>
                                    </a>
                                </div>
                                <div>
                                    <a class="shop-button with-margin w-inline-block" data-w-id="fba3057a-8780-7c1e-d882-8dca6c731a68" href="#">
                                        <div class="shop-text">F&B Offers</div>
                                        <div class="shop-line" style="width: 0%;"></div>
                                    </a>
                                </div>
                                <div>
                                    <a class="shop-button with-margin w-inline-block" data-w-id="fba3057a-8780-7c1e-d882-8dca6c731a68" href="Tools.aspx">
                                        <div class="shop-text">Tools</div>
                                        <div class="shop-line" style="width: 0%;"></div>
                                    </a>
                                </div>
                                <div>
                                    <a class="shop-button with-margin w-inline-block" data-w-id="fba3057a-8780-7c1e-d882-8dca6c731a68" href="Tools.aspx">
                                        <div class="shop-text">Donation</div>
                                        <div class="shop-line" style="width: 0%;"></div>
                                    </a>
                                </div>
                                <%--<div>
                                                <a class="shop-button with-margin w-inline-block" data-w-id="fba3057a-8780-7c1e-d882-8dca6c731a68" href="about.aspx">
                                                    <div class="shop-text dark">About</div>
                                                    <div class="shop-line black" style="width: 0%;"></div>
                                                </a>
                                            </div>--%>
                            </div>
                        </div>
                    </div>
                    <div class="w-col w-col-3 w-col-stack"></div>
                    <div class="w-col w-col-6 w-col-stack">
                        <div class="margin-top-device">
                            <div>
                                <h4 class="footer-title">All good stuff in your inbox<br>
                                </h4>
                                <p class="shop-text light">All the latest &amp; updates from My Halal Stop. No spam, we promise.</p>
                            </div>
                            <div class="top-margin-2">
                                <div class="subscribe-content">
                                    <div class="form-block w-form">
                                        <form class="w-clearfix" data-name="Email Form" id="email-form" name="email-form">
                                            <input style="background-color: transparent;" class="subscribe-field w-input" data-name="Email 2" id="email-2" maxlength="256"
                                                name="email-2" placeholder="Enter your email" required="" type="email">
                                            <input style="background-color: transparent; margin-left: 0px;" class="submit-button w-button" data-wait="Please wait..." type="submit"
                                                value="Subscribe">
                                        </form>
                                        <div class="success-message w-form-done">
                                            <div>Thank you! Your submission has been received!</div>
                                        </div>
                                        <div class="error-state w-form-fail">
                                            <div>Oops! Something went wrong while submitting the form.</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="top-margin-2 more">
                    <div>
                        <div class="flex-row make-vertical w-row">
                            <div class="w-col w-col-6 w-col-stack">
                                <div class="">
                                    <p class="copyright" style="color: #ffffff;">© 2019&nbsp;My Halal Stop by&nbsp;<a class="link-2" href="https://www.giift.com/" target="_blank">Giift.com</a>.</p>
                                </div>
                            </div>
                            <div class="w-col w-col-6 w-col-stack">
                                <div style="" class="stripe-flex make-right">
                                    <p class="secure-text black" style="color: #fff; text-align: start;">Powered by</p>
                                    <img src="https://d27504pnkoride.cloudfront.net/uploads/2019/03/logo-white.png" class="image-powered-giift" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </form>
</body>
</html>
