﻿using Framework.EnterpriseLibrary.Adapters;
using Giift.SuciHalal.Entities;
using Giift.SuciHalal.Helper;
using Microsoft.CSharp.RuntimeBinder;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SuciHalalWebclient
{
    public partial class DonationDetail : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (HttpContext.Current.Session["MemberEmail"] != null)
                {
                    txtEmail.Value = Convert.ToString(HttpContext.Current.Session["MemberEmail"]);
                }
            }
        }

        [WebMethod]
        public static BuyableDetail BindDonation(string pstrId, string pstrAccessToken)
        {
            BuyableDetail lobjBuyableDetail = null;
            try
            {
                string lstrDetails = ApiHelper.BuyableDetails(pstrAccessToken, "card", pstrId, ""); // removed myr
                lobjBuyableDetail = JsonConvert.DeserializeObject<BuyableDetail>(lstrDetails);
                lobjBuyableDetail.units = lobjBuyableDetail.units.FindAll(x => x.type.Equals("fixed"));
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("DonationDetail.aspx.cs BindDonation:" + ex.Message + Environment.NewLine + "StackTrace:" + ex.StackTrace + Environment.NewLine + "InnerException:" + ex.InnerException);
            }
            return lobjBuyableDetail;
        }

        [WebMethod]
        public static string DonateNow(string pstrId, string pstrName, string pstrEmail, int pintValue, string pstrUnit, string pstrPrice)
        {
            string lstrResponse = string.Empty;
            double pdblPrice = Convert.ToDouble(pstrPrice);

            try
            {
                string lstrAccessToken = Convert.ToString(HttpContext.Current.Session["AccessToken"]);
                string lstrCartId = string.Empty;
                string lstrOrderId = string.Empty;
                bool proceed = false;
                string lstrSenderEmail = string.Empty;

                if (HttpContext.Current.Session["MemberEmail"] != null)
                {
                    lstrSenderEmail = Convert.ToString(HttpContext.Current.Session["MemberEmail"]);
                    string lstrNewCartResponse = ApiHelper.NewCart(lstrAccessToken);

                    dynamic lobjNewCartResponse = JsonConvert.DeserializeObject<dynamic>(lstrNewCartResponse);
                    if (IsPropertyExist(lobjNewCartResponse, "error"))
                    {
                        proceed = false;
                        LoggingAdapter.WriteLog("DonateNow NewCart Error:" + lobjNewCartResponse.error_description);
                    }
                    else
                    {
                        proceed = true;
                        lstrCartId = lobjNewCartResponse.Id;
                    }

                    if (proceed)
                    {
                        string lstrAddCartItem = ApiHelper.AddCartItem(lstrAccessToken, "card", pstrId, pstrUnit, pintValue, lstrCartId);

                        dynamic lobjResponse = JsonConvert.DeserializeObject<dynamic>(lstrAddCartItem);
                        if (IsPropertyExist(lobjResponse, "error"))
                        {
                            proceed = false;
                            LoggingAdapter.WriteLog("DonateNow AddCartItem Error:" + lobjResponse.error_description);
                        }
                        else
                        {
                            if (IsPropertyExist(lobjResponse, "Currency"))
                            {
                                HttpContext.Current.Session["CartCurrency"] = lobjResponse.Currency.ToString();
                                proceed = true;
                            }
                        }
                    }


                    if (proceed)
                    {
                        string lstrCartCheckout = ApiHelper.CartCheckout(lstrAccessToken, lstrCartId);

                        dynamic lobjResponse = JsonConvert.DeserializeObject<dynamic>(lstrCartCheckout);
                        if (IsPropertyExist(lobjResponse, "error"))
                        {
                            proceed = false;
                            LoggingAdapter.WriteLog("DonateNow CartCheckout Error:" + lobjResponse.error_description);
                        }
                        else
                        {
                            proceed = true;
                            lstrOrderId = lobjResponse.Id;
                            for (int i = 0; i < lobjResponse.CartOrderItems.Count; i++)
                            {
                                string lstr1 = ApiHelper.SetDeliveryEmail(lstrAccessToken, Convert.ToString(lobjResponse.CartOrderItems[i].Id), "email", pstrName, pstrEmail, lstrSenderEmail);
                            }
                        }
                    }

                    if (proceed)
                    {
                        PaymentDetails paymentDetails = new PaymentDetails();
                        paymentDetails.OrderId = lstrOrderId;
                        paymentDetails.Amount = pdblPrice;
                        HttpContext.Current.Session["PaymentDetails"] = paymentDetails;

                        lstrResponse = "SUCCESS";
                    }
                    else
                    {
                        lstrResponse = "FAILED";
                    }
                }
                else
                {
                    lstrResponse = "FAILED";
                }
            }
            catch (Exception ex)
            {
                lstrResponse = "FAILED";
                LoggingAdapter.WriteLog("DonationDetail.aspx.cs DonateNow:" + ex.Message + Environment.NewLine + "StackTrace:" + ex.StackTrace + Environment.NewLine + "InnerException:" + ex.InnerException);
            }

            return lstrResponse;
        }

        public static bool IsPropertyExist(dynamic dynamicObj, string property)
        {
            bool lblnStatus = false;
            try
            {
                var value = dynamicObj[property].Value;
                lblnStatus = true;
            }
            catch (RuntimeBinderException)
            {
                lblnStatus = false;
            }
            catch (Exception)
            {
                lblnStatus = false;
            }
            return lblnStatus;
        }
    }
}