﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SuciHalalSite.Master" AutoEventWireup="true" CodeBehind="Mosque.aspx.cs" Inherits="SuciHalalWebclient.Mosque" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="tools_list">
        <div class="banner sub"
            style="background-image: -webkit-gradient(linear, 0% 0%, 0% 100%, from(rgba(0, 0, 0, 0.3)), to(rgba(0, 0, 0, 0.3))), url('https://images.unsplash.com/photo-1515091943-9d5c0ad475af?ixlib=rb-1.2.1&auto=format&fit=crop&w=375&q=80'); background-size: cover; background-position: center 80%;">
            <div class="container center in-sub w-container">
                <h1 class="sub-heading-title">Nearby Mosque</h1>
                <div class="breadcrumb">
                    <div class="breadcrumb-content">
                        <a class="bread-link" href="Index.aspx">Home</a>
                        <img alt="" src="https://uploads-ssl.webflow.com/5c3ca7fbdc8fab3302b31532/5c41f1941b7ac65ffde90630_right-arrow%20(3).svg" width="18">
                        <a class="bread-link" href="Tools.aspx">Tools</a>
                        <img alt="" src="https://uploads-ssl.webflow.com/5c3ca7fbdc8fab3302b31532/5c41f1941b7ac65ffde90630_right-arrow%20(3).svg" width="18">
                        <div class="bread-link gray-color">Nearby Mosque</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="section">
            <h3 style="margin-top: 0px; margin-bottom: 30px;">Nearby Mosque</h3>
            <div id="divMosqueList">
            </div>
            <div class="center">
                <ul id="ulPagingItems" class="pagination"></ul>
            </div>
        </div>
    </div>
    <div id="divLoader" class="divLoader" style="display: none;"></div>
    <script src="js/jquery-3.3.1.min.js"></script>
    <script type="text/javascript">
        var lintPageSize = 15;
        var lintTotalPages = 0;
        var latitude = "";
        var longitude = "";

        $(document).ready(function () {

            $("#divMosqueList").text("");
            getLocation();
        });

        function GetMosque() {
            $('#divLoader').show();

            $.ajax({
                type: "POST",
                url: "Mosque.aspx/GetNearbyMosque",
                data: "{pintPageNo:1,pintTakeRecords:10,pstrLatitude:'" + latitude + "',pstrLongitude:'" + longitude + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: true,
                cache: false,
                success: function (msg) {
                    $('#divLoader').hide();
                    if (msg.d != null) {
                        lintTotalPages = msg.d.TotalPages;
                        if (lintTotalPages > 0) {
                            $("#divMosqueList").html("");
                            printMosqueData(msg);

                            $('#ulPagingItems').twbsPagination({
                                totalPages: lintTotalPages,
                                visiblePages: 10,
                                next: 'Next',
                                prev: 'Prev',
                                onPageClick: function (event, page) {
                                    pageData(page);
                                }
                            });
                        }
                        else {
                            $("#divMosqueList").text("No records were found.");
                        }
                    }
                    else {
                        swal("Oops!", "Something went wrong.", "error");
                    }
                },
                error: function () {
                    $('#divLoader').hide();
                    $("#divMosqueList").text("");
                    $("#divMosqueList").text("No records were found.");
                }
            });
        }

        function printMosqueData(msg) {
            $('#divLoader').show();
            var mosques = msg.d.MDetails;
            var html = "";
            $("#divMosqueList").text(html);
            var count = 0;
            for (var i = 0; i < mosques.length; i++) {

                html += '<a href="./MosqueDetail.aspx?id=' + mosques[i].Id + '">';
                html += '<div class="row">';
                html += '<div class="restaurant-list-card">';
                html += '<div class="restaurant-list-card-left">';
                //if (mosques[i].ServiceLanguage != "") {
                //    html += '<p class="restaurant-list-card-certified">Service language: <b>' + mosques[i].ServiceLanguage + '</b></p>';
                //}
                html += '<p class="restaurant-list-card-name">' + mosques[i].Name + '</p>';
                html += '<p class="restaurant-list-card-address">' + mosques[i].Address + '</p>';
                html += '<p class="restaurant-list-card-distance"><b>' + mosques[i].Distance + ' KM</b> | ' + '' + '</p>';
                html += '</div>';
                html += '<div class="restaurant-list-card-right">';
                html += '<img class="restaurant-list-card-image" src="' + mosques[i].ImageUrl + '">';
                html += '</div>';
                html += '</div>';
                html += '</div>';
                html += '</a>';
            }
            $("#divMosqueList").append(html);
            $('#divLoader').hide();
        }

        function pageData(e) {
            $('#divLoader').show();
            //var lintSkipRecords = e == 1 ? 0 : (e * lintPageSize) - lintPageSize;
            $.ajax({
                type: "POST",
                url: "Mosque.aspx/GetNearbyMosque",
                data: "{pintPageNo:" + e + ",pintTakeRecords:10,pintTakeRecords:" + lintPageSize + ",pstrLatitude:'" + latitude + "',pstrLongitude:'" + longitude + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: true,
                cache: false,
                success: function (msg) {
                    $('#divLoader').hide();
                    printMosqueData(msg);
                },
                error: function () {
                    $('#divLoader').hide();
                    $("#divMosqueList").text("");
                    $("#divMosqueList").text("No records were found.");
                }
            });
            return false;
        }

        function getLocation() {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(showPosition, showError);
            } else {
                //x.innerHTML = "Geolocation is not supported by this browser.";
            }
        }

        function showPosition(position) {
            latitude = position.coords.latitude;
            longitude = position.coords.longitude;
            GetMosque();
        }

        function showError(error) {
            switch (error.code) {
                case error.PERMISSION_DENIED:
                    //x.innerHTML = "User denied the request for Geolocation."
                    break;
                case error.POSITION_UNAVAILABLE:
                    //x.innerHTML = "Location information is unavailable."
                    break;
                case error.TIMEOUT:
                    //x.innerHTML = "The request to get user location timed out."
                    break;
                case error.UNKNOWN_ERROR:
                    //x.innerHTML = "An unknown error occurred."
                    break;
            }
            latitude = "3.142615";
            longitude = "101.691110";

            GetMosque();
        }

    </script>
</asp:Content>
