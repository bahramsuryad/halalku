﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SuciHalalSite.Master" AutoEventWireup="true" CodeBehind="OrderDetails.aspx.cs" Inherits="SuciHalalWebclient.OrderDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style type="text/css">
        #content {
            background-color: #eee !important;
        }
    </style>
    <div class="order-detail">
        <div class="row">
            <div class="restaurant-detail-card">
                <div id="divOrderDetailsLeft" class="restaurant-detail-card-left">
                    <%--<p class="restaurant-detail-card-certified">0bb573e8-8a55-46db-9f41-db3037e2cd7b</p>
                    <p class="restaurant-detail-card-name">Dunkin Donuts Special</p>
                    <p class="restaurant-detail-card-address">Thanon Samsen, Banglumphu, Pranakorn, Bangkok 10200</p>
                    <div class="restaurant-detail-card-detail-container">
                        <img class="restaurant-detail-card-detail-image"
                            src="./assets/images/calendar.png">
                        <p class="restaurant-detail-card-detail-text">Date - <b>May 5, 2019</b></p>
                    </div>
                    <div class="restaurant-detail-card-detail-container">
                        <img class="restaurant-detail-card-detail-image"
                            src="./assets/images/money-bag.png">
                        <p class="restaurant-detail-card-detail-text">Price - <b>$25</b></p>
                    </div>
                    <div class="restaurant-detail-card-detail-container">
                        <img class="restaurant-detail-card-detail-image"
                            src="./assets/images/bag.png">
                        <p class="restaurant-detail-card-detail-text">Orders - <b>3 package</b></p>
                    </div>
                    <div class="restaurant-detail-card-detail-container">
                        <img class="restaurant-detail-card-detail-image"
                            src="./assets/images/check.png">
                        <p class="restaurant-detail-card-detail-text">Completed Order</p>
                    </div>--%>
                    <!--                            <div class="restaurant-detail-card-detail-container">-->
                    <!--                                <img class="restaurant-detail-card-detail-image"-->
                    <!--                                     src="./assets/images/close.png">-->
                    <!--                                <p class="restaurant-detail-card-detail-text">Cancelled Order</p>-->
                    <!--                            </div>-->
                    <!--                            <div class="restaurant-detail-card-detail-container">-->
                    <!--                                <img class="restaurant-detail-card-detail-image"-->
                    <!--                                     src="./assets/images/money-icon.png">-->
                    <!--                                <p class="restaurant-detail-card-detail-text">Waiting Payment</p>-->
                    <!--                            </div>-->
                    <!--                            <div class="restaurant-detail-card-detail-container">-->
                    <!--                                <img class="restaurant-detail-card-detail-image"-->
                    <!--                                     src="./assets/images/loading-circles.png">-->
                    <!--                                <p class="restaurant-detail-card-detail-text">Processing Order</p>-->
                    <!--                            </div>-->
                </div>
                <div id="divOrderDetailsRight" class="restaurant-detail-card-right">
                    <%--<img class="restaurant-detail-card-image"
                        src="https://cdn-2.tstatic.net/travel/foto/bank/images/dunkin-donuts_20180930_130941.jpg">
                    <img class="restaurant-detail-card-image"
                        src="http://cdn2.tstatic.net/wow/foto/bank/images/dunkin-donuts.jpg">--%>
                </div>
            </div>
        </div>

        <h3 style="margin-top: 5vh;">Item Detail</h3>

        <div id="divItemDetails" class="order-detail-item-parent">
            <%--<a href="./product-detail.html">
                <div class="row">
                    <div class="item-list-card">
                        <div class="item-list-card-left">
                            <p class="item-list-card-certified">Total Price - <b>$5</b></p>
                            <p class="item-list-card-name">Donuts 1/2 Dozen</p>
                            <p class="item-list-card-address">Lorem ipsum dolor sit amet, consectectur elit.</p>
                            <p class="item-list-card-distance"><b>2 Items</b> | Bought</p>
                        </div>
                        <div class="item-list-card-right">
                            <img class="item-list-card-image" src="https://static1.squarespace.com/static/5159fc77e4b0098fd5ffcaba/t/51ce2576e4b01d5d20f44b6d/1372464503893/51_Q7041DznDonHz_0209_Lo.jpg?format=1000w">
                        </div>
                    </div>
                </div>
            </a>

            <a href="./product-detail.html">
                <div class="row">
                    <div class="item-list-card">
                        <div class="item-list-card-left">
                            <p class="item-list-card-certified">Total Price - <b>$10</b></p>
                            <p class="item-list-card-name">Donuts 1 Dozen</p>
                            <p class="item-list-card-address">Lorem ipsum dolor sit amet, consectectur elit.</p>
                            <p class="item-list-card-distance"><b>1 Items</b> | Bought</p>
                        </div>
                        <div class="item-list-card-right">
                            <img class="item-list-card-image" src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxITEhUSEhIVFhUXFRUVFRgVFxUXFRcXFxUXFxUVFRcYHSggGBolGxUVITEhJSkrLi4uFx8zODMtNygtLisBCgoKDg0OGxAQGi0lHyUtLS0tLS0rLS0tLS0rLS0tLS0tLS0tLS0rLS0tKy0tLS0tLS0tLS0vLSstLS0tLS0tLf/AABEIAMIBAwMBIgACEQEDEQH/xAAcAAABBQEBAQAAAAAAAAAAAAAEAQIDBQYABwj/xAA+EAACAQMCBAQFAQQJAwUAAAABAhEAAyEEMQUSQVEGImFxEzKBkaFCB7HB0RQVIzNSYpLh8HKC8RZTY6LC/8QAGgEAAwEBAQEAAAAAAAAAAAAAAQIDBAAFBv/EAC0RAAICAQMDAwMDBQEAAAAAAAABAhEDEiExBEFREzJhFCLwcYGRNERSodEj/9oADAMBAAIRAxEAPwCkLU0ikmuZqzFRrrVfqLM1ZAVE60QGf1Oj7UGUYVob9qh/6PNFSOZVKCakGmo19HGRUtnhl1/lUn91HUgUBLbApxGK0Ol8LXW+YgfmrjS+FbS/N5j6/wAqlLLFFI4pMw1gMTABJ9M1bWOCX32SPetxY0VpPlUVOboFQl1HgtHp/Jj7fgwt/eP9Bij9L4P0ybqCfXNXpvE7Cu+E56Vnl1MvJeOCK7A9nh9lPlQfYVPIGwFT29CTuambhgjeo+snyV0JcADXKha5R/8AVR71De4W4p1OLFaZW3btLp9CbmTtSmz5oNX2jCxFLknWyGiu5Q6jRhBVfc1UVpeI6edqy/EtC+4FShkvZlNIPc1hqK5rCRFDPpbnaol01ydoqyQpMFmobqUULPKMmobtVUhKAbhipdPfqC8pmhw8GtEJWTnGjQ2L1WWnvTWe092rDT36oRZeLXNQ9i7RE11CkcV1Ka6uo4qPh1wt16GnDrJEhRUg4dbH6RXayeg89Fo9j9jSnTsdlP2Nehf0S32FR3lRRsKDyUMsZhbXCrjfoj3oqz4b6s0e2Kv7uqFDNqu1QlnZePTruR6fhVpOkmjFCjYCoPMaeLbVB5b5ZVY0uESG5TGenjTtUb2zRUkzmmge7eipNJaL77UPdXNWekdeWKlmlWw8FYbZsqBim3bwFIpxg0HrtMzbGscrKpInfiChaDbi3Sgn4ZcO7VPZ4OBkkmu0vyNUUXGk1Mip3u4oJTyiBUN6/R1UhNNjOIhYnrQWm1/rTdddJUxk9qz2n0eqLStpyD3Efvp4wlNDLSuTZ2tUp3pWKGqzT6O+F81sikdbg3RvsaR45J8AtdgjUW06VTahN+/pRN3U9KuvC3DA03HHtNasO7onkdKzOLwW4wBiKLTgK45ga36aNKe2jXtW9QijI8jZhU4fZGOWoNTwey26Vsb+mtqdsUFfRI8tUXwI2zMW+B2o2qC/wRRkTV7e7UP5gZ3FMCyhRCvrU9u7VrdRW2xVLrAEOKNHWEfErqAXUikrqON/wbhV2zbCPd+JGx5eU/vNHtaPajRSAVk1Ms0itfFUWtuOzwNq1zoCM1WavQDdRSZHJrYeCSe5Rpoe5qYadRU8QcigeIakjYVgm33NSDZUDfNA3NVFAHUO2ADU+n0jkZEUiTY1JcljpdQTiaLZVih9PpQBvRITrVOBGVXErMLIqu0upNXHEL4iKyguwxA70X96DGkaaxeMb0QlyetZ+1qiKLsaqoOLGLu0QDmpH1agERVQLxO1WHDuHl3E5AyaaEJS2Qkmo7sI0uie7tgd6s7XA0GWE+9WdpAogYpnxGJjpXow6aEFurZilmlLgF0vD7fMQEAo42ANgK4qelQMr960JKK4JtuQ7+jCd/pRD6VY2FMRBuTmns3WaZIVspuJ8DtXMEQe4rtPpfhqEnA61aO8mKG1K+Ru8VNxV2h9T7gb3+Q4zRv9JWAWO9ZJOMKCyNv60I/GiHVW2JiaVTodws2mt04Kkis3eYg+29XB14KfSs1xTiKW5k/Q1dSRNIla5OR+a576nG1ZXX+KBj4dVt7xETBjNPuDY19+FbB+lAasTOKyd3xExYEjIo5PEqkeYQa6mdaEuJkwaSoLmsQkkMM11NbOs9wU0+q+zqaItXprLRawgGlZZpoNLzV2kFlbqLcYP0NDBFO4q21FoMsGqEuVJU7isvVJKKZfE72DLVlZwBS6m2uymglvMSAu5q20WiKmWy34FZsac1VDyekFt6G4RKijbPA3I87R7Va2uhpLl45zWyHT44q2Z5ZZvgrU8P2OoJPqTRWl4RYU4tqPpRmhyJNT30HStEIRStIlKcm6bAL2htbBAfpQj8CsndACe1GuN96lWCsztSUnyg20Zq94fKSyEkdjRvDiBjbGauJx3Bqh8QsEAcY6GgoKG6DqctmXNi4rbEGpiR03rI8P1YUjlO4q3TW8phmEmqRyJiyx0WPK0b1BduBZLEwKj/rCB5jjvWf4r4isqSObm9BRfwBfJZPrQ4lWggxB7Vb2GSIma8v1fiZeWEQgzMmpNJ45ZcMg5fTehFNOxpU1sei3bIDc4JEbjpQOr14jtuKzJ8eWSIJI9xUVriaalgFYCOk5NCbo6KAdZqELnGZ3qDVMqgMSN6m8WaVbTo6/qGfcVk7upZnzgdqSMNQ7nSLfiPii6PKmB7Zqi1mpLjmdpJ70mqfGKrifWtcYpEW7EJp7KkYJn8U0CmXAOm1UFZG3vSc1I0ztTCKZCj/jClqHmFdRBZ7stw0VYu1XWnqdWrAbGi4sX6ID1S27tF279OmLQdz1VcXsfr+h/hRpugUHxC+pRhI2qeWClGhoWmS+HklS53mBVzpIkk5qp4BfAsrkEEYIyKsxdG9JjhUVQJt6nYSbpGQuKVjzDAH1oY8RUggREVQvxufLzZ6CjOaidHG5F0bzKAZAzFPvanGWjrWR/rC4A5ubKZE4JFCnxLPaB3NS1SZTQjZ2uIAnBmcZpbt5Y5ACMTWC0/H1VyZJEzApLnjPzkC25X1IBqkVJoVqNm8/rNQkTmszx7VMyGcdhQQ8W6doBRkPc5H4pOMN8S3KEMIkctCWrhnKuUC6fUkRBojUcTRY+ICzD5RMfes8dYVjuNqE1GqDNLNk00Mbb3OlIstbxN3nJAPQHFVbXTO1TW7nRl9j0pSQDtWhbEQS48fpqK7YJHyxU92/OIFCf05lPKwlZ2P8KJwLdtkdKit3ipBUkEbEUXrNQCcHFVrCmW4TccP4mNVZcX/nQeUjrjqKy2tvcrEEEA7N6UHpdS9tgymCPsR2NX2tKsMgcrqGHpNZ5XjnfZloxU413Kl7YiVeR60NE4ijX4UY/sm+hqK0Wt4uWzEySM47VeGWMuGQlilHlC2tGxEhZz/wVHqbLbRmOg+5PrVn8ZGBa05mMj16AA/vpNHauCfl5j1J2prJlLb07MSANqg1FnlwTmtIFFhX5SGYgebAjfYfas3fJZtySTiNyT/vTRkc0DRXVqdP4C1DKGLoCcwWXHpvXUfUj5G+nyeDeBqmS/HWhefFRowmsheyzW/U9m9VeCKiOuCMATvXI7ku3vVUMvnJnB3pvFuIpbQuT0/8VlbfF7l3yjBY5jtRDEL4Vxt7F1gvmTmModiJwR2Mda9B0JS8guW3MHvuD1U+orEcE8Mi4z3WcfDVo5VPmJgTPYVYrrRpb7BAfhEiV9CMET+oZ/IrO04b9jXl05do+6jQ3tJbTJb3iqHX8Wsoea2vMwOJ29zVR4k4u3xI2QiVIOGU7MP+Y2qptvzdcZp44r3Zjcq2DOIay5eabhn0G32pF045csB2HWhw/Sf50zrAaZ77VdJE7FI5TINKl0tAIDZ9AahTcjf2qf8Aq4xMw3QTke9E4TXaZh5ghC+8ipvD3Gzp38yhrZ+Zesdx61XX9ZcQ8jE+00DyljNBrbcaKs2/j0WBas3bQMXCTjECOo6ZrJaO+jBhvAkcx39q0I006VC4MrJznBPX6VW/+nbVzzW2Kn02rPHqIraRV9O6tAdniAEKdhVlY1aHO9A/1NfB2RwPWCafq/i2LYNyyAndCCQf8xirLLjlsmReOS7BTcQskwVzQXE0wYGDvAG1Drc05j9Lb5kUbqdUrL8O38uM8sknrJG1O9hUmzPcpJgDNT3tMVWXlSYIBG47zWm4T4eZeZ2EyIUA99zihdfw27dChifLuXnP32xGKn9Tjbq+CvozMwxx3irST8GyW9QPuYq94F4a5m2nuYn0wOtbG34S09teV0+KZLANgSTiB2jp60s8iyccLuVhicO559pNBeIDqjFTsQCR2o/+g3IBNts7GDB9PwftW51+jX4akIy21wygQAI8zQpEx2z7Gq9+K2lJRkLBByW2ZTzcyibXp3AMenrSvBq3L9jzzW+HtSXJsWnOJYAQB0nNBNwvUgkPzIR3B/E71601+86h7YYAKx/tAA1w55JAPl3GQenrQll/hkW7iXCGkqSxugty/wB23MfmgE7RjerKUlGiL6eDdnl78MfqWNaDwpwHlDXigwIRnmAc8zDoYE74rWanglq7zxbNogwp/wAWNyg2FV+k0Ytg2r1lW5ZCNgqwuGGwcRESTke0Ujc2qbOjj0ytItj8WTyWgyyYMrnNdQl7Uqp5ZYxAxadhAAiGgziuo6Pku8q8fn8kHPNMNwCqEcbRRJcfvqs1fiMtIUVRQbPP1JG6W+oQsSIGZrEcc46rmFGO+32qmu8Rdhysx5ZmJx9qB1T9qpHF5JvL4DxrnaAzEgbAmasbfEhbXyfMcT1rLrqDR2mM5rpwrcfHPUek/soU3L11S2TbLR0JDj+dFeNgEvhNm5AT6iSAazHg7jB0t9LwEgSGHdTuP3H6VceIeNf0u8z8sKY+GDHMsADcd4yPbtWLLJaWjfihL1VJcURW0R05Lu2SrRJRj1A6g4kdYncVWPaNpipGewIMjoy9wREGj7dMuWUujkuYieR+qycg90P4OR1BnhzaXplwP1HT67lHkFtKx2GfUGnfDfqnsREDPWhkS7aLIVcESAfXdT2I29INO0uuuhvNaLTuMgH7Vuv5PO0vwEjTsAfISxHlKnY9ZFRaTUMpK3uYTsT/ABkUcA5IPwMdAOYR6zNTNw57iRduYBJVT5o9O4qUssFyx44pPsVfElV1RUUlhMnoROPtTtPo7thUvso5Q0cpOesmK3Wn4c68oVEUkZAILGE54EbkgNGemYqx0vDEY8ly2GXmJBuqCp8gIAQggnJ+q1JTnkpKOxVKMVyZji2qRtOXBBUr09qzPDr7KMH6fyr07QcPdSyRaVCGMi0vyxHL5QFQ5UhSf3V2n4c3zvp9OE+HzqbVpQzQFZZUhSCRiM5xHUrHpPtav5KSy6WYfT63/ejm16kcrQR960Gh4XzXXcWEIfn8t1EHIwJMIB03mT+mpdT4asMxtqALgAWV5lBblXBSIAJDZxuKi+jveLG9ZXTRibF7T8x5baYMSFAPpVgupXcCPpTtVwU22JKAdDB6gkfeoFQDtWfInxuMpRJ24hFWfC+HG557qGDBUTECck5E77elU+g5bl0KTIA5oA3zgfetmdXyCCSCBzcoAkgAiCD/AMxV+nwL3SC5bEtrTBG5eYDry+XtiB0+lIwRZflkorQFHm2nlT1MD60ImntXyLiIvxOb4hJgXFBHw2Azg9AcgEVMl7mh3tEOpCAKVLZ5QXwcDExvivUjH7fz8oi+XZDrpywBZSVAUbqJksZaGzBOJxE1T3uMAqBcuiCpUcuRzJIlCdtoj1AqZwyygthXm41vkgxMlnBPUySR6+tZTWJfa6023IXAhG2J+faYJG9SyOo2uR4tpmh0PiHIQ235SAokczT0EnM7DtRem1gk8lvlBkkH52ciRzDZcQCZ7RVNwLQ3yy3Htf2Yz/aHlDdPL1wevSKPbQswcteuJbKsWCsCpwsFeWBAg9JMVKDk1c9vzwM33os2thk/9t4VywMgPG+QASsKM7+1Ra/hpKubUK7TB6c5UCWHUEKBPbanaO2rW4HNyYCc0A8oAg+U5Bic1Mi3GAPKS4GRaDQTOFA35dsGrb9gUuf9mFHHr/6tOxIwSOaMYxA9K6tkfD2vORZwf8ToD9QTSUPTl4YPXj/kjwS3ePWiE1FCssMRXRFegeITtcM113zCowaWgEhAojTXeU+lMialtL32oS3QYtxdo0OgyKtU6Vn9BqAkAk8p/B/lV5bfIrys8Gme502RSVotFNJpxkn7U1TT0xWVo0p7Fvw7iAEW7h8v6WP6T/hb/Kfwfc0TfuBWIjPbtWfunFXHCbq3Qtq6QDgW3Pb/AAN6dj9PacouT2ElBJahUutcPKoycA/pHua0PD+DBHUsisDbIYsQektdAIIGHUQegz1o+9YIVSRIQGFPMZAUiIB3Jj6TUqalASjkypQhfKpUco8qhTlN969Hp+nhB1LdsxZJSkqQ1rDrcS3bACwztcgs2YIAYyAZPMN8DaKP1all5ApJOCQ3IVDCC3MMz7RUdwc5LWzysAVEgxmOn6gMHcT3qn8R8eNsMtpOd1g5MKcd8526Vo9SMbchdOqkR8WcXLhVg5RVEAM68rArDdDBVm679Ki1evCEIzpBP9orOSx5Tyq6vPmmOYjYnNZjiniBmUEEc5zMk8s5IHrmKj4VfF50WEZitwNzYC48pk9SRtncdah62q0aHhhFJm5tXea2QrXDCgBhiSeaTaZpXm9PUdKDS3qTebnH9mSvwykM4hmYCcfDnygkT8vXEGcK0hRPh8gUKs75JOWOw9M/yFEIHVQEHORyjMjqBJPeJPrFWxy1Mi4pboebQuqUNuVdyrgknkHm8ynoeaMe0bV5V4mnTX2stJIypz5kaeVvwfqDXr1pngh4mAJUROIJGTWV4jobWpvhPkCLyIRmeXpn6/ak6mMLTnyBNpNowfAdRzlzlSrJB2PWtzwfWO7mXB5FzzSbjMTHMSekVWcU8Oppk+IjMxZgpkADqR+R+aG0utdfMkTswPUTtWOU/TyfBXFLUrZvNLcBwR9t6F1NrlF242RCgKhaWUAABl25pZsgbGe9Z7h3ELpuhWPzGVCyY9PX3itZZ4VqnkpKcxU+cCFA+blHcitWN+p7Q5HGH3WBWbFskKrKWSJB5GZQdkLAdNu/lE086xzhP0sFkwQR+uD+PQ9K0em8OoCS0Sfm5RE9pPWjtNwqyhkIJzk5IneJ226VZdNJ78GaXWY48bmGvcL1d+SsQQQUiFJJ3L9MbQKuF8K3GUq13lUgAqJaBseWIhiOsnpWtpDWlYI9zJLrZtVHYrNNwSygAgtAjzZ/81YIgUQoAHYCKdSGqqKXCM0skpcsSupK6mEPjq/0NLFJEipLIJX2qRUZFJzdqLsIgMuCwnZTyn70t0J+kEehMkDpnrQs4gRTRC2uXeD1mZrsD1xP+1OXfY+w/wB6DCh73wcBfLRvDtXykKxwT5fTsJ7VXjB295/jVh8FHwucZxEd4FRnBSVMthySg7RorNypwaotFrArfDkkfpZhE+h9atkuV5uTG4umexiyKatEl5+lEocCq4tRyGoUaJbI1nhzjpcfAdiHH90wiWIyEJOJxE9jWkWyOYFSAxINwEiYCkAKI2kAfWvKifNjvW88PcfFxeS5/egQpj54/wD1WvDmbWlszZsdbovdTaDvyM2VAYDJkCNxHQ9+1VXEdEmqIgcqjy8wHIz5Oc7gY33jcURYvnmzdaTGABIgyY79vpUml17srhvKSTynGM7kEem1aftk62ruZ2tJQN4P03KGHO2CSSwI2MQF5cE9zU+g0L2lNlFUM3KA/K3KAWL+aPmjME7YE1pNJc8g+KUZjI8qwAOg9aEOqK3DzOApUBRAG08xn6jFK4wRycpKmBaXh5t3F8rN8/MQGVBOQUBOFz+KmDIbtyUPK4KuQx5SwCxKiIMEid/LTk4kXbltBnJxCif/AAKLu+FLt4RccICBIEsdwdsAGRvmnxy1e3cSThD3bAnFOKLbxzeYg8vWCBiY9Yqj4WGa9bKqzHmElQT1yTW10/hXSW25nU3GJ3uGc+wwKv7VpVEKoUdgIFLk6KeWScnSRKXWRiqirsyvEPDt2/bKeVQ0ZbMQZmB1pOG/s/09uDcZ7h/0r9hn81qX1CAhSygnYEiftQl7iyA4BMXBbbpBIwfbarvFgj7jNGeZ7RJ9JoLVoRbtqv8A0gD81NcuBQSxAA3JrO/167OqEcvmZGjbsCD0INDaRbjl1uKzcymyGGeUrkFv50n1kOMasp9JLmbLzVcYtJImSrBWA3E9c9M0To7xdZZSplhB7AwD7EZrK63R2zL37ioRZHMOYFw6ws8o3BxWj4TqhctoV5iOUCWUqSRgypyNqrhlmlJuapdgZ8EIY04/v+cBppKU02tRiOpppTTTXHHV1dNJXHHx2hqXSmGI71AlSMYIPapMqGMM5+++afZAJ80+m2B7U0qTkSQYjt+KUEgZBAn0z0pAi27ZJgkKJicx9acJUyrY7yB/vTVlcwPr0p880GeuYGM9u9DcbYYLTNLHImCZ6mp9Hyny/u396ZbmfScdjRZMrlgIOF8x33IxA/FczkMv6ViBP/bnI6j1+tG8M1pJ5HPmGx/xD+dBuxOCZ+p6bTUd84wPMMzOR7VGePWqZfFleN2i+VqPDVQ6DWc+GgONwOvqKt0evOnjcXTPXWRTSaHKfNRAcjIMEZB9RQlps/ep6mUlyabh/E/jCZ5bqjpiQP1D1ogl2BPMaxtm6VYFTBBwa2XCLy3xHMFcfMO/qKjkU3vEnNKPBHauuFhm2q74R4dvaghnJVO7bn/pH8aufDPArLD4ploJAnbB3jrWsAr0Ol6CU0p5Xt4PN6jrNLcYcgnDuGWrC8ttY7nqfc0TSk0lexGKiqR5jbbtlVrr/KxLZiOURJM7ADqaqvE76lbIvLcKFTzlBGVGSpjc1Z8c1RslLwUMMqw99iP+daW2TfgrytaZTzE7z/hA6RXRyx1ae6N2J6NOSlX5sVWssm4xuJa5xetKVbojd56dKTjaJZ5TcvEB+TmVVLMxSPMse1B8V0nJZto/Obdm9yuqlpNp/lkLkwf3U7hnDdTC6i1ujXFtJfmfhNESdwZ/FT+ixW5Pv+xsVKKlq23X/N967dh3EvEGmt8xRQ3xE+KjNlDcGOUjdTIH3oXX8fco83AsixesgeXmBIFy3j5jIarnQeGVARrxBcNdZwAOQ/EmVg9BRNi1pEAtoqubatcRfmIEyeUn1rTGOOPtQnrYI+1OTXf8/QqrXh9FvXUWyDbu2AylphXBypfcTg/SrHwtw+9ZRxeOC0qvOX5BGRzNkyaKbU33j4dsKMSX7Ebgd5iltcPcsrXbhYgQVGEJIAJIP1+9BydGbJnnKDjNrevl7FhSGupDSmAQ0hrqaTXHHUlAXeNaZSVbUWQRuDcQEe4muo0zj5JAqTcRShJFcqmo2UDNC8rncY7RUo7Rj1oTRYuAESCRgzn0x/CrO+eZixVVGMKIXHQD+NK+RkQKkn1/51p3wTMHB/MdI7VILUkx32g4EdzSrajPvtQsItvsP9/qe1PaTIGJxgU2MjpUjrsfMffA+lKGxqgjoO20/XNM5YJ/eaeonuYOegzSOWG0ff8AcaASO4pmRhh1q10Gs5xnBG4/iPSq/wCIvLkGe4FRMYh0PmG4iAe49alkx618mjDmeN/BoLD5ooNVToNUHEj6jqDVgrV58otOmerqUt0OHzVpfBX98f8AprN9ZrSeCx/aOf8ALQgt6DJ7HrfArfLZX1k/czR5NRaNIRR2Ufup7GvoIqkkfNTdybOmurqSiKDcS0gu22SYnY9iNqA4bwwacEm8c7jAX3E9auKrV4PbkM5ZyAR5jiCZiBg1P0oOfqPkvjytQcG9v0Ota60P7sFpaGIBORIz9ac7X3ClQLchuYNlgf0xHSjkQDYAe1OqtiuavZfyAWOHQ3O1x2MneIAIgrHairWnVRCqBUlR3ryqJZgAOpIA+5rtxZTk+WPpDWQ41+0rh2nkfG+Kwxy2QXz2LDyj6msZxH9qmsvSuj0yoP8AFc87e/KIA+5oOlyJuewO4GSYHrWX434/4dppD6hWYfotf2jfULt9a8X4lrddqSRqtRcfPyzyr7cqwI96Bt8LQHaY/wCRQ1oOlm94x+2G4wI0elA/z3jP15F/iaxvFePcR1X9/qXCn9Fs8iD6Ln7mkdAowABtHWkIMSwMdKXWxtNFT/U3pPrFdVsL8Y5j+a6l1MJldHkURyUJw9qsgtK+QoA1KxBHQ1a2iWAjbHQUHdSZHen8Ku+UqdxQe6Cg9rUHAgx3n69qXOxONxBGCabaDE7R/L608iIBEewpRhLjqThcjvTQO5PpU3OOU4zO/p2ipNKqsYadsER+6hwjuWB8oiDM0jJ3B9posxOBt+aZdIMk59e1EAK7GYA+4/FRqMxR91Cyg4jYd6hwoMbnB7QelAZAasbZ5lz3H+Ifzq90mpDgEHf/AJFVlpFfy4B6kmB7UNauNaeRt1H8RUcuPXxyacGbRs+DTo1a7wGku3qVH/2FYixfDCQa9A/Zlbm5/wB0/YGsuKP/AKJfJvyTqDfwesbCmrSOa4V7jPnh00ldQut4hasrzXbiIB1ZgP30DgqurBcY/arorciyHvt/8akL/raAfpWO4r+0biN/FkW9OvcDmf7tj8V2y5Opns+q1lu2Oa46oo3LEAfmsfxb9qOgteW27X27WlJX/WfL+a8h1lq5dbm1F57x/wAzEj7bCnJpQn6Y7UNa7B0mq4t+1DX3fLp7SWVP6m87/nAP3rK6v+kakhtTqLl0dQzQvtyiB+KeL0AiB703T6oqwJAIBwD1pHNjqBEnDVXIED2NOW8yzysR3ip9TxFWyAVPuY+1BMDvNIpeRtIQGMyTO0mlcgElSdqgCz1z+KiiJk+9cAlW0WwPrNM1PMBBk/XAqNtUoOFJMZ7Uw68geUZODORRs6iOD60lM5z2NLXagUUGh3q4WurqWXIy4I7lD6H++NdXVy4OLU9PcVNqWJcyelLXUncZCk+X6VGhytLXUQCE71y7fWurq7sBCTUd75R711dQCgZetdd/hXV1EYl4Kx83vXr/AOyn5v8AV/CkrqzR/qEb/wC2f6HplOrq6vUZ45S+KrzLZYqxUwcgkH8V86XtS9285uuzwxjnYtHtNdXV0vaGJbcNQTsOtXFpB8BjA69K6urLkL4+5V29x70twZPuK6uovkC4I7nWgzua6urkEg1HX6VLa2rq6gzhrmh9V8q+9dXURCJ96Wx8v1rq6uOIXYzvXV1dTHH/2Q==">
                        </div>
                    </div>
                </div>
            </a>--%>
        </div>
    </div>
    <div id="divLoader" class="divLoader" style="display: none;"></div>
    <script src="js/jquery-3.3.1.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            GetOrderDetails();
        });

        function GetOrderDetails() {
            $('#divLoader').show();
            $.ajax({
                url: 'OrderDetails.aspx/GetOrderDetails',
                type: 'POST',
                contentType: 'application/json; charset=utf-8',
                data: "",
                dataType: 'json',
                success: function (data) {
                    try {
                        $("#divOrderDetailsLeft").empty();
                        $("#divOrderDetailsRight").empty();
                        $("#divItemDetails").empty();
                        var divLeftHtml = '', divRightHtml = "", divItemsHtml = '', formattedDate = '', address = '', ordersCount = 0, status = '', imgPath;
                        var orderStatus = [];
                        var ordersData = data.d;
                        if (ordersData != "" && ordersData != null) {
                            formattedDate = new Date(parseInt(ordersData.CreatedAt.substr(6)));
                            orderStatus = getOrderStatus(ordersData.Status);
                            //if (ordersData.CartDeliveries != null && ordersData.CartDeliveries.length > 0) {
                            //    address = ordersData.CartDeliveries[ordersData.CartDeliveries.length - 1].Infos.Address1;
                            //}
                            if (ordersData.CartOrderItems != null && ordersData.CartOrderItems.length > 0) {
                                ordersCount = ordersData.CartOrderItems.length;
                            }
                            if (orderStatus != null) {
                                status = orderStatus[0];
                                imgPath = orderStatus[1];

                            }
                            divLeftHtml += '<p class="restaurant-detail-card-certified"></p>' +
                                '<p class="restaurant-detail-card-name">Order Details</p>' +
                                '<p class="restaurant-detail-card-address">' + address + '</p> ' +
                                '<div class="restaurant-detail-card-detail-container">' +
                                '<img class="restaurant-detail-card-detail-image" src="images/calendar.png">' +
                                '<p class="restaurant-detail-card-detail-text">Date - <b>' + formattedDate.toDateString() + '</b></p></div>' +
                                '<div class="restaurant-detail-card-detail-container">' +
                                '<img class="restaurant-detail-card-detail-image" src="images/money-bag.png">' +
                                '<p class="restaurant-detail-card-detail-text">Price - <b>RM ' + ordersData.Value + '</b></p></div>' +
                                '<div class="restaurant-detail-card-detail-container">' +
                                '<img class="restaurant-detail-card-detail-image" src="images/bag.png">' +
                                '<p class="restaurant-detail-card-detail-text">Orders - <b>' + ordersCount + ' package</b></p></div>' +
                                '<div class="restaurant-detail-card-detail-container">' +
                                '<img class="restaurant-detail-card-detail-image" src="' + imgPath + '">' +
                                '<p class="restaurant-detail-card-detail-text">' + status + '</p></div>';
                            if (ordersData.CartOrderItems != null && ordersData.CartOrderItems.length > 0) {
                                for (var j = 0; j < ordersData.CartOrderItems.length; j++) {
                                    divItemsHtml += '<a href="#">' +
                                        '<div class="row">' +
                                        '<div class="item-list-card">' +
                                        '<div class="item-list-card-left">' +
                                        '<p class="item-list-card-certified">Total Price - <b>RM ' + ordersData.CartOrderItems[j].Value + '</b></p>' +
                                        '<p class="item-list-card-name">' + ordersData.CartOrderItems[j].Name + '</p>' +
                                        '<p class="item-list-card-address">' + address + '</p>' +
                                        '<p class="item-list-card-distance"><b>' + ordersData.CartOrderItems[j].Qty + ' Items</b> | Bought</p></div>' +
                                        '<div class="item-list-card-right">' +
                                        '<img class="item-list-card-image" src="' + ordersData.CartOrderItems[j].Img + '"></div></div></div></a>';
                                    divRightHtml += '<img class="restaurant-detail-card-image" src="' + ordersData.CartOrderItems[j].Img + '">';
                                }
                            }

                            $("#divOrderDetailsLeft").append(divLeftHtml);
                            $("#divOrderDetailsRight").append(divRightHtml);
                            $("#divItemDetails").append(divItemsHtml);
                        }
                        $('#divLoader').hide();
                    } catch (e) {
                        $('#divLoader').hide();
                        swal("Oops!", "Something went wrong.", "error");
                        return false;
                    }
                    return false;
                },
                error: function (errmsg) {
                    $('#divLoader').hide();
                    swal("Oops!", "Something went wrong.", "error");
                }
            });
        }

        function getOrderStatus(status) {
            try {
                var retValue = [];
                if (status == 'open') {
                    retValue = ['Waiting Payment', 'images/money-icon.png'];
                    return retValue;
                }
                else if (status == 'progess') {
                    retValue = ['Processing Order', 'images/loading-circles.png'];
                    return retValue;
                }
                else if (status == 'complete') {
                    retValue = ['Completed Order', 'images/check.png'];
                    return retValue;
                }
                else if (status == 'cancelled') {
                    retValue = ['Cancelled Order', 'images/close.png'];
                    return retValue;
                }
                else if (status == 'in delivery') {
                    retValue = ['Processing Order', 'images/loading-circles.png'];
                    return retValue;
                }
                else {
                    retValue = ['Processing Order', 'images/loading-circles.png'];
                    return retValue;
                }
            } catch (e) {
                swal("Oops!", "Something went wrong.", "error");
            }
        }
    </script>
</asp:Content>
