﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SuciHalalSite.Master" AutoEventWireup="true" CodeBehind="Travel.aspx.cs" Inherits="SuciHalalWebclient.Travel" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="tools_list" style="background-color: #eee;">
        <section>
            <div id="slick-slider" style="margin-bottom: 0px; margin-top: 0px;">
                <div class="home-banner" style="background-image: url('https://d2t62toc9frg03.cloudfront.net/assets/img/web/offers/travel_banner_1.jpg');">
                    <div class="hero-text-container">
                        <div class="home-banner-overlay-container">
                            <div class="above-overlay home-banner-overlay">
                                <h1 class="shop-text light overlay-title">Travel</h1>
                                <p class="shop-text light overlay-content">Book the best Muslim-friendly package with My Halal Stop</p>
                                <a class="btn go w-commerce-commercecartcheckoutbutton checkout-button white-button overlay-button" href="http://www.myhalalstop.com/travel.aspx">BOOK NOW
                                </a>
                            </div>
                        </div>
                        <div class="instant-overlay"></div>
                    </div>
                </div>
                <div class="home-banner" style="background-image: url('https://d2t62toc9frg03.cloudfront.net/assets/img/web/offers/travel_banner_2.jpg');">
                    <div class="hero-text-container">
                        <div class="home-banner-overlay-container">
                            <div class="above-overlay home-banner-overlay">
                                <h1 class="shop-text light overlay-title">Travel</h1>
                                <p class="shop-text light overlay-content">Book the best Muslim-friendly package with My Halal Stop</p>
                                <a class="btn go w-commerce-commercecartcheckoutbutton checkout-button white-button overlay-button" href="http://www.myhalalstop.com/travel.aspx">BOOK NOW
                                </a>
                            </div>
                        </div>
                        <div class="instant-overlay"></div>
                    </div>
                </div>
                <div class="home-banner" style="background-image: url('https://d2t62toc9frg03.cloudfront.net/assets/img/web/offers/travel_banner_3.jpg');">
                    <div class="hero-text-container">
                        <div class="home-banner-overlay-container">
                            <div class="above-overlay home-banner-overlay">
                                <h1 class="shop-text light overlay-title">Travel</h1>
                                <p class="shop-text light overlay-content">Book the best Muslim-friendly package with My Halal Stop</p>
                                <a class="btn go w-commerce-commercecartcheckoutbutton checkout-button white-button overlay-button" href="http://www.myhalalstop.com/travel.aspx">BOOK NOW
                                </a>
                            </div>
                        </div>
                        <div class="instant-overlay"></div>
                    </div>
                </div>
            </div>
        </section>
        <div class="section" style="padding-left: 10vw; padding-right: 10vw;">
            <h3 style="margin-top: 0px; margin-bottom: 10px; text-align: center;">Halal Travel Packages and Tours</h3>
            <h5 style="margin-top: 0px; margin-bottom: 30px; text-align: center;">Book the best Muslim-friendly holiday packages to most popular travel destinations with My Halal Stop</h5>

            <%--<div class="filter-container-travel">
                <div class="ant-tag ant-tag-has-color" style="background-color: rgb(16, 142, 233);">All Region</div>
                <div class="ant-tag ant-tag-blue">Europe</div>
                <div class="ant-tag ant-tag-blue">Asia</div>
                <div class="ant-tag ant-tag-blue">Middle East</div>
                <div class="ant-tag ant-tag-blue">Africa</div>
            </div>--%>

            <div class="donation-list-content-container-new" runat="server" id="divTravelListContainer">
                
            </div>

            <div class="w-pagination-wrapper pagination"></div>
            <div id="divPagination" style="display: none;" class="center">
                <ul id="ulPagingItems" class="pagination"></ul>
            </div>


        </div>
    </div>
    <div id="divLoader" class="divLoader" style="display: none;"></div>
    <script src="js/jquery-3.3.1.min.js"></script>
    
    <script type="text/javascript">
        var lintPageSize = 15;
        var lintTotalPages = 0;

        $(document).ready(function () {

            $("#ContentPlaceHolder1_divTravelListContainer").text("");
            var latitude = "4.582010";
            var longitude = "101.470624";

            getLocation();
            BindTravelPackages(); 
           
        });

        function BindTravelPackages() {
            
            $('#divLoader').show();
            $.ajax({
                type: "POST",
                url: "Travel.aspx/BindTravelPackages",
                data: "{pintSkipRecords:0,pintTakeRecords:" + lintPageSize + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: true,
                cache: false,
                success: function (msg) {
                    $('#divLoader').hide();
                    if (msg.d != null) {
                        lintTotalPages = msg.d.TotalPages;
                        if (lintTotalPages > 0) {
                            $("#ContentPlaceHolder1_divTravelListContainer").html("");
                            printTravelListData(msg);
                            $('#divPagination').show();
                            $('#ulPagingItems').twbsPagination({
                                totalPages: lintTotalPages,
                                visiblePages: 10,
                                next: 'Next',
                                prev: 'Prev',
                                onPageClick: function (event, page) {
                                    pageData(page);
                                }
                            });
                        }
                        else {
                            $('#divPagination').hide();
                            $("#ContentPlaceHolder1_divTravelListContainer").text("No records were found.");
                        }
                    }
                    else {
                        $('#divPagination').hide();
                        alert("Oops something went wrong.")
                    }
                },
                error: function () {
                    $('#divLoader').hide();
                    $('#divPagination').hide();
                    $("#ContentPlaceHolder1_divTravelListContainer").text("No records were found.");
                }
            });
        }

        function printTravelListData(msg) {
            $('#divLoader').show();
            var travels = msg.d.PDetails;
            var html = "";
            $("#ContentPlaceHolder1_divTravelListContainer").text(html);
            var count = 0;

            for (var i = 0; i < travels.length; i++) {

                var price = parseFloat(travels[i].Price);
                price = price.toFixed(2);

                html += '<a href="TravelDetail.aspx?id=' + travels[i].Id + '">';
                html += '<div class="donation-list-card-new">';
                html += '<img class="donation-list-card-image-new" src="' + travels[i].ImageUrl + '" />';
                html += '<div class="donation-list-card-content-new">';
                html += '<p class="travel-list-card-name-new">' + travels[i].Name + '</p>';
                html += '<p class="travel-list-card-price-header-new">' + "Starting from" + '</p>';
                html += '<p class="travel-list-card-price-new"> ' + (travels[i].Currency).toUpperCase() + ' ' + travels[i].Price + '</p>';
                html += '</div>';
                html += '<div class="donation-list-card-footer-new">';
                html += '<div class="donation-list-card-book-container-new">';
                html += '<p style="margin: 0px; width: 160px;" class="donation-list-card-book-now-new">Book Now</p>';
                html += '</div>';
                html += '</div>';
                html += '</div>';
                html += '</a>';  
            }
            $("#ContentPlaceHolder1_divTravelListContainer").append(html);
            $('#divLoader').hide();
        }

        function pageData(e) {
            $('#divLoader').show();
            var lintSkipRecords = e == 1 ? 0 : (e * lintPageSize) - lintPageSize;
            $.ajax({
                type: "POST",
                url: "Travel.aspx/BindTravelPackages",
                data: "{pintSkipRecords:" + lintSkipRecords + ",pintTakeRecords:" + lintPageSize + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: true,
                cache: false,
                success: function (msg) {
                    $('#divLoader').hide();
                    printTravelListData(msg);
                    $('#divPagination').show();
                },
                error: function () {
                    $('#divLoader').hide();
                    $('#divPagination').hide();
                    $("#ContentPlaceHolder1_divTravelListContainer").text("");
                    $("#ContentPlaceHolder1_divTravelListContainer").text("No records were found.");
                }

            });

            return false;
        }

        function getLocation() {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(showPosition);
            } else {
                x.innerHTML = "Geolocation is not supported by this browser.";
            }
        }

        function showPosition(position) {
            latitude = position.coords.latitude;
            longitude = position.coords.longitude;
        }

    </script>
</asp:Content>
