﻿using Framework.EnterpriseLibrary.Adapters;
using Giift.SuciHalal.Entities;
using Giift.SuciHalal.Helper;
using Newtonsoft.Json.Linq;
using Stripe;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SuciHalalWebclient
{
    public partial class PostPayment_OLD : System.Web.UI.Page
    {
        public string PostAmount = string.Empty;
        public string PostPublishableKey = string.Empty;
        public static string lstrAccessToken = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (HttpContext.Current.Session["AccessToken"] == null)
                {
                    Response.Redirect("Login.aspx");
                }
                else
                {
                    lstrAccessToken = Convert.ToString(HttpContext.Current.Session["AccessToken"]);
                }

                PaymentDetails lobjPaymentDetails = null;
                //Payment Post Details from Checkout page
                if (HttpContext.Current.Session["PaymentDetails"] != null)
                {
                    lobjPaymentDetails = HttpContext.Current.Session["PaymentDetails"] as PaymentDetails;
                    txtChargeAmount.Value = Convert.ToString(lobjPaymentDetails.Amount);
                    txtOrderReference1.Value = lobjPaymentDetails.OrderId;

                    this.PostAmount = Convert.ToString(Convert.ToString(lobjPaymentDetails.Amount * 100));
                }
                else
                {
                    //Response.Redirect("PaymentFailed.aspx");
                }

                this.PostPublishableKey = Convert.ToString(WebConfigurationManager.AppSettings["StripePublishableKey"]);
                var secretKey = WebConfigurationManager.AppSettings["StripeSecretKey"];
                StripeConfiguration.SetApiKey(secretKey);

                //CreatePayment Api to Get PaymentAPi

                LoggingAdapter.WriteLog("CreatePayment Request:" + lstrAccessToken + "#" + lobjPaymentDetails.OrderId + "#" + "stripe");
                string lstrResponse = ApiHelper.CreatePayment(lstrAccessToken, lobjPaymentDetails.OrderId, "stripe");
                LoggingAdapter.WriteLog("CreatePayment Response:" + lstrAccessToken + "#" + lstrResponse);

                JToken outer = JToken.Parse(lstrResponse);
                if (outer["error"] != null)
                {
                    JArray array = outer.Value<JArray>("error");
                    Response.Redirect("PaymentFailed.aspx");
                }
                else if (outer["Id"] != null)
                {
                    lobjPaymentDetails.PaymentId = outer.Value<string>("Id");
                    HttpContext.Current.Session["PaymentDetails"] = lobjPaymentDetails;
                }
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("PostPayment Ex: " + ex.Message + Environment.NewLine + ex.StackTrace + ex.InnerException);
                Response.Redirect("PaymentFailed.aspx");
            }
        }
    }
}