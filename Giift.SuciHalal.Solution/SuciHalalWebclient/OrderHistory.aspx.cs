﻿using Framework.EnterpriseLibrary.Adapters;
using Giift.SuciHalal.Helper;
using Microsoft.CSharp.RuntimeBinder;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Giift.SuciHalal.Entities;

namespace SuciHalalWebclient
{
    public partial class OrderHistory : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (HttpContext.Current.Session["AccessToken"] == null)
            {
                Response.Redirect("Login.aspx");
            }
        }

        [System.Web.Script.Services.ScriptMethod()]
        [System.Web.Services.WebMethod]
        public static List<OrderHistoryDetails> GetOrderHistory()
        {
            try
            {
                string lstrResponse = string.Empty;
                string lstrMessage = string.Empty;
                List<OrderHistoryDetails> lobjListOfOrders = null;
                string lstrAccessToken = Convert.ToString(HttpContext.Current.Session["AccessToken"]);
                if (lstrAccessToken != "")
                {
                    lstrResponse = ApiHelper.GetAllOrders(lstrAccessToken);
                }

                if (lstrResponse != "")
                {
                    dynamic lobjResponse = JsonConvert.DeserializeObject<dynamic>(lstrResponse);
                    if (IsPropertyExist(lobjResponse, "error"))
                    {
                        if (IsPropertyExist(lobjResponse, "error_description"))
                        {
                            lstrMessage = lobjResponse.error_description;
                        }
                    }
                    else
                    {
                        lobjListOfOrders = new List<OrderHistoryDetails>();
                        lobjListOfOrders = JsonConvert.DeserializeObject<List<OrderHistoryDetails>>(lstrResponse);
                    }
                }
                else
                {
                    lstrMessage = "NotFound";
                }
                return lobjListOfOrders;
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("GetOrderHistory Ex: " + ex.Message + Environment.NewLine + ex.InnerException + Environment.NewLine + ex.StackTrace);
                return null;
            }
        }
        public static bool IsPropertyExist(dynamic dynamicObj, string property)
        {
            bool lblnStatus = false;
            try
            {
                var value = dynamicObj[property].Value;
                lblnStatus = true;
            }
            catch (RuntimeBinderException)
            {
                lblnStatus = false;
            }
            catch (Exception)
            {
                lblnStatus = false;
            }
            return lblnStatus;
        }
    }
}