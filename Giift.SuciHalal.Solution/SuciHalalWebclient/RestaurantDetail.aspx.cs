﻿using Giift.SuciHalal.Entities;
using Giift.SuciHalal.Helper;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SuciHalalWebclient
{
    public partial class RestaurantDetail : System.Web.UI.Page
    {
        string lstrRestaurantId = string.Empty;
        //string lstrAccessToken = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Request.QueryString["id"] != null)
                {
                    lstrRestaurantId = Convert.ToString(Request.QueryString["id"]);

                    if (HttpContext.Current.Session["RestaurantDetails"] != null)
                    {
                        List<RestaurantDetails> lobjListOfRestaurantDetails = new List<RestaurantDetails>();
                        lobjListOfRestaurantDetails = HttpContext.Current.Session["RestaurantDetails"] as List<RestaurantDetails>;

                        if (lobjListOfRestaurantDetails != null && lobjListOfRestaurantDetails.Count > 0)
                        {
                            RestaurantDetails lobjRestaurantDetails = new RestaurantDetails();
                            lobjRestaurantDetails = lobjListOfRestaurantDetails.Find(lobj => lobj.Id.Equals(lstrRestaurantId));

                            spanName.InnerText = lobjRestaurantDetails.Name;
                            spanRestaurantNameTitle.InnerText = lobjRestaurantDetails.Name;
                            spanAddress.InnerText = lobjRestaurantDetails.Address;
                            if (!string.IsNullOrEmpty(lobjRestaurantDetails.PhoneNo))
                            {
                                divPhone.InnerHtml += "<i class=\"fa fa-phone\" style=\"margin-top: 5px; flex: 0.025;\"></i><p class=\"restaurant-detail-card-detail-text\" style=\"flex: 0.7;\">Phone Number - " + lobjRestaurantDetails.PhoneNo + "</p>";
                                //spanPhoneNo.InnerText = "Phone Number - " + lobjRestaurantDetails.PhoneNo;
                            }
                            if (!string.IsNullOrEmpty(lobjRestaurantDetails.Locality))
                            {
                                spanLocality.InnerText = lobjRestaurantDetails.Locality;
                            }
                            if (!string.IsNullOrEmpty(lobjRestaurantDetails.AveragePrice))
                            {
                                spanAveragePrice.InnerText = lobjRestaurantDetails.AveragePrice;
                            }
                            if (!string.IsNullOrEmpty(lobjRestaurantDetails.Cuisine))
                            {
                                spanCuisine.InnerText = lobjRestaurantDetails.Cuisine;
                            }
                            if (!string.IsNullOrEmpty(lobjRestaurantDetails.Website))
                            {
                                //spanWebsite.InnerText = lobjRestaurantDetails.Website;
                                spanWebsite.InnerHtml += "<a href=\"" + lobjRestaurantDetails.Website + "\" style=\"color:#04629d;\" target=\"_blank\">" + lobjRestaurantDetails.Website + "</a>";
                            }
                            else
                            {
                                spanWebsite.InnerText = "NA";
                            }
                            if (!string.IsNullOrEmpty(lobjRestaurantDetails.Deal))
                            {
                                spanDeal.InnerHtml = lobjRestaurantDetails.Deal;
                            }
                            if (!string.IsNullOrEmpty(lobjRestaurantDetails.DealMore))
                            {
                                spanDealMore.InnerHtml = lobjRestaurantDetails.DealMore;
                            }
                            if (!string.IsNullOrEmpty(lobjRestaurantDetails.Description))
                            {
                                spanDescription.InnerHtml = lobjRestaurantDetails.Description;
                            }

                            if (!string.IsNullOrEmpty(lobjRestaurantDetails.HalalType))
                            {
                                dvHalalType.Visible = true;
                                spanHalaltype.InnerHtml = lobjRestaurantDetails.HalalType;
                            }
                            else
                            {
                                dvHalalType.Visible = false;
                            }

                            //string mapAddressUrl = "https://www.google.com/maps/@{0},{1}";
                            string mapAddressUrl = "http://maps.google.com/maps?z=12&q=loc:{0}+{1}";

                            mapAddress.HRef = string.Format(mapAddressUrl, lobjRestaurantDetails.Latitude, lobjRestaurantDetails.Longitude);

                            imgRestaurantImage.Src = lobjRestaurantDetails.ImageUrl;

                            string lstrContent = ApiHelper.RetrieveContent(Convert.ToString(ConfigurationManager.AppSettings["RestaurantRetailerId"]), "halal_restaurant_faq");
                            JToken jTokenOuter = JToken.Parse(lstrContent);
                            JObject lobjJObject = ((JObject)jTokenOuter["retailer_content"]);
                            RetailerContent retailerContent = JsonConvert.DeserializeObject<RetailerContent>(((JProperty)lobjJObject.First).Value.ToString());
                            if(!string.IsNullOrEmpty(retailerContent.value))
                            {
                                spanFAQ.InnerHtml = retailerContent.value;
                            }
                        }
                    }
                    else
                    {
                        Response.Redirect("Login.aspx?Callbackurl=RestaurantDetail.aspx?id=" + lstrRestaurantId);
                    }
                }
                else
                {
                    Response.Redirect("restaurant.aspx");
                }
            }
            else
            {
                Response.Redirect("Login.aspx?Callbackurl=Restaurant.aspx");
            }
        }
    }
}