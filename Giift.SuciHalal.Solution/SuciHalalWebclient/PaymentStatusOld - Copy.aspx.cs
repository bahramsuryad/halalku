﻿using Framework.EnterpriseLibrary.Adapters;
using Giift.SuciHalal.Entities;
using Giift.SuciHalal.Helper;
using Newtonsoft.Json.Linq;
using Stripe;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SuciHalalWebclient
{
    public partial class PaymentStatus : System.Web.UI.Page
    {
        public static string lstrAccessToken = string.Empty;
        string pstrStripeToken = string.Empty;
        string lstrPayStatus = string.Empty;
        string lstrResponseURL = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                string stripeCurrency = Convert.ToString(WebConfigurationManager.AppSettings["StripeCurrency"]);

                if (HttpContext.Current.Session["AccessToken"] == null)
                {
                    lstrResponseURL = "Login.aspx";
                }
                else
                {
                    lstrAccessToken = Convert.ToString(HttpContext.Current.Session["AccessToken"]);
                }

                if (Request.Form["stripeToken"] != null)
                {
                    var customers = new CustomerService();
                    var charges = new ChargeService();
                    PaymentDetails lobjPaymentDetails = null;

                    try
                    {
                        //Payment Post Details from Checkout page
                        if (HttpContext.Current.Session["PaymentDetails"] != null)
                        {
                            lobjPaymentDetails = HttpContext.Current.Session["PaymentDetails"] as PaymentDetails;
                        }
                        else { lstrResponseURL = "PaymentFailed.aspx"; }

                        //Received the stripeToken
                        pstrStripeToken = Convert.ToString(Request.Form["stripeToken"]);
                        lobjPaymentDetails.PaymentToken = pstrStripeToken;

                        //var customer = customers.Create(new CustomerCreateOptions
                        //{
                        //    Email = Request.Form["stripeEmail"],
                        //    SourceToken = Request.Form["stripeToken"]
                        //});

                        //Payment Pay call 
                        string lstrDeliveryEmail = string.Empty;
                        lstrDeliveryEmail = Convert.ToString(HttpContext.Current.Session["DeliveryEmail"]);

                        LoggingAdapter.WriteLog("PaymentPay Request:" + lstrAccessToken + "#" + lobjPaymentDetails.PaymentId + "#" + pstrStripeToken);
                        string lstrResponse = ApiHelper.PaymentPay(lstrAccessToken, lobjPaymentDetails.PaymentId, pstrStripeToken, lstrDeliveryEmail);
                        LoggingAdapter.WriteLog("PaymentPay Response:" + lstrAccessToken + "#" + lstrResponse);



                        HttpContext.Current.Session["PaymentDetails"] = null;

                        JToken outer = JToken.Parse(lstrResponse);
                        if (outer["error"] != null)
                        {
                            JArray array = outer.Value<JArray>("error");
                            //lstrMessage = array[0].Value<string>("msg");
                        }
                        else if (outer["payment"] != null)
                        {
                            if (outer["payment"]["Status"] != null)
                            {
                                lstrPayStatus = Convert.ToString(outer["payment"]["Status"]);

                                if (lstrPayStatus == "completed")
                                {
                                    lstrResponseURL = "PaymentSuccessful.aspx";
                                }
                                else
                                {
                                    lstrResponseURL = "PaymentFailed.aspx";
                                }
                            }
                            else
                            {
                                lstrResponseURL = "PaymentFailed.aspx";
                            }
                        }

                        //var charge = charges.Create(new ChargeCreateOptions
                        //{
                        //    Amount = 100,
                        //    Description = "My Order Details",
                        //    Currency = stripeCurrency,
                        //    CustomerId = customer.Id,
                        //    ReceiptEmail = customer.Email
                        //});

                        //if (charge.Status == "succeeded")
                        //{
                        //    Console.WriteLine("succeeded");
                        //}
                        //else
                        //{
                        //    Console.WriteLine("failed");
                        //}

                    }
                    catch (Exception ex)
                    {
                        LoggingAdapter.WriteLog("PaymentStatus Inner Ex: " + ex.Message + Environment.NewLine + ex.StackTrace + ex.InnerException);
                        HttpContext.Current.Session["PaymentDetails"] = null;
                        Response.Redirect("PaymentFailed.aspx");
                    }
                    
                    //Console.WriteLine(charge);
                }
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("PaymentStatus Ex: " + ex.Message + Environment.NewLine + ex.StackTrace + ex.InnerException);
                HttpContext.Current.Session["PaymentDetails"] = null;
                lstrResponseURL = "PaymentFailed.aspx";
            }
            finally
            {
                Response.Redirect(lstrResponseURL);
                HttpContext.Current.Session["DeliveryEmail"] = null;
            }
        }
    }
}
