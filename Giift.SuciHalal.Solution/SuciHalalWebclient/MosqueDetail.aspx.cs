﻿using Giift.SuciHalal.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SuciHalalWebclient
{
    public partial class MosqueDetail : System.Web.UI.Page
    {
        string lstrMosqueId = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Request.QueryString["id"] != null)
                {
                    lstrMosqueId = Convert.ToString(Request.QueryString["id"]);
                    if (HttpContext.Current.Session["MosqueDetails"] != null)
                    {
                        List<MosqueDetails> lobjListOfMosqueDetails = new List<MosqueDetails>();
                        lobjListOfMosqueDetails = HttpContext.Current.Session["MosqueDetails"] as List<MosqueDetails>;

                        if (lobjListOfMosqueDetails != null && lobjListOfMosqueDetails.Count > 0)
                        {
                            MosqueDetails lobjMosqueDetails = new MosqueDetails();
                            lobjMosqueDetails = lobjListOfMosqueDetails.Find(lobj => lobj.Id.Equals(lstrMosqueId));

                            spanMosqueName.InnerText = lobjMosqueDetails.Name;
                            spanMosqueNameTitle.InnerText = lobjMosqueDetails.Name;

                            spanMosqueAddress.InnerText = lobjMosqueDetails.Address;
                            if (!string.IsNullOrEmpty(lobjMosqueDetails.PhoneNo))
                            {
                                //divPhone.InnerText = "Phone number - " + lobjMosqueDetails.PhoneNo;
                                divPhone.InnerHtml += "<i class=\"fa fa-phone\" style=\"margin-top: 5px; flex: 0.025;\"></i><p class=\"restaurant-detail-card-detail-text\" style=\"flex: 0.7;\">Phone Number - " + lobjMosqueDetails.PhoneNo + "</p>";
                            }
                            if (!string.IsNullOrEmpty(lobjMosqueDetails.ServiceLanguage))
                            {
                                //divServiceLanguage.InnerText = "Language of service - " + lobjMosqueDetails.ServiceLanguage;
                                divServiceLanguage.InnerHtml += "<i class=\"fa fa-microphone\" style=\"margin-top: 5px; flex: 0.025;\"></i><p class=\"restaurant-detail-card-detail-text\" style=\"flex: 0.7;\">Language of service - " + lobjMosqueDetails.ServiceLanguage + "</p>";
                            }
                            if (!string.IsNullOrEmpty(lobjMosqueDetails.QuickFacts))
                            {
                                //divQuickFacts.InnerText = "Quick facts - " + lobjMosqueDetails.QuickFacts;
                                divQuickFacts.InnerHtml += "<i class=\"fa fa-comments-o\" style=\"margin-top: 5px; flex: 0.025;\"></i><p class=\"restaurant-detail-card-detail-text\" style=\"flex: 0.7;\">Quick facts - " + lobjMosqueDetails.QuickFacts + "</p>";
                            }
                            spanAboutMosqueName.InnerText = lobjMosqueDetails.Name;
                            spanMosqueNameMain.InnerText = lobjMosqueDetails.Name;
                            imgMosqueImage.Src = lobjMosqueDetails.ImageUrl;
                            if (!string.IsNullOrEmpty(lobjMosqueDetails.Description))
                            {
                                spanMosqueDescription.InnerText = lobjMosqueDetails.Description;
                            }

                            //string mapAddressUrl = "https://www.google.com/maps/@{0},{1}";
                            string mapAddressUrl = "http://maps.google.com/maps?z=12&q=loc:{0}+{1}";
                            mapAddress.HRef = string.Format(mapAddressUrl, lobjMosqueDetails.Latitude, lobjMosqueDetails.Longitude);
                        }
                    }
                }
            }
        }
    }
}