﻿using Framework.EnterpriseLibrary.Adapters;
using Giift.SuciHalal.Helper;
using Microsoft.CSharp.RuntimeBinder;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SuciHalalWebclient
{
    public partial class ChangePassword : System.Web.UI.Page
    {
        public static string lstrAccessToken = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (HttpContext.Current.Session["AccessToken"] == null)
            {
                Response.Redirect("Login.aspx");
            }
            else
            {
                lstrAccessToken = Convert.ToString(HttpContext.Current.Session["AccessToken"]);
            }
        }

        [System.Web.Script.Services.ScriptMethod()]
        [System.Web.Services.WebMethod]
        public static string ChangePwd(string pstrOldPassword, string pstrNewPassword, string pstrConfirmPassword)
        {
            string lstrResponse = string.Empty;
            string lstrMessage = string.Empty;

            try
            {
                lstrResponse = ApiHelper.ChangePassword(lstrAccessToken, pstrOldPassword, pstrNewPassword, pstrConfirmPassword);

                JToken outer = JToken.Parse(lstrResponse);
                if (outer["error"] != null)
                {
                    JArray array = outer.Value<JArray>("error");
                    lstrMessage = array[0].Value<string>("msg");
                }
                else if (outer["valid"] != null)
                {
                    bool lblnStatus = outer.Value<bool>("valid");
                    if (lblnStatus)
                    {
                        lstrMessage = "SUCCESS";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("ChangePwd: " + ex.Message + Environment.NewLine + ex.StackTrace);
            }
            
            return lstrMessage;
        }
    }
}