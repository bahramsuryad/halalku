﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SuciHalalSite.Master" AutoEventWireup="true" CodeBehind="About.aspx.cs" Inherits="SuciHalalWebclient.About" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="dashboard animated fadeIn about" style="margin-top: 5px;">
        <div class="banner sub about" id="banner-store"
            style="background-image: url(https://images.unsplash.com/photo-1528356857578-6dc41b9a62e0?ixlib=rb-1.2.1&amp;ixid=eyJhcHBfaWQiOjEyMDd9&amp;auto=format&amp;fit=crop&amp;w=1350&amp;q=80) !important; background-size: cover !important;">
            <div class="container center in-sub w-container">
                <h1 class="sub-heading-title">About</h1>
                <div class="breadcrumb">
                    <div class="breadcrumb-content">
                        <a class="bread-link" href="Index.aspx">Home</a><img
                            alt=""
                            src="https://uploads-ssl.webflow.com/5c3ca7fbdc8fab3302b31532/5c41f1941b7ac65ffde90630_right-arrow%20(3).svg" width="18">
                        <div class="bread-link gray-color">About</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="section">
            <div class="container w-container">
                <div runat="server" id="divAbout" >

                </div>
                <%--<div class="flex-row w-row">
                    <div class="w-col w-col-6 w-col-stack">
                        <div class="about-wrapper">
                            <img
                                alt=""
                                class="image"
                                sizes="(max-width: 479px) 83vw, (max-width: 767px) 90vw, (max-width: 991px) 94vw, 46vw"
                                src="https://uploads-ssl.webflow.com/5c3ca7fbdc8fab3302b31532/5c49e47528aefe0ba4573331_about.jpg" srcset="https://uploads-ssl.webflow.com/5c3ca7fbdc8fab3302b31532/5c49e47528aefe0ba4573331_about-p-500.jpeg 500w, https://uploads-ssl.webflow.com/5c3ca7fbdc8fab3302b31532/5c49e47528aefe0ba4573331_about-p-800.jpeg 800w, https://uploads-ssl.webflow.com/5c3ca7fbdc8fab3302b31532/5c49e47528aefe0ba4573331_about-p-1080.jpeg 1080w, https://uploads-ssl.webflow.com/5c3ca7fbdc8fab3302b31532/5c49e47528aefe0ba4573331_about-p-1600.jpeg 1600w, https://uploads-ssl.webflow.com/5c3ca7fbdc8fab3302b31532/5c49e47528aefe0ba4573331_about.jpg 2000w">
                            <div class="about-content">
                                <div class="number-text">10</div>
                                <div class="years-text">Years of experience</div>
                            </div>
                        </div>
                    </div>
                    <div class="w-hidden-small w-hidden-tiny w-col w-col-1 w-col-stack"></div>
                    <div class="w-col w-col-5 w-col-stack">
                        <div class="right-padding">
                            <div class="top-title-wrapper left">
                                <h1>Quality is what matters most.<br>
                                </h1>
                            </div>
                            <div>
                                <p>
                                    Based on collective work and shared knowledge, Structure aims to favor
                                        dialogue and debate, to transform individual knowledge into increased creative
                                        potential.<br>
                                    <br>
                                    Our studio is a architecture practice based in NY and Paris.
                                        Today, it includes 150 architects, urban planners, landscape and interior
                                        designers of 25 different nationalities. The company principle of
                                        Architecture-Studio is the collective conception. From the very beginning, the
                                        practice has believed in the virtues of exchange, crossing ideas, common effort,
                                        shred knowledge.<br>
                                </p>
                            </div>
                            <div class="top-margin">
                                <a class="shop-button w-inline-block"
                                    data-w-id="f925fe12-35e4-48f0-5ad8-58e230c7e837" href="Catalogue.aspx">
                                    <div class="shop-text dark">Shop Now</div>
                                    <div class="shop-line black" style="width: 0%;"></div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>--%>
            </div>
        </div>
    </div>
</asp:Content>
