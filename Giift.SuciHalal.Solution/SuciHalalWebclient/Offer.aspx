﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SuciHalalSite.Master" AutoEventWireup="true" CodeBehind="Offer.aspx.cs" Inherits="SuciHalalWebclient.Offer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="tools_list" style="background-color: #eee;">
        <%--<div class="banner sub about" id="banner-store"
            style="background-image: url('https://images.unsplash.com/photo-1504674900247-0877df9cc836?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1050&q=80') !important; background-size: cover !important;">
            <div class="container center in-sub w-container">
                <h1 class="sub-heading-title">F&B Offers</h1>
                <div class="breadcrumb">
                    <div class="breadcrumb-content">
                        <a class="bread-link" href="Index.aspx">Home</a><img
                            alt=""
                            src="https://uploads-ssl.webflow.com/5c3ca7fbdc8fab3302b31532/5c41f1941b7ac65ffde90630_right-arrow%20(3).svg" width="18">
                        <div class="bread-link gray-color">F&B Offers</div>
                    </div>
                </div>
            </div>
        </div>--%>
        <section>
            <div id="slick-slider" style="margin-bottom: 0px; margin-top: 0px;">
                <div class="home-banner" style="background-image: url('https://d2t62toc9frg03.cloudfront.net/assets/img/web/offers/offer_banner_1.jpg');">
                    <div class="hero-text-container">
                        <div class="home-banner-overlay-container">
                            <div class="above-overlay home-banner-overlay">
                                <h1 class="shop-text light overlay-title">F&B Offers</h1>
                                <p class="shop-text light overlay-content">Find the best offers on My Halal Stop</p>
                                <a class="btn go w-commerce-commercecartcheckoutbutton checkout-button white-button overlay-button" href="http://www.myhalalstop.com/offer.aspx">FIND NOW
                                </a>
                            </div>
                        </div>
                        <div class="instant-overlay"></div>
                    </div>
                </div>
                <div class="home-banner" style="background-image: url('https://d2t62toc9frg03.cloudfront.net/assets/img/web/offers/offer_banner_2.jpg');">
                    <div class="hero-text-container">
                        <div class="home-banner-overlay-container">
                            <div class="above-overlay home-banner-overlay">
                                <h1 class="shop-text light overlay-title">F&B Offers</h1>
                                <p class="shop-text light overlay-content">Find the best offers on My Halal Stop</p>
                                <a class="btn go w-commerce-commercecartcheckoutbutton checkout-button white-button overlay-button" href="http://www.myhalalstop.com/offer.aspx">FIND NOW
                                </a>
                            </div>
                        </div>
                        <div class="instant-overlay"></div>
                    </div>
                </div>
                <div class="home-banner" style="background-image: url('https://d2t62toc9frg03.cloudfront.net/assets/img/web/offers/offer_banner_3.jpg');">
                    <div class="hero-text-container">
                        <div class="home-banner-overlay-container">
                            <div class="above-overlay home-banner-overlay">
                                <h1 class="shop-text light overlay-title">F&B Offers</h1>
                                <p class="shop-text light overlay-content">Find the best offers on My Halal Stop</p>
                                <a class="btn go w-commerce-commercecartcheckoutbutton checkout-button white-button overlay-button" href="http://www.myhalalstop.com/offer.aspx">FIND NOW
                                </a>
                            </div>
                        </div>
                        <div class="instant-overlay"></div>
                    </div>
                </div>
            </div>

      
        <div class="wrapper-dropdown">
           
            <div class="menu">
                <button class="menu-button">Filter Price</button>
                <div class="dropdown-menu">
                    <div class="ascending">
                       <a href="#">Ascending</a>
                    </div>
                    <div class="descending">
                        <a href="#">Descending</a>
                    </div>
                
                    
                </div>
            </div>
        </div>
        
        </section>
      
        <div class="section" style="padding-left: 0px !important; padding-right: 0px !important;">
            <%--<div id="divOffers" style="width: 100vw; padding-left: 10vw; padding-right: 10vw;">
            </div>--%>
            <div id="divOffers" class="offer-container">

            </div>
            <div class="center">
                <ul id="ulPagingItems" class="pagination">
                </ul>
            </div>
        </div>
    </div>
    <div id="divLoader" class="divLoader" style="display: none;"></div>
    <script src="js/jquery-3.3.1.min.js"></script>
    <script type="text/javascript">
        var lintPageSize = 15;
        var lintTotalPages = 0;
        
        $(document).ready(function () {
            $("#divOffers").text("");
            GetOffers();
        });

        function GetOffers() {
            $('#divLoader').show();

            $.ajax({
                type: "POST",
                url: "Offer.aspx/GetRestaurantOffers",
                data: "{pintSkipRecords:0,pintTakeRecords:" + lintPageSize + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: true,
                cache: false,
                success: function (msg) {
                    $('#divLoader').hide();
                    if (msg != null) {
                        lintTotalPages = msg.d.TotalPages;
                        if (lintTotalPages > 0) {
                            $("#divOffers").html("");
                            printRestaurantOffersData(msg);

                            $('#ulPagingItems').twbsPagination({
                                totalPages: lintTotalPages,
                                visiblePages: 10,
                                next: 'Next',
                                prev: 'Prev',
                                onPageClick: function (event, page) {
                                    pageData(page);
                                }
                            });
                        }
                        else {
                            $("#divOffers").text("No records were found.");
                        }
                    }
                    else {
                        swal("Oops!", "Something went wrong.", "error");
                    }
                },
                error: function () {
                    $('#divLoader').hide();
                    $("#divOffers").text("");
                    $("#divOffers").text("No records were found.");
                }
            });
        }

        function printRestaurantOffersData(msg) {
            $('#divLoader').show();
            var offers = msg.d.RDetails;
            var html = "";
            $("#divOffers").text(html);
            var count = 0;
            for (var i = 0; i < offers.length; i++) {
                
                html += '<a href="./RestaurantDetail.aspx?id=' + offers[i].Id + '">'
                html += '<div class="row">'
                html += '<div class="offer-list-card">'
                html += '<div class="offer-list-card-left">'
                html += '<p class="offer-list-card-certified">' + offers[i].Address + '</p>'
                html += '<img class="offer-card-image" src="' + offers[i].ImageUrl + '">'
                html += '<p class="offer-list-card-name">' + offers[i].Name + '</p>'
                html += '<div class="offer-list-card-highlight-container"><span class="offer-list-card-highlight">' + offers[i].DealTeaser + '</span></div>'
               
                html += '<p class="offer-list-card-address">' + offers[i].Deal.replace('<p>', '').replace('</p>', '').replace('<p>', '').replace('</p>', '') + '</p>'
              
            
                html += '</div>'

                
                html += '</div>'
                html += '<div class="card-footer-offers"><a href="./RestaurantDetail.aspx?id=' + offers[i].Id + '" class="view-more-button" >View More</a></div>'
                html += '</div>'
      
                html += '</a>'
            }
            $("#divOffers").append(html);
            $('#divLoader').hide();
        }

      

      
        function pageData(e) {
            $('#divLoader').show();
            var lintSkipRecords = e == 1 ? 0 : (e * lintPageSize) - lintPageSize;
            $.ajax({
                type: "POST",
                url: "Offer.aspx/GetRestaurantOffers",
                data: "{pintSkipRecords:" + lintSkipRecords + ",pintTakeRecords:" + lintPageSize + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: true,
                cache: false,
                success: function (msg) {
                    $('#divLoader').hide();
                    printRestaurantOffersData(msg);
                },
                error: function () {
                    $('#divLoader').hide();
                    $("#divOffers").text("");
                    $("#divOffers").text("No records were found.");
                }

            });

            return false;
        }
    </script>
</asp:Content>
