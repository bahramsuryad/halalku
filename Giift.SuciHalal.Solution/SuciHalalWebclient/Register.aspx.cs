﻿using Framework.EnterpriseLibrary.Adapters;
using Giift.SuciHalal.Helper;
using Microsoft.CSharp.RuntimeBinder;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SuciHalalWebclient
{
    public partial class Register : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [System.Web.Script.Services.ScriptMethod()]
        [System.Web.Services.WebMethod]
        public static string SignUpUser(string pstrMemberEmail, string pstrMemberPassword)
        {
            string lstrResponse = string.Empty;
            string lstrMessage = string.Empty;
            try
            {
                lstrResponse = ApiHelper.SignUp(pstrMemberEmail, pstrMemberPassword);

                JToken outer = JToken.Parse(lstrResponse);
                if (outer["error_description"] != null)
                {
                    lstrMessage = outer.Value<string>("error_description");
                }
                else if (outer["access_token"] != null)
                {
                    HttpContext.Current.Session["AccessToken"] = outer.Value<string>("access_token");
                    lstrMessage = "SUCCESS";
                }
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("SignupUser: " + ex.Message + Environment.NewLine + ex.StackTrace);
            }

            return lstrMessage;

        }
    }
}