﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SuciHalalSite.Master" AutoEventWireup="true" CodeBehind="PrayerSchedule.aspx.cs" Inherits="SuciHalalWebclient.PrayerSchedule" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <link rel="stylesheet" type="text/css" href="css/bootstrap-select.min.css" />
    <link rel="stylesheet" type="text/css" href="css/prayertime.css" />

    <section class="prayer_time_box">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <h1 class="prayertime-font">Showing Prayer Times for </h1>
                    <span class="calculate-widthspan"></span>
                    <div class="select-locationbox custom-location-drp">
                        <div id="prayer-location-search">
                            <input id="geocomplete1" class="form-control foundlocation-values" value="Singapore" type="text" placeholder="Location" />
                            <fieldset class="details" id="search-fields">
                                <input class="location-field" data-geo="name" type="hidden" name="name" value="" />
                                <input class="location-field" data-geo="formatted_address" type="hidden" id="formatted_address" name="f" value="Singapore, Singapore" />
                                <input class="location-field" data-geo="locality" id='locality' name="l" type="hidden" value="Singapore">
                                <input class="location-field" data-geo="sublocality" name="s" type="hidden" value="" />
                                <input class="location-field" data-geo="country" id="country" name="c" type="hidden" value="Singapore" />
                                <input class="location-field" data-geo="lat" id="lat" name="lat" type="hidden" required="true" value="1.28967" />
                                <input class="location-field" data-geo="lng" name="lng" id="lng" type="hidden" required="true" value="103.85" />
                                <input id="no_days" name="no_days" type="hidden" required="true" value="7" />
                            </fieldset>
                        </div>
                    </div>

                    <button type="button" class="travel-list-card-book-now" style="flex: 1; margin: 0px 10px 0px 10px; height: 45px; vertical-align: -webkit-baseline-middle;"
                        onclick="GetPrayerTime();">
                        <i aria-label="icon: search" class="anticon anticon-search">
                            <svg viewBox="64 64 896 896" class="" data-icon="search" width="1em" height="1em" fill="currentColor" aria-hidden="true" focusable="false">
                                <path d="M909.6 854.5L649.9 594.8C690.2 542.7 712 479 712 412c0-80.2-31.3-155.4-87.9-212.1-56.6-56.7-132-87.9-212.1-87.9s-155.5 31.3-212.1 87.9C143.2 256.5 112 331.8 112 412c0 80.1 31.3 155.5 87.9 212.1C256.5 680.8 331.8 712 412 712c67 0 130.6-21.8 182.7-62l259.7 259.6a8.2 8.2 0 0 0 11.6 0l43.6-43.5a8.2 8.2 0 0 0 0-11.6zM570.4 570.4C528 612.7 471.8 636 412 636s-116-23.3-158.4-65.6C211.3 528 188 471.8 188 412s23.3-116.1 65.6-158.4C296 211.3 352.2 188 412 188s116.1 23.2 158.4 65.6S636 352.2 636 412s-23.3 116.1-65.6 158.4z"></path>
                            </svg>
                        </i>
                        <span>Get Prayer Time</span>
                    </button>
                    <%--<div id="prayerCalculationModal" class="modal fade prayer-calculation-modal" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel">Prayer Calculation</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="calculation-box">

                                        <div class="separated-boxcontainer">
                                            <div class="dimensionbox left-s-box">
                                                <div class="formfield-box">
                                                    <label>Prayer Calculation Method</label>
                                                    <div class="dropdown-box form-group">
                                                        <select class="form-control selectpicker show-menu-arrow" name="cal_method" id="cal_method">
                                                            <option value="10">Central Mosque London</option>
                                                            <option value="8">Diyanet İşleri Başkanlığı</option>
                                                            <option value="1">Egyptian General Authority of Survey</option>
                                                            <option value="2">Institute of Geophysics, University of Tehran</option>
                                                            <option value="3">Islamic Society of North America (ISNA)</option>
                                                            <option value="4">Ithna Ashari</option>
                                                            <option value="9">Majlis Ugama Islam Singapura</option>
                                                            <option value="5" selected="selected">Muslim World League (MWL)</option>
                                                            <option value="6">Umm al-Qura University, Makkah</option>
                                                            <option value="7">University of Islamic Sciences, Karachi</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="dimensionbox right-s-box">
                                                <div class="formfield-box">
                                                    <label>Asr Prayer Time Method</label>
                                                    <div class="dropdown-box form-group">
                                                        <select class="form-control selectpicker show-menu-arrow" name="asr_method" id="asr_method">
                                                            <option value="1">Hanafi</option>
                                                            <option value="2" selected="selected">Shafii</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="separated-boxcontainer">
                                            <div class="dimensionbox left-s-box">
                                                <div class="formfield-box">
                                                    <label>Adjustment For Higher Latitudes</label>
                                                    <div class="dropdown-box form-group">
                                                        <select class="form-control selectpicker show-menu-arrow" name="higher_lat" id="higher_lat">
                                                            <option value="1">1/7th of night</option>
                                                            <option value="2" selected="selected">Angle/60th of night</option>
                                                            <option value="3">Middle of night</option>
                                                            <option value="4">No adjustment</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="dimensionbox right-s-box">
                                                <div class="formfield-box">
                                                    <label>Cruising Altitude/Height(m)</label>
                                                    <div class="dropdown-box form-group">
                                                        <input class="form-control selectpicker show-menu-arrow" type="text" value="11800" name="cruising_height" id="cruising_height" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <input type="submit" class="viewall-link tmargin20" value="Calculate">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>--%>
                    <div class="day-active-board">
                        <%--<span class="countDownTimer"><span id="spnCurrentTime" class="run-timespn counter counter-analog" data-direction="up" data-format="23:59:59">8:20:35</span></span>--%>
                        <%-- <a class="change-settings" data-toggle="modal" data-target="#prayerCalculationModal" href="#prayerCalculationModal">Change
Prayer Calculation Method</a>--%>
                        <div class="pt-timedate-box">
                            <div class="left-datetime-box">
                                <em id="prayerDay"></em>
                                <span id="prayerMonthYear" class="month-year"></span>
                            </div>
                            <div class="showactive-info">
                                <ul id="ulTimes" class="day-repeatsul clearfix">
                                    <li class="">
                                        <div id="divFajr" class="time-box-content ">
                                            <span id="spnNowFajr" class="now-tag">Now</span>
                                            <em class="block-name">Fajr</em>
                                            <span id="spnFajr" class="block-time"></span>
                                            <%--<div class="time-left-toend">
                                                <label></label>
                                                <time class="countDownTimer"><span class="counter counter-analog" data-direction="down" data-format="23:59:59">02 : 24 : 17</span></time>
                                            </div>--%>
                                        </div>
                                    </li>
                                    <li class="">
                                        <div id="divShurooq" class="time-box-content ">
                                            <span id="spnNowShurooq" class="now-tag">Now</span>
                                            <em class="block-name">Shurooq</em>
                                            <span id="spnShurooq" class="block-time"></span>
                                            <%--<div class="time-left-toend">
                                                <label></label>
                                                <time class="countDownTimer"><span class="counter counter-analog" data-direction="down" data-format="23:59:59">02 : 24 : 17</span></time>
                                            </div>--%>
                                        </div>
                                    </li>
                                    <li class="">
                                        <div id="divDhuhr" class="time-box-content">
                                            <span id="spnNowDhuhr" class="now-tag">Now</span>
                                            <em class="block-name">Dhuhr</em>
                                            <span id="spnDhuhr" class="block-time"></span>
                                            <%--<div class="time-left-toend">
                                                <label>Ends in </label>
                                                <time class="countDownTimer"><span class="counter counter-analog" data-direction="down" data-format="23:59:59">02 : 05 : 44</span></time>
                                            </div>--%>
                                        </div>
                                    </li>
                                    <li class="">
                                        <div id="divAsr" class="time-box-content">
                                            <span id="spnNowAsr" class="now-tag">Now</span>
                                            <em class="block-name">Asr</em>
                                            <span id="spnAsr" class="block-time"></span>
                                            <%--<div class="time-left-toend">
                                                <label></label>
                                                <time class="countDownTimer"><span class="counter counter-analog" data-direction="down" data-format="23:59:59">02 : 05 : 44</span></time>
                                            </div>--%>
                                        </div>
                                    </li>
                                    <li class="">
                                        <div id="divMaghrib" class="time-box-content ">
                                            <span id="spnNowMaghrib" class="now-tag">Now</span>
                                            <em class="block-name">Maghrib</em>
                                            <span id="spnMaghrib" class="block-time"></span>
                                            <%--<div class="time-left-toend">
                                                <label></label>
                                                <time class="countDownTimer"><span class="counter counter-analog" data-direction="down" data-format="23:59:59">02 : 05 : 44</span></time>
                                            </div>--%>
                                        </div>
                                    </li>
                                    <li class="">
                                        <div id="divIsha" class="time-box-content ">
                                            <span id="spnNowIsha" class="now-tag">Now</span>
                                            <em class="block-name">Isha</em>
                                            <span id="spnIsha" class="block-time"></span>
                                            <%--<div class="time-left-toend">
                                                <label></label>
                                                <time class="countDownTimer"><span class="counter counter-analog" data-direction="down" data-format="23:59:59">02 : 05 : 44</span></time>
                                            </div>--%>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="recap-detail">
                        <p>Enter the City Name and get Prayer Times (Salat/Salah/Salath/Namaz times).</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="nextdays-prayertime" id="scrnextdays-box">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-sm-12 col-xs-12" id="ptime">
                    <h2 class="pt-secondaryfont">Showing Prayer times for next</h2>
                    <div class="select-locationbox small-drpdown make-scrolltobox">
                        <select class="selectpicker form-control" id="no_days_select" name="no_days_select" onchange="GetPrayerNextTimeTable()">
                            <option value="weekly" selected="selected">7 Days</option>
                            <option value="monthly">1 Month</option>
                            <option value="yearly">12 Months</option>
                        </select>
                    </div>
                    <%--<form id='downloadpdf' method='POST' action="https://www.halaltrip.com/export/prayercalender/" target='_blank' style="display: initial;">
                        <input type="hidden" name="data" value="YTo3OntzOjEwOiIxMS0wNi0yMDE5IjthOjY6e2k6MDtzOjU6IjA1OjQzIjtpOjE7czo1OiIwNjo1OCI7aToyO3M6NToiMTM6MDQiO2k6MztzOjU6IjE2OjMwIjtpOjQ7czo1OiIxOToxMCI7aTo1O3M6NToiMjA6MjEiO31zOjEwOiIxMi0wNi0yMDE5IjthOjY6e2k6MDtzOjU6IjA1OjQzIjtpOjE7czo1OiIwNjo1OCI7aToyO3M6NToiMTM6MDQiO2k6MztzOjU6IjE2OjMwIjtpOjQ7czo1OiIxOToxMCI7aTo1O3M6NToiMjA6MjEiO31zOjEwOiIxMy0wNi0yMDE5IjthOjY6e2k6MDtzOjU6IjA1OjQ0IjtpOjE7czo1OiIwNjo1OSI7aToyO3M6NToiMTM6MDUiO2k6MztzOjU6IjE2OjMxIjtpOjQ7czo1OiIxOToxMCI7aTo1O3M6NToiMjA6MjEiO31zOjEwOiIxNC0wNi0yMDE5IjthOjY6e2k6MDtzOjU6IjA1OjQ0IjtpOjE7czo1OiIwNjo1OSI7aToyO3M6NToiMTM6MDUiO2k6MztzOjU6IjE2OjMxIjtpOjQ7czo1OiIxOToxMSI7aTo1O3M6NToiMjA6MjEiO31zOjEwOiIxNS0wNi0yMDE5IjthOjY6e2k6MDtzOjU6IjA1OjQ0IjtpOjE7czo1OiIwNjo1OSI7aToyO3M6NToiMTM6MDUiO2k6MztzOjU6IjE2OjMxIjtpOjQ7czo1OiIxOToxMSI7aTo1O3M6NToiMjA6MjIiO31zOjEwOiIxNi0wNi0yMDE5IjthOjY6e2k6MDtzOjU6IjA1OjQ0IjtpOjE7czo1OiIwNjo1OSI7aToyO3M6NToiMTM6MDUiO2k6MztzOjU6IjE2OjMxIjtpOjQ7czo1OiIxOToxMSI7aTo1O3M6NToiMjA6MjIiO31zOjEwOiIxNy0wNi0yMDE5IjthOjY6e2k6MDtzOjU6IjA1OjQ0IjtpOjE7czo1OiIwNjo1OSI7aToyO3M6NToiMTM6MDUiO2k6MztzOjU6IjE2OjMyIjtpOjQ7czo1OiIxOToxMSI7aTo1O3M6NToiMjA6MjIiO319">
                        <input type="hidden" name="request" value="YTo1OntzOjM6ImxhdCI7ZDoxLjI4OTY3MDAwMDAwMDAwMDE7czozOiJsbmciO2Q6MTAzLjg0OTk5OTk5OTk5OTk5O3M6MToibCI7czo5OiJTaW5nYXBvcmUiO3M6MToiYyI7czo5OiJTaW5nYXBvcmUiO3M6MToiZiI7czoyMDoiU2luZ2Fwb3JlLCBTaW5nYXBvcmUiO30=">
                        <input type="hidden" name="method" value="5">
                        <input type="hidden" name="asr" value="0">
                        <a href="javascript:void(0);" onclick="$('#downloadpdf').submit()" class="download-rpt-link">Download</a>
                    </form>--%>
                    <div class="slides-for-print">
                        <div class="months-data display-value">
                            <label class="timefor">Prayer times for <span id="spnLocation"></span>&nbsp;in&nbsp;<span id="spnTimeFor"></span></label>
                            <div class="mobile-scroller">
                                <ul class="fornext-time-table" id="ulTimeTable">
                                    <%--<li class="heading-hrz-line">
                                        <ul class="tm-horizontal-listitem clearfix">
                                            <li>Date</li>
                                            <li>Fajar</li>
                                            <li>Sunrise</li>
                                            <li>Dhuhr</li>
                                            <li>Asr</li>
                                            <li>Maghrib</li>
                                            <li>Isha</li>
                                        </ul>
                                    </li>
                                    <li>
                                        <ul class="tm-horizontal-listitem clearfix highlighted">
                                            <li>11 Jun</li>
                                            <li>05:43</li>
                                            <li>06:58</li>
                                            <li>13:04</li>
                                            <li>16:30</li>
                                            <li>19:10</li>
                                            <li>20:21</li>
                                        </ul>
                                    </li>
                                    <li>
                                        <ul class="tm-horizontal-listitem clearfix ">
                                            <li>12 Jun</li>
                                            <li>05:43</li>
                                            <li>06:58</li>
                                            <li>13:04</li>
                                            <li>16:30</li>
                                            <li>19:10</li>
                                            <li>20:21</li>
                                        </ul>
                                    </li>
                                    <li>
                                        <ul class="tm-horizontal-listitem clearfix ">
                                            <li>13 Jun</li>
                                            <li>05:44</li>
                                            <li>06:59</li>
                                            <li>13:05</li>
                                            <li>16:31</li>
                                            <li>19:10</li>
                                            <li>20:21</li>
                                        </ul>
                                    </li>
                                    <li>
                                        <ul class="tm-horizontal-listitem clearfix ">
                                            <li>14 Jun</li>
                                            <li>05:44</li>
                                            <li>06:59</li>
                                            <li>13:05</li>
                                            <li>16:31</li>
                                            <li>19:11</li>
                                            <li>20:21</li>
                                        </ul>
                                    </li>
                                    <li>
                                        <ul class="tm-horizontal-listitem clearfix ">
                                            <li>15 Jun</li>
                                            <li>05:44</li>
                                            <li>06:59</li>
                                            <li>13:05</li>
                                            <li>16:31</li>
                                            <li>19:11</li>
                                            <li>20:22</li>
                                        </ul>
                                    </li>
                                    <li>
                                        <ul class="tm-horizontal-listitem clearfix ">
                                            <li>16 Jun</li>
                                            <li>05:44</li>
                                            <li>06:59</li>
                                            <li>13:05</li>
                                            <li>16:31</li>
                                            <li>19:11</li>
                                            <li>20:22</li>
                                        </ul>
                                    </li>
                                    <li>
                                        <ul class="tm-horizontal-listitem clearfix ">
                                            <li>17 Jun</li>
                                            <li>05:44</li>
                                            <li>06:59</li>
                                            <li>13:05</li>
                                            <li>16:32</li>
                                            <li>19:11</li>
                                            <li>20:22</li>
                                        </ul>
                                    </li>--%>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <%--<div class="col-md-4 col-sm-12 col-xs-12">
                    <div class="row">
                        <div class="col-md-12 col-sm-6 col-xs-6 res-full">
                            <div class="add-new-mosque q-dirbox" style="background-image: url('../../img2018/q-dir-bg.jpg')">
                                <h3>Get <span>Qibla
                                    <br>
                                    Direction</span> for
                                    <br>
                                    your location</h3>
                                <a target="_blank" href="https://www.halaltrip.com/prayertimes/qibla-direction/" class="viewall-link">Get Direction <span class="n-sprite right-arrow"></span></a>
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-6 col-xs-6 res-full">
                            <div class="add-new-mosque q-dirbox msq-nr" style="background-image: url('../../img2018/addmosq-bg.jpg')">
                                <h3>Find Mosque</h3>
                                <a target="_blank" href="https://www.halaltrip.com/find-mosques-near-me/" class="viewall-link">Find Mosque Near Me</a>
                            </div>
                        </div>
                    </div>
                </div>--%>
            </div>
        </div>
    </section>
    <section class="qibla-faqs-section qibla-faqs-section-greybg keep-textside" id="faqlink">
        <div class="container" style="max-width: 1110px">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <h2 class="main-heading">Local Prayer times FAQs</h2>
                    <ul class="clearfix faqs-list-chain faqs-list-chain-two-column">
                        <li>
                            <div class="box">
                                <h3>How to get prayer times for any city?</h3>
                                <p>Just enter the address or the city on this application. The application will then provide the prayer times and nearby mosques for any location. It will also indicate the direction to the nearby Mosques as well as whether you can reach the Masjid in time.</p>
                            </div>
                        </li>
                        <li>
                            <div class="box">
                                <h3>What time does Zuhr Salat end?</h3>
                                <p>Zuhr ends when Asr starts.</p>
                            </div>
                        </li>
                        <li>
                            <div class="box">
                                <h3>What time is Maghrib prayers?</h3>
                                <p>Maghrib prayers start just after sunset.</p>
                            </div>
                        </li>
                        <li>
                            <div class="box">
                                <h3>What time is Jummah (Jumuah) Salat (Friday prayers)?</h3>
                                <p>
                                    In many parts of the world Jummah (Jumuah) starts just after Zuhr Azan (Athan). In general, the Jumuah program timing will be as follows:
The 1st Azan will be done at the time of Zuhr. Then around 5 to 7 mins is given for the congregation to perform the Sunnah prayers. After that, the 2nd Azan will be done. The Imam will then deliver the Khutbah. The duration of the Khutbah can vary for each Masjid/City. It can be anywhere from 15 minutes to 30 minutes. At the end of the Khutbah, two raka'ats of Jumuah prayers will be conducted, which can take between 5 to 10 minutes.
                                </p>
                            </div>
                        </li>
                        <%--<li>
                            <div class="download-app-greenbox">
                                <div class="dag-content">
                                    <h2>Get a Better Experience</h2>
                                    <p>The fully integrated, all-in-one, Lifestyle App for Muslims</p>
                                    <div class="app-links">
                                        <a target="_blank" rel="nofollow" href="https://play.google.com/store/apps/details?id=com.halaltrip.app">
                                            <img alt="Play store" src="../../img2018/playstore-btn.png"></a>
                                        <a target="_blank" rel="nofollow" href="https://itunes.apple.com/app/id680194589">
                                            <img alt="App store" src="../../img2018/app-store-btn.png"></a>
                                    </div>
                                    <img style="max-width: 100%; width: 300px;" src="../../img2018/app-image.png">
                                </div>
                            </div>
                        </li>--%>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <div id="divLoader" class="divLoader" style="display: none;"></div>

    <script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" src="js/bootstrap-select.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?v=3&amp;sensor=false&amp;libraries=places&amp;key=AIzaSyDbt3_InYrxCpwjkoGa7CxgceqvWonBJlg"></script>

    <script type="text/javascript">
        $('.counter').counter({});
        $(document).ready(function () {
            var drpwidth = $('.calculate-widthspan').text($('.foundlocation-values').val()).width();
            if ($(window).width() > 767) {
                $('.custom-location-drp').css('width', drpwidth + 40);
                $('.foundlocation-values').keyup(function () {
                    $('.custom-location-drp').width($('.calculate-widthspan').text($(this).val()).width() + 40);
                });
            }
            else {
                $('.custom-location-drp').css('width', drpwidth + 30);
                $('.foundlocation-values').keyup(function () {
                    $('.custom-location-drp').width($('.calculate-widthspan').text($(this).val()).width() + 30);
                });
            }

            $('.next-print').click(function () {
                $('.months-data.display-value').removeClass('display-value').next('.months-data').addClass('display-value');
                if ($('.months-data:last-of-type').hasClass('display-value')) {
                    $('.next-print').hide();
                }
                else {
                    $('.next-print').fadeIn(300);
                }
                $('.prev-print').fadeIn(300);
            });
            $('.prev-print').click(function () {
                $('.months-data.display-value').removeClass('display-value').prev('.months-data').addClass('display-value');
                if ($('.months-data:first-of-type').hasClass('display-value')) {
                    $('.prev-print').hide();
                }
                else {
                    $('.prev-print').fadeIn(300);
                }
                $('.next-print').fadeIn(300);
            });

            $('.print-button').click(function () {

                var printContents = $('.slides-for-print').html();
                var originalContents = document.body.innerHTML;
                document.body.innerHTML = printContents;
                window.print();
                document.body.innerHTML = originalContents;
            });
            $('#no_days_select').on('change', function () {
                $('#no_days').val($('#no_days_select :selected').val());

                //if (window.location.hash !== "#scrnextdays-box") {
                //    window.location = window.location + '#scrnextdays-box';
                //}
            })
            $('.foundlocation-values').click(function (e) {
                e.stopPropagation();
            });
            $('.open-loc-vals').children('.filter-option').text($('.foundlocation-values').val());
            getLocation();
            $('#set-location-search').submit(function () {
                if ($('#geocomplete1').val() == '') {
                    $('.location-field').each(function () {
                        $(this).val('');
                    });
                }
            });
            GetPrayerTime();
        });

        function getLocation() {
            if ($('#lat').val() == '' && $('#lng').val() == '') {
                if (navigator.geolocation) {
                    navigator.geolocation.getCurrentPosition(showPosition);
                }
            }
        }
        function showPosition(position) {

            $('#lat').val(position.coords.latitude);
            $('#lng').val(position.coords.longitude);
            var latlng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
            geocoder = new google.maps.Geocoder();
            geocoder.geocode({ 'latLng': latlng }, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    //alert(JSON.stringify(results[0].address_components));
                    for (var i = 0; i < results[0].address_components.length; i++) {
                        if (results[0].address_components[i].types[0] == 'locality')
                            $('#locality').val(results[0].address_components[i].long_name);
                        if (results[0].address_components[i].types[0] == 'country')
                            $('#country').val(results[0].address_components[i].long_name);

                    }

                    if (results[1]) {
                        $('#formatted_address').val(results[0].formatted_address);
                        $('#geocomplete1').val(results[0].formatted_address);
                        $('#set-location-search').submit();
                    } else {
                        consol.log('No results found');
                    }
                } else {
                    consol.log('Geocoder failed due to: ' + status);
                }
            });

        }
        function GetPrayerTime() {
            $('#divLoader').show();
            jQuery(function ($) {
                var requestUrl = 'https://muslimsalat.com/' + $("#geocomplete1").val() + '/daily.json?jsoncallback=?';
                const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
                    "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
                $('#spnLocation').text($("#geocomplete1").val());
                $.getJSON(requestUrl, function (times) {
                    try {
                        var prayerDate = new Date(times.items[0].date_for);
                        $("#prayerDay").html(prayerDate.getDate());
                        $("#prayerMonthYear").html(monthNames[prayerDate.getMonth()] + ', ' + prayerDate.getFullYear());
                        $("#spnFajr").text(times.items[0].fajr);
                        $("#spnShurooq").text(times.items[0].shurooq);
                        $("#spnDhuhr").text(times.items[0].dhuhr);
                        $("#spnAsr").text(times.items[0].asr);
                        $("#spnMaghrib").text(times.items[0].maghrib);
                        $("#spnIsha").text(times.items[0].isha);
                        $("#spnTimeFor").text(monthNames[prayerDate.getMonth()] + ' ' + prayerDate.getFullYear());
                        GetCurrentTime(times.latitude + ',' + times.longitude, prayerDate);
                    } catch (e) {

                    }
                });
            });
            GetPrayerNextTimeTable();

            $('#divLoader').hide();

        }
        function GetPrayerNextTimeTable() {
            jQuery(function ($) {
                var strPeriod = $('#no_days_select :selected').val()
                var requestUrl = 'https://muslimsalat.com/' + $("#geocomplete1").val() + '/' + strPeriod + '.json?jsoncallback=?';
                const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
                    "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
                $.getJSON(requestUrl, function (times) {
                    try {
                        var prayerDate = '';
                        $('#ulTimeTable').empty();
                        var tableHtml = '';
                        var tableHeader = '<li class="heading-hrz-line"><ul class="tm-horizontal-listitem clearfix"><li>Date</li><li>Fajar</li><li>Sunrise</li><li>Dhuhr</li><li>Asr</li><li>Maghrib</li><li>Isha</li></ul></li>';
                        if (times.items.length > 0) {
                            for (var i = 0; i < times.items.length; i++) {
                                prayerDate = new Date(times.items[i].date_for)
                                tableHtml += '<li>' +
                                    '<ul class="tm-horizontal-listitem clearfix" >' +
                                    '<li>' + monthNames[prayerDate.getMonth()] + ' ' + prayerDate.getDate() + '</li>' +
                                    '<li>' + times.items[i].fajr + '</li>' +
                                    '<li>' + times.items[i].shurooq + '</li>' +
                                    '<li>' + times.items[i].dhuhr + '</li>' +
                                    '<li>' + times.items[i].asr + '</li>' +
                                    '<li>' + times.items[i].maghrib + '</li>' +
                                    '<li>' + times.items[i].isha + '</li>' +
                                    '</ul >' +
                                    '</li > '
                            }
                            $('#ulTimeTable').append(tableHeader + tableHtml);
                        }

                    } catch (e) {

                    }
                });
            });
        }
        function GetCurrentTime(loc, date) {
            var targetDate = new Date();
            var timestamp = targetDate.getTime() / 1000 + targetDate.getTimezoneOffset() * 60;
            var apikey = 'AIzaSyDbt3_InYrxCpwjkoGa7CxgceqvWonBJlg';
            var requestUrl = 'https://maps.googleapis.com/maps/api/timezone/json?location=' + loc + '&timestamp=' + timestamp + '&key=' + apikey;
            $.getJSON(requestUrl, function (result) {
                try {
                    if (result.status == 'OK') {
                        var offsets = result.dstOffset * 1000 + result.rawOffset * 1000
                        var localdate = new Date(timestamp * 1000 + offsets)
                        //console.log(localdate.toLocaleString()) 
                        $('#spnCurrentTime').text(localdate.toLocaleString().trim().split(',')[1].slice(0, -2));

                        var now = localdate.toLocaleString().trim().split(',')[1];
                        $(function () {
                            var els = $('#ulTimes li div.time-box-content span.block-time'),
                                times = els.map(function () { return $(this).text() }).get(),
                                diff = Infinity, i = 0

                            times.forEach(function (time, index) {
                                if (now.slice(8, 11).trim().toLowerCase() == time.slice(4, 7).trim().toLowerCase()) {
                                    var a1 = now.split(':');
                                    var d1 = new Date(date.getFullYear(), date.getMonth(), date.getDate(), a1[0], a1[1]);
                                    var a2 = time.trim().slice(0, -2).split(':');
                                    var d2 = new Date(date.getFullYear(), date.getMonth(), date.getDate(), a2[0], a2[1]);
                                    var d = Math.abs(d1 - d2);
                                    if (d < diff) {
                                        diff = d; i = index;
                                    }
                                }
                            });                            
                            var spnNow = els.eq(i)[0].id.replace("spn", "spnNow");                            
                            $("#spnNowFajr").removeClass('display-block');
                            $("#spnNowShurooq").removeClass('display-block');
                            $("#spnNowDhuhr").removeClass('display-block');
                            $("#spnNowAsr").removeClass('display-block');
                            $("#spnNowMaghrib").removeClass('display-block');
                            $("#spnNowIsha").removeClass('display-block');
                            $("#spnNowTimeFor").removeClass('display-block');
                            $("#" + spnNow).addClass('display-block');
                            //els.eq(i).addClass('selected').siblings().removeClass('selected');
                        });
                    }
                } catch (e) {

                }
            });
        }
    </script>
</asp:Content>
