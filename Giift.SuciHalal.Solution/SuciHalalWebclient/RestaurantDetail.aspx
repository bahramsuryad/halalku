﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SuciHalalSite.Master" AutoEventWireup="true" CodeBehind="RestaurantDetail.aspx.cs" Inherits="SuciHalalWebclient.RestaurantDetail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="tools_list" style="background-color: #eee;">
        <div class="banner sub"
            style="background-image: -webkit-gradient(linear, 0% 0%, 0% 100%, from(rgba(0, 0, 0, 0.3)), to(rgba(0, 0, 0, 0.3))), url('https://images.unsplash.com/photo-1522413452208-996ff3f3e740?ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80'); background-size: cover; background-position: center 80%;">
            <div class="container center in-sub w-container">
                <h1 class="sub-heading-title"><span id="spanRestaurantNameTitle" runat="server"></span></h1>
                <div class="breadcrumb">
                    <div class="breadcrumb-content">
                        <a class="bread-link" href="Index.aspx">Home</a>
                        <img alt="" src="https://uploads-ssl.webflow.com/5c3ca7fbdc8fab3302b31532/5c41f1941b7ac65ffde90630_right-arrow%20(3).svg" width="18">
                        <a class="bread-link" href="Tools.aspx">Tools</a>
                        <img alt="" src="https://uploads-ssl.webflow.com/5c3ca7fbdc8fab3302b31532/5c41f1941b7ac65ffde90630_right-arrow%20(3).svg" width="18">
                        <a class="bread-link" href="Restaurant.aspx">Restaurant</a>
                        <img alt="" src="https://uploads-ssl.webflow.com/5c3ca7fbdc8fab3302b31532/5c41f1941b7ac65ffde90630_right-arrow%20(3).svg" width="18">
                        <div class="bread-link gray-color">Restaurant Detail</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="section">
            <div class="row">
                <div class="restaurant-detail-card">
                    <div class="restaurant-detail-card-left">
                        <p class="restaurant-detail-card-certified"><span id="spanLocality" runat="server"></span></p>
                        <p class="restaurant-detail-card-name"><span id="spanName" runat="server"></span></p>
                        <p class="restaurant-detail-card-address"><a id="mapAddress" href="#" runat="server" target="_blank"><span id="spanAddress" runat="server"></span></a></p>
                        <div id="divPhone" runat="server" class="restaurant-detail-card-detail-container">
                        </div>
                        <div class="restaurant-detail-card-detail-container">
                            <i class="fa fa-money" style="margin-top: 5px; flex: 0.025;"></i>
                            <p class="restaurant-detail-card-detail-text" style="flex: 0.7;">Average Price - <span id="spanAveragePrice" runat="server">NA</span></p>
                        </div>
                        <div class="restaurant-detail-card-detail-container">
                            <i class="fa fa-cutlery" style="margin-top: 5px; flex: 0.025;"></i>
                            <p class="restaurant-detail-card-detail-text" style="flex: 0.7;">Cuisine - <span id="spanCuisine" runat="server">NA</span></p>
                        </div>
                        <div class="restaurant-detail-card-detail-container">
                            <i class="fa fa-globe" style="margin-top: 5px; flex: 0.025;"></i>
                            <p class="restaurant-detail-card-detail-text" style="flex: 0.7;">Website - <span id="spanWebsite" runat="server"></span></p>
                        </div>
                        <div id="dvHalalType" class="restaurant-detail-card-detail-container" runat="server">
                            <i class="fa fa-certificate" style="margin-top: 5px; flex: 0.025;"></i>
                            <p class="restaurant-detail-card-detail-text" style="flex: 0.7;">Halal Assurance - <span id="spanHalaltype" runat="server"></span></p>
                        </div>
                    </div>
                    <div class="restaurant-detail-card-right">
                        <%--<img class="restaurant-detail-card-image" runat="server"
                            src="https://images.unsplash.com/photo-1414235077428-338989a2e8c0?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1050&q=80">--%>
                        <img id="imgRestaurantImage" class="restaurant-detail-card-image" runat="server" src="">
                    </div>
                </div>
            </div>
        </div>

        <div class="section" style="padding-top: 0px !important;">
            <div class="card-white-background">
                <input id="tab1" type="radio" name="tabs" checked>
                <label class="label-tab" for="tab1">Deals</label>

                <input id="tab2" type="radio" name="tabs">
                <label class="label-tab" for="tab2">About</label>

                <input id="tab3" type="radio" name="tabs">
                <label class="label-tab" for="tab3">FAQ</label>

                <!--                        <input id="tab3" type="radio" name="tabs">-->
                <!--                        <label class="label-tab" for="tab3">Photos</label>-->

                <section class="tab" style="padding: 15px;" id="content1">
                    <div class="restaurant-deals-container">
                        <div class="restaurant-deals-left">
                            <%--<p class="restaurant-deals-bought">20 Bought</p>--%>
                            <p class="restaurant-deals-title">
                                <span id="spanDeal" runat="server">No Promotion at the moment.</span>
                                <span id="spanDealMore" runat="server"></span>
                            </p>
                            <div class="restaurant-deals-specification-container">
                                <%--<p class="restaurant-deals-specification">Valid for : <span class="restaurant-deals-specification-value">1 Person</span> |</p>
                                    <p class="restaurant-deals-specification">Valid on : <span class="restaurant-deals-specification-value">All Days</span> |</p>
                                    <p class="restaurant-deals-specification">Timings : <span class="restaurant-deals-specification-value">11 AM - 11 PM</span></p>--%>
                            </div>
                        </div>

                        <%--<div class="restaurant-deals-right">
                            <p class="restaurant-deals-discount">40% OFF</p>
                            <p class="restaurant-deals-price-after">
                                <span class="restaurant-deals-price-before">$15</span>
                                $9
                            </p>
                            <p class="restaurant-deals-tax">Inc. of all Taxes</p>
                            <!--                                    <button class="restaurant-deals-button">-->
                            <!--                                        <span class="restaurant-deals-button-text">Add</span>-->
                            <!--                                        <span class="restaurant-deals-button-text">+</span>-->
                            <!--                                    </button>-->
                        </div>--%>
                    </div>

                    <%--<div class="restaurant-deals-container">
                        <div class="restaurant-deals-left">
                            <p class="restaurant-deals-bought">20 Bought</p>
                            <p class="restaurant-deals-title">1 Classic Donut Donut + 1 Cappucino</p>
                            <div class="restaurant-deals-specification-container">
                                <p class="restaurant-deals-specification">Valid for : <span class="restaurant-deals-specification-value">1 Person</span> |</p>
                                <p class="restaurant-deals-specification">Valid on : <span class="restaurant-deals-specification-value">All Days</span> |</p>
                                <p class="restaurant-deals-specification">Timings : <span class="restaurant-deals-specification-value">11 AM - 11 PM</span></p>
                            </div>
                        </div>

                        <div class="restaurant-deals-right">
                            <p class="restaurant-deals-discount">40% OFF</p>
                            <p class="restaurant-deals-price-after">
                                <span class="restaurant-deals-price-before">$15</span>
                                $9
                            </p>
                            <p class="restaurant-deals-tax">Inc. of all Taxes</p>
                            <!--                                    <button class="restaurant-deals-button">-->
                            <!--                                        <span class="restaurant-deals-button-text">Add</span>-->
                            <!--                                        <span class="restaurant-deals-button-text">+</span>-->
                            <!--                                    </button>-->
                        </div>
                    </div>

                    <div class="restaurant-deals-container">
                        <div class="restaurant-deals-left">
                            <p class="restaurant-deals-bought">20 Bought</p>
                            <p class="restaurant-deals-title">1 Classic Donut Donut + 1 Cappucino</p>
                            <div class="restaurant-deals-specification-container">
                                <p class="restaurant-deals-specification">Valid for : <span class="restaurant-deals-specification-value">1 Person</span> |</p>
                                <p class="restaurant-deals-specification">Valid on : <span class="restaurant-deals-specification-value">All Days</span> |</p>
                                <p class="restaurant-deals-specification">Timings : <span class="restaurant-deals-specification-value">11 AM - 11 PM</span></p>
                            </div>
                        </div>

                        <div class="restaurant-deals-right">
                            <p class="restaurant-deals-discount">40% OFF</p>
                            <p class="restaurant-deals-price-after">
                                <span class="restaurant-deals-price-before">$15</span>
                                $9
                            </p>
                            <p class="restaurant-deals-tax">Inc. of all Taxes</p>
                            <!--                                    <button class="restaurant-deals-button">-->
                            <!--                                        <span class="restaurant-deals-button-text">Add</span>-->
                            <!--                                        <span class="restaurant-deals-button-text">+</span>-->
                            <!--                                    </button>-->
                        </div>
                    </div>--%>
                </section>

                <section class="tab" style="padding: 15px;" id="content2">
                    <span id="spanDescription" runat="server">No description available.</span>
                    <%--<div class="restaurant-about-container">
                        <h3>About Jake's Charbroil Steaks</h3>

                        <p>
                            Bacon ipsum dolor sit amet landjaeger sausage brisket, jerky drumstick fatback boudin.
                        </p>
                        <p>
                            Jerky jowl pork chop tongue, kielbasa shank venison. Capicola shank pig ribeye leberkas filet mignon brisket beef kevin tenderloin porchetta. Capicola fatback venison shank kielbasa, drumstick ribeye landjaeger beef kevin tail meatball pastrami prosciutto pancetta. Tail kevin spare ribs ground round ham ham hock brisket shoulder. Corned beef tri-tip leberkas flank sausage ham hock filet mignon beef ribs pancetta turkey.
                        </p>

                        <h5>Average Cost</h5>

                        <p style="color: #000;">$5</p>

                        <h5>Address</h5>
                        <table>
                            <tr>
                                <td style="width: 5.5vw;">
                                    <p>Street</p>
                                </td>
                                <td style="width: 10px;">
                                    <p>:</p>
                                </td>
                                <td style="width: 50vw;">
                                    <p>231 Jln Mahkota Taman Maluri 55100</p>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 5.5vw;">
                                    <p>City (Town)</p>
                                </td>
                                <td style="width: 10px;">
                                    <p>:</p>
                                </td>
                                <td style="width: 50vw;">
                                    <p>Kuala Lumpur</p>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 5.5vw;">
                                    <p>State (Area)</p>
                                </td>
                                <td style="width: 10px;">
                                    <p>:</p>
                                </td>
                                <td style="width: 50vw;">
                                    <p>Wilayah Persekutuan</p>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 5.5vw;">
                                    <p>Zip Code</p>
                                </td>
                                <td style="width: 10px;">
                                    <p>:</p>
                                </td>
                                <td style="width: 50vw;">
                                    <p>55100</p>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 5.5vw;">
                                    <p>Country</p>
                                </td>
                                <td style="width: 10px;">
                                    <p>:</p>
                                </td>
                                <td style="width: 50vw;">
                                    <p>Malaysia</p>
                                </td>
                            </tr>
                        </table>

                        <h5>Phone no.</h5>

                        <p>+62 82217461029</p>
                    </div>

                    <div class="restaurant-about-container">
                        <h3>Jake's Charbroil Steaks Timings</h3>
                        <table>
                            <tr>
                                <td style="width: 10vw;">
                                    <p>MONDAY</p>
                                </td>
                                <td style="width: 50vw;">
                                    <p>
                                        <strong>9 AM - 11 PM</strong>
                                    </p>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 10vw;">
                                    <p>TUESDAY</p>
                                </td>
                                <td style="width: 50vw;">
                                    <p>
                                        <strong>9 AM - 11 PM</strong>
                                    </p>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 10vw;">
                                    <p>WEDNESDAY</p>
                                </td>
                                <td style="width: 50vw;">
                                    <p>
                                        <strong>9 AM - 11 PM</strong>
                                    </p>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 10vw;">
                                    <p>THURSDAY</p>
                                </td>
                                <td style="width: 50vw;">
                                    <p>
                                        <strong>9 AM - 11 PM</strong>
                                    </p>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 10vw;">
                                    <p>FRIDAY</p>
                                </td>
                                <td style="width: 50vw;">
                                    <p>
                                        <strong>9 AM - 11 PM</strong>
                                    </p>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 10vw;">
                                    <p>SATURDAY</p>
                                </td>
                                <td style="width: 50vw;">
                                    <p>
                                        <strong>9 AM - 11 PM</strong>
                                    </p>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 10vw;">
                                    <p>SUNDAY</p>
                                </td>
                                <td style="width: 50vw;">
                                    <p>
                                        <strong>9 AM - 11 PM</strong>
                                    </p>
                                </td>
                            </tr>
                        </table>
                    </div>--%>
                </section>

                <section class="tab" style="padding: 15px;" id="content3">
                    <span id="spanFAQ" runat="server">No description available.</span>
                </section>
            </div>
        </div>
    </div>
</asp:Content>
