﻿using Giift.SuciHalal.Entities;
using Giift.SuciHalal.Helper;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SuciHalalWebclient
{
    public partial class About : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!Page.IsPostBack)
            {
                BindAboutUsContent();
            }
        }

        public void BindAboutUsContent()
        {
            string lstrContent = ApiHelper.RetrieveContent(Convert.ToString(ConfigurationManager.AppSettings["AboutRetailerId"]), "about");
            JToken jTokenOuter = JToken.Parse(lstrContent);
            JObject lobjJObject = ((JObject)jTokenOuter["retailer_content"]);
            RetailerContent retailerContent = JsonConvert.DeserializeObject<RetailerContent>(((JProperty)lobjJObject.First).Value.ToString());
            if (!string.IsNullOrEmpty(retailerContent.value))
            {
                divAbout.InnerHtml = "";
                divAbout.InnerHtml = retailerContent.value;
            }
        }
    }
}