﻿using Framework.EnterpriseLibrary.Adapters;
using Giift.SuciHalal.Entities;
using Giift.SuciHalal.Helper;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SuciHalalWebclient
{
    public partial class Travel : System.Web.UI.Page
    {
        public static string lstrAccessToken = string.Empty;
        public static List<ProductDetails> lobjListOfProductDetails = null;

        
        public List<ProductDetails> GetListOfProducts()
        {
            try
            {
                lstrAccessToken = Convert.ToString(HttpContext.Current.Session["AccessToken"]);
                string lstrBuyableListResponse = string.Empty;
                List<ProductDetails> lobjAllProductDetails = new List<ProductDetails>();

                Dictionary<string, string> ldctFilterItems = new Dictionary<string, string>();

                ldctFilterItems.Add("buying_with", Convert.ToString(ConfigurationManager.AppSettings["BuyingWith"]));
                //ldctFilterItems.Add("currency", "myr"); // removed myr
                ldctFilterItems.Add("type", "card");
                ldctFilterItems.Add("category",HttpUtility.UrlPathEncode("Travel and Hotel"));

                lstrBuyableListResponse = ApiHelper.BuyableList(ldctFilterItems);

                if (!string.IsNullOrEmpty(lstrBuyableListResponse))
                {
                    dynamic lobjTravelpackages = JsonConvert.DeserializeObject(lstrBuyableListResponse);
                }

                lstrBuyableListResponse = ApiHelper.BuyableList(ldctFilterItems);
                JToken jTokenOuter = JToken.Parse(lstrBuyableListResponse);
                if (jTokenOuter["unauthorized"] != null)
                {
                    lobjAllProductDetails = null;
                }
                else if (jTokenOuter["payload"] != null)
                {
                    JArray array = jTokenOuter.Value<JArray>("payload");
                    foreach (JObject parsedObject in array.Children<JObject>())
                    {
                        ProductDetails lobjProductDetails = new ProductDetails();
                        int count = 0;
                        foreach (JProperty parsedProperty in parsedObject.Properties())
                        {
                            string propertyName = parsedProperty.Name;
                            if (propertyName.Equals("underlying_id"))
                            {
                                lobjProductDetails.Id = (string)parsedProperty.Value;
                                count++;
                            }
                            else if (propertyName.Equals("name"))
                            {
                                lobjProductDetails.Name = (string)parsedProperty.Value;
                                count++;
                            }
                            else if (propertyName.Equals("units"))
                            {
                                JToken priceArray = parsedProperty.Value[0];
                                if ((priceArray != null) && (priceArray.Value<string>("price") != null))
                                {
                                    lobjProductDetails.Price = priceArray.Value<string>("price");
                                    count++;
                                }
                                if((priceArray != null) && (priceArray.Value<string>("unit") != null))
                                {
                                    lobjProductDetails.Currency = priceArray.Value<string>("unit");
                                    count++;
                                }
                            }
                            else if (propertyName.Equals("imgs"))
                            {
                                foreach (dynamic imageUrl in parsedProperty.Value)
                                {
                                    lobjProductDetails.ImageUrl = imageUrl.Value;
                                    count++;
                                    break;
                                }
                            }
                            else if (propertyName.Equals("categories"))
                            {
                                lobjProductDetails.Category = Convert.ToString(((JValue)parsedProperty.Value[0]).Value);
                            }
                            if (count == 6)
                            {
                                break;
                            }
                        }
                        lobjAllProductDetails.Add(lobjProductDetails);
                    }
                }
                else
                {
                    lobjAllProductDetails = null;
                }


                if (lobjAllProductDetails != null && lobjAllProductDetails.Count > 0)
                {
                    return lobjAllProductDetails;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("Travel.aspx GetListOfProducts :" + ex.Message + Environment.NewLine + ex.StackTrace);
                return null;
            }
        }

        [WebMethod]
        public static ProductDetailsData BindTravelPackages(int pintSkipRecords, int pintTakeRecords)
        {
            try
            {
                Travel lobjTravel = new Travel();
                ProductDetailsData lobjProductDetailsData = new ProductDetailsData();

                if (pintSkipRecords == 0)
                {
                    lobjListOfProductDetails = null;
                    lobjListOfProductDetails = new List<ProductDetails>();
                    lobjListOfProductDetails = lobjTravel.GetListOfProducts();
                }

                if (lobjListOfProductDetails != null && lobjListOfProductDetails.Count > 0)
                {
                    var query = (from ProductDetails in lobjListOfProductDetails
                                 select ProductDetails)
                                .Skip(pintSkipRecords)
                                .Take(pintTakeRecords)
                                .ToList();

                    lobjProductDetailsData.PDetails = query;
                    int lintProductsCount = (from p in lobjListOfProductDetails
                                             select p).Count();
                    double pageCount = (lintProductsCount / 15) + 1;

                    lobjProductDetailsData.TotalPages = Math.Ceiling(pageCount);

                    return lobjProductDetailsData;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("Travel.aspx.cs : BindTravelsPackages :" + ex.Message + Environment.NewLine + ex.StackTrace);
                return null;
            }
        }
    }
}