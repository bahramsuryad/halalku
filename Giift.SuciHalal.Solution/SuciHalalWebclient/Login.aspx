﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SuciHalalSite.Master" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="SuciHalalWebclient.Login" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="dashboard">
        <div class="w-row" style="margin-top: 200px;">
            <div class="w-col w-col-3"></div>
            <div class="w-col w-col-6">
                <div class="w-row">
                    <div class="w-col w-col-12">
                        <div class="form-margin">
                            <input class="subscribe-field-2 full w-input" data-name="email"
                                id="txtEmail" maxlength="256" name="Email"
                                placeholder="Enter your email" required type="text">
                        </div>
                    </div>
                </div>
                <div class="top-margin-2 w-row">
                    <div class="w-col w-col-12">
                        <div class="form-margin">
                            <input class="subscribe-field-2 full w-input" data-name="password"
                                id="txtPassword" maxlength="256" name="Password"
                                placeholder="Enter your password" required type="password">
                        </div>
                    </div>
                </div>
                <div class="top-margin-2 w-row" style="display: flex; flex-direction: row; justify-content: center;">
                    <a class="nav-link w-nav-link w--current" href="ForgotPassword.aspx"
                        style="max-width: 1200px; text-align: center;">Forgot Password?</a>
                </div>
                <div class="top-margin-2 medium">
                    <div>
                        <input class="checkout-button full-button w-button" data-wait="Please wait..." type="submit" value="Login" onclick="var retvalue = SigninUser(); event.returnValue = retvalue; return retvalue;">
                    </div>
                </div>
                <div class="top-margin-3 w-row marginBottom">
                    <div class="w-col w-col-6" style="display: flex; justify-content: center; align-items: center; height: 65px;">
                        <p style="text-align: center;">Not yet a Member?</p>
                    </div>
                    <div class="w-col w-col-6">
                        <a class="checkout-button full-button w-button" style="text-align: center; color:white !important" href="Register.aspx">Register
                                <%--<button class="checkout-button full-button w-button" formnovalidate>Register</button>--%>
                        </a>
                    </div>
                </div>
            </div>
            <div class="w-col w-col-3"></div>
        </div>
    </div>
    <div id="divLoader" class="divLoader" style="display: none;"></div>
    <script src="js/jquery-3.3.1.min.js"></script>
    <script type="text/javascript">

        $("#txtEmail").focusout(function () {
            if ($(this).val() == "") {
                $(this).addClass('w-commerce-commercecheckouterrorstate');
            }
            else {
                $(this).removeClass('w-commerce-commercecheckouterrorstate');
            }

        });

        $("#txtPassword").focusout(function () {
            if ($(this).val() == "") {
                $(this).addClass('w-commerce-commercecheckouterrorstate');
            }
            else {
                $(this).removeClass('w-commerce-commercecheckouterrorstate');
            }
        });

        function SigninUser() {
            var memberEmail = $("#txtEmail").val();
            var memberPassword = $("#txtPassword").val();

            if (memberEmail == "") {
                $("#txtEmail").addClass('w-commerce-commercecheckouterrorstate');

            }
            else if (memberPassword == "") {
                $("#txtPassword").addClass('w-commerce-commercecheckouterrorstate');
            }
            else {

                $('#divLoader').show();

                $("#txtEmail").removeClass('w-commerce-commercecheckouterrorstate');
                $("#txtPassword").removeClass('w-commerce-commercecheckouterrorstate');

                $.ajax({
                    url: 'Login.aspx/SigninUser',
                    type: 'POST',
                    contentType: 'application/json; charset =utf-8',
                    data: "{'pstrMemberEmail':'" + memberEmail.toString() + "'" + "," + "'pstrMemberPassword':'" + memberPassword.toString() + "'}",
                    dataType: 'json',
                    success: function (data) {
                        $('#divLoader').hide();
                        try {
                            var newData = data.d;

                            if ((newData != "") || newData == null) {
                                if (newData == "SUCCESS") {
                                    var url = GetParameterValues('Callbackurl');
                                    if (url != null && url != "") {
                                        window.location.href = url;
                                    }
                                    else {
                                        window.location.href = "Index.aspx";
                                    }

                                }
                                else {
                                    swal(" ", "User Id or Password incorrect.", "error");
                                }
                            }
                            else {
                                swal("Oops!", "Something went wrong.", "error");
                            }
                        }
                        catch (e) {
                            swal("Oops!", "Something went wrong.", "error");
                            return false;
                        }
                        return false;
                    },
                    error: function (errmsg) {
                        $('#divLoader').hide();
                        swal("Oops!", "Something went wrong.", "error");
                    }
                });
            }
        };

        function GetParameterValues(param) {
            var url = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
            for (var i = 0; i < url.length; i++) {
                //var urlparam = url[i].split('=');  
                ////var urlparam = url[i].substring(url[i].indexOf('=') + 1);
                ////var urlparam = url[i].slice(url[i].indexOf("=")+1 );
                //if (urlparam[0] == param) {  
                //    return urlparam[1];  
                //}  

                var urlKey = url[i].slice(0, url[i].indexOf('='));
                if (urlKey == param) {
                    return url[i].slice(url[i].indexOf('=') + 1);
                }
            }
        }


    </script>
</asp:Content>
