﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SuciHalalSite.Master" AutoEventWireup="true" CodeBehind="DonationDetail.aspx.cs" Inherits="SuciHalalWebclient.DonationDetail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <input id="hdfAccessToken" type="hidden" value='<%= Session["AccessToken"] %>' />
    <input id="hdfUnit" type="hidden" value='' />
    <div class="tools_list" style="background-color: #eee;">
        <div class="banner sub"
            style="background-image: -webkit-gradient(linear, 0% 0%, 0% 100%, from(rgba(0, 0, 0, 0.3)), to(rgba(0, 0, 0, 0.3))), url('https://images.unsplash.com/photo-1506869673173-31eba16560ab?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1050&q=80'); background-size: cover; background-position: center 80%;">
            <div class="container center in-sub w-container">
                <h1 class="sub-heading-title">Donation Detail</h1>
                <div class="breadcrumb">
                    <div class="breadcrumb-content">
                        <a class="bread-link" href="index.aspx">Home</a>
                        <img alt="" src="https://uploads-ssl.webflow.com/5c3ca7fbdc8fab3302b31532/5c41f1941b7ac65ffde90630_right-arrow%20(3).svg" width="18">
                        <a class="bread-link" href="donations.aspx">Donations</a>
                        <img alt="" src="https://uploads-ssl.webflow.com/5c3ca7fbdc8fab3302b31532/5c41f1941b7ac65ffde90630_right-arrow%20(3).svg" width="18">
                        <div class="bread-link gray-color">Donation Detail</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="section">
            <div class="row">
                <div class="donation-detail-card">
                    <div class="donation-detail-card-left">
                        <%--<p class="donation-detail-card-certified">Alexandria, VA</p>--%>
                        <p class="donation-detail-card-certified"></p>
                        <p id="pname" class="donation-detail-card-name"></p>
                        <%--<p class="goal">
                            <strong>$50,250</strong>
                            <span class="smaller">of $60,000 goal</span>
                        </p>--%>
                        <%--<div aria-valuemax="100" aria-valuemin="0" aria-valuenow="84" aria-valuetext="84 percent" class="progress large-12" role="progressbar" tabindex="0">
                            <div class="progress-meter" style="width: 84%"></div>
                        </div>--%>
                        <div class="travel-detail-card-button-container">
                            <fieldset class="w-donation-checkoutblockcontent">
                                <label class="w-donation-checkoutlabel field-label-2">Units *</label>
                                <asp:DropDownList ID="ddlUnit" runat="server" CssClass="w-commerce-commercecheckoutshippingfullname">
                                    <asp:ListItem Selected="True" Text="--Select Unit--" Value="0"></asp:ListItem>
                                </asp:DropDownList>
                                <%--<select id="ddlUnit" class="w-commerce-commercecheckoutshippingfullname">
                                    <option value="0">--Select Unit--</option>
                                </select>--%>
                                <div id="divCustomAmount" style="display:none">
                                    <label class="w-donation-checkoutlabel field-label-2">Enter Custom Units *</label>
                                    <input id="txtCustomQty" type="number" name="amount" class="w-commerce-commercecheckoutshippingfullname">
                                </div>
                                <label class="w-donation-checkoutlabel field-label-2">Full Name *</label>
                                <input id="txtName" type="text" name="email" class="w-commerce-commercecheckoutshippingfullname">

                                <label class="w-donation-checkoutlabel field-label-2">Email *</label>
                                <input runat="server" id="txtEmail" type="text" name="email" class="w-commerce-commercecheckoutshippingfullname" disabled>

                                <a class="donation-list-card-book-now" style="color: white;" onclick="var retvalue = DonateNow(); event.returnValue = retvalue; return retvalue;" href="">Donate Now</a>
                            </fieldset>
                        </div>
                    </div>
                    <div class="restaurant-detail-card-right">
                        <img id="img_Donation" class="restaurant-detail-card-image" src="" />
                    </div>
                </div>
            </div>
        </div>

        <div class="section" style="padding-top: 0px !important;">
            <div class="card-white-background">
                <input id="tab1" type="radio" name="tabs" checked>
                <label class="label-tab" for="tab1">Description</label>

                <input id="tab2" type="radio" name="tabs">
                <label class="label-tab" for="tab2">FAQ</label>

                <section class="tab" style="padding: 15px;" id="content1">
                    <%--<div class="restaurant-deals-container">
                        <div class="restaurant-deals-left">
                            <p class="restaurant-deals-title">
                                <span id="spanDeal" runat="server">No Promotion at the moment.</span>
                                <span id="spanDealMore" runat="server"></span>
                            </p>
                            <div id="dvDescription" class="restaurant-deals-specification-container">                                
                            </div>
                        </div>
                    </div>  --%>
                    <div class="card-white-background">
                        <div id="dvDescription" class="content-spacingbox bottomradius">
                        </div>
                    </div>
                </section>

                <section class="tab" style="padding: 15px;" id="content2">
                    <div class="card-white-background">
                        <div id="dvTnc" class="content-spacingbox bottomradius">
                        </div>
                    </div>
                </section>
            </div>
        </div>

        <%--<div style="width: 100vw; padding-left: 10vw; padding-right: 10vw; margin: 10vh 0vh;">
            <div class="card-white-background">
                <label class="contenttag-Font show-on-medium-and-down">Description</label>
                <div id="dvDescription" class="content-spacingbox bottomradius">
                </div>
            </div>
        </div>--%>
    </div>
    <div id="divLoader" class="divLoader" style="display: none;"></div>
    <script src="js/jquery-3.3.1.min.js"></script>
    <script type="text/javascript">
        function getUrlParameter(name) {
            name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
            var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
            var results = regex.exec(location.search);
            return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
        };

        $(document).ready(function () {
            var Id = getUrlParameter("id");
            var AccessToken = $('#hdfAccessToken').val();

            if (AccessToken.trim() != "") {

                $('#divLoader').show();
                $.ajax({
                    type: "POST",
                    url: "DonationDetail.aspx/BindDonation",
                    data: "{pstrId:'" + Id + "', pstrAccessToken:'" + AccessToken + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    async: true,
                    cache: false,
                    success: function (msg) {
                        $('#divLoader').hide();
                        if (msg.d != null) {
                            $('#pname').html(msg.d.name);
                            $("#img_Donation").attr("src", msg.d.imgs[0]);
                            $('#dvDescription').html(msg.d.description);
                            var faqWorking = "<p><strong>How It Works?</strong></p>"
                                + "<p>Flow of Donation Funds: Donors donate to their choice of campaign category using their Giift account. The money is then transferred to GlobalSadaqah, our partner. GlobalSadaqah allocates the transferred donation to the most needy campaign under the chosen category which is live at the moment on the platform.</p>"
                                + "<p>Campaign Updates: Once the campaign has been completed and the funds have been transferred to the charity partner, GlobalSadaqah follows up regularly for updates, usage of funds and development in the case of the beneficiary. As and when updates are made available by the charity partner, GlobalSadaqah notifies the donors in a number of ways.</p>"
                                + "<p>The campaign updates are notified via:</br>"
                                + "1. The respective campaign page with the entire campaign timeline.</br>"
                                + "2. An email is sent out to all the donors.</br>"
                                + "3. WhatsApp updates are sent to those who have opted for it.</br></p>"
                                + "<p>Tax Exemption Receipts : This is available for Malaysia only as of now. If the campaign to which the donations are being channeled to provides tax exemption receipts, the donors will be sent an email asking them for their details. A hard copy of the receipt will be sent by the charity partner once the process is complete.</p>"
                            if (msg.d.tc != null) {
                                $('#dvTnc').html(msg.d.tc);
                                $('#dvTnc').append(faqWorking);
                            } else {
                                $('#dvTnc').html(faqWorking);
                            }

                            $.each(msg.d.units, function (data, value) {
                                $('#hdfUnit').val(value.unit);
                                $("#ContentPlaceHolder1_ddlUnit").append($("<option></option>").val(value.price).html(' ' + value.unit.toUpperCase() + ' ' + value.price));
                            });
                            //$("#ContentPlaceHolder1_ddlUnit").append($("<option></option>").val(1000).html('Custom'));
                        } else {
                            swal("Oops!", "Something went wrong.", "error");
                            setTimeout(function () {
                                window.location.href = "Donations.aspx";
                            }, 2000);

                        }
                    },
                    error: function (error) {
                        swal("Oops!", "Something went wrong.", "error");
                        $('#divLoader').hide();
                    }
                });
            } else {
                window.location.href = "Login.aspx?Callbackurl=DonationDetail.aspx?id=" + Id;
            }
        });

        $(document.body).on('change', '#<%=ddlUnit.ClientID %>', function () {
            var amountValue = $("#ContentPlaceHolder1_ddlUnit").val();
            if (amountValue == 1000) {
                $('#divCustomAmount').show();
            }
            else {
                $('#divCustomAmount').hide();
            }
        });

        function DonateNow() {

            var lstrId = getUrlParameter("id");
            var txtName = $("#txtName").val();
            var txtEmail = $("#ContentPlaceHolder1_txtEmail").val();
            var qty = $("#ContentPlaceHolder1_ddlUnit").val();
            var unit = $('#hdfUnit').val();
            var price = $("#ContentPlaceHolder1_ddlUnit option:selected").val();
            var customQty = $("#txtCustomQty").val();

            $('#divLoader').show();

            var msg = '';

            if (txtName.trim() == "") {
                msg += "Please enter name. ";
            }
            if (txtEmail.trim() == "" || !validateEmail(txtEmail)) {
                msg += "Please enter valid Email Id. ";
            }
            if (qty == 0) {
                msg += "Please select units.";
            }

            if (qty == 1000 && customQty.trim() == '')
            {
                 msg += "Please enter custom amount."
            }

            if (msg != '') {
                swal(" ", msg, "warning");
                $('#divLoader').hide();
            }

            
            else {
                var amount = 0;
                if (qty == 1000) {
                    amount = customQty;
                }
                else {
                    amount = qty;
                }
                $.ajax({
                    url: 'DonationDetail.aspx/DonateNow',
                    type: 'POST',
                    contentType: 'application/json; charset =utf-8',
                    data: "{'pstrId':'" + lstrId + "','pstrName':'" + txtName + "','pstrEmail':'" + txtEmail + "','pintValue':" + amount + ",'pstrUnit':'" + unit + "','pstrPrice':'" + price + "'}",
                    dataType: 'json',
                    success: function (data) {
                        $('#divLoader').hide();

                        var newData = data.d;
                        if ((newData != "") || newData == null) {

                            if (newData == "SUCCESS") {
                                window.location.href = "PostPayment.aspx";
                            } else {
                                swal("Oops!", "Something went wrong.", "error");
                            }
                        } else {
                            alert("Oops, something went wrong!")
                        }

                        return false;
                    },
                    error: function (errmsg) {
                        $('#divLoader').hide();
                        swal("Oops!", "Something went wrong.", "error");
                    }
                });
            }
        }

        function validateEmail(email) {
            var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(email);
        }

    </script>
</asp:Content>
