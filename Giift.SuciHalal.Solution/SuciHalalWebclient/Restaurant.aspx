﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SuciHalalSite.Master" AutoEventWireup="true" CodeBehind="Restaurant.aspx.cs" Inherits="SuciHalalWebclient.Restaurant" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="tools_list" style="background-color: #eee;">
        <div class="banner sub"
            style="background-image: -webkit-gradient(linear, 0% 0%, 0% 100%, from(rgba(0, 0, 0, 0.3)), to(rgba(0, 0, 0, 0.3))), url('https://images.unsplash.com/photo-1522413452208-996ff3f3e740?ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80'); background-size: cover; background-position: center 80%;">
            <div class="container center in-sub w-container">
                <h1 class="sub-heading-title">Restaurant</h1>
                <div class="breadcrumb">
                    <div class="breadcrumb-content">
                        <a class="bread-link" href="Index.aspx">Home</a>
                        <img alt="" src="https://uploads-ssl.webflow.com/5c3ca7fbdc8fab3302b31532/5c41f1941b7ac65ffde90630_right-arrow%20(3).svg" width="18">
                        <a class="bread-link" href="Tools.aspx">Tools</a>
                        <img alt="" src="https://uploads-ssl.webflow.com/5c3ca7fbdc8fab3302b31532/5c41f1941b7ac65ffde90630_right-arrow%20(3).svg" width="18">
                        <div class="bread-link gray-color">Restaurant</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="section">
            <h3 style="margin-top: 0px; margin-bottom: 30px;">Nearby Restaurant</h3>
            <div id="divRestaurantList">
            </div>
            <div class="center">
                <ul id="ulPagingItems" class="pagination"></ul>
            </div>
        </div>
    </div>
    <div id="divLoader" class="divLoader" style="display: none;"></div>
    <script src="js/jquery-3.3.1.min.js"></script>
    <script type="text/javascript">
        var lintPageSize = 15;
        var lintTotalPages = 0;
        var latitude = "";
        var longitude = "";

        $(document).ready(function () {
            $("#divRestaurantList").text("");
            getLocation();
        });

        function GetRestaurants() {
            $('#divLoader').show();

            $.ajax({
                type: "POST",
                url: "Restaurant.aspx/GetNearbyRestaurants",
                data: "{pintPageNo:1,pintTakeRecords:10,pstrLatitude:'" + latitude + "',pstrLongitude:'" + longitude + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: true,
                cache: false,
                success: function (msg) {
                    $('#divLoader').hide();
                    if (msg != null) {
                        lintTotalPages = msg.d.TotalPages;
                        if (lintTotalPages > 0) {
                            $("#divRestaurantList").html("");
                            printRestaurantData(msg);

                            $('#ulPagingItems').twbsPagination({
                                totalPages: lintTotalPages,
                                visiblePages: 10,
                                next: 'Next',
                                prev: 'Prev',
                                onPageClick: function (event, page) {
                                    pageData(page);
                                }
                            });
                        }
                        else {
                            $("#divRestaurantList").text("No records were found.");
                        }
                    }
                    else {
                        swal("Oops!", "Something went wrong", "error");
                    }
                },
                error: function () {
                    $('#divLoader').hide();
                    $("#divRestaurantList").text("");
                    $("#divRestaurantList").text("No records were found.");
                }
            });
        }

        function printRestaurantData(msg) {
            $('#divLoader').show();
            var restaurants = msg.d.RDetails;
            var html = "";
            $("#divRestaurantList").text(html);
            var count = 0;
            for (var i = 0; i < restaurants.length; i++) {
                html += '<a href="./RestaurantDetail.aspx?id=' + restaurants[i].Id + '">';
                html += '<div class="row">';
                html += '<div class="restaurant-list-card">';
                html += '<div class="restaurant-list-card-left">';
                //html += '<p class="restaurant-list-card-certified">The owners of this restaurant are Muslim and have confirmed its halal status</p>';
                html += '<p class="restaurant-list-card-name">' + restaurants[i].Name + '</p>';
                html += '<p class="restaurant-list-card-address">' + restaurants[i].Address + '</p>';
                html += '<p class="restaurant-list-card-distance"><b>' + restaurants[i].Distance + ' KM</b> | ' + '' + '</p>';
                html += '</div>';
                html += '<div class="restaurant-list-card-right">';
                //html += '<img class="restaurant-list-card-image" src="https://www.1ummah.org.au/wp-content/uploads/2017/04/Bilal-Bin-Rabah-Masjid.jpg">';
                html += '<img class="restaurant-list-card-image" src="' + restaurants[i].ImageUrl + '">';
                html += '</div>';
                html += '</div>';
                html += '</div>';
                html += '</a>';
            }
            $("#divRestaurantList").append(html);
            $('#divLoader').hide();
        }

        function pageData(e) {
            $('#divLoader').show();
            //var lintSkipRecords = e == 1 ? 0 : (e * lintPageSize) - lintPageSize;
            $.ajax({
                type: "POST",
                url: "Restaurant.aspx/GetNearbyRestaurants",
                data: "{pintPageNo:" + e + ",pintTakeRecords:10,pstrLatitude:'" + latitude + "',pstrLongitude:'" + longitude + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: true,
                cache: false,
                success: function (msg) {
                    $('#divLoader').hide();
                    printRestaurantData(msg);
                },
                error: function () {
                    $('#divLoader').hide();
                    $("#divRestaurantList").text("");
                    $("#divRestaurantList").text("No records were found.");
                }

            });

            return false;
        }

        function getLocation() {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(showPosition, showError);
            } else {
                //x.innerHTML = "Geolocation is not supported by this browser.";
            }
        }

        function showPosition(position) {
            latitude = position.coords.latitude;
            longitude = position.coords.longitude;
            GetRestaurants();
        }

        function showError(error) {
            switch (error.code) {
                case error.PERMISSION_DENIED:
                    //x.innerHTML = "User denied the request for Geolocation."
                    break;
                case error.POSITION_UNAVAILABLE:
                    //x.innerHTML = "Location information is unavailable."
                    break;
                case error.TIMEOUT:
                    //x.innerHTML = "The request to get user location timed out."
                    break;
                case error.UNKNOWN_ERROR:
                    //x.innerHTML = "An unknown error occurred."
                    break;
            }
            latitude = "3.142615";
            longitude = "101.691110";

            GetRestaurants();
        }

    </script>
</asp:Content>
