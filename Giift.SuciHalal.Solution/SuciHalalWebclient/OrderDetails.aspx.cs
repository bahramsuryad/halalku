﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Framework.EnterpriseLibrary.Adapters;
using Giift.SuciHalal.Helper;
using Microsoft.CSharp.RuntimeBinder;
using Newtonsoft.Json;
using Giift.SuciHalal.Entities;

namespace SuciHalalWebclient
{
    public partial class OrderDetails : System.Web.UI.Page
    {
        public static string lstrOrderId = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (HttpContext.Current.Session["AccessToken"] == null)
            {
                Response.Redirect("Login.aspx");
            }
            else
            {
                lstrOrderId = Convert.ToString(Request.QueryString["Id"]);
            }
        }

        [System.Web.Script.Services.ScriptMethod()]
        [System.Web.Services.WebMethod]
        public static OrderHistoryDetails GetOrderDetails()
        {
            try
            {
                string lstrResponse = string.Empty;
                string lstrMessage = string.Empty;
                string lstrAccessToken = Convert.ToString(HttpContext.Current.Session["AccessToken"]);
                lstrResponse = ApiHelper.GetOrderDetails(lstrAccessToken, lstrOrderId);
                OrderHistoryDetails lobjOrderHistoryDetails = null;
                if (lstrResponse != "")
                {
                    //List<Dictionary<string, string>> lobjListOfOrders = new List<Dictionary<string, string>>();
                    dynamic lobjResponse = JsonConvert.DeserializeObject<dynamic>(lstrResponse);
                    if (IsPropertyExist(lobjResponse, "error"))
                    {
                        if (IsPropertyExist(lobjResponse, "error_description"))
                        {
                            lobjOrderHistoryDetails = new OrderHistoryDetails();
                            lobjOrderHistoryDetails.ResponseMessage = lobjResponse.error_description;
                        }
                    }
                    else
                    {
                        //JObject obj = JObject.Parse(lstrResponse);
                        //Dictionary<string, string> lobjOrder = new Dictionary<string, string>();
                        //lobjOrder.Add("Id", obj.SelectToken("Id").ToString());
                        //lobjOrder.Add("Status", obj.SelectToken("Status").ToString());
                        //lobjOrder.Add("value", obj.SelectToken("value").ToString());
                        //lobjOrder.Add("CreatedAt", obj.SelectToken("CreatedAt").ToString());
                        //lobjOrder.Add("CartOrderItems", obj.SelectToken("CartOrderItems").ToString());
                        //lobjOrder.Add("CartDeliveries", obj.SelectToken("CartDeliveries").ToString());
                        lobjOrderHistoryDetails = JsonConvert.DeserializeObject<OrderHistoryDetails>(lstrResponse);
                    }
                }
                return lobjOrderHistoryDetails;
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("GetOrderHistory Ex: " + ex.Message + Environment.NewLine + ex.InnerException + Environment.NewLine + ex.StackTrace);
                return null;
            }
        }

        public static bool IsPropertyExist(dynamic dynamicObj, string property)
        {
            bool lblnStatus = false;
            try
            {
                var value = dynamicObj[property].Value;
                lblnStatus = true;
            }
            catch (RuntimeBinderException)
            {
                lblnStatus = false;
            }
            catch (Exception)
            {
                lblnStatus = false;
            }
            return lblnStatus;
        }
    }
}