﻿using Framework.EnterpriseLibrary.Adapters;
using Giift.SuciHalal.Entities;
using Giift.SuciHalal.Helper;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SuciHalalWebclient
{
    public partial class Profile : System.Web.UI.Page
    {
        public static string lstrAccessToken = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (HttpContext.Current.Session["AccessToken"] != null)
                {
                    lstrAccessToken = Convert.ToString(HttpContext.Current.Session["AccessToken"]);
                }
                else
                {
                    Response.Redirect("Login.aspx");
                }
            }
        }

        [System.Web.Script.Services.ScriptMethod()]
        [System.Web.Services.WebMethod]
        public static UserData GetUserProfile()
        {
            string lstrMessage = string.Empty;
            string lstrResponse = string.Empty;
            UserData lobjUserData = new UserData();
            try
            {
                lstrAccessToken = Convert.ToString(HttpContext.Current.Session["AccessToken"]);
                lstrResponse = ApiHelper.GetUserProfile(lstrAccessToken);
                if (lstrResponse.Contains("error"))
                {
                    JArray outer = JArray.Parse(lstrResponse);
                    if (outer["error"] != null)
                    {
                        lobjUserData.ResponseMessage = outer.Value<string>("error");
                    }
                }
                else
                {
                    List<UserDetails> lobjListOfUserDetails = new List<UserDetails>();
                    lobjListOfUserDetails = JsonConvert.DeserializeObject<List<UserDetails>>(lstrResponse);


                    lobjUserData.UserDetailsList = lobjListOfUserDetails;
                    lobjUserData.ResponseMessage = "SUCCESS";
                }
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("Profile.aspx.cs : GetUserProfile : " + ex.Message + Environment.NewLine + ex.StackTrace);
                return null;
            }

            return lobjUserData;
        }


        [System.Web.Script.Services.ScriptMethod()]
        [System.Web.Services.WebMethod]
        public static string UpdateProfileData(string pstrFirstName, string pstrLastName, string pstrPhoneNo, string pstrDOB, string pstrStreetAddress, string pstrZipCode, string pstrCity, string pstrCountry, string pstrGender, string pstrLanguage)
        {
            string lstrMessage = string.Empty;
            string lstrResponse = string.Empty;
            try
            {
                IFormatProvider culture = new CultureInfo("en-US", true);
                //DateTime dateVal = DateTime.ParseExact(pstrDOB, "yyyy-MM-dd", culture);
                //string newDate = dateVal.ToString();
                string newDate = DateTime.ParseExact(pstrDOB, "yyyyMMdd", null).ToString("yyyy-MM-dd");


                lstrResponse = ApiHelper.UpdateProfile(lstrAccessToken, pstrFirstName, pstrLastName, pstrCity, pstrPhoneNo, newDate, pstrGender, pstrStreetAddress, pstrZipCode, pstrLanguage, string.Empty, pstrCountry, string.Empty);
                JToken outer = JToken.Parse(lstrResponse);
                if (outer["error"] != null)
                {
                    lstrMessage = outer.Value<string>("error");
                }
                else if (outer["id"] != null && !string.IsNullOrEmpty(Convert.ToString(outer["id"])))
                {
                    lstrMessage = "SUCCESS";
                }
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("Profile.aspx.cs : UpdateProfileData : " + ex.Message + Environment.NewLine + ex.StackTrace);
                throw;
            }
            return lstrMessage;
        }
    }
}