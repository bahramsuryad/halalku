﻿using Framework.EnterpriseLibrary.Adapters;
using Giift.SuciHalal.Entities;
using Giift.SuciHalal.Helper;
using Microsoft.CSharp.RuntimeBinder;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SuciHalalWebclient
{
    public partial class TravelDetail : System.Web.UI.Page
    {

        string lstrProductId = string.Empty;
        public static string lstrAccessToken = string.Empty;
        string lstrResponse = string.Empty;
        string lstrDivContent = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (HttpContext.Current.Session["AccessToken"] != null)
            {
                lstrAccessToken = Convert.ToString(HttpContext.Current.Session["AccessToken"]);
                if (Request.QueryString["id"] != null)
                {

                    try
                    {
                        lstrProductId = Convert.ToString(Request.QueryString["id"]);
                        lstrResponse = ApiHelper.BuyableDetails(lstrAccessToken, "card", lstrProductId, ""); // removed myr

                        JObject outer = JObject.Parse(lstrResponse);
                        JArray lobjImageArray = (JArray)outer["imgs"];
                        string[] lstrImageArray = lobjImageArray.Select(jv => (string)jv).ToArray();

                        string lstrProductName = Convert.ToString(((JValue)outer["name"]).Value);
                        string lstrProductType = Convert.ToString(((JValue)outer["type"]).Value);
                        string lstrDescription = Convert.ToString(((JValue)outer["description"]).Value);
                        string lstrTC = Convert.ToString(((JValue)outer["tc"]).Value);

                        Double ldoublePrice = Convert.ToDouble(((JToken)outer["units"][0])["price"]);
                        ldoublePrice = Math.Round((Double)ldoublePrice, 2);

                        //string lstrPrice = "RM " + Convert.ToDecimal(string.Format("{0:F2}", Convert.ToString(((JToken)outer["units"][0])["price"])));
                        string lstrCurrency = Convert.ToString(((JToken)outer["units"][0])["unit"]);

                        hfdProductID.Value = lstrProductId;
                        hfdPrice.Value = Convert.ToString(((JToken)outer["units"][0])["price"]);
                        hfdUnit.Value = Convert.ToString(((JToken)outer["units"][0])["unit"]);
                        hfdValue.Value = Convert.ToString(((JToken)outer["units"][0])["value"]);

                        spanProductName.InnerText = lstrProductName;
                        spanPrice.InnerText = lstrCurrency.ToUpper() + " " + ldoublePrice.ToString();
                        imgTravelrightImage.Src = lstrImageArray[0];
                        divDiscription.InnerHtml = lstrDescription;
                        divTC.InnerHtml = lstrTC;

                        if(HttpContext.Current.Session["MemberEmail"] != null)
                        {
                            txtEmail.Value = Convert.ToString(HttpContext.Current.Session["MemberEmail"]);
                        }

                    }
                    catch (Exception ex)
                    {
                        LoggingAdapter.WriteLog("TravelDetail.aspx.cs : PageLoad : " + ex.Message + Environment.NewLine + ex.StackTrace);
                        Response.Redirect("Travel.aspx");
                    }
                }
                else
                {
                    Response.Redirect("Travel.aspx");
                }
            }
            else
            {
                Response.Redirect("Login.aspx?Callbackurl=TravelDetail.aspx?id=" + Convert.ToString(Request.QueryString["id"]));
            }
        }

        [WebMethod]
        public static string BookNow(string pstrName, string pstrEmail, string pstrProductID, double pdblPrice, string pstrUnit, string pstrValue, int pintQty)
        {
            string lstrResponse = string.Empty;
            double ldblTotalPrice = 0;

            try
            {
                string lstrAccessToken = Convert.ToString(HttpContext.Current.Session["AccessToken"]);
                string lstrCartId = string.Empty;
                string lstrOrderId = string.Empty;
                bool proceed = false;
                string lstrSenderEmail = string.Empty;

                if(HttpContext.Current.Session["MemberEmail"] != null)
                {
                    lstrSenderEmail = Convert.ToString(HttpContext.Current.Session["MemberEmail"]);
                    string lstrNewCartResponse = ApiHelper.NewCart(lstrAccessToken);

                    dynamic lobjNewCartResponse = JsonConvert.DeserializeObject<dynamic>(lstrNewCartResponse);
                    if (IsPropertyExist(lobjNewCartResponse, "error"))
                    {
                        proceed = false;
                        LoggingAdapter.WriteLog("Travel.aspx BookNow NewCart Error:" + lobjNewCartResponse.error_description);
                    }
                    else
                    {
                        proceed = true;
                        lstrCartId = lobjNewCartResponse.Id;
                    }

                    if (proceed)
                    {
                        for (int i = 0; i < pintQty; i++)
                        {
                            string lstrAddCartItem = ApiHelper.AddCartItem(lstrAccessToken, "card", pstrProductID, pstrUnit, (int)Math.Round(decimal.Parse(pstrValue)), lstrCartId);

                            dynamic lobjResponse = JsonConvert.DeserializeObject<dynamic>(lstrAddCartItem);
                            if (IsPropertyExist(lobjResponse, "error"))
                            {
                                proceed = false;
                                LoggingAdapter.WriteLog("Travel.aspx BookNow AddCartItem Error:" + lobjResponse.error_description);
                            }
                            else
                            {
                                if (IsPropertyExist(lobjResponse, "Currency"))
                                {
                                    HttpContext.Current.Session["CartCurrency"] = lobjResponse.Currency.ToString();
                                    proceed = true;
                                }
                            }
                        }
                    }

                    if (proceed)
                    {
                        string lstrCartCheckout = ApiHelper.CartCheckout(lstrAccessToken, lstrCartId);

                        dynamic lobjResponse = JsonConvert.DeserializeObject<dynamic>(lstrCartCheckout);
                        if (IsPropertyExist(lobjResponse, "error"))
                        {
                            proceed = false;
                            LoggingAdapter.WriteLog("Travel.aspx BookNow CartCheckout Error:" + lobjResponse.error_description);
                        }
                        else
                        {
                            proceed = true;
                            lstrOrderId = lobjResponse.Id;
                            ldblTotalPrice = lobjResponse.value;
                            for (int i = 0; i < lobjResponse.CartOrderItems.Count; i++)
                            {
                                string lstr1 = ApiHelper.SetDeliveryEmail(lstrAccessToken, Convert.ToString(lobjResponse.CartOrderItems[i].Id), "email", pstrName, pstrEmail, lstrSenderEmail);
                            }
                        }
                    }

                    if (proceed)
                    {
                        PaymentDetails paymentDetails = new PaymentDetails();
                        paymentDetails.OrderId = lstrOrderId;
                        paymentDetails.Amount = ldblTotalPrice;
                        HttpContext.Current.Session["PaymentDetails"] = paymentDetails;

                        lstrResponse = "SUCCESS";
                    }
                    else
                    {
                        lstrResponse = "FAILED";
                    }
                }
                else
                {
                    lstrResponse = "FAILED";
                }
            }
            catch (Exception ex)
            {
                lstrResponse = "FAILED";
                LoggingAdapter.WriteLog("TravelDetail.aspx.cs BookNow:" + ex.Message + Environment.NewLine + "StackTrace:" + ex.StackTrace + Environment.NewLine + "InnerException:" + ex.InnerException);
            }

            return lstrResponse;
        }


        public static bool IsPropertyExist(dynamic dynamicObj, string property)
        {
            bool lblnStatus = false;
            try
            {
                var value = dynamicObj[property].Value;
                lblnStatus = true;
            }
            catch (RuntimeBinderException)
            {
                lblnStatus = false;
            }
            catch (Exception)
            {
                lblnStatus = false;
            }
            return lblnStatus;
        }

    }
}