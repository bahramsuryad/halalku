﻿using Framework.EnterpriseLibrary.Adapters;
using Giift.SuciHalal.Entities;
using Giift.SuciHalal.Helper;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SuciHalalWebclient
{
    public partial class Donations : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }
        public static List<ProductDetails> lobjListOfProductDetails = null;

        public static List<ProductDetails> GetListOfDonations()
        {
            try
            {
                //lstrAccessToken = Convert.ToString(HttpContext.Current.Session["AccessToken"]);
                string lstrBuyableListResponse = string.Empty;
                List<ProductDetails> lobjAllProductDetails = new List<ProductDetails>();


                Dictionary<string, string> ldctFilterItems = new Dictionary<string, string>();
                ldctFilterItems.Add("buying_with", Convert.ToString(ConfigurationManager.AppSettings["BuyingWith"]));
                ldctFilterItems.Add("type", "card");
                ldctFilterItems.Add("category", "Charity");
                //ldctFilterItems.Add("currency", "myr"); // removed myr


                lstrBuyableListResponse = ApiHelper.BuyableList(ldctFilterItems);
                JToken jTokenOuter = JToken.Parse(lstrBuyableListResponse);
                if (jTokenOuter["unauthorized"] != null)
                {
                    lobjAllProductDetails = null;
                }
                else if (jTokenOuter["payload"] != null)
                {
                    JArray array = jTokenOuter.Value<JArray>("payload");
                    foreach (JObject parsedObject in array.Children<JObject>())
                    {
                        ProductDetails lobjProductDetails = new ProductDetails();
                        int count = 0;
                        foreach (JProperty parsedProperty in parsedObject.Properties())
                        {
                            string propertyName = parsedProperty.Name;
                            if (propertyName.Equals("underlying_id"))
                            {
                                lobjProductDetails.Id = (string)parsedProperty.Value;
                                count++;
                            }
                            else if (propertyName.Equals("name"))
                            {
                                lobjProductDetails.Name = (string)parsedProperty.Value;
                                count++;
                            }
                            else if (propertyName.Equals("units"))
                            {
                                JToken priceArray = parsedProperty.Value[0];
                                if ((priceArray != null) && (priceArray.Value<string>("price") != null))
                                {
                                    lobjProductDetails.Price = priceArray.Value<string>("price");
                                    count++;
                                }
                                if ((priceArray != null) && (priceArray.Value<string>("unit") != null))
                                {
                                    lobjProductDetails.Currency = priceArray.Value<string>("unit");
                                    count++;
                                }
                            }
                            else if (propertyName.Equals("imgs"))
                            {
                                foreach (dynamic imageUrl in parsedProperty.Value)
                                {
                                    //if (imageUrl.Value.Contains("_thumb"))
                                    //{
                                    lobjProductDetails.ImageUrl = imageUrl.Value;
                                    count++;
                                    break;
                                    //}
                                }
                            }
                            else if (propertyName.Equals("categories"))
                            {
                                lobjProductDetails.Category = Convert.ToString(((JValue)parsedProperty.Value[0]).Value);
                            }
                            if (count == 5)
                            {
                                break;
                            }
                        }
                        lobjAllProductDetails.Add(lobjProductDetails);
                    }
                }
                else
                {
                    lobjAllProductDetails = null;
                }


                if (lobjAllProductDetails != null && lobjAllProductDetails.Count > 0)
                {
                    return lobjAllProductDetails;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("Catalogue.aspx GetListOfProducts :" + ex.Message + Environment.NewLine + ex.StackTrace);
                return null;
            }
        }

        [WebMethod]
        public static ProductDetailsData BindDonations(int pintSkipRecords, int pintTakeRecords)
        {
            try
            {
                ProductDetailsData lobjProductDetailsData = new ProductDetailsData();

                if (pintSkipRecords == 0)
                {
                    lobjListOfProductDetails = null;
                    lobjListOfProductDetails = new List<ProductDetails>();
                    lobjListOfProductDetails = GetListOfDonations();
                }

                if (lobjListOfProductDetails != null && lobjListOfProductDetails.Count > 0)
                {
                    var query = (from ProductDetails in lobjListOfProductDetails
                                 select ProductDetails)
                                .Skip(pintSkipRecords)
                                .Take(pintTakeRecords)
                                .ToList();

                    lobjProductDetailsData.PDetails = query;
                    int lintProductsCount = (from p in lobjListOfProductDetails
                                             select p).Count();
                    double pageCount = (lintProductsCount / 15) + 1;

                    lobjProductDetailsData.TotalPages = Math.Ceiling(pageCount);

                    return lobjProductDetailsData;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("Donations.aspx.cs : BindDonations :" + ex.Message + Environment.NewLine + ex.StackTrace);
                return null;
            }
        }
    }


}