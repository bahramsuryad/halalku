﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Framework.EnterpriseLibrary.Adapters;
using Giift.SuciHalal.Helper;
using Microsoft.CSharp.RuntimeBinder;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace SuciHalalWebclient
{
    public partial class ForgotPassword : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [System.Web.Script.Services.ScriptMethod()]
        [System.Web.Services.WebMethod]
        public static string ForgotPwd(string pstrMemberEmail)
        {
            string lstrResponse = string.Empty;
            string lstrMessage = string.Empty;

            try
            {
                lstrResponse = ApiHelper.ForgotPassword(pstrMemberEmail);

                JToken outer = JToken.Parse(lstrResponse);
                if (outer["error_description"] != null)
                {
                    lstrMessage = outer.Value<string>("error_description");
                }
                else if (outer["success"] != null)
                {
                    if (outer.Value<bool>("success"))
                    {
                        lstrMessage = "SUCCESS";
                    }
                }
            }
            catch(Exception ex)
            {
                LoggingAdapter.WriteLog("ForgotPwd: " + ex.Message + Environment.NewLine + ex.StackTrace);
            }
            
            return lstrMessage;
        }
    }
}