﻿using Framework.EnterpriseLibrary.Adapters;
using Giift.SuciHalal.Entities;
using Giift.SuciHalal.Helper;
using Microsoft.CSharp.RuntimeBinder;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SuciHalalWebclient
{
    public partial class Checkout : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (HttpContext.Current.Session["AccessToken"] == null)
                {
                    Response.Redirect("Login.aspx");
                }
                else
                {
                    if (HttpContext.Current.Session["MemberEmail"] != null)
                    {
                        txtEmail.Value = Convert.ToString(HttpContext.Current.Session["MemberEmail"]);
                    }
                }
            }

            
        }

        [System.Web.Script.Services.ScriptMethod()]
        [System.Web.Services.WebMethod]
        public static List<CartDetails> GetCartItemsByStatus(string pstrCartStatus)
        {
            try
            {
                string[] lstrResponseData = null;
                string lstrResponse = string.Empty;
                List<CartDetails> lobjListOfCartDetails = null;
                string lstrAccessToken = Convert.ToString(HttpContext.Current.Session["AccessToken"]);
                if (lstrAccessToken != "")
                {
                    lstrResponse = ApiHelper.GetAllCart(lstrAccessToken, pstrCartStatus);
                }

                if (lstrResponse != "")
                {
                    dynamic lobjResponse = JsonConvert.DeserializeObject<dynamic>(lstrResponse);
                    if (IsPropertyExist(lobjResponse, "error"))
                    {
                        if (IsPropertyExist(lobjResponse, "error_description"))
                        {
                            CartDetails lobjCartDetails = new CartDetails();
                            lobjCartDetails.ResponseMessage = lobjResponse.error_description;
                            lobjListOfCartDetails.Add(lobjCartDetails);
                        }
                    }
                    else
                    {
                        var settings = new JsonSerializerSettings
                        {
                            NullValueHandling = NullValueHandling.Ignore,
                            MissingMemberHandling = MissingMemberHandling.Ignore
                        };
                        lobjListOfCartDetails = JsonConvert.DeserializeObject<List<CartDetails>>(lstrResponse, settings).FindAll(lobj => lobj.Status.Equals(pstrCartStatus));
                        if (lobjListOfCartDetails.Count > 0)
                        {
                            for (int i = 0; i < lobjListOfCartDetails.Count; i++)
                            {
                                lobjListOfCartDetails[i].CartItems.RemoveAll(lobj => lobj.Status != pstrCartStatus || lobj.BuyableType != "product");
                            }
                        }
                    }
                }
                List<CartDetails> lobjListOfCartDetailsNew = new List<CartDetails>();
                if (lobjListOfCartDetails.Count > 1)
                {
                    lobjListOfCartDetails.Sort((x, y) => y.CreatedAt.CompareTo(x.CreatedAt));
                    lobjListOfCartDetailsNew.Add(lobjListOfCartDetails[lobjListOfCartDetails.Count - 1]);
                }
                else
                {
                    lobjListOfCartDetailsNew.Add(lobjListOfCartDetails[0]);
                }
                
                //return lobjListOfCartDetails;
                return lobjListOfCartDetailsNew;
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("GetCartItemsByStatus Ex: " + ex.Message + Environment.NewLine + ex.InnerException + Environment.NewLine + ex.StackTrace);
                return null;
            }
        }

        [System.Web.Script.Services.ScriptMethod()]
        [System.Web.Services.WebMethod]
        public static int GetCartItemsCount(string pstrCartStatus)
        {
            try
            {
                string lstrResponse = string.Empty;
                int lintItemCount = 0;
                string lstrPreviousCartId = string.Empty;
                List<CartDetails> lobjListOfCartDetails = null;
                string lstrAccessToken = Convert.ToString(HttpContext.Current.Session["AccessToken"]);
                if (lstrAccessToken != "")
                {
                    lstrResponse = ApiHelper.GetAllCart(lstrAccessToken, pstrCartStatus);
                }

                if (lstrResponse != "")
                {
                    dynamic lobjResponse = JsonConvert.DeserializeObject<dynamic>(lstrResponse);
                    if (IsPropertyExist(lobjResponse, "error"))
                    {
                        if (IsPropertyExist(lobjResponse, "error_description"))
                        {
                            lintItemCount = 0;
                        }
                    }
                    else
                    {
                        var settings = new JsonSerializerSettings
                        {
                            NullValueHandling = NullValueHandling.Ignore,
                            MissingMemberHandling = MissingMemberHandling.Ignore
                        };
                        lobjListOfCartDetails = JsonConvert.DeserializeObject<List<CartDetails>>(lstrResponse, settings).FindAll(lobj => lobj.Status.Equals(pstrCartStatus));
                        if (lobjListOfCartDetails.Count > 0)
                        {


                            for (int i = 0; i < lobjListOfCartDetails.Count; i++)
                            {
                                lobjListOfCartDetails[i].CartItems.RemoveAll(lobj => lobj.Status != pstrCartStatus || lobj.BuyableType != "product");
                            }

                            //if (HttpContext.Current.Session["CartCurrency"] == null || Convert.ToString(HttpContext.Current.Session["CartCurrency"]) == string.Empty)
                            //{
                            //    HttpContext.Current.Session["CartCurrency"] = lobjListOfCartDetails[0].Currency;
                            //}

                            List<CartDetails> lobjListOfCartDetailsNew = new List<CartDetails>();
                            if (lobjListOfCartDetails.Count > 1)
                            {
                                lobjListOfCartDetails.Sort((x, y) => y.CreatedAt.CompareTo(x.CreatedAt));
                                lobjListOfCartDetailsNew.Add(lobjListOfCartDetails[lobjListOfCartDetails.Count - 1]);
                            }
                            else
                            {
                                lobjListOfCartDetailsNew.Add(lobjListOfCartDetails[0]);
                            }


                            for (int j = 0; j < lobjListOfCartDetailsNew[0].CartItems.Count; j++)
                            {
                                lintItemCount += Convert.ToInt32(lobjListOfCartDetailsNew[0].CartItems[j].Quantity);
                            }

                            HttpContext.Current.Session["CartId"] = lobjListOfCartDetailsNew[0].Id;

                        }
                    }
                }
                return lintItemCount;
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("GetCartItemsCount Ex: " + ex.Message + Environment.NewLine + ex.InnerException + Environment.NewLine + ex.StackTrace);
                return 0;
            }
        }

        [System.Web.Script.Services.ScriptMethod()]
        [System.Web.Services.WebMethod]
        public static string RemoveCartItem(string pstrCartItemId)
        {
            try
            {
                string lstrResponse = string.Empty;
                string lstrMessage = string.Empty;
                CartDetails lobjCartDetails = null;
                string lstrAccessToken = Convert.ToString(HttpContext.Current.Session["AccessToken"]);
                if (lstrAccessToken != "")
                {
                    lstrResponse = ApiHelper.RemoveCartItem(lstrAccessToken, pstrCartItemId);
                }

                if (lstrResponse != "")
                {
                    dynamic lobjResponse = JsonConvert.DeserializeObject<dynamic>(lstrResponse);
                    if (IsPropertyExist(lobjResponse, "error"))
                    {
                        if (IsPropertyExist(lobjResponse, "error_description"))
                        {
                            lstrMessage = lobjResponse.error_description;
                        }
                    }
                    else
                    {
                        lobjCartDetails = new CartDetails();
                        lobjCartDetails = JsonConvert.DeserializeObject<CartDetails>(lstrResponse);
                        lstrMessage = lobjCartDetails.CartItems.Find(lobj => lobj.Id.Equals(pstrCartItemId)).Status.ToString();
                    }
                }
                return lstrMessage;
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("RemoveCartItem Ex: " + ex.Message + Environment.NewLine + ex.InnerException + Environment.NewLine + ex.StackTrace);
                return string.Empty;
            }
        }

        [System.Web.Script.Services.ScriptMethod()]
        [System.Web.Services.WebMethod]
        public static string CartCheckout(string pstrCartId, string pstrProviderKey, string pstrName, string pstrAddress, string pstrEmail,
           string pstrCity, string pstrState, string pstrCountry, string pstrZip, string pstrPhone, string pstrDeliveryId)
        {
            try
            {
                string lstrResponse = string.Empty;
                string lstrCheckOutStatus = string.Empty;
                string lstrDeliveryStatus = string.Empty;
                //CartDetails lobjCartDetails = null;
                string lstrUpdateUserDataResponse = string.Empty;

                HttpContext.Current.Session["DeliveryEmail"] = pstrEmail;

                string lstrAccessToken = Convert.ToString(HttpContext.Current.Session["AccessToken"]);
                if (lstrAccessToken != "")
                {
                    lstrUpdateUserDataResponse = ApiHelper.UpdateProfile(lstrAccessToken, pstrName.Split(' ')[0], pstrName.Split(' ')[1], pstrCity, pstrPhone, string.Empty, string.Empty, pstrAddress, pstrZip, string.Empty, string.Empty, pstrCountry, pstrState);
                    lstrResponse = ApiHelper.CartCheckout(lstrAccessToken, pstrCartId);
                }

                if (lstrResponse != "")
                {
                    dynamic lobjResponse = JsonConvert.DeserializeObject<dynamic>(lstrResponse);
                    if (IsPropertyExist(lobjResponse, "error"))
                    {
                        if (IsPropertyExist(lobjResponse, "error_description"))
                        {
                            lstrCheckOutStatus = lobjResponse.error_description;
                        }
                    }
                    else
                    {
                        //lobjCartDetails = new CartDetails();
                        //lobjCartDetails = JsonConvert.DeserializeObject<CartDetails>(lstrResponse); //To be mapped with order object
                        lstrCheckOutStatus = lobjResponse.Cart.Status == null ? "" : lobjResponse.Cart.Status.ToString();
                        if (lstrCheckOutStatus.Equals("locked")) //change the condition to check the cart status as locked same to be change in Jquery
                        {
                            PaymentDetails lobjPaymentDetails = new PaymentDetails();
                            lobjPaymentDetails.OrderId = lobjResponse.Id;
                            lobjPaymentDetails.Amount = Math.Round(Convert.ToSingle(lobjResponse.Cart.value), 2);
                            if (HttpContext.Current.Session["CartCurrency"] == null || Convert.ToString(HttpContext.Current.Session["CartCurrency"]).Equals(""))
                            {
                                HttpContext.Current.Session["CartCurrency"] = lobjResponse.Currency;
                            }

                            List<OrderHistoryDetails> lobjListOfOrders = new List<OrderHistoryDetails>();
                            lobjListOfOrders = GetOrderHistoryByCartId(pstrCartId);
                            if (lobjListOfOrders.Count > 0)
                            {
                                for (int i = 0; i < lobjListOfOrders.Count; i++)
                                {
                                    if (lobjListOfOrders[i].CartOrderItems.Count > 0)
                                    {
                                        for (int j = 0; j < lobjListOfOrders[i].CartOrderItems.Count; j++)
                                        {
                                            if (lobjListOfOrders[i].CartOrderItems[j].Name == "Shipping Fees")
                                            {
                                                lobjPaymentDetails.ShippingFee = lobjListOfOrders[i].CartOrderItems[j].Value;
                                            }
                                            else
                                            {
                                                string lstrProviderkey = GetDeliveryProvider(lobjListOfOrders[i].CartOrderItems[j].Id);
                                                if (lstrProviderkey != string.Empty)
                                                {
                                                    lstrDeliveryStatus = SetDeliveryProduct(lobjListOfOrders[i].CartOrderItems[j].Id, lstrProviderkey, pstrName, pstrAddress, pstrCity, pstrState,
                                                        pstrCountry, pstrZip, pstrPhone, pstrDeliveryId);
                                                }
                                            }
                                           
                                        }
                                    }
                                }
                            }

                            HttpContext.Current.Session["PaymentDetails"] = lobjPaymentDetails;
                        }
                    }
                }
                return lstrCheckOutStatus;
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("CartCheckout Ex: " + ex.Message + Environment.NewLine + ex.InnerException + Environment.NewLine + ex.StackTrace);
                return string.Empty;
            }
        }


        public static string GetDeliveryProvider(string pstrCartOrderItemId)
        {
            string lstrMessage = string.Empty;
            string lstrResponse = string.Empty;
            string lstrAccessToken = Convert.ToString(HttpContext.Current.Session["AccessToken"]);
            if (lstrAccessToken != "")
            {
                lstrResponse = ApiHelper.GetDeliveryProvider(lstrAccessToken, pstrCartOrderItemId);
            }

            if (lstrResponse != "" && lstrResponse != "[]")
            {
                dynamic lobjResponse = JsonConvert.DeserializeObject<dynamic>(lstrResponse);
                if (IsPropertyExist(lobjResponse, "error"))
                {
                    if (IsPropertyExist(lobjResponse, "error_description"))
                    {
                        LoggingAdapter.WriteLog("SetDeliveryProduct Response: #CartOrderItemId=" + pstrCartOrderItemId + Environment.NewLine + "#Response=" + lstrResponse);
                    }
                }
                else
                {

                    lstrMessage = lobjResponse[0].key;
                }
            }
            return lstrMessage;
        }

        [System.Web.Script.Services.ScriptMethod()]
        [System.Web.Services.WebMethod]
        public static string SetDeliveryProduct(string pstrOrderItemId, string pstrProviderKey, string pstrName, string pstrAddress,
           string pstrCity, string pstrState, string pstrCountry, string pstrZip, string pstrPhone, string pstrDeliveryId)
        {
            try
            {
                string lstrResponse = string.Empty;
                string lstrMessage = string.Empty;
                DeliveryProviderDetails lobjDeliveryProviderDetails = null;
                string lstrAccessToken = Convert.ToString(HttpContext.Current.Session["AccessToken"]);
                if (lstrAccessToken != "")
                {
                    lstrResponse = ApiHelper.SetDeliveryProduct(lstrAccessToken, pstrOrderItemId, pstrProviderKey, pstrName, pstrAddress, pstrCity, pstrState, pstrCountry,
                        pstrZip, pstrPhone, pstrDeliveryId);
                }

                if (lstrResponse != "")
                {
                    dynamic lobjResponse = JsonConvert.DeserializeObject<dynamic>(lstrResponse);
                    if (IsPropertyExist(lobjResponse, "error"))
                    {
                        if (IsPropertyExist(lobjResponse, "error_description"))
                        {
                            lstrMessage = lobjResponse.error_description;
                            LoggingAdapter.WriteLog("SetDeliveryProduct Response: #CartOrderItemId=" + pstrOrderItemId + Environment.NewLine + "#Response=" + lstrResponse);
                        }
                    }
                    else
                    {
                        lobjDeliveryProviderDetails = new DeliveryProviderDetails();
                        lobjDeliveryProviderDetails = JsonConvert.DeserializeObject<DeliveryProviderDetails>(lstrResponse);
                        lstrMessage = lobjDeliveryProviderDetails.Status;
                    }
                }
                return lstrMessage;
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("SetDeliveryProduct Ex: " + ex.Message + Environment.NewLine + ex.InnerException + Environment.NewLine + ex.StackTrace);
                return string.Empty;
            }
        }

        [System.Web.Script.Services.ScriptMethod()]
        [System.Web.Services.WebMethod]
        public static List<OrderHistoryDetails> GetOrderHistoryByCartId(string pstrCartId)
        {
            try
            {
                string lstrResponse = string.Empty;
                string lstrMessage = string.Empty;
                List<OrderHistoryDetails> lobjListOfOrders = null;
                string lstrAccessToken = Convert.ToString(HttpContext.Current.Session["AccessToken"]);
                if (lstrAccessToken != "")
                {
                    lstrResponse = ApiHelper.GetAllOrders(lstrAccessToken);
                }

                if (lstrResponse != "")
                {
                    dynamic lobjResponse = JsonConvert.DeserializeObject<dynamic>(lstrResponse);
                    if (IsPropertyExist(lobjResponse, "error"))
                    {
                        if (IsPropertyExist(lobjResponse, "error_description"))
                        {
                            lstrMessage = lobjResponse.error_description;
                        }
                    }
                    else
                    {
                        lobjListOfOrders = new List<OrderHistoryDetails>();
                        lobjListOfOrders = JsonConvert.DeserializeObject<List<OrderHistoryDetails>>(lstrResponse);

                        if (lobjListOfOrders.Count > 0)
                        {
                            lobjListOfOrders.RemoveAll(lobj => lobj.CartId != pstrCartId);
                        }
                    }
                }
                else
                {
                    lstrMessage = "NotFound";
                }
                return lobjListOfOrders;
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("GetOrderHistory Ex: " + ex.Message + Environment.NewLine + ex.InnerException + Environment.NewLine + ex.StackTrace);
                return null;
            }
        }

        [System.Web.Script.Services.ScriptMethod()]
        [System.Web.Services.WebMethod]
        public static void ClearSession(string pstrSessionName)
        {
            HttpContext.Current.Session[pstrSessionName] = null;
        }

        [System.Web.Script.Services.ScriptMethod()]
        [System.Web.Services.WebMethod]
        public static void IsUserLoggedIn(string pstrSessionName)
        {
            HttpContext.Current.Session["AccessToke"] = null;
        }

        public static bool IsPropertyExist(dynamic dynamicObj, string property)
        {
            bool lblnStatus = false;
            try
            {
                var value = dynamicObj[property].Value;
                lblnStatus = true;
            }
            catch (RuntimeBinderException)
            {
                lblnStatus = false;
            }
            catch (Exception)
            {
                lblnStatus = false;
            }
            return lblnStatus;
        }
    }
}