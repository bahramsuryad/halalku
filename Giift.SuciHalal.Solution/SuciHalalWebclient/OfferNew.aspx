﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SuciHalalSite.Master" AutoEventWireup="true" CodeBehind="OfferNew.aspx.cs" Inherits="SuciHalalWebclient.OfferNew" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="dashboard animated fadeIn about" style="margin-top: 5px; background-color: #eee;">
        <div class="banner sub about" id="banner-store"
            style="background-image: url('https://images.unsplash.com/photo-1504674900247-0877df9cc836?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1050&q=80') !important; background-size: cover !important;">
            <div class="container center in-sub w-container">
                <h1 class="sub-heading-title">F&B Offers</h1>
                <div class="breadcrumb">
                    <div class="breadcrumb-content">
                        <a class="bread-link" href="Index.aspx">Home</a><img
                            alt=""
                            src="https://uploads-ssl.webflow.com/5c3ca7fbdc8fab3302b31532/5c41f1941b7ac65ffde90630_right-arrow%20(3).svg" width="18">
                        <div class="bread-link gray-color">F&B Offers</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="section">
            <div class="offer-container">
                <a href="./restaurant-detail.html">
                    <div class="row">
                        <div class="offer-list-card">
                            <div class="offer-list-card-left">
                                <p class="offer-list-card-certified">2 Thanon Samsen, Banglumphu, Pranakorn, Bangkok 10200</p>
                                <img class="offer-card-image"
                                    src="https://images.unsplash.com/photo-1414235077428-338989a2e8c0?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1050&q=80">
                                <p class="offer-list-card-name">Jake's Charbroil Steaks</p>
                                <div class="offer-list-card-highlight-container">
                                            <span class="offer-list-card-highlight">10% OFF</span>
                                        </div>
                                <p class="offer-list-card-address">
                                    Buy 1 get 1 free on all drinks during Happy Hour when you pay with a valid Visa commercial card.
                                            <br>
                                    Elegant French dining in Tsim Sha Tsui, offering only the highest quality seafood. With a feast of French crab dishes, dry aged beef
                                            &amp; and
                                            an impressive array of European wines, this team of dedicated culinary talents is more than looking forward to serving you on your next
                                            visit to
                                            La Mer!
                                </p>
                                <!--                                        <p class="restaurant-list-card-distance"><b>0,5 KM</b> | Central Thailand</p>-->
                            </div>
                        </div>
                    </div>
                </a>
                <a href="./restaurant-detail.html">
                    <div class="row">
                        <div class="offer-list-card">
                            <div class="offer-list-card-left">
                                <p class="offer-list-card-certified">Unit A-B 1/F, China Insurance Building, 48 Cameron Road Tsim Sha Tsui</p>
                                <img class="offer-card-image"
                                    src="https://www.visa.com/images/merchantoffers/2018-09/1537949305096.GC_BusinessDining_LaMer_image2_608x220.jpg">
                                <p class="offer-list-card-name">La Mer Restaurant & Lounge</p>
                                <p class="offer-list-card-address">
                                    Buy 1 get 1 free on all drinks during Happy Hour when you pay with a valid Visa commercial card.
                                            <br>
                                    Elegant French dining in Tsim Sha Tsui, offering only the highest quality seafood. With a feast of French crab dishes, dry aged beef
                                            &amp; and
                                            an impressive array of European wines, this team of dedicated culinary talents is more than looking forward to serving you on your next
                                            visit to
                                            La Mer!
                                </p>
                                <!--                                        <p class="restaurant-list-card-distance"><b>0,5 KM</b> | Central Thailand</p>-->
                            </div>
                        </div>
                    </div>
                </a>
                <a href="./restaurant-detail.html">
                    <div class="row">
                        <div class="offer-list-card">
                            <div class="offer-list-card-left">
                                <p class="offer-list-card-certified">Unit A-B 1/F, China Insurance Building, 48 Cameron Road Tsim Sha Tsui</p>
                                <img class="offer-card-image"
                                    src="https://www.visa.com/images/merchantoffers/2018-09/1537949305096.GC_BusinessDining_LaMer_image2_608x220.jpg">
                                <p class="offer-list-card-name">La Mer Restaurant & Lounge</p>
                                <p class="offer-list-card-address">
                                    Buy 1 get 1 free on all drinks during Happy Hour when you pay with a valid Visa commercial
                                            &amp; and
                                </p>
                                <!--                                        <p class="restaurant-list-card-distance"><b>0,5 KM</b> | Central Thailand</p>-->
                            </div>
                        </div>
                    </div>
                </a>
                <a href="./restaurant-detail.html">
                    <div class="row">
                        <div class="offer-list-card">
                            <div class="offer-list-card-left">
                                <p class="offer-list-card-certified">Unit A-B 1/F, China Insurance Building, 48 Cameron Road Tsim Sha Tsui</p>
                                <img class="offer-card-image"
                                    src="https://www.visa.com/images/merchantoffers/2018-09/1537949305096.GC_BusinessDining_LaMer_image2_608x220.jpg">
                                <p class="offer-list-card-name">La Mer Restaurant & Lounge</p>
                                <p class="offer-list-card-address">
                                    Buy 1 get 1 free on all drinks during Happy Hour when you pay with a valid Visa commercial
                                            &amp; and
                                </p>
                                <!--                                        <p class="restaurant-list-card-distance"><b>0,5 KM</b> | Central Thailand</p>-->
                            </div>
                        </div>
                    </div>
                </a>
                <a href="./restaurant-detail.html">
                    <div class="row">
                        <div class="offer-list-card">
                            <div class="offer-list-card-left">
                                <p class="offer-list-card-certified">Unit A-B 1/F, China Insurance Building, 48 Cameron Road Tsim Sha Tsui</p>
                                <img class="offer-card-image"
                                    src="https://www.visa.com/images/merchantoffers/2018-09/1537949305096.GC_BusinessDining_LaMer_image2_608x220.jpg">
                                <p class="offer-list-card-name">La Mer Restaurant & Lounge</p>
                                <p class="offer-list-card-address">
                                    Buy 1 get 1 free on all drinks during Happy Hour when you pay with a valid Visa commercial
                                            &amp; and
                                </p>
                                <!--                                        <p class="restaurant-list-card-distance"><b>0,5 KM</b> | Central Thailand</p>-->
                            </div>
                        </div>
                    </div>
                </a>
                <a href="./restaurant-detail.html">
                    <div class="row">
                        <div class="offer-list-card">
                            <div class="offer-list-card-left">
                                <p class="offer-list-card-certified">Unit A-B 1/F, China Insurance Building, 48 Cameron Road Tsim Sha Tsui</p>
                                <img class="offer-card-image"
                                    src="https://www.visa.com/images/merchantoffers/2018-09/1537949305096.GC_BusinessDining_LaMer_image2_608x220.jpg">
                                <p class="offer-list-card-name">La Mer Restaurant & Lounge</p>
                                <p class="offer-list-card-address">
                                    Buy 1 get 1 free on all drinks during Happy Hour when you pay with a valid Visa commercial
                                            &amp; and
                                </p>
                                <!--                                        <p class="restaurant-list-card-distance"><b>0,5 KM</b> | Central Thailand</p>-->
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="center">
                <ul id="ulPagingItems" class="pagination"></ul>
            </div>
        </div>
    </div>
    <div id="divLoader" class="divLoader" style="display: none;"></div>
    <script src="js/jquery-3.3.1.min.js"></script>
    <script type="text/javascript">
        var lintPageSize = 15;
        var lintTotalPages = 0;
        
        $(document).ready(function () {
            $("#divOffers").text("");
            GetOffers();
        });

        function GetOffers() {
            $('#divLoader').show();

            $.ajax({
                type: "POST",
                url: "Offer.aspx/GetRestaurantOffers",
                data: "{pintSkipRecords:0,pintTakeRecords:" + lintPageSize + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: true,
                cache: false,
                success: function (msg) {
                    $('#divLoader').hide();
                    if (msg != null) {
                        lintTotalPages = msg.d.TotalPages;
                        if (lintTotalPages > 0) {
                            $("#divOffers").html("");
                            printRestaurantOffersData(msg);

                            $('#ulPagingItems').twbsPagination({
                                totalPages: lintTotalPages,
                                visiblePages: 10,
                                next: 'Next',
                                prev: 'Prev',
                                onPageClick: function (event, page) {
                                    pageData(page);
                                }
                            });
                        }
                        else {
                            $("#divOffers").text("No records were found.");
                        }
                    }
                    else {
                        swal("Oops!", "Something went wrong.", "error");
                    }
                },
                error: function () {
                    $('#divLoader').hide();
                    $("#divOffers").text("");
                    $("#divOffers").text("No records were found.");
                }
            });
        }

        function printRestaurantOffersData(msg) {
            $('#divLoader').show();
            var offers = msg.d.RDetails;
            var html = "";
            $("#divOffers").text(html);
            var count = 0;
            for (var i = 0; i < offers.length; i++) {
                
                html += '<a href="./RestaurantDetail.aspx?id=' + offers[i].Id + '">'
                html += '<div class="row">'
                html += '<div class="restaurant-list-card">'
                html += '<div class="restaurant-list-card-left">'
                html += '<p class="restaurant-list-card-certified">' + offers[i].Address + '</p>'
                html += '<p class="restaurant-list-card-name">' + offers[i].Name + '</p>'
                html += '<p class="restaurant-list-card-address">'
                html += offers[i].Deal + '<br>'
                html += offers[i].Description + '</p>'
                html += '</div>'
                html += '<div class="restaurant-list-card-right">'
                html += '<img class="offer-list-card-image" src="' + offers[i].ImageUrl + '">'
                html += '</div>'
                html += '</div>'
                html += '</div>'
                html += '</a>'
            }
            $("#divOffers").append(html);
            $('#divLoader').hide();
        }

        function pageData(e) {
            $('#divLoader').show();
            var lintSkipRecords = e == 1 ? 0 : (e * lintPageSize) - lintPageSize;
            $.ajax({
                type: "POST",
                url: "Offer.aspx/GetRestaurantOffers",
                data: "{pintSkipRecords:" + lintSkipRecords + ",pintTakeRecords:" + lintPageSize + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: true,
                cache: false,
                success: function (msg) {
                    $('#divLoader').hide();
                    printRestaurantOffersData(msg);
                },
                error: function () {
                    $('#divLoader').hide();
                    $("#divOffers").text("");
                    $("#divOffers").text("No records were found.");
                }

            });

            return false;
        }
    </script>
</asp:Content>
