﻿using Framework.EnterpriseLibrary.Adapters;
using Giift.SuciHalal.Helper;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Giift.SuciHalal.Entities;
using Microsoft.CSharp.RuntimeBinder;
using Newtonsoft.Json;

namespace SuciHalalWebclient
{
    public partial class ProductDetail : System.Web.UI.Page
    {
        string lstrProductId = string.Empty;
        public static string lstrAccessToken = string.Empty;
        string lstrResponse = string.Empty;
        string lstrDivContent = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["id"] != null)
            {
                lstrProductId = Convert.ToString(Request.QueryString["id"]);
                if (HttpContext.Current.Session["AccessToken"] != null)
                {
                    lstrAccessToken = Convert.ToString(HttpContext.Current.Session["AccessToken"]);
                    try
                    {

                        lstrResponse = ApiHelper.BuyableDetails("", "product", lstrProductId, ""); // removed myr

                        JObject outer = JObject.Parse(lstrResponse);
                        JArray lobjImageArray = (JArray)outer["imgs"];
                        string[] lstrImageArray = lobjImageArray.Select(jv => (string)jv).ToArray();

                        string lstrProductName = Convert.ToString(((JValue)outer["name"]).Value);
                        string lstrProductType = Convert.ToString(((JValue)outer["type"]).Value);
                        string lstrDescription = Convert.ToString(((JValue)outer["description"]).Value);
                        //string lstrETA = Convert.ToString(((JValue)outer["eta"]).Value);
                        string lstrPrice = Convert.ToString(((JToken)outer["units"][0])["price"]);
                        string lstrCurrency = Convert.ToString(((JToken)outer["units"][0])["unit"]);

                        spanProductName.InnerText = lstrProductName;
                        spanDescription.InnerHtml = lstrDescription;
                        spanPrice.InnerText = lstrPrice;
                        spanFinalPrice.InnerText = Convert.ToString(Decimal.Round(Decimal.Parse(lstrPrice), 2));
                        imgProductLarge.Src = lstrImageArray.Where(x => x.Contains("_medium.jpg")).FirstOrDefault();
                        hfProductType.Value = lstrProductType;
                        hfProductCCY.Value = lstrCurrency;
                        spanCurrency.InnerText = lstrCurrency.ToUpper();
                        spanCurrencyFinal.InnerText = lstrCurrency.ToUpper();

                        //foreach (string imgPath in lstrImageArray)
                        //{
                        //    lstrDivContent += "<a class=\"tab-photo w-inline-block w-tab-link w--current\" data-w-tab=\"Tab 1\"><img alt = \"\""
                        //                   + "data-wf-sku-bindings=\"%5B%7B%22from%22%3A%22f_main_image_4dr%22%2C%22to%22%3A%22src%22%7D%5D\" sizes=\"100vw\""
                        //                   + "src=\"" + imgPath + "\"></a>";
                        //}

                        divSmallImages.InnerHtml = lstrDivContent;
                    }
                    catch (Exception ex)
                    {
                        LoggingAdapter.WriteLog("ProductDetail.aspx.cs : PageLoad : " + ex.Message + Environment.NewLine + ex.StackTrace);
                        Response.Redirect("Catalogue.aspx");
                    }
                }
                else
                {
                    Response.Redirect("Login.aspx?Callbackurl=ProductDetail.aspx?id=" + lstrProductId);
                }
            }
            else
            {
                Response.Redirect("Catlogue.aspx");
            }
        }

        [System.Web.Script.Services.ScriptMethod()]
        [System.Web.Services.WebMethod]
        public static string AddToCart(string pstrProductId, string pstrProductType, string pstrCurrency, int pintProductQuantity)
        {
            string lstrResponse = string.Empty;
            string lstrMessage = string.Empty;
            string lstrCartId = string.Empty;
            string lstrPreviousCartId = string.Empty;

            try
            {
                if (HttpContext.Current.Session["AccessToken"] != null)
                {
                    if (HttpContext.Current.Session["CartId"] != null)
                    {
                        lstrCartId = Convert.ToString(HttpContext.Current.Session["CartId"]);
                    }
                    else
                    {
                        lstrPreviousCartId = GetPreviousCartId("open");
                        if (lstrPreviousCartId != "")
                        {
                            lstrCartId = lstrPreviousCartId;
                            HttpContext.Current.Session["CartId"] = lstrCartId;
                        }
                        else
                        {
                            string lstrNewCartResponse = ApiHelper.NewCart(lstrAccessToken);
                            JObject lobjNewCart = JObject.Parse(lstrNewCartResponse);
                            if (lobjNewCart != null && lobjNewCart["Id"] != null)
                            {
                                lstrCartId = Convert.ToString(lobjNewCart["Id"]);
                                HttpContext.Current.Session["CartId"] = lstrCartId;
                            }
                        }
                    }

                    lstrResponse = ApiHelper.AddCartItem(lstrAccessToken, pstrProductType, pstrProductId, pstrCurrency, pintProductQuantity, lstrCartId);
                    JObject lobjAddCartResponse = JObject.Parse(lstrResponse);
                    if (lobjAddCartResponse != null && lobjAddCartResponse["Status"] != null)
                    {
                        string lstrAddCardStatus = Convert.ToString(lobjAddCartResponse["Status"]);
                        if (lstrAddCardStatus == "open")
                        {
                            lstrMessage = "SUCCESS";
                        }
                    }
                }
                else
                {
                    lstrMessage = "TIMEOUT";
                }
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("ProductDetail.aspx.cs: AddToCart: " + ex.Message + Environment.NewLine + ex.StackTrace);
            }

            return lstrMessage;
        }

        public static string GetPreviousCartId(string pstrCartStatus)
        {
            try
            {
                string lstrResponse = string.Empty;
                string lstrAccessToken = Convert.ToString(HttpContext.Current.Session["AccessToken"]);
                string lstrPreviousCartId = string.Empty;
                List<CartDetails> lobjListOfCartDetails = null;
                lstrResponse = ApiHelper.GetAllCart(lstrAccessToken, pstrCartStatus);
                var settings = new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore,
                    MissingMemberHandling = MissingMemberHandling.Ignore
                };

                lobjListOfCartDetails = JsonConvert.DeserializeObject<List<CartDetails>>(lstrResponse, settings).FindAll(lobj => lobj.Status.Equals(pstrCartStatus));
                if (lobjListOfCartDetails.Count > 0)
                {
                    for (int i = 0; i < lobjListOfCartDetails.Count; i++)
                    {
                        List<CartItemDetails> lobjListOfOpenCartItem = new List<CartItemDetails>();
                        lobjListOfOpenCartItem = lobjListOfCartDetails[i].CartItems.FindAll(lobj => lobj.Status.Equals(pstrCartStatus));
                        //lobjListOfCartDetails[i].CartItems.RemoveAll(lobj => lobj.Status != pstrCartStatus);
                        lobjListOfCartDetails[i].CartItems.RemoveAll(lobj => lobj.Status != pstrCartStatus || lobj.BuyableType != "product");
                    }
                    //if (lobjListOfCartDetails.FindAll(cart => cart.CartItems.Any(item => item.Status != "")).Count > 0)
                    //{
                    //    lstrPreviousCartId = lobjListOfCartDetails.FirstOrDefault().Id;
                    //}
                    //else if (lobjListOfCartDetails.FindAll(cart => cart.Status != "").Count > 0)
                    //{
                    //    lstrPreviousCartId = lobjListOfCartDetails.FirstOrDefault().Id;
                    //}

                    if (lobjListOfCartDetails.Count > 1)
                    {
                        lobjListOfCartDetails.Sort((x, y) => y.CreatedAt.CompareTo(x.CreatedAt));
                        lstrPreviousCartId = lobjListOfCartDetails[lobjListOfCartDetails.Count - 1].Id;
                    }
                    else
                    {
                        lstrPreviousCartId = lobjListOfCartDetails[0].Id;
                    }
                }
                return lstrPreviousCartId;
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("GetPreviousCartId Ex: " + ex.Message + Environment.NewLine + ex.InnerException + Environment.NewLine + ex.StackTrace);
                return string.Empty;
            }
        }
    }
}