﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SuciHalalSite.Master" AutoEventWireup="true" CodeBehind="Tools.aspx.cs" Inherits="SuciHalalWebclient.Tools" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="tools" style="">
        <div class="banner sub">
            <div class="container center in-sub w-container">
                <h1 class="sub-heading-title">Tools</h1>
                <p class="shop-text light overlay-content">Find the best offers on My Halal Stop</p>
                <div class="breadcrumb">
                    <div class="breadcrumb-content">
                        <a class="bread-link" href="/home">Home</a>
                        <img alt=""
                            src="https://uploads-ssl.webflow.com/5c3ca7fbdc8fab3302b31532/5c41f1941b7ac65ffde90630_right-arrow%20(3).svg"
                            width="18">
                        <div class="bread-link gray-color">Tools</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="section">
            <div class="container w-container">
                <div class="flex-store">
                    <div class="tools-new-container">
                        <a href="./mosque.aspx">
                            <div class="tools-new-list">
                                <div class="tools-new-image-container">
                                    <img alt="" class="tools-new-image"
                                        data-wf-sku-bindings="%5B%7B%22from%22%3A%22f_main_image_4dr%22%2C%22to%22%3A%22src%22%7D%5D" height="320"
                                        src="https://images.unsplash.com/photo-1515091943-9d5c0ad475af?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=375&q=80" style="opacity: 1; object-fit: cover;" width="320">
                                </div>
                                <div class="tools-new-content" style="margin-bottom: 18px;">
                                    <div class="product-top-title">
                                        <h4 class="product-title" style="text-align: center;">Mosque</h4>
                                    </div>
                                    <div class="price" style="text-align: center;">
                                        Find your nearest Mosque
                                    </div>
                                </div>
                            </div>
                        </a>
                        <a href="./restaurant.aspx">
                            <div class="tools-new-list">
                                <div class="tools-new-image-container">
                                    <img alt="" class="tools-new-image"
                                        data-wf-sku-bindings="%5B%7B%22from%22%3A%22f_main_image_4dr%22%2C%22to%22%3A%22src%22%7D%5D" height="320"
                                        src="https://images.unsplash.com/photo-1522413452208-996ff3f3e740?ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80" style="opacity: 1; object-fit: cover;" width="320">
                                </div>
                                <div class="tools-new-content" style="margin-bottom: 18px;">
                                    <div class="product-top-title">
                                        <h4 class="product-title" style="text-align: center;">Restaurant</h4>
                                    </div>
                                    <div class="price" style="text-align: center;">
                                        Find your nearest halal restaurant
                                    </div>
                                </div>
                            </div>
                        </a>
                        <a href="./prayerschedule.aspx">
                            <div class="tools-new-list">
                                <div class="tools-new-image-container">
                                    <img alt="" class="tools-new-image"
                                        data-wf-sku-bindings="%5B%7B%22from%22%3A%22f_main_image_4dr%22%2C%22to%22%3A%22src%22%7D%5D" height="320"
                                        src="https://images.pexels.com/photos/1449051/pexels-photo-1449051.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940" style="opacity: 1; object-fit: cover;" width="320">
                                </div>
                                <div class="tools-new-content" style="margin-bottom: 18px;">
                                    <div class="product-top-title">
                                        <h4 class="product-title" style="text-align: center;">Prayer Time</h4>
                                    </div>
                                    <div class="price" style="text-align: center;">
                                        Prayer times at a glance
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
