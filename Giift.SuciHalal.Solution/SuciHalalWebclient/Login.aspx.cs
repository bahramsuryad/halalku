﻿using Framework.EnterpriseLibrary.Adapters;
using Giift.SuciHalal.Helper;
using Microsoft.CSharp.RuntimeBinder;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SuciHalalWebclient
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [System.Web.Script.Services.ScriptMethod()]
        [System.Web.Services.WebMethod]
        public static string SigninUser(string pstrMemberEmail, string pstrMemberPassword)
        {
            string lstrResponse = string.Empty;
            string lstrMessage = string.Empty;

            try
            {

                lstrResponse = ApiHelper.SignIn(pstrMemberEmail, pstrMemberPassword);

                JToken outer = JToken.Parse(lstrResponse);
                if (outer["error_description"] != null)
                {
                    lstrMessage = outer.Value<string>("error_description");
                }
                else if (outer["access_token"] != null)
                {
                    HttpContext.Current.Session["AccessToken"] = outer.Value<string>("access_token");
                    HttpContext.Current.Session["MemberEmail"] = pstrMemberEmail;
                    lstrMessage = "SUCCESS";
                }
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("SigninUser: " + ex.Message + Environment.NewLine + ex.StackTrace);
            }

            return lstrMessage;
        }

        [System.Web.Script.Services.ScriptMethod()]
        [System.Web.Services.WebMethod]
        public static string LogoutUser()
        {
            string lstrResponse = string.Empty;
            HttpContext.Current.Session.Abandon();
            return "SUCCESS";
        }

        [System.Web.Script.Services.ScriptMethod()]
        [System.Web.Services.WebMethod]
        public static string NewsLetterSubmit(string pstrMemberEmail)
        {
            string lstrResponse = string.Empty;
            string lstrMessage = string.Empty;
            try
            {
                lstrResponse = ApiHelper.SubscribeNewsletter(Convert.ToString(HttpContext.Current.Session["AccessToken"]), pstrMemberEmail);

                JToken outer = JToken.Parse(lstrResponse);
                if (outer["error_description"] != null)
                {
                    lstrMessage = outer.Value<string>("error_description");
                }
                else if (outer["access_token"] != null)
                {
                    HttpContext.Current.Session["AccessToken"] = outer.Value<string>("access_token");
                    lstrMessage = "SUCCESS";
                }
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("NewsLetterSubmit Ex: " + ex.Message + Environment.NewLine + ex.StackTrace);
            }

            return lstrMessage;
        }
    }
}