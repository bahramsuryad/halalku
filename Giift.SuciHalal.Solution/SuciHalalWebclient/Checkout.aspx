﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SuciHalalSite.Master" AutoEventWireup="true" CodeBehind="Checkout.aspx.cs" Inherits="SuciHalalWebclient.Checkout" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div data-node-type="commerce-checkout-form-container" data-wf-page-link-href-prefix=""
        class="w-commerce-commercecheckoutformcontainer section gray checkout" style="padding-top: 150px;">
        <div class="w-commerce-commercelayoutcontainer w-container">
            <div class="w-commerce-commercelayoutmain">
                <div data-node-type="commerce-checkout-customer-info-wrapper" class="w-commerce-commercecheckoutcustomerinfowrapper customer-info">
                    <div class="w-commerce-commercecheckoutblockheader block-header-2">
                        <h4>Customer Info</h4>
                        <div class="required-text">* Required</div>
                    </div>
                    <fieldset class="w-commerce-commercecheckoutblockcontent">
                        <label
                            class="w-commerce-commercecheckoutlabel field-label-2">
                            Email *</label>
                        <input runat="server"
                            id="txtEmail" type="text" name="email"
                            class="w-commerce-commercecheckoutemailinput" disabled>
                    </fieldset>
                </div>
                <div data-node-type="commerce-checkout-shipping-address-wrapper" class="w-commerce-commercecheckoutshippingaddresswrapper">
                    <div class="w-commerce-commercecheckoutblockheader block-header">
                        <h4>Shipping Address</h4>
                        <div class="required-text">* Required</div>
                    </div>
                    <fieldset class="w-commerce-commercecheckoutblockcontent">
                        <label
                            class="w-commerce-commercecheckoutlabel field-label-2">
                            Full Name *</label>

                        <input
                            id="txtFullName" type="text" name="name"
                            class="w-commerce-commercecheckoutshippingfullname">
                        <label
                            class="w-commerce-commercecheckoutlabel field-label-2">
                            Phone No *</label>

                        <input
                            id="txtPhoneNo" type="text" name="name"
                            class="w-commerce-commercecheckoutshippingfullname">
                        <label
                            class="w-commerce-commercecheckoutlabel field-label-2">
                            Street Address
                                *</label>
                        <input id="txtAddressLine1" type="text" name="address_line1"
                            data-wf-bindings="%5B%7B%22value%22%3A%7B%22type%22%3A%22PlainText%22%2C%22filter%22%3A%7B%22type%22%3A%22identity%22%2C%22params%22%3A%5B%5D%7D%2C%22dataPath%22%3A%22database.commerceOrder.shippingAddressLine1%22%7D%7D%5D"
                            data-wf-conditions="%5B%5D"
                            class="w-commerce-commercecheckoutshippingstreetaddress">
                        <input
                            id="txtAddressLine2" type="text" name="address_line2"
                            data-wf-bindings="%5B%7B%22value%22%3A%7B%22type%22%3A%22PlainText%22%2C%22filter%22%3A%7B%22type%22%3A%22identity%22%2C%22params%22%3A%5B%5D%7D%2C%22dataPath%22%3A%22database.commerceOrder.shippingAddressLine2%22%7D%7D%5D"
                            data-wf-conditions="%5B%5D"
                            class="w-commerce-commercecheckoutshippingstreetaddressoptional">
                        <div class="w-commerce-commercecheckoutrow">
                            <div class="w-commerce-commercecheckoutcolumn">
                                <label
                                    class="w-commerce-commercecheckoutlabel field-label-2">
                                    City *</label>
                                <input
                                    id="txtCity" type="text" name="address_city"
                                    data-wf-conditions="%5B%5D" class="w-commerce-commercecheckoutshippingcity">
                            </div>
                            <div class="w-commerce-commercecheckoutcolumn">
                                <label
                                    class="w-commerce-commercecheckoutlabel field-label-2">
                                    State/Province</label>
                                <input
                                    id="txtState" type="text" name="address_state"
                                    data-wf-conditions="%5B%5D"
                                    class="w-commerce-commercecheckoutshippingstateprovince">
                            </div>
                            <div class="w-commerce-commercecheckoutcolumn">
                                <label
                                    class="w-commerce-commercecheckoutlabel field-label-2">
                                    Zip/Postal Code
                                        *</label>
                                <input id="txtZipCode" type="text" name="address_zip"
                                    data-wf-conditions="%5B%5D"
                                    class="w-commerce-commercecheckoutshippingzippostalcode">
                            </div>
                        </div>
                        <label class="w-commerce-commercecheckoutlabel">Country *</label>
                        <select
                            id="selCountry" name="address_country"
                            data-wf-conditions="%5B%5D"
                            class="w-commerce-commercecheckoutshippingcountryselector">
                            <option value="select">-- select --</option>
                            <option value="AF">Afghanistan</option>
                            <option value="AX">Aland Islands</option>
                            <option value="AL">Albania</option>
                            <option value="DZ">Algeria</option>
                            <option value="AS">American Samoa</option>
                            <option value="AD">Andorra</option>
                            <option value="AO">Angola</option>
                            <option value="AI">Anguilla</option>
                            <option value="AG">Antigua and Barbuda</option>
                            <option value="AR">Argentina</option>
                            <option value="AM">Armenia</option>
                            <option value="AW">Aruba</option>
                            <option value="AU">Australia</option>
                            <option value="AT">Austria</option>
                            <option value="AZ">Azerbaijan</option>
                            <option value="BS">Bahamas</option>
                            <option value="BH">Bahrain</option>
                            <option value="BD">Bangladesh</option>
                            <option value="BB">Barbados</option>
                            <option value="BY">Belarus</option>
                            <option value="BE">Belgium</option>
                            <option value="BZ">Belize</option>
                            <option value="BJ">Benin</option>
                            <option value="BM">Bermuda</option>
                            <option value="BT">Bhutan</option>
                            <option value="BO">Bolivia</option>
                            <option value="BQ">Bonaire, Saint Eustatius and Saba</option>
                            <option value="BA">Bosnia and Herzegovina</option>
                            <option value="BW">Botswana</option>
                            <option value="BR">Brazil</option>
                            <option value="IO">British Indian Ocean Territory</option>
                            <option value="VG">British Virgin Islands</option>
                            <option value="BN">Brunei</option>
                            <option value="BG">Bulgaria</option>
                            <option value="BF">Burkina Faso</option>
                            <option value="BI">Burundi</option>
                            <option value="KH">Cambodia</option>
                            <option value="CM">Cameroon</option>
                            <option value="CA">Canada</option>
                            <option value="CV">Cape Verde</option>
                            <option value="KY">Cayman Islands</option>
                            <option value="CF">Central African Republic</option>
                            <option value="TD">Chad</option>
                            <option value="CL">Chile</option>
                            <option value="CN">China</option>
                            <option value="CX">Christmas Island</option>
                            <option value="CC">Cocos Islands</option>
                            <option value="CO">Colombia</option>
                            <option value="KM">Comoros</option>
                            <option value="CK">Cook Islands</option>
                            <option value="CR">Costa Rica</option>
                            <option value="HR">Croatia</option>
                            <option value="CU">Cuba</option>
                            <option value="CW">Curacao</option>
                            <option value="CY">Cyprus</option>
                            <option value="CZ">Czechia</option>
                            <option value="CD">Democratic Republic of the Congo</option>
                            <option value="DK">Denmark</option>
                            <option value="DJ">Djibouti</option>
                            <option value="DM">Dominica</option>
                            <option value="DO">Dominican Republic</option>
                            <option value="EC">Ecuador</option>
                            <option value="EG">Egypt</option>
                            <option value="SV">El Salvador</option>
                            <option value="GQ">Equatorial Guinea</option>
                            <option value="ER">Eritrea</option>
                            <option value="EE">Estonia</option>
                            <option value="ET">Ethiopia</option>
                            <option value="FK">Falkland Islands</option>
                            <option value="FO">Faroe Islands</option>
                            <option value="FJ">Fiji</option>
                            <option value="FI">Finland</option>
                            <option value="FR">France</option>
                            <option value="GF">French Guiana</option>
                            <option value="PF">French Polynesia</option>
                            <option value="TF">French Southern Territories</option>
                            <option value="GA">Gabon</option>
                            <option value="GM">Gambia</option>
                            <option value="GE">Georgia</option>
                            <option value="DE">Germany</option>
                            <option value="GH">Ghana</option>
                            <option value="GI">Gibraltar</option>
                            <option value="GR">Greece</option>
                            <option value="GL">Greenland</option>
                            <option value="GD">Grenada</option>
                            <option value="GP">Guadeloupe</option>
                            <option value="GU">Guam</option>
                            <option value="GT">Guatemala</option>
                            <option value="GG">Guernsey</option>
                            <option value="GN">Guinea</option>
                            <option value="GW">Guinea-Bissau</option>
                            <option value="GY">Guyana</option>
                            <option value="HT">Haiti</option>
                            <option value="HN">Honduras</option>
                            <option value="HK">Hong Kong</option>
                            <option value="HU">Hungary</option>
                            <option value="IS">Iceland</option>
                            <option value="IN">India</option>
                            <option value="ID">Indonesia</option>
                            <option value="IR">Iran</option>
                            <option value="IQ">Iraq</option>
                            <option value="IE">Ireland</option>
                            <option value="IM">Isle of Man</option>
                            <option value="IL">Israel</option>
                            <option value="IT">Italy</option>
                            <option value="CI">Ivory Coast</option>
                            <option value="JM">Jamaica</option>
                            <option value="JP">Japan</option>
                            <option value="JE">Jersey</option>
                            <option value="JO">Jordan</option>
                            <option value="KZ">Kazakhstan</option>
                            <option value="KE">Kenya</option>
                            <option value="KI">Kiribati</option>
                            <option value="XK">Kosovo</option>
                            <option value="KW">Kuwait</option>
                            <option value="KG">Kyrgyzstan</option>
                            <option value="LA">Laos</option>
                            <option value="LV">Latvia</option>
                            <option value="LB">Lebanon</option>
                            <option value="LS">Lesotho</option>
                            <option value="LR">Liberia</option>
                            <option value="LY">Libya</option>
                            <option value="LI">Liechtenstein</option>
                            <option value="LT">Lithuania</option>
                            <option value="LU">Luxembourg</option>
                            <option value="MO">Macao</option>
                            <option value="MK">Macedonia</option>
                            <option value="MG">Madagascar</option>
                            <option value="MW">Malawi</option>
                            <option value="MY">Malaysia</option>
                            <option value="MV">Maldives</option>
                            <option value="ML">Mali</option>
                            <option value="MT">Malta</option>
                            <option value="MH">Marshall Islands</option>
                            <option value="MQ">Martinique</option>
                            <option value="MR">Mauritania</option>
                            <option value="MU">Mauritius</option>
                            <option value="YT">Mayotte</option>
                            <option value="MX">Mexico</option>
                            <option value="FM">Micronesia</option>
                            <option value="MD">Moldova</option>
                            <option value="MC">Monaco</option>
                            <option value="MN">Mongolia</option>
                            <option value="ME">Montenegro</option>
                            <option value="MS">Montserrat</option>
                            <option value="MA">Morocco</option>
                            <option value="MZ">Mozambique</option>
                            <option value="MM">Myanmar</option>
                            <option value="NA">Namibia</option>
                            <option value="NR">Nauru</option>
                            <option value="NP">Nepal</option>
                            <option value="NL">Netherlands</option>
                            <option value="NC">New Caledonia</option>
                            <option value="NZ">New Zealand</option>
                            <option value="NI">Nicaragua</option>
                            <option value="NE">Niger</option>
                            <option value="NG">Nigeria</option>
                            <option value="NU">Niue</option>
                            <option value="NF">Norfolk Island</option>
                            <option value="KP">North Korea</option>
                            <option value="MP">Northern Mariana Islands</option>
                            <option value="NO">Norway</option>
                            <option value="OM">Oman</option>
                            <option value="PK">Pakistan</option>
                            <option value="PW">Palau</option>
                            <option value="PS">Palestinian Territory</option>
                            <option value="PA">Panama</option>
                            <option value="PG">Papua New Guinea</option>
                            <option value="PY">Paraguay</option>
                            <option value="PE">Peru</option>
                            <option value="PH">Philippines</option>
                            <option value="PN">Pitcairn</option>
                            <option value="PL">Poland</option>
                            <option value="PT">Portugal</option>
                            <option value="PR">Puerto Rico</option>
                            <option value="QA">Qatar</option>
                            <option value="CG">Republic of the Congo</option>
                            <option value="RE">Reunion</option>
                            <option value="RO">Romania</option>
                            <option value="RU">Russia</option>
                            <option value="RW">Rwanda</option>
                            <option value="BL">Saint Barthelemy</option>
                            <option value="SH">Saint Helena</option>
                            <option value="KN">Saint Kitts and Nevis</option>
                            <option value="LC">Saint Lucia</option>
                            <option value="MF">Saint Martin</option>
                            <option value="PM">Saint Pierre and Miquelon</option>
                            <option value="VC">Saint Vincent and the Grenadines</option>
                            <option value="WS">Samoa</option>
                            <option value="SM">San Marino</option>
                            <option value="ST">Sao Tome and Principe</option>
                            <option value="SA">Saudi Arabia</option>
                            <option value="SN">Senegal</option>
                            <option value="RS">Serbia</option>
                            <option value="SC">Seychelles</option>
                            <option value="SL">Sierra Leone</option>
                            <option value="SG">Singapore</option>
                            <option value="SX">Sint Maarten</option>
                            <option value="SK">Slovakia</option>
                            <option value="SI">Slovenia</option>
                            <option value="SB">Solomon Islands</option>
                            <option value="SO">Somalia</option>
                            <option value="ZA">South Africa</option>
                            <option value="GS">South Georgia and the South Sandwich Islands</option>
                            <option value="KR">South Korea</option>
                            <option value="SS">South Sudan</option>
                            <option value="ES">Spain</option>
                            <option value="LK">Sri Lanka</option>
                            <option value="SD">Sudan</option>
                            <option value="SR">Suriname</option>
                            <option value="SJ">Svalbard and Jan Mayen</option>
                            <option value="SZ">Swaziland</option>
                            <option value="SE">Sweden</option>
                            <option value="CH">Switzerland</option>
                            <option value="SY">Syria</option>
                            <option value="TW">Taiwan</option>
                            <option value="TJ">Tajikistan</option>
                            <option value="TZ">Tanzania</option>
                            <option value="TH">Thailand</option>
                            <option value="TL">Timor Leste</option>
                            <option value="TG">Togo</option>
                            <option value="TK">Tokelau</option>
                            <option value="TO">Tonga</option>
                            <option value="TT">Trinidad and Tobago</option>
                            <option value="TN">Tunisia</option>
                            <option value="TR">Turkey</option>
                            <option value="TM">Turkmenistan</option>
                            <option value="TC">Turks and Caicos Islands</option>
                            <option value="TV">Tuvalu</option>
                            <option value="VI">U.S. Virgin Islands</option>
                            <option value="UG">Uganda</option>
                            <option value="UA">Ukraine</option>
                            <option value="AE">United Arab Emirates</option>
                            <option value="GB">United Kingdom</option>
                            <option value="US">United States</option>
                            <option value="UM">United States Minor Outlying Islands</option>
                            <option value="UY">Uruguay</option>
                            <option value="UZ">Uzbekistan</option>
                            <option value="VU">Vanuatu</option>
                            <option value="VA">Vatican</option>
                            <option value="VE">Venezuela</option>
                            <option value="VN">Vietnam</option>
                            <option value="WF">Wallis and Futuna</option>
                            <option value="EH">Western Sahara</option>
                            <option value="YE">Yemen</option>
                            <option value="ZM">Zambia</option>
                            <option value="ZW">Zimbabwe</option>
                        </select>
                    </fieldset>
                </div>
                <%--<div data-node-type="commerce-checkout-shipping-methods-wrapper"
                    class="w-commerce-commercecheckoutshippingmethodswrapper">
                    <div class="w-commerce-commercecheckoutblockheader block-header">
                        <h4>Shipping Method</h4>
                    </div>
                    <fieldset>
                        <script type="text/x-wf-template"
                            id="wf-template-5c3dfbded636579ba04c338000000000003a">%3Clabel%20class%3D%22w-commerce-commercecheckoutshippingmethoditem%22%3E%3Cinput%20type%3D%22radio%22%20required%3D%22%22%20data-wf-bindings%3D%22%255B%257B%2522id%2522%253A%257B%2522type%2522%253A%2522PlainText%2522%252C%2522filter%2522%253A%257B%2522type%2522%253A%2522identity%2522%252C%2522params%2522%253A%255B%255D%257D%252C%2522dataPath%2522%253A%2522database.commerceOrder.availableShippingMethods%255B%255D.id%2522%257D%257D%252C%257B%2522value%2522%253A%257B%2522type%2522%253A%2522PlainText%2522%252C%2522filter%2522%253A%257B%2522type%2522%253A%2522identity%2522%252C%2522params%2522%253A%255B%255D%257D%252C%2522dataPath%2522%253A%2522database.commerceOrder.availableShippingMethods%255B%255D.id%2522%257D%257D%252C%257B%2522checked%2522%253A%257B%2522type%2522%253A%2522Bool%2522%252C%2522filter%2522%253A%257B%2522type%2522%253A%2522identity%2522%252C%2522params%2522%253A%255B%255D%257D%252C%2522dataPath%2522%253A%2522database.commerceOrder.availableShippingMethods%255B%255D.selected%2522%257D%257D%255D%22%20data-wf-conditions%3D%22%255B%255D%22%20name%3D%22shipping-method-choice%22%2F%3E%3Cdiv%20class%3D%22w-commerce-commercecheckoutshippingmethoddescriptionblock%22%3E%3Cdiv%20data-wf-bindings%3D%22%255B%257B%2522innerHTML%2522%253A%257B%2522type%2522%253A%2522PlainText%2522%252C%2522filter%2522%253A%257B%2522type%2522%253A%2522identity%2522%252C%2522params%2522%253A%255B%255D%257D%252C%2522dataPath%2522%253A%2522database.commerceOrder.availableShippingMethods%255B%255D.name%2522%257D%257D%255D%22%20data-wf-conditions%3D%22%255B%255D%22%20class%3D%22w-commerce-commerceboldtextblock%20text-block-2%20w-dyn-bind-empty%22%3E%3C%2Fdiv%3E%3Cdiv%20data-wf-bindings%3D%22%255B%257B%2522innerHTML%2522%253A%257B%2522type%2522%253A%2522PlainText%2522%252C%2522filter%2522%253A%257B%2522type%2522%253A%2522identity%2522%252C%2522params%2522%253A%255B%255D%257D%252C%2522dataPath%2522%253A%2522database.commerceOrder.availableShippingMethods%255B%255D.description%2522%257D%257D%255D%22%20data-wf-conditions%3D%22%255B%255D%22%20class%3D%22text-block-3%20w-dyn-bind-empty%22%3E%3C%2Fdiv%3E%3C%2Fdiv%3E%3Cdiv%20data-wf-bindings%3D%22%255B%257B%2522innerHTML%2522%253A%257B%2522type%2522%253A%2522CommercePrice%2522%252C%2522filter%2522%253A%257B%2522type%2522%253A%2522price%2522%252C%2522params%2522%253A%255B%255D%257D%252C%2522dataPath%2522%253A%2522database.commerceOrder.availableShippingMethods%255B%255D.price%2522%257D%257D%255D%22%20data-wf-conditions%3D%22%255B%255D%22%20class%3D%22price%20smaller%22%3E%240.00%3C%2Fdiv%3E%3C%2Flabel%3E</script>
                        <div data-node-type="commerce-checkout-shipping-methods-empty-state"
                            class="w-commerce-commercecheckoutshippingmethodsemptystate">
                            <div>No shipping methods are available for the address given.</div>
                        </div>
                    </fieldset>
                </div>--%>
                <div class="w-commerce-commercecheckoutorderitemswrapper">
                    <div class="w-commerce-commercecheckoutsummaryblockheader">
                        <h4>Items in Order</h4>
                    </div>
                    <fieldset class="w-commerce-commercecheckoutblockcontent">
                        <div class="w-commerce-commercecheckoutorderitemslist">
                            <input type="hidden" id="hdCheckoutCartList" name="hdCheckoutCartList" value="" />
                            <input type="hidden" id="hdLockedCartList" name="hdLockedCartList" value="" />
                            <div id="divItemSummary" style="display: flex; flex-direction: column; justify-content: space-between; width: 100%;">
                            </div>
                        </div>
                    </fieldset>
                    <div style="display: flex; border-top: none; border-right: 1px solid rgb(230, 230, 230); border-bottom: 1px solid rgb(230, 230, 230); border-left: 1px solid rgb(230, 230, 230); border-image: initial; background-color: rgb(255, 255, 255); justify-content: space-between; padding: 8px 16px;">
                        <div class="text-block-2">Total</div>
                        <div><span id="spanCurrency"></span>&nbsp<span id="spanSubTotal"></span></div>
                    </div>
                </div>
                <%--<div class="w-commerce-commercecheckoutorderitemswrapper">
                    <div class="w-commerce-commercecheckoutsummaryblockheader">
                        <h4>Payment Method</h4>
                    </div>
                    <fieldset class="w-commerce-commercecheckoutblockcontent" style="height: 120px;">
                        <div class="w-commerce-commercecheckoutorderitemslist">
                            <label class="w-commerce-commercecheckoutlabel">Select Payment Method</label>
                            <select
                                name="address_country"
                                class="w-commerce-commercecheckoutshippingcountryselector"
                                style="margin-bottom: 16px;">
                                <option value="points_only">Points Redemption</option>
                                <option value="point_cash">Points + Cash</option>
                                <option value="cash_only">Cash Only</option>
                            </select>
                        </div>
                    </fieldset>
                    <fieldset class="w-commerce-commercecheckoutblockcontent"
                        style="margin-bottom: 16px; height: 90px;">
                        <div class="w-commerce-commercecheckoutorderitemslist">
                            <div style="display: flex; justify-content: space-between;">
                                <div class="text-block-2">Your current points</div>
                                <div>0</div>
                            </div>
                            <div style="display: flex; justify-content: space-between;">
                                <div class="text-block-2">Point after use</div>
                                <div>0</div>
                            </div>
                        </div>
                    </fieldset>
                </div>--%>
            </div>
            <div class="w-commerce-commercelayoutsidebar sticky-sidebar">
                <div class="w-commerce-commercecheckoutordersummarywrapper">
                    <div class="w-commerce-commercecheckoutsummaryblockheader">
                        <h4>Order Summary</h4>
                    </div>
                    <fieldset class="w-commerce-commercecheckoutblockcontent">
                        <div style="display: flex; justify-content: space-between;">
                            <div class="text-block-2">Price (<span id="spanNoOfItems"></span> Items)</div>
                            <div><span id="spanTotalPrice"></span></div>
                        </div>
                        <%--<div style="display: flex; justify-content: space-between; margin-top: 10px;">
                            <div class="text-block-2">Delivery Charges</div>
                            <div><span id="spanDeliveryCharges"></span></div>
                        </div>--%>
                    </fieldset>
                    <fieldset class="w-commerce-commercecheckoutblockcontent">
                        <div style="display: flex; justify-content: space-between;">
                            <div class="text-block-2">Amount Payable</div>
                            <div><span id="spanPayableCurrency"></span>&nbsp<span id="spanAmountPayable"></span></div>
                        </div>
                    </fieldset>
                </div>

                <div class="w-commerce-commercecheckoutplaceorderbutton checkout-button" onclick="var retvalue = CartCheckout(); event.returnValue = retvalue; return retvalue;">Place Order</div>
                <div data-node-type="commerce-checkout-error-state"
                    class="w-commerce-commercecheckouterrorstate" style="display: none;">
                    <div class="w-checkout-error-msg"
                        data-w-info-error="There was an error processing your customer info.  Please try again, or contact us if you continue to have problems."
                        data-w-shipping-error="Sorry. We can’t ship your order to the address provided."
                        data-w-billing-error="Your payment could not be completed with the payment information provided.  Please make sure that your card and billing address information is correct, or try a different payment card, to complete this order.  Contact us if you continue to have problems."
                        data-w-payment-error="There was an error processing your payment.  Please try again, or contact us if you continue to have problems.">
                        There was an error processing your customer info. Please try again, or contact us if you
                                continue to have problems.
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="hdnShippingFee" data-value="@Request.RequestContext.HttpContext.Session['ShippingFee']" />
    <div id="divLoader" class="divLoader" style="display: none;"></div>
    <script src="js/jquery-3.3.1.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            GetUserProfile();
            GetCheckOutItem('open');
        });

        function GetUserProfile() {
            $('#divLoader').show();
            $.ajax({
                url: 'Profile.aspx/GetUserProfile',
                type: 'POST',
                contentType: 'application/json; charset =utf-8',
                data: "",
                dataType: 'json',
                success: function (data) {
                    $('#divLoader').hide();
                    try {
                        var newData = data.d;
                        if (newData.ResponseMessage == "SUCCESS") {
                            if (newData.UserDetailsList[0].givenName != null) {
                                $("#txtFullName").val(newData.UserDetailsList[0].givenName + " " + newData.UserDetailsList[0].familyName);
                            }
                            $("#txtPhoneNo").val(newData.UserDetailsList[0].tel[0].value);
                            $("#txtAddressLine1").val(newData.UserDetailsList[0].adr[0].streetaddress);
                            $("#txtCity").val(newData.UserDetailsList[0].adr[0].locality);
                            $("#txtState").val(newData.UserDetailsList[0].adr[0].region);
                            $("#txtZipCode").val(newData.UserDetailsList[0].adr[0].postalcode);
                            $("#selCountry").val(newData.UserDetailsList[0].adr[0].countryname);
                            
                        }
                        else if (newData.ResponseMessage != "") {

                        }
                        else {
                            swal("Oops!", "Something went wrong", "error");
                        }
                    }
                    catch (e) {
                        swal("Oops!", "Something went wrong", "error");
                        return false;
                    }
                    return false;
                },
                error: function (errmsg) {
                    $('#divLoader').hide();
                    swal("Oops!", "Something went wrong", "error");
                }
            });
        }

        function GetCheckOutItem(strCartStatus) {
            $('#divLoader').show();
            var itemCount = 0;
            $.ajax({
                url: 'Checkout.aspx/GetCartItemsByStatus',
                type: 'POST',
                contentType: 'application/json; charset=utf-8',
                data: "{'pstrCartStatus':'" + strCartStatus.toString() + "'}",
                dataType: 'json',
                success: function (data) {
                    try {
                        $("#divItemSummary").empty();
                        var divSummaryHtml = '', itemTotalPrice = 0, itemDeliveryCharges = 0;
                        var checkOutCartIdList = [];
                        var cartData = data.d;
                        if (cartData != null) {
                            for (var i = 0; i < cartData.length; i++) {
                                if (cartData[i].CartItems != null && cartData[i].CartItems.length > 0) {
                                    itemTotalPrice += cartData[i].value;
                                    itemDeliveryCharges += cartData[i].shipping_fee;
                                    for (var j = 0; j < cartData[i].CartItems.length; j++) {
                                        divSummaryHtml += '<div><div style="display: flex; flex-direction: row; justify-content: space-between; width: 100%;">' +
                                            '<div style="display: flex; flex-direction: row; width: 100%;">' +
                                            '<div style="margin-right: 30px;">' +
                                            '<img src="' + cartData[i].CartItems[j].Img + '" style="width: 10vw; height: 9vw;"></div><div>' +
                                            '<p style="font-size: 16px; font-weight: 700;">' + cartData[i].CartItems[j].Name + '</p>' +
                                            '<p style="font-size: 15px; font-weight: 500; color: rgb(145, 145, 151);"> ' + cartData[i].Currency.toUpperCase() + ' ' + cartData[i].CartItems[j].Value + '</p>' +
                                            '<p style="font-size: 15px; font-weight: 500;">Qty: ' + cartData[i].CartItems[j].Quantity + '</p>' +
                                            '<a href="#" class="remove-button-cart in-cart-page w-inline-block" data-wf-cart-action="remove-item" style="cursor:pointer;" onclick="return RemoveCheckOutItem(&quot;' + cartData[i].CartItems[j].Id.toString() + '&quot;)">' +
                                            '<div class="shop-text dark">Remove</div>' +
                                            '<div data-w-id="8cc31205-9369-1f71-260d-63dbb3fc58f2" class="line-cart"></div></a></div></div></div>' +
                                            '<div class="ant-divider ant-divider-horizontal"></div></div>';
                                        itemCount += cartData[i].CartItems[j].Quantity;
                                        if (!checkOutCartIdList.includes(cartData[i].CartItems[j].CartId)) {
                                            checkOutCartIdList.push(cartData[i].CartItems[j].CartId);
                                        }
                                    }
                                }
                            }
                            if (itemCount == 0) {
                                divSummaryHtml = 'No items found!';
                            }
                            $("#divItemSummary").append(divSummaryHtml);
                            $("#spanSubTotal").text(itemTotalPrice.toFixed(2));
                            $("#spanTotalPrice").text(itemTotalPrice.toFixed(2));
                            //$("#spanDeliveryCharges").text(itemDeliveryCharges.toFixed(2));
                            $("#spanNoOfItems").text(itemCount.toString());
                            $("#spanAmountPayable").text((itemTotalPrice + itemDeliveryCharges).toFixed(2));
                            $("#hdCheckoutCartList").val(checkOutCartIdList);
                            $("#spanCurrency").text(cartData[0].Currency.toUpperCase());
                            $("#spanPayableCurrency").text(cartData[0].Currency.toUpperCase());
                            
                        }
                        else {
                            $("#divItemSummary").append("No items found!");
                            $("#spanSubTotal").text(itemTotalPrice.toString());
                            $("spanTotalPrice").text(itemTotalPrice.toFixed(2));
                            $("spanNoOfItems").text(itemCount.toString());
                            $("spanAmountPayable").text(itemTotalPrice.toFixed(2));
                            $("#spanCurrency").text("");
                            $("#spanPayableCurrency").text("");
                        }
                        $('#divLoader').hide();
                    } catch (e) {
                        $("#divItemSummary").append("No items found!");
                        $("#spanSubTotal").text(itemTotalPrice.toString());
                        $('#divLoader').hide();
                        return false;
                    }
                    return false;
                },
                error: function (errmsg) {
                    $("#divItemSummary").append("No items found!");
                    $("#spanSubTotal").text(itemTotalPrice.toString());
                    $('#divLoader').hide();
                    swal("Oops!", "Something went wrong, please try again.", "error");
                }
            });
        }

        function RemoveCheckOutItem(strCartItemId) {
            $('#divLoader').show();
            $.ajax({
                url: 'Checkout.aspx/RemoveCartItem',
                type: 'POST',
                contentType: 'application/json; charset=utf-8',
                data: "{'pstrCartItemId':'" + strCartItemId.toString() + "'}",
                dataType: 'json',
                success: function (data) {
                    try {
                        var strResponse = data.d;
                        if (strResponse == 'deleted') {
                            GetCartItemCount('open');
                            GetCheckOutItem('open');
                        }
                        else if (strResponse != "") {
                            swal(" ", strResponse, "error");
                        }
                        $('#divLoader').hide();
                    } catch (e) {
                        swal("Oops!", "Something went wrong, please try again.", "error");
                        $('#divLoader').hide();
                        return false;
                    }
                    return false;
                },
                error: function (errmsg) {
                    swal("Oops!", "Something went wrong, please try again.", "error");
                    $('#divLoader').hide();
                }
            });
        }

        function CartCheckout() {
            var customerEmail = $("#ContentPlaceHolder1_txtEmail").val();
            var customerName = $("#txtFullName").val();
            var customerPhoneNo = $("#txtPhoneNo").val();
            var customerAddress = $("#txtAddressLine1").val() + $("#txtAddressLine2").val();
            var customerCity = $("#txtCity").val();
            var customerState = $("#txtState").val();
            var customerZipCode = $("#txtZipCode").val();
            var customerCountry = $("#selCountry").val();
            var providerKey = 'retailer';
            var deliveryId = '';
            var isValidated = false;
            var isLockCartSuccess = false;

            if (customerEmail.length > 0) {
                $("#ContentPlaceHolder1_txtEmail").removeClass('w-commerce-commercecheckouterrorstate');
                isValidated = true;
            }
            else {
                $("#ContentPlaceHolder1_txtEmail").addClass('w-commerce-commercecheckouterrorstate');
                isValidated = false;
            }

            if (customerName.length > 0) {
                $("#txtFullName").removeClass('w-commerce-commercecheckouterrorstate');
                isValidated = true;
            }
            else {
                $("#txtFullName").addClass('w-commerce-commercecheckouterrorstate');
                isValidated = false;
            }

            if (customerPhoneNo.length > 0) {
                $("#txtPhoneNo").removeClass('w-commerce-commercecheckouterrorstate');
                isValidated = true;
            }
            else {
                $("#txtPhoneNo").addClass('w-commerce-commercecheckouterrorstate');
                isValidated = false;
            }

            if (customerAddress.length > 0) {
                $("#txtAddressLine1").removeClass('w-commerce-commercecheckouterrorstate');
                isValidated = true;
            }
            else {
                $("#txtAddressLine1").addClass('w-commerce-commercecheckouterrorstate');
                isValidated = false;
            }

            if (customerCity.length > 0) {
                $("#txtCity").removeClass('w-commerce-commercecheckouterrorstate');
                isValidated = true;
            }
            else {
                $("#txtCity").addClass('w-commerce-commercecheckouterrorstate');
                isValidated = false;
            }

            if (customerState.length > 0) {
                $("#txtState").removeClass('w-commerce-commercecheckouterrorstate');
                isValidated = true;
            }
            else {
                $("#txtState").addClass('w-commerce-commercecheckouterrorstate');
                isValidated = false;
            }

            if (customerZipCode.length > 0) {
                $("#txtZipCode").removeClass('w-commerce-commercecheckouterrorstate');
                isValidated = true;
            }
            else {
                $("#txtZipCode").addClass('w-commerce-commercecheckouterrorstate');
                isValidated = false;
            }

            if (customerCountry != "select") {
                $("#selCountry").removeClass('w-commerce-commercecheckouterrorstate');
                isValidated = true;
            }
            else {
                $("#selCountry").addClass('w-commerce-commercecheckouterrorstate');
                isValidated = false;
            }

            if (isValidated) {
                try {



                    $('#divLoader').show();
                    var cartId = $("#hdCheckoutCartList").val();
                    var lockCartStatus = '';
                    var lockedCartList = [];
                    if (cartId != "") {
                        var checkoutCartList = cartId.split(",");
                        if (checkoutCartList.length > 0) {
                            for (var i = 0; i < checkoutCartList.length; i++) {
                                lockCartStatus = LockCartForPlaceOrder(checkoutCartList[i], providerKey, customerName, customerAddress, customerEmail, customerCity, customerState, customerCountry, customerZipCode, customerPhoneNo, deliveryId);
                                if (lockCartStatus == 'locked') {
                                    lockedCartList.push(checkoutCartList[i]);
                                    isLockCartSuccess = true;
                                }
                            }
                            $("#hdLockedCartList").val(lockedCartList)
                        }
                        $('#divLoader').hide();
                        if (isLockCartSuccess) {
                            window.location.href = "PostPayment.aspx"
                            ClearFields();
                            ClearSession('CartId');
                        }
                        else {
                            //alert(lockCartStatus);
                        }
                    }
                    else {
                        $('#divLoader').hide();
                        swal("Failed!", "No any cart item found to place order.", "warning");
                    }

                } catch (e) {
                    $('#divLoader').hide();
                    swal("Oops!", "Something went wrong, please try again.", "warning");
                    return false;
                }
            }
            else {
                swal(" ", "Please fill the required fields!", "warning");
            }
        }

        function LockCartForPlaceOrder(cartId, providerKey, customerName, customerAddress, customerEmail, customerCity, customerState, customerCountry, customerZipCode, customerPhoneNo, deliveryId) {
            var retValue;
            var postData = {
                pstrCartId: cartId,
                pstrProviderKey: providerKey,
                pstrName: customerName,
                pstrAddress: customerAddress,
                pstrEmail : customerEmail,
                pstrCity: customerCity,
                pstrState: customerState,
                pstrCountry: customerCountry,
                pstrZip: customerZipCode,
                pstrPhone: customerPhoneNo,
                pstrDeliveryId: deliveryId
            };
            var jsonData = JSON.stringify(postData);
            $.ajax({
                url: 'Checkout.aspx/CartCheckout',
                type: 'POST',
                contentType: 'application/json; charset=utf-8',
                data: jsonData,
                dataType: 'json',
                success: function (data) {
                    var newData = data.d;
                    if (newData != "" || newData == null) {
                        retValue = newData;
                    }
                },
                error: function (errmsg) {
                    $('#divLoader').hide();
                },
                async: false
            });
            return retValue;
        }

        function ClearFields() {
            $("#ContentPlaceHolder1_txtEmail").val('');
            $("#txtFullName").val('');
            $("#txtPhoneNo").val('');
            $("#txtAddressLine1").val('');
            $("#txtAddressLine2").val('');
            $("#txtCity").val('');
            $("#txtState").val('');
            $("#txtZipCode").val('');
            $("#selCountry").val('');
        }

        function ClearSession(sessionName) {
            $.ajax({
                url: 'Checkout.aspx/ClearSession',
                type: 'POST',
                contentType: 'application/json; charset=utf-8',
                data: "{'pstrSessionName':'" + sessionName.toString() + "'}",
                dataType: 'json',
                success: function (data) {
                },
                error: function (errmsg) {
                    $('#divLoader').hide();
                },
                async: false
            });
        }
    </script>
</asp:Content>
