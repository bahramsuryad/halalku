﻿using Core.Platform.Transactions.Entites;
using Framework.EnterpriseLibrary.Adapters;
using Giift.SuciHalal.Entities;
using Giift.SuciHalal.Helper;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace SuciHalalWebclient
{
    public partial class PostPayment : System.Web.UI.Page
    {
        public static string lstrAccessToken = string.Empty;

        public string lstrInfiPayCurrency = Convert.ToString(WebConfigurationManager.AppSettings["InfiPayCurrency"]);
        public string lstrPGMerchantHash = Convert.ToString(WebConfigurationManager.AppSettings["PGMerchantHash"]);
        public string lstrPGMerchantId = Convert.ToString(WebConfigurationManager.AppSettings["PGMerchantId"]);
        public string lstrMerchantName = Convert.ToString(WebConfigurationManager.AppSettings["PGMerchantName"]);
        public string lstrPGURL = Convert.ToString(WebConfigurationManager.AppSettings["PGURL"]);

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {


                    if (HttpContext.Current.Session["AccessToken"] == null)
                    {
                        Response.Redirect("Login.aspx");
                    }
                    else
                    {
                        lstrAccessToken = Convert.ToString(HttpContext.Current.Session["AccessToken"]);
                    }

                    PaymentDetails lobjPaymentDetails = null;
                    //Payment Post Details from Checkout page
                    if (HttpContext.Current.Session["PaymentDetails"] != null)
                    {
                        lobjPaymentDetails = HttpContext.Current.Session["PaymentDetails"] as PaymentDetails;
                        HttpContext.Current.Session["lstrChargeAmount"] = Convert.ToString(lobjPaymentDetails.Amount);
                        HttpContext.Current.Session["lstOrderReference"] = lobjPaymentDetails.OrderId;
                        txtChargeAmount.Value = Convert.ToString(lobjPaymentDetails.Amount);
                        txtOrderReference1.Value = lobjPaymentDetails.OrderId;
                        if(lobjPaymentDetails.ShippingFee > 0)
                        {
                            spanShippinFees.InnerText = "(Includes Shipping Fee: " + lobjPaymentDetails.ShippingFee + ")";
                        }
                    }

                    //LoggingAdapter.WriteLog("CreatePayment Request:" + lstrAccessToken + "#" + lobjPaymentDetails.OrderId + "#" + "stripe");
                    //string lstrResponse = ApiHelper.CreatePayment(lstrAccessToken, lobjPaymentDetails.OrderId, "stripe");

                    LoggingAdapter.WriteLog("CreatePayment Request:" + lstrAccessToken + "#" + lobjPaymentDetails.OrderId + "#" + "payondelivery");
                    string lstrResponse = ApiHelper.CreatePayment(lstrAccessToken, lobjPaymentDetails.OrderId, "payondelivery");

                    LoggingAdapter.WriteLog("CreatePayment Response:" + lstrAccessToken + "#" + lstrResponse);
                    JToken outer = JToken.Parse(lstrResponse);
                    if (outer["error"] != null)
                    {
                        JArray array = outer.Value<JArray>("error");
                        Response.Redirect("PaymentFailed.aspx");
                    }
                    else if (outer["Id"] != null)
                    {

                        lobjPaymentDetails.PaymentId = outer.Value<string>("Id");
                        //lstrTxnId = lobjPaymentDetails.PaymentId;
                        HttpContext.Current.Session["PaymentDetails"] = lobjPaymentDetails;
                        LoggingAdapter.WriteLog("PostPayment OrderId -" + lobjPaymentDetails.OrderId + "# PaymentId-" + lobjPaymentDetails.PaymentId);
                    }

                }
                catch (Exception ex)
                {
                    LoggingAdapter.WriteLog("PostPayment Ex: " + ex.Message + Environment.NewLine + ex.StackTrace + ex.InnerException);
                    Response.Redirect("PaymentFailed.aspx");
                }
            }
            else
            {
                //Not do any
            }
        }

        protected void Button1_Click1(object sender, EventArgs e)
        {
            try
            {
                LoggingAdapter.WriteLog("PostPayment Button1_Click1");
                List<UserDetails> lobjListOfUserDetails = null;
                if (HttpContext.Current.Session["AccessToken"] == null)
                {
                    Response.Redirect("Login.aspx");
                }
                else
                {
                    lstrAccessToken = Convert.ToString(HttpContext.Current.Session["AccessToken"]);
                }

                string lstrChargeAmount = HttpContext.Current.Session["lstrChargeAmount"] as string;
                Session["lstrChargeAmount"] = null;

                string lstOrderReference = HttpContext.Current.Session["lstOrderReference"] as string;
                Session["lstOrderReference"] = null;

                string text = string.Empty;
                UserData userData = new UserData();
                text = ApiHelper.GetUserProfile(lstrAccessToken);
                if (text.Contains("error"))
                {
                    JArray outer = JArray.Parse(text);
                    if (outer["error"] != null)
                    {
                        userData.ResponseMessage = outer.Value<string>("error");
                    }
                }
                else
                {
                    lobjListOfUserDetails = new List<UserDetails>();
                    lobjListOfUserDetails = JsonConvert.DeserializeObject<List<UserDetails>>(text);
                }

                string s = string.Concat(new object[]
                {
                    lstrPGMerchantHash,
                    "|",
                    lstrPGMerchantId,
                    "|",
                    lstrChargeAmount,
                    "|",
                    0,
                    "|",
                    lstOrderReference
                });
                MD5 mD = MD5.Create();
                byte[] bytes = Encoding.ASCII.GetBytes(s);
                byte[] array = mD.ComputeHash(bytes);
                StringBuilder stringBuilder = new StringBuilder();
                for (int i = 0; i < array.Length; i++)
                {
                    stringBuilder.Append(array[i].ToString("X2"));
                }
                TransactionDetails transactionDetails = new TransactionDetails();
                transactionDetails.Amounts = (Convert.ToSingle(lstrChargeAmount));
                transactionDetails.Points = (Convert.ToInt32(0));
                transactionDetails.TransactionDate = (DateTime.Now);
                transactionDetails.TransactionCurrency = (lstrInfiPayCurrency);
                transactionDetails.RelationReference = lobjListOfUserDetails[0].id;
                transactionDetails.TransactionDetailBreakage.Amount = (Convert.ToSingle(lstrChargeAmount));
                Session["PurchasePoints"] = transactionDetails;
                PaymentDetails item = HttpContext.Current.Session["PaymentDetails"] as PaymentDetails;
                string item2 = Convert.ToString(Session["AccessToken"]);
                string item3 = HttpContext.Current.Session["URL"] as string;
                List<object> list = new List<object>
                {
                    transactionDetails,
                    item2,
                    item,
                    item3
                };
                CachingAdapter.Add(lstOrderReference, list);
                string arg = Convert.ToString(stringBuilder);
                base.Response.Clear();
                StringBuilder stringBuilder2 = new StringBuilder();
                stringBuilder2.Append("<html>");
                stringBuilder2.AppendFormat("<body onload='document.forms[\"form\"].submit()'>", new object[0]);
                stringBuilder2.AppendFormat("<form name='form' action='{0}' method='post'>", lstrPGURL);
                stringBuilder2.AppendFormat("<input type='hidden' runat='server' name='PG_MerchantId' value='{0}'>", lstrPGMerchantId);
                stringBuilder2.AppendFormat("<input type='hidden' runat='server' name='PG_MerchantName' value='{0}'>", lstrMerchantName);
                stringBuilder2.AppendFormat("<input type='hidden' runat='server' name='PG_Amount' value='{0}'>", lstrChargeAmount);
                stringBuilder2.AppendFormat("<input type='hidden' runat='server' name='PG_Points' value='{0}'>", "0");
                stringBuilder2.AppendFormat("<input type='hidden' runat='server' name='PG_PaymentType' value='{0}'>", 4);
                stringBuilder2.AppendFormat("<input type='hidden' runat='server' name='PG_RedemptionType' value='{0}'>", 5);
                stringBuilder2.AppendFormat("<input type='hidden' runat='server' name='PG_TxnCurrency' value='{0}'>", "MYR");
                stringBuilder2.AppendFormat("<input type='hidden' runat='server' name='PG_Narration' value='{0}'>", "Purchase Points-" + transactionDetails.RelationReference);
                stringBuilder2.AppendFormat("<input type='hidden' runat='server' name='PG_TxnId' value='{0}'>", lstOrderReference);
                stringBuilder2.AppendFormat("<input type='hidden' runat='server' name='PG_SecureHash' value='{0}'>", arg);
                stringBuilder2.Append("</form>");
                stringBuilder2.Append("</body>");
                stringBuilder2.Append("</html>");
                base.Response.Write(stringBuilder2.ToString());
                base.Response.End();
                LoggingAdapter.WriteLog("PostPayment RelationRef###PostDetails-" + transactionDetails.RelationReference + "###" + stringBuilder2.ToString());
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog(string.Concat(new object[]
                {
                    "PostPayment RelationRef###PostDetails-",
                    ex.Message,
                    ex.InnerException,
                    ex.StackTrace
                }));
            }
        }
    }
}