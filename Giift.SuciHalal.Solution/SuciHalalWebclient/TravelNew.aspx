﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SuciHalalSite.Master" AutoEventWireup="true" CodeBehind="TravelNew.aspx.cs" Inherits="SuciHalalWebclient.TravelNew" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="tools_list" style="background-color: #eee;">
        <div class="banner sub"
            style="background-image: -webkit-gradient(linear, 0% 0%, 0% 100%, from(rgba(0, 0, 0, 0.3)), to(rgba(0, 0, 0, 0.3))), url('https://images.unsplash.com/photo-1504609773096-104ff2c73ba4?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1050&q=80'); background-size: cover; background-position: center 80%;">
            <div class="container center in-sub w-container">
                <h1 class="sub-heading-title">Travel</h1>
                <div class="breadcrumb">
                    <div class="breadcrumb-content">
                        <a class="bread-link" href="Index.aspx">Home</a>
                        <img alt="" src="https://uploads-ssl.webflow.com/5c3ca7fbdc8fab3302b31532/5c41f1941b7ac65ffde90630_right-arrow%20(3).svg" width="18">
                        <div class="bread-link gray-color">Travel</div>
                    </div>
                </div>
            </div>
        </div>
        <div style="width: 100vw; padding-left: 10vw; padding-right: 10vw; padding-top: 10vh; padding-bottom: 15vh;">
            <h3 style="margin-top: 0px; margin-bottom: 10px; text-align: center;">Halal Travel Packages and Tours</h3>
            <h5 style="margin-top: 0px; margin-bottom: 30px; text-align: center;">Book the best Muslim-friendly holiday packages to most popular travel destinations with My Halal Stop</h5>

            <div class="filter-container-travel">
                <div class="ant-tag ant-tag-has-color" style="background-color: rgb(16, 142, 233);">All Region</div>
                <div class="ant-tag ant-tag-blue">Europe</div>
                <div class="ant-tag ant-tag-blue">Asia</div>
                <div class="ant-tag ant-tag-blue">Middle East</div>
                <div class="ant-tag ant-tag-blue">Africa</div>
            </div>


            <div class="donation-list-content-container-new" runat="server">
                <a href="./DonationDetail.aspx">
                    <div class="donation-list-card-new">
                        <img class="donation-list-card-image-new" src="https://media.halaltrip.com/package/original_180501112055-tahrir-square-cairo.jpg" />
                        <div class="donation-list-card-content-new">
                            <p class="travel-list-card-name-new">Weekend Getaway Adventure to Cairo</p>
                            <p class="travel-list-card-price-header-new">Starting from</p>
                            <p class="travel-list-card-price-new">RM 50</p>
                        </div>
                        <div class="donation-list-card-footer-new">
                            <div class="donation-list-card-book-container-new">
                                <p style="margin: 0px; width: 160px;" class="donation-list-card-book-now-new">Book Now</p>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="w-pagination-wrapper pagination"></div>
            <div id="divPagination" style="display: none;" class="center">
                <ul id="ulPagingItems" class="pagination"></ul>
            </div>
        </div>
    </div>
    <div id="divLoader" class="divLoader" style="display: none;"></div>
    <script src="js/jquery-3.3.1.min.js"></script>
    
    <script type="text/javascript">
        var lintPageSize = 15;
        var lintTotalPages = 0;

        $(document).ready(function () {

            $("#ContentPlaceHolder1_divTravelListContainer").text("");
            var latitude = "4.582010";
            var longitude = "101.470624";

            getLocation();
            BindTravelPackages(); 
           
        });

        function BindTravelPackages() {
            
            $('#divLoader').show();
            $.ajax({
                type: "POST",
                url: "Travel.aspx/BindTravelPackages",
                data: "{pintSkipRecords:0,pintTakeRecords:" + lintPageSize + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: true,
                cache: false,
                success: function (msg) {
                    $('#divLoader').hide();
                    if (msg != null) {
                        lintTotalPages = msg.d.TotalPages;
                        if (lintTotalPages > 0) {
                            $("#ContentPlaceHolder1_divTravelListContainer").html("");
                            printTravelListData(msg);
                            $('#divPagination').show();
                            $('#ulPagingItems').twbsPagination({
                                totalPages: lintTotalPages,
                                visiblePages: 10,
                                next: 'Next',
                                prev: 'Prev',
                                onPageClick: function (event, page) {
                                    pageData(page);
                                }
                            });
                        }
                        else {
                            $('#divPagination').hide();
                            $("#ContentPlaceHolder1_divTravelListContainer").text("No records were found.");
                        }
                    }
                    else {
                        $('#divPagination').hide();
                        alert("Oops something went wrong.")
                    }
                },
                error: function () {
                    $('#divLoader').hide();
                    $('#divPagination').hide();
                    $("#ContentPlaceHolder1_divTravelListContainer").text("No records were found.");
                }
            });
        }

        function printTravelListData(msg) {
            $('#divLoader').show();
            var travels = msg.d.PDetails;
            var html = "";
            $("#ContentPlaceHolder1_divTravelListContainer").text(html);
            var count = 0;

            for (var i = 0; i < travels.length; i++) {

                var price = parseFloat(travels[i].Price);
                price = price.toFixed(2);


                html += '<a href="TravelDetail.aspx?id=' + travels[i].Id + '">';
                html += '<div class="travel-list-card">';
                html += '<img class="travel-list-card-image" src="' + travels[i].ImageUrl + '" />';
                html += '<div class="travel-list-card-content">';
                html += '<p class="travel-list-card-name">' + travels[i].Name + '</p>';
                html += '</div>';
                html += '<div class="travel-list-card-footer">';
                html += '<div class="travel-list-card-price-container">';
                //html += '<p style="margin: 0px;">Starting from </p>';
                html += '<p style="font-weight: bold; font-size: 2em; color: green; margin: 0px;"> RM ' + price + '</p>';
                //html += '<p style="margin: 0px;">per person</p>';
                html += '</div>';
                html += '</div><div class="travel-list-card-book-container">';
                html += '<p style="margin: 0px;margin-bottom: 5%;" class="travel-list-card-book-now">Book Now</p>';
                html += '</div>';
                html += '</div>';
                html += '</div>';
                html += '</a>';
            }
            $("#ContentPlaceHolder1_divTravelListContainer").append(html);
            $('#divLoader').hide();
        }

        function pageData(e) {
            $('#divLoader').show();
            var lintSkipRecords = e == 1 ? 0 : (e * lintPageSize) - lintPageSize;
            $.ajax({
                type: "POST",
                url: "Travel.aspx/BindTravelPackages",
                data: "{pintSkipRecords:" + lintSkipRecords + ",pintTakeRecords:" + lintPageSize + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: true,
                cache: false,
                success: function (msg) {
                    $('#divLoader').hide();
                    printTravelListData(msg);
                    $('#divPagination').show();
                },
                error: function () {
                    $('#divLoader').hide();
                    $('#divPagination').hide();
                    $("#ContentPlaceHolder1_divTravelListContainer").text("");
                    $("#ContentPlaceHolder1_divTravelListContainer").text("No records were found.");
                }

            });

            return false;
        }

        function getLocation() {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(showPosition);
            } else {
                x.innerHTML = "Geolocation is not supported by this browser.";
            }
        }

        function showPosition(position) {
            latitude = position.coords.latitude;
            longitude = position.coords.longitude;
        }

    </script>
</asp:Content>
