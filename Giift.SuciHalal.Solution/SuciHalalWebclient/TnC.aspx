﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SuciHalalSite.Master" AutoEventWireup="true" CodeBehind="TnC.aspx.cs" Inherits="SuciHalalWebclient.TnC" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="dashboard animated fadeIn about" style="margin-top: 5px;">
        <div class="banner sub about" id="banner-store"
            style="background-image: url(https://images.unsplash.com/photo-1528356857578-6dc41b9a62e0?ixlib=rb-1.2.1&amp;ixid=eyJhcHBfaWQiOjEyMDd9&amp;auto=format&amp;fit=crop&amp;w=1350&amp;q=80) !important; background-size: cover !important;">
            <div class="container center in-sub w-container">
                <h1 class="sub-heading-title">Terms and Conditions</h1>
                <div class="breadcrumb">
                    <div class="breadcrumb-content">
                        <a class="bread-link" href="Index.aspx">Home</a><img
                            alt=""
                            src="https://uploads-ssl.webflow.com/5c3ca7fbdc8fab3302b31532/5c41f1941b7ac65ffde90630_right-arrow%20(3).svg" width="18">
                        <div class="bread-link gray-color">TnC</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="section">
            <div class="container w-container">
                <div runat="server" id="divTnC" >

                </div>
            </div>
        </div>
    </div>
</asp:Content>

