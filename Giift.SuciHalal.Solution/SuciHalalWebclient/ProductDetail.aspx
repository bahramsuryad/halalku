﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SuciHalalSite.Master" AutoEventWireup="true" CodeBehind="ProductDetail.aspx.cs" Inherits="SuciHalalWebclient.ProductDetail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="">
        <div class="section gray checkout">
            <div class="container w-container">
                <div>
                    <div class="w-row">
                        <div class="w-col w-col-4 w-col-stack">
                            <div>
                                <div>
                                    <div class="w-tabs" data-duration-in="300" data-duration-out="100">
                                        <div class="w-tab-content">
                                            <div class="w-tab-pane w--tab-active" data-w-tab="Tab 1">
                                                <%--<div class="image-wrapper-line">--%>
                                                <img id="imgProductLarge" runat="server"
                                                    alt=""
                                                    data-wf-sku-bindings="%5B%7B%22from%22%3A%22f_main_image_4dr%22%2C%22to%22%3A%22src%22%7D%5D" sizes="100vw" src="https://d16yh42raur1q5.cloudfront.net/img/retailers/bestdeal/1553139342WBXS3/67852da2-4b8a-11e9-9645-0242ae679666_full.jpg">
                                                <%--</div>--%>
                                            </div>
                                        </div>
                                        <div id="divSmallImages" runat="server" class="tabs-menu w-tab-menu">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="w-col w-col-8 w-col-stack">
                            <div class="left-sku-wrapper">
                                <a class="shop-button w-inline-block"
                                    data-w-id="6000e9c3-bc7b-9a42-d848-6e2665b90fe6"
                                    href="Catalogue.aspx">
                                    <div class="shop-text dark">Back to Store</div>
                                    <div class="shop-line black" style="width: 0%;"></div>
                                </a>
                                <h2 class="product-title in-page">
                                    <span id="spanProductName" runat="server"></span>
                                </h2>
                                <%--<div class="sku-wrapper">
                                        <div class="sku-text">SKU:</div>
                                        <div class="sku-text"
                                            data-wf-sku-bindings="%5B%7B%22from%22%3A%22f_sku_%22%2C%22to%22%3A%22innerHTML%22%7D%5D">
                                            <span id="spanSKU" runat="server"></span>
                                        </div>
                                    </div>--%>
                                <div class="top-margin less">
                                    <div class="compare-price left w-condition-invisible">
                                        <div class="price compare in-page w-dyn-bind-empty"
                                            data-wf-sku-bindings="%5B%7B%22from%22%3A%22f_compare_at_price_7dr10dr%22%2C%22to%22%3A%22innerHTML%22%7D%5D">
                                        </div>
                                        <div class="price in-page"
                                            data-wf-sku-bindings="%5B%7B%22from%22%3A%22f_price_%22%2C%22to%22%3A%22innerHTML%22%7D%5D">
                                            <span id="spanCurrency" runat="server"></span>&nbsp<span id="spanPrice" runat="server"></span>
                                        </div>
                                    </div>
                                    <div>
                                        <div class="price in-page"
                                            data-wf-sku-bindings="%5B%7B%22from%22%3A%22f_price_%22%2C%22to%22%3A%22innerHTML%22%7D%5D">
                                            <span id="spanCurrencyFinal" runat="server"></span>&nbsp<span id="spanFinalPrice" runat="server"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="line-divider"></div>
                                <div class="top-margin">
                                    <div>
                                        <label class="field-label"
                                            for="quantity-aafccc62c0cf3312edf3ca7efb16aa17">
                                            Quantity</label><input class="w-commerce-commerceaddtocartquantityinput cart-quantity-2 float"
                                                id="txtQuantity"
                                                min="1"
                                                max="5"
                                                name="commerce-add-to-cart-quantity-input"
                                                type="number"
                                                value="1">
                                        <button class="w-commerce-commerceaddtocartbutton checkout-button with-height" onclick="var retvalue = AddToCart(); event.returnValue = retvalue; return retvalue;">
                                            Add To Cart
                                        </button>
                                        <div class="w-commerce-commerceaddtocartoutofstock"
                                            style="display: none;">
                                            <div>This product is out of stock.</div>
                                        </div>
                                        <div class="w-commerce-commerceaddtocarterror error-state"
                                            data-node-type="commerce-add-to-cart-error"
                                            style="display: none;">
                                            <div class="w-add-to-cart-error-msg"
                                                data-w-add-to-cart-general-error="Something went wrong when adding this item to the cart."
                                                data-w-add-to-cart-quantity-error="Product is not available in this quantity.">
                                                Product is not available in this quantity.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="w-row">
                        <div class="w-col w-col-12 w-col-stack">
                            <p>
                                <span id="spanDescription" runat="server"></span>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField runat="server" ID="hfProductType" />
    <asp:HiddenField runat="server" ID="hfProductCCY" />

    <div id="divLoader" class="divLoader" style="display: none;"></div>
    <script src="js/jquery-3.3.1.min.js"></script>
    <script type="text/javascript">
        function getUrlParameter(name) {
            name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
            var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
            var results = regex.exec(location.search);
            return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
        };

        function AddToCart() {
            var lstrProductId = getUrlParameter("id");
            var lintProductQuantity = $("#txtQuantity").val();
            var lstrProductType = $("#ContentPlaceHolder1_hfProductType").val();
            var lstrProductCCY = $("#ContentPlaceHolder1_hfProductCCY").val();

            if (lintProductQuantity == "" || lintProductQuantity == "0") {
                $("#txtQuantity").addClass('w-commerce-commercecheckouterrorstate');
            }
            else {

                $('#divLoader').show();
                $("#txtQuantity").removeClass('w-commerce-commercecheckouterrorstate');

                $.ajax({
                    url: 'ProductDetail.aspx/AddToCart',
                    type: 'POST',
                    contentType: 'application/json; charset =utf-8',
                    data: "{'pstrProductId':'" + lstrProductId + "','pstrProductType':'" + lstrProductType + "','pstrCurrency':'" + lstrProductCCY + "','pintProductQuantity':" + lintProductQuantity + "}",
                    dataType: 'json',
                    success: function (data) {
                        $('#divLoader').hide();
                        try {
                            var newData = data.d;
                            if ((newData != "") || newData == null) {
                                if (newData == "SUCCESS") {
                                    swal("Added to cart!", "", "success");
                                    GetCartItemCount('open');
                                }
                                else if (newData == "TIMEOUT") {
                                    window.location.href = "Login.aspx";
                                }
                                else {
                                    swal("Oops!", "Something went wrong", "error");
                                }
                            }
                            else {
                                swal("Oops!", "Something went wrong", "error");
                            }
                        }
                        catch (e) {
                            swal("Oops!", "Something went wrong", "error");
                            return false;
                        }
                        return false;
                    },
                    error: function (errmsg) {
                        $('#divLoader').hide();
                        swal("Oops!", "Something went wrong", "error");
                    }
                });
            }
        };


    </script>
</asp:Content>
