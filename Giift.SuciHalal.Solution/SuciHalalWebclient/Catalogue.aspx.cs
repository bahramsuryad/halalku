﻿using Framework.EnterpriseLibrary.Adapters;
using Giift.SuciHalal.Entities;
using Giift.SuciHalal.Helper;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SuciHalalWebclient
{
    class Category
    {
        public string Label { get; set; }
        public string Value { get; set; }
    }

    public partial class Catalogue : System.Web.UI.Page
    {
        public static List<ProductDetails> lobjListOfProductDetails = null;
        public static string lstrAccessToken = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                lobjListOfProductDetails = null;
                BindCategories();
            }
        }

        public void BindCategories()
        {
            try
            {
                string lstrCategoriesResponse = string.Empty;
                lstrCategoriesResponse = ApiHelper.GetCategories("product", Convert.ToString(ConfigurationManager.AppSettings["BuyingWith"]));
                if (!string.IsNullOrEmpty(lstrCategoriesResponse) && lstrCategoriesResponse.Contains("name"))
                {
                    dynamic lobjCategories = JsonConvert.DeserializeObject(lstrCategoriesResponse);

                    List<Category> categories = new List<Category>();
                    
                    for(int i=0; i < lobjCategories.Count; i++)
                    {   
                       

                        Category category = new Category();
                        category.Value = lobjCategories[i]["name"].Value;
                        category.Label = lobjCategories[i]["name"].Value;
                        categories.Add(category);

                    
                    }

                    dropdownListCategory.DataSource = categories;
                    dropdownListCategory.DataTextField = "Label";
                    dropdownListCategory.DataValueField = "Value";
                    //dropdownListCategory.SelectedIndexChanged = 
                    dropdownListCategory.DataBind();

                   
                    dropdownListCategory.Items.Insert(0, new ListItem("All"));
                    //string lstrCategoriesHtml = string.Empty;
                    //for (int i = 0; i < lobjCategories.Count; i++)
                    //{
                    //    lstrCategoriesHtml += "<div class=\"w-dyn-item\">"

                    //                        + "<asp:DropDownList>"
                    //                        + "<asp:ListItem data-w-id=\"5c7abf2b-aadf-7367-9d60-75ef7c82ab80\" onclick=\"var retvalue = BindProducts('" + lobjCategories[i]["name"].Value + "','all',0,0); event.returnValue = retvalue; return retvalue;\">" + lobjCategories[i]["name"].Value + "</asp:ListItem>"
                    //                        + "</asp:DropDownList>"

                    //                        + "</div>";
                    //}
                    //divCategories.InnerHtml = lstrCategoriesHtml;
                }


            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("Catalogue.aspx : BindCategories: " + ex.Message + Environment.NewLine + ex.StackTrace);
            }
        }

        void Index_Changed(Object sender, EventArgs e)
        {
            //BindProducts(dropdownListCategory.SelectedValue, "all", 0, 0);
            //Label1.Text = "You selected " + RadioButtonList1.SelectedItem.Text +
            //              " with a value of $" + RadioButtonList1.SelectedItem.Value +
            //              ".";

        }

        public List<ProductDetails> GetListOfProducts(string pstrCategory, string pstrName)
        {
            try
            {
                lstrAccessToken = Convert.ToString(HttpContext.Current.Session["AccessToken"]);
                string lstrBuyableListResponse = string.Empty;
                List<ProductDetails> lobjAllProductDetails = new List<ProductDetails>();


                Dictionary<string, string> ldctFilterItems = new Dictionary<string, string>();
                ldctFilterItems.Add("type", "product");
                //ldctFilterItems.Add("country", "MY");
                //ldctFilterItems.Add("currency", "myr");
                ldctFilterItems.Add("buying_with", Convert.ToString(ConfigurationManager.AppSettings["BuyingWith"]));


                if (!string.IsNullOrEmpty(pstrCategory) && pstrCategory != "all")
                {
                    ldctFilterItems.Add("category", pstrCategory);
                }

                if (!string.IsNullOrEmpty(pstrName) && pstrName != "all")
                {
                    ldctFilterItems.Add("name", pstrName);
                }

                lstrBuyableListResponse = ApiHelper.BuyableList(ldctFilterItems);
                JToken jTokenOuter = JToken.Parse(lstrBuyableListResponse);
                if (jTokenOuter["unauthorized"] != null)
                {
                    lobjAllProductDetails = null;
                }
                else if (jTokenOuter["payload"] != null)
                {
                    JArray array = jTokenOuter.Value<JArray>("payload");
                    foreach (JObject parsedObject in array.Children<JObject>())
                    {
                        ProductDetails lobjProductDetails = new ProductDetails();
                        int count = 0;
                        foreach (JProperty parsedProperty in parsedObject.Properties())
                        {
                            string propertyName = parsedProperty.Name;
                            if (propertyName.Equals("underlying_id"))
                            {
                                lobjProductDetails.Id = (string)parsedProperty.Value;
                                count++;
                            }
                            else if (propertyName.Equals("name"))
                            {
                                lobjProductDetails.Name = (string)parsedProperty.Value;
                                count++;
                            }
                            else if (propertyName.Equals("units"))
                            {
                                JToken priceArray = parsedProperty.Value[0];
                                if ((priceArray != null) && (priceArray.Value<string>("price") != null))
                                {
                                    lobjProductDetails.Price = priceArray.Value<string>("price");
                                    count++;
                                }
                                if ((priceArray != null) && (priceArray.Value<string>("unit") != null))
                                {
                                    lobjProductDetails.Currency = priceArray.Value<string>("unit").ToUpper();
                                    count++;
                                }
                            }
                            else if (propertyName.Equals("imgs"))
                            {
                                foreach (dynamic imageUrl in parsedProperty.Value)
                                {
                                    if (imageUrl.Value.Contains("_medium.jpg"))
                                    {
                                        lobjProductDetails.ImageUrl = imageUrl.Value;
                                        count++;
                                        break;
                                    }
                                }
                            }
                            else if (propertyName.Equals("categories"))
                            {
                                if (((Newtonsoft.Json.Linq.JContainer)parsedProperty.Value).Count > 0)
                                {
                                    lobjProductDetails.Category = Convert.ToString(((JValue)parsedProperty.Value[0]).Value);
                                }
                            }
                            if (count == 6)
                            {
                                break;
                            }
                        }
                        lobjAllProductDetails.Add(lobjProductDetails);
                    }
                }
                else
                {
                    lobjAllProductDetails = null;
                }


                if (lobjAllProductDetails != null && lobjAllProductDetails.Count > 0)
                {
                    return lobjAllProductDetails;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("Catalogue.aspx GetListOfProducts :" + ex.Message + Environment.NewLine + ex.StackTrace);
                return null;
            }
        }

        [WebMethod]
        public static ProductDetailsData BindProducts(int pintSkipRecords, int pintTakeRecords, string pstrCategory, string pstrName, int pintStartRange, int pintEndRange)
        {
            try
            {
                Catalogue lobjCatalogue = new Catalogue();
                ProductDetailsData lobjProductDetailsData = new ProductDetailsData();

                if (pintSkipRecords == 0)
                {
                    lobjListOfProductDetails = null;
                    lobjListOfProductDetails = new List<ProductDetails>();
                    lobjListOfProductDetails = lobjCatalogue.GetListOfProducts(pstrCategory, pstrName);
                }

                if (lobjListOfProductDetails != null && lobjListOfProductDetails.Count > 0)
                {
                    if(pintStartRange == 0 && pintEndRange != 0)
                    {
                        lobjListOfProductDetails = lobjListOfProductDetails.FindAll(lobj => Convert.ToSingle(lobj.Price) > 0 && Convert.ToSingle(lobj.Price) <= pintEndRange);
                    }
                    else if(pintStartRange != 0 && pintEndRange == 0)
                    {
                        lobjListOfProductDetails = lobjListOfProductDetails.FindAll(lobj => Convert.ToSingle(lobj.Price) >= pintStartRange);
                    }
                    else if(pintStartRange != 0 && pintEndRange != 0)
                    {
                        lobjListOfProductDetails = lobjListOfProductDetails.FindAll(lobj => Convert.ToSingle(lobj.Price) >= pintStartRange && Convert.ToSingle(lobj.Price) <= pintEndRange);
                    }

                    var query = (from ProductDetails in lobjListOfProductDetails
                                 select ProductDetails)
                                .Skip(pintSkipRecords)
                                .Take(pintTakeRecords)
                                .ToList();

                    lobjProductDetailsData.PDetails = query;
                    int lintProductsCount = (from p in lobjListOfProductDetails
                                             select p).Count();
                    double pageCount = (lintProductsCount / 15) + 1;

                    lobjProductDetailsData.TotalPages = Math.Ceiling(pageCount);

                    string lstrMetasHtml = string.Empty;
                    if (pstrCategory != "all")
                    {
                        Dictionary<string, string> ldctFilterItems = new Dictionary<string, string>();
                        ldctFilterItems.Add("buying_with", "1553831506Ah8UJ");
                        ldctFilterItems.Add("type", "product");
                        ldctFilterItems.Add("country", "MY");
                        string lstrMetasResponse = ApiHelper.GetMetas(ldctFilterItems);
                        dynamic lobjMetas = JsonConvert.DeserializeObject(lstrMetasResponse);
                        if (lobjMetas != null && lobjMetas.Count > 0)
                        {
                            for (int i = 0; i < lobjMetas.Count; i++)
                            {
                                lstrMetasHtml += "<div class=\"top-margin\">"
                                              + "<h4 class=\"footer-title\">" + lobjMetas[i].name + "</h4>"
                                              + "<select class=\"w-commerce-commercecheckoutemailinput\" required=\"\">";
                                string lstrOptionsHtml = string.Empty;
                                if (lobjMetas[i].name != null && lobjMetas[i].options != null)
                                {
                                    lstrOptionsHtml += "<option value=\"\">Select " + lobjMetas[i].name + "</option>";
                                    for (int j = 0; j < lobjMetas[0].options.Count; j++)
                                    {
                                        lstrOptionsHtml += "<option value = \"" + lobjMetas[0].options[j] + "\">" + lobjMetas[0].options[j] + "</option>";
                                    }
                                }

                                lstrMetasHtml += lstrOptionsHtml;
                                lstrMetasHtml += "</select></div>";
                            }
                            lstrMetasHtml += "<div class=\"top-margin\">"
                                          + "<button class=\"w-commerce-commercecartcheckoutbutton checkout-button\" style=\"width:100%;\">"
                                          + "Search</button></div>";
                        }
                    }
                    lobjProductDetailsData.MetasHtml = lstrMetasHtml;
                    return lobjProductDetailsData;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("Catalogue.aspx.cs : BindProducts :" + ex.Message + Environment.NewLine + ex.StackTrace);
                return null;
            }
        }
    }
}