﻿using Giift.SuciHalal.Entities;
using Giift.SuciHalal.Helper;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SuciHalalWebclient
{
    public partial class TnC : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                BindTnCContent();
            }
        }

        public void BindTnCContent()
        {
            string lstrContent = ApiHelper.RetrieveContent(Convert.ToString(ConfigurationManager.AppSettings["AboutRetailerId"]), "tnc");
            JToken jTokenOuter = JToken.Parse(lstrContent);
            JObject lobjJObject = ((JObject)jTokenOuter["retailer_content"]);
            RetailerContent retailerContent = JsonConvert.DeserializeObject<RetailerContent>(((JProperty)lobjJObject.First).Value.ToString());
            if (!string.IsNullOrEmpty(retailerContent.value))
            {
                divTnC.InnerHtml = "";
                divTnC.InnerHtml = retailerContent.value;
            }
        }
    }
}