﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SuciHalalSite.Master" AutoEventWireup="true" CodeBehind="PaymentFailed.aspx.cs" Inherits="SuciHalalWebclient.PaymentFailed" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="pages-wrapper">
        <div>
            <img alt="" class="pages-icon" src="https://www.paidmembershipspro.com/wp-content/uploads/2017/07/Failed-Payment-Limit.png" width="230">
            <h1>Sorry</h1>
            <p>
                Your payment cannot processed by the system because of something,
                <br>
                Please try again later or changing your payment method.
            </p>
            <div class="top-margin">
                <a class="shop-button w-inline-block" href="Index.aspx">
                    <div class="shop-text dark">Back To Homepage</div>
                    <div class="shop-line black" style="width: 0%;"></div>
                </a>
            </div>
        </div>
    </div>
</asp:Content>
