﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SuciHalalSite.Master" AutoEventWireup="true" CodeBehind="Payment.aspx.cs" Inherits="SuciHalalWebclient.Payment" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="">
        <div class="w-commerce-commercecheckoutformcontainer section gray checkout" data-node-type="commerce-checkout-form-container"
            data-wf-page-link-href-prefix="" style="padding-top: 150px;">
            <div class="w-commerce-commercelayoutcontainer w-container">
                <div class="w-commerce-commercelayoutmain">
                    <form class="w-commerce-commercecheckoutcustomerinfowrapper customer-info"
                        data-node-type="commerce-checkout-customer-info-wrapper">
                        <div class="w-commerce-commercecheckoutblockheader block-header-2">
                            <h4>Customer
                                    Info</h4>
                            <div class="required-text">* Required</div>
                        </div>
                        <fieldset class="w-commerce-commercecheckoutblockcontent">
                            <label
                                class="w-commerce-commercecheckoutlabel field-label-2">
                                Email *</label><input
                                    class="w-commerce-commercecheckoutemailinput" data-wf-bindings="%5B%7B%22value%22%3A%7B%22type%22%3A%22PlainText%22%2C%22filter%22%3A%7B%22type%22%3A%22identity%22%2C%22params%22%3A%5B%5D%7D%2C%22dataPath%22%3A%22database.commerceOrder.customerEmail%22%7D%7D%5D"
                                    data-wf-conditions="%5B%5D"
                                    name="email" type="text">
                        </fieldset>
                    </form>
                    <form class="w-commerce-commercecheckoutshippingaddresswrapper"
                        data-node-type="commerce-checkout-shipping-address-wrapper">
                        <div class="w-commerce-commercecheckoutblockheader block-header">
                            <h4>Shipping
                                    Address</h4>
                            <div class="required-text">* Required</div>
                        </div>
                        <fieldset class="w-commerce-commercecheckoutblockcontent">
                            <label
                                class="w-commerce-commercecheckoutlabel field-label-2">
                                Full Name *</label><input
                                    class="w-commerce-commercecheckoutshippingfullname" data-wf-bindings="%5B%7B%22value%22%3A%7B%22type%22%3A%22PlainText%22%2C%22filter%22%3A%7B%22type%22%3A%22identity%22%2C%22params%22%3A%5B%5D%7D%2C%22dataPath%22%3A%22database.commerceOrder.shippingAddressAddressee%22%7D%7D%5D"
                                    data-wf-conditions="%5B%5D"
                                    name="name" type="text"><label
                                        class="w-commerce-commercecheckoutlabel field-label-2">Street Address
                                    *</label><input class="w-commerce-commercecheckoutshippingstreetaddress" data-wf-bindings="%5B%7B%22value%22%3A%7B%22type%22%3A%22PlainText%22%2C%22filter%22%3A%7B%22type%22%3A%22identity%22%2C%22params%22%3A%5B%5D%7D%2C%22dataPath%22%3A%22database.commerceOrder.shippingAddressLine1%22%7D%7D%5D"
                                        data-wf-conditions="%5B%5D"
                                        name="address_line1"
                                        type="text"><input
                                            class="w-commerce-commercecheckoutshippingstreetaddressoptional" data-wf-bindings="%5B%7B%22value%22%3A%7B%22type%22%3A%22PlainText%22%2C%22filter%22%3A%7B%22type%22%3A%22identity%22%2C%22params%22%3A%5B%5D%7D%2C%22dataPath%22%3A%22database.commerceOrder.shippingAddressLine2%22%7D%7D%5D"
                                            data-wf-conditions="%5B%5D"
                                            name="address_line2"
                                            type="text">
                            <div class="w-commerce-commercecheckoutrow">
                                <div class="w-commerce-commercecheckoutcolumn">
                                    <label
                                        class="w-commerce-commercecheckoutlabel field-label-2">
                                        City
                                            *</label><input class="w-commerce-commercecheckoutshippingcity" data-wf-bindings="%5B%7B%22value%22%3A%7B%22type%22%3A%22PlainText%22%2C%22filter%22%3A%7B%22type%22%3A%22identity%22%2C%22params%22%3A%5B%5D%7D%2C%22dataPath%22%3A%22database.commerceOrder.shippingAddressCity%22%7D%7D%5D"
                                                data-wf-conditions="%5B%5D"
                                                name="address_city"
                                                type="text">
                                </div>
                                <div class="w-commerce-commercecheckoutcolumn">
                                    <label
                                        class="w-commerce-commercecheckoutlabel field-label-2">
                                        State/Province</label><input
                                            class="w-commerce-commercecheckoutshippingstateprovince" data-wf-bindings="%5B%7B%22value%22%3A%7B%22type%22%3A%22PlainText%22%2C%22filter%22%3A%7B%22type%22%3A%22identity%22%2C%22params%22%3A%5B%5D%7D%2C%22dataPath%22%3A%22database.commerceOrder.shippingAddressState%22%7D%7D%5D"
                                            data-wf-conditions="%5B%5D"
                                            name="address_state"
                                            type="text">
                                </div>
                                <div class="w-commerce-commercecheckoutcolumn">
                                    <label
                                        class="w-commerce-commercecheckoutlabel field-label-2">
                                        Zip/Postal Code
                                            *</label><input class="w-commerce-commercecheckoutshippingzippostalcode" data-wf-bindings="%5B%7B%22value%22%3A%7B%22type%22%3A%22PlainText%22%2C%22filter%22%3A%7B%22type%22%3A%22identity%22%2C%22params%22%3A%5B%5D%7D%2C%22dataPath%22%3A%22database.commerceOrder.shippingAddressPostalCode%22%7D%7D%5D"
                                                data-wf-conditions="%5B%5D"
                                                name="address_zip"
                                                type="text">
                                </div>
                            </div>
                            <label class="w-commerce-commercecheckoutlabel">Country *</label><select
                                class="w-commerce-commercecheckoutshippingcountryselector"
                                data-wf-bindings="%5B%7B%22value%22%3A%7B%22type%22%3A%22PlainText%22%2C%22filter%22%3A%7B%22type%22%3A%22identity%22%2C%22params%22%3A%5B%5D%7D%2C%22dataPath%22%3A%22database.commerceOrder.shippingAddressCountry%22%7D%7D%5D"
                                data-wf-conditions="%5B%5D"
                                name="address_country">
                                <option value="AF">Afghanistan</option>
                                <option value="AX">Aland Islands</option>
                                <option value="AL">Albania</option>
                                <option value="DZ">Algeria</option>
                                <option value="AS">American Samoa</option>
                                <option value="AD">Andorra</option>
                                <option value="AO">Angola</option>
                                <option value="AI">Anguilla</option>
                                <option value="AG">Antigua and Barbuda</option>
                                <option value="AR">Argentina</option>
                                <option value="AM">Armenia</option>
                                <option value="AW">Aruba</option>
                                <option value="AU">Australia</option>
                                <option value="AT">Austria</option>
                                <option value="AZ">Azerbaijan</option>
                                <option value="BS">Bahamas</option>
                                <option value="BH">Bahrain</option>
                                <option value="BD">Bangladesh</option>
                                <option value="BB">Barbados</option>
                                <option value="BY">Belarus</option>
                                <option value="BE">Belgium</option>
                                <option value="BZ">Belize</option>
                                <option value="BJ">Benin</option>
                                <option value="BM">Bermuda</option>
                                <option value="BT">Bhutan</option>
                                <option value="BO">Bolivia</option>
                                <option value="BQ">Bonaire, Saint Eustatius and Saba</option>
                                <option value="BA">Bosnia and Herzegovina</option>
                                <option value="BW">Botswana</option>
                                <option value="BR">Brazil</option>
                                <option value="IO">British Indian Ocean Territory</option>
                                <option value="VG">British Virgin Islands</option>
                                <option value="BN">Brunei</option>
                                <option value="BG">Bulgaria</option>
                                <option value="BF">Burkina Faso</option>
                                <option value="BI">Burundi</option>
                                <option value="KH">Cambodia</option>
                                <option value="CM">Cameroon</option>
                                <option value="CA">Canada</option>
                                <option value="CV">Cape Verde</option>
                                <option value="KY">Cayman Islands</option>
                                <option value="CF">Central African Republic</option>
                                <option value="TD">Chad</option>
                                <option value="CL">Chile</option>
                                <option value="CN">China</option>
                                <option value="CX">Christmas Island</option>
                                <option value="CC">Cocos Islands</option>
                                <option value="CO">Colombia</option>
                                <option value="KM">Comoros</option>
                                <option value="CK">Cook Islands</option>
                                <option value="CR">Costa Rica</option>
                                <option value="HR">Croatia</option>
                                <option value="CU">Cuba</option>
                                <option value="CW">Curacao</option>
                                <option value="CY">Cyprus</option>
                                <option value="CZ">Czechia</option>
                                <option value="CD">Democratic Republic of the Congo</option>
                                <option value="DK">Denmark</option>
                                <option value="DJ">Djibouti</option>
                                <option value="DM">Dominica</option>
                                <option value="DO">Dominican Republic</option>
                                <option value="EC">Ecuador</option>
                                <option value="EG">Egypt</option>
                                <option value="SV">El Salvador</option>
                                <option value="GQ">Equatorial Guinea</option>
                                <option value="ER">Eritrea</option>
                                <option value="EE">Estonia</option>
                                <option value="ET">Ethiopia</option>
                                <option value="FK">Falkland Islands</option>
                                <option value="FO">Faroe Islands</option>
                                <option value="FJ">Fiji</option>
                                <option value="FI">Finland</option>
                                <option value="FR">France</option>
                                <option value="GF">French Guiana</option>
                                <option value="PF">French Polynesia</option>
                                <option value="TF">French Southern Territories</option>
                                <option value="GA">Gabon</option>
                                <option value="GM">Gambia</option>
                                <option value="GE">Georgia</option>
                                <option value="DE">Germany</option>
                                <option value="GH">Ghana</option>
                                <option value="GI">Gibraltar</option>
                                <option value="GR">Greece</option>
                                <option value="GL">Greenland</option>
                                <option value="GD">Grenada</option>
                                <option value="GP">Guadeloupe</option>
                                <option value="GU">Guam</option>
                                <option value="GT">Guatemala</option>
                                <option value="GG">Guernsey</option>
                                <option value="GN">Guinea</option>
                                <option value="GW">Guinea-Bissau</option>
                                <option value="GY">Guyana</option>
                                <option value="HT">Haiti</option>
                                <option value="HN">Honduras</option>
                                <option value="HK">Hong Kong</option>
                                <option value="HU">Hungary</option>
                                <option value="IS">Iceland</option>
                                <option value="IN">India</option>
                                <option value="ID">Indonesia</option>
                                <option value="IR">Iran</option>
                                <option value="IQ">Iraq</option>
                                <option value="IE">Ireland</option>
                                <option value="IM">Isle of Man</option>
                                <option value="IL">Israel</option>
                                <option value="IT">Italy</option>
                                <option value="CI">Ivory Coast</option>
                                <option value="JM">Jamaica</option>
                                <option value="JP">Japan</option>
                                <option value="JE">Jersey</option>
                                <option value="JO">Jordan</option>
                                <option value="KZ">Kazakhstan</option>
                                <option value="KE">Kenya</option>
                                <option value="KI">Kiribati</option>
                                <option value="XK">Kosovo</option>
                                <option value="KW">Kuwait</option>
                                <option value="KG">Kyrgyzstan</option>
                                <option value="LA">Laos</option>
                                <option value="LV">Latvia</option>
                                <option value="LB">Lebanon</option>
                                <option value="LS">Lesotho</option>
                                <option value="LR">Liberia</option>
                                <option value="LY">Libya</option>
                                <option value="LI">Liechtenstein</option>
                                <option value="LT">Lithuania</option>
                                <option value="LU">Luxembourg</option>
                                <option value="MO">Macao</option>
                                <option value="MK">Macedonia</option>
                                <option value="MG">Madagascar</option>
                                <option value="MW">Malawi</option>
                                <option value="MY">Malaysia</option>
                                <option value="MV">Maldives</option>
                                <option value="ML">Mali</option>
                                <option value="MT">Malta</option>
                                <option value="MH">Marshall Islands</option>
                                <option value="MQ">Martinique</option>
                                <option value="MR">Mauritania</option>
                                <option value="MU">Mauritius</option>
                                <option value="YT">Mayotte</option>
                                <option value="MX">Mexico</option>
                                <option value="FM">Micronesia</option>
                                <option value="MD">Moldova</option>
                                <option value="MC">Monaco</option>
                                <option value="MN">Mongolia</option>
                                <option value="ME">Montenegro</option>
                                <option value="MS">Montserrat</option>
                                <option value="MA">Morocco</option>
                                <option value="MZ">Mozambique</option>
                                <option value="MM">Myanmar</option>
                                <option value="NA">Namibia</option>
                                <option value="NR">Nauru</option>
                                <option value="NP">Nepal</option>
                                <option value="NL">Netherlands</option>
                                <option value="NC">New Caledonia</option>
                                <option value="NZ">New Zealand</option>
                                <option value="NI">Nicaragua</option>
                                <option value="NE">Niger</option>
                                <option value="NG">Nigeria</option>
                                <option value="NU">Niue</option>
                                <option value="NF">Norfolk Island</option>
                                <option value="KP">North Korea</option>
                                <option value="MP">Northern Mariana Islands</option>
                                <option value="NO">Norway</option>
                                <option value="OM">Oman</option>
                                <option value="PK">Pakistan</option>
                                <option value="PW">Palau</option>
                                <option value="PS">Palestinian Territory</option>
                                <option value="PA">Panama</option>
                                <option value="PG">Papua New Guinea</option>
                                <option value="PY">Paraguay</option>
                                <option value="PE">Peru</option>
                                <option value="PH">Philippines</option>
                                <option value="PN">Pitcairn</option>
                                <option value="PL">Poland</option>
                                <option value="PT">Portugal</option>
                                <option value="PR">Puerto Rico</option>
                                <option value="QA">Qatar</option>
                                <option value="CG">Republic of the Congo</option>
                                <option value="RE">Reunion</option>
                                <option value="RO">Romania</option>
                                <option value="RU">Russia</option>
                                <option value="RW">Rwanda</option>
                                <option value="BL">Saint Barthelemy</option>
                                <option value="SH">Saint Helena</option>
                                <option value="KN">Saint Kitts and Nevis</option>
                                <option value="LC">Saint Lucia</option>
                                <option value="MF">Saint Martin</option>
                                <option value="PM">Saint Pierre and Miquelon</option>
                                <option value="VC">Saint Vincent and the Grenadines</option>
                                <option value="WS">Samoa</option>
                                <option value="SM">San Marino</option>
                                <option value="ST">Sao Tome and Principe</option>
                                <option value="SA">Saudi Arabia</option>
                                <option value="SN">Senegal</option>
                                <option value="RS">Serbia</option>
                                <option value="SC">Seychelles</option>
                                <option value="SL">Sierra Leone</option>
                                <option value="SG">Singapore</option>
                                <option value="SX">Sint Maarten</option>
                                <option value="SK">Slovakia</option>
                                <option value="SI">Slovenia</option>
                                <option value="SB">Solomon Islands</option>
                                <option value="SO">Somalia</option>
                                <option value="ZA">South Africa</option>
                                <option value="GS">South Georgia and the South Sandwich Islands</option>
                                <option value="KR">South Korea</option>
                                <option value="SS">South Sudan</option>
                                <option value="ES">Spain</option>
                                <option value="LK">Sri Lanka</option>
                                <option value="SD">Sudan</option>
                                <option value="SR">Suriname</option>
                                <option value="SJ">Svalbard and Jan Mayen</option>
                                <option value="SZ">Swaziland</option>
                                <option value="SE">Sweden</option>
                                <option value="CH">Switzerland</option>
                                <option value="SY">Syria</option>
                                <option value="TW">Taiwan</option>
                                <option value="TJ">Tajikistan</option>
                                <option value="TZ">Tanzania</option>
                                <option value="TH">Thailand</option>
                                <option value="TL">Timor Leste</option>
                                <option value="TG">Togo</option>
                                <option value="TK">Tokelau</option>
                                <option value="TO">Tonga</option>
                                <option value="TT">Trinidad and Tobago</option>
                                <option value="TN">Tunisia</option>
                                <option value="TR">Turkey</option>
                                <option value="TM">Turkmenistan</option>
                                <option value="TC">Turks and Caicos Islands</option>
                                <option value="TV">Tuvalu</option>
                                <option value="VI">U.S. Virgin Islands</option>
                                <option value="UG">Uganda</option>
                                <option value="UA">Ukraine</option>
                                <option value="AE">United Arab Emirates</option>
                                <option value="GB">United Kingdom</option>
                                <option value="US">United States</option>
                                <option value="UM">United States Minor Outlying Islands</option>
                                <option value="UY">Uruguay</option>
                                <option value="UZ">Uzbekistan</option>
                                <option value="VU">Vanuatu</option>
                                <option value="VA">Vatican</option>
                                <option value="VE">Venezuela</option>
                                <option value="VN">Vietnam</option>
                                <option value="WF">Wallis and Futuna</option>
                                <option value="EH">Western Sahara</option>
                                <option value="YE">Yemen</option>
                                <option value="ZM">Zambia</option>
                                <option value="ZW">Zimbabwe</option>
                            </select>
                        </fieldset>
                    </form>
                    <form class="w-commerce-commercecheckoutshippingmethodswrapper"
                        data-node-type="commerce-checkout-shipping-methods-wrapper">
                        <div class="w-commerce-commercecheckoutblockheader block-header">
                            <h4>Shipping
                                    Method</h4>
                        </div>
                        <fieldset>
                            <script id="wf-template-5c3dfbded636579ba04c338000000000003a"
                                type="text/x-wf-template">%3Clabel%20class%3D%22w-commerce-commercecheckoutshippingmethoditem%22%3E%3Cinput%20type%3D%22radio%22%20required%3D%22%22%20data-wf-bindings%3D%22%255B%257B%2522id%2522%253A%257B%2522type%2522%253A%2522PlainText%2522%252C%2522filter%2522%253A%257B%2522type%2522%253A%2522identity%2522%252C%2522params%2522%253A%255B%255D%257D%252C%2522dataPath%2522%253A%2522database.commerceOrder.availableShippingMethods%255B%255D.id%2522%257D%257D%252C%257B%2522value%2522%253A%257B%2522type%2522%253A%2522PlainText%2522%252C%2522filter%2522%253A%257B%2522type%2522%253A%2522identity%2522%252C%2522params%2522%253A%255B%255D%257D%252C%2522dataPath%2522%253A%2522database.commerceOrder.availableShippingMethods%255B%255D.id%2522%257D%257D%252C%257B%2522checked%2522%253A%257B%2522type%2522%253A%2522Bool%2522%252C%2522filter%2522%253A%257B%2522type%2522%253A%2522identity%2522%252C%2522params%2522%253A%255B%255D%257D%252C%2522dataPath%2522%253A%2522database.commerceOrder.availableShippingMethods%255B%255D.selected%2522%257D%257D%255D%22%20data-wf-conditions%3D%22%255B%255D%22%20name%3D%22shipping-method-choice%22%2F%3E%3Cdiv%20class%3D%22w-commerce-commercecheckoutshippingmethoddescriptionblock%22%3E%3Cdiv%20data-wf-bindings%3D%22%255B%257B%2522innerHTML%2522%253A%257B%2522type%2522%253A%2522PlainText%2522%252C%2522filter%2522%253A%257B%2522type%2522%253A%2522identity%2522%252C%2522params%2522%253A%255B%255D%257D%252C%2522dataPath%2522%253A%2522database.commerceOrder.availableShippingMethods%255B%255D.name%2522%257D%257D%255D%22%20data-wf-conditions%3D%22%255B%255D%22%20class%3D%22w-commerce-commerceboldtextblock%20text-block-2%20w-dyn-bind-empty%22%3E%3C%2Fdiv%3E%3Cdiv%20data-wf-bindings%3D%22%255B%257B%2522innerHTML%2522%253A%257B%2522type%2522%253A%2522PlainText%2522%252C%2522filter%2522%253A%257B%2522type%2522%253A%2522identity%2522%252C%2522params%2522%253A%255B%255D%257D%252C%2522dataPath%2522%253A%2522database.commerceOrder.availableShippingMethods%255B%255D.description%2522%257D%257D%255D%22%20data-wf-conditions%3D%22%255B%255D%22%20class%3D%22text-block-3%20w-dyn-bind-empty%22%3E%3C%2Fdiv%3E%3C%2Fdiv%3E%3Cdiv%20data-wf-bindings%3D%22%255B%257B%2522innerHTML%2522%253A%257B%2522type%2522%253A%2522CommercePrice%2522%252C%2522filter%2522%253A%257B%2522type%2522%253A%2522price%2522%252C%2522params%2522%253A%255B%255D%257D%252C%2522dataPath%2522%253A%2522database.commerceOrder.availableShippingMethods%255B%255D.price%2522%257D%257D%255D%22%20data-wf-conditions%3D%22%255B%255D%22%20class%3D%22price%20smaller%22%3E%240.00%3C%2Fdiv%3E%3C%2Flabel%3E</script>
                            <div class="w-commerce-commercecheckoutshippingmethodsemptystate"
                                data-node-type="commerce-checkout-shipping-methods-empty-state">
                                <div>No shipping methods are available for the address given.</div>
                            </div>
                        </fieldset>
                    </form>
                    <div class="w-commerce-commercecheckoutorderitemswrapper">
                        <div class="w-commerce-commercecheckoutsummaryblockheader">
                            <h4>Items in Order</h4>
                        </div>
                        <fieldset class="w-commerce-commercecheckoutblockcontent">
                            <div class="w-commerce-commercecheckoutorderitemslist"></div>
                        </fieldset>
                        <div style="display: flex; border-top: none; border-right: 1px solid rgb(230, 230, 230); border-bottom: 1px solid rgb(230, 230, 230); border-left: 1px solid rgb(230, 230, 230); border-image: initial; background-color: rgb(255, 255, 255); justify-content: space-between; padding: 8px 16px;">
                            <div class="text-block-2">Total</div>
                            <div>RM 0 / 0 Points</div>
                        </div>
                    </div>
                    <div class="w-commerce-commercecheckoutorderitemswrapper">
                        <div class="w-commerce-commercecheckoutsummaryblockheader">
                            <h4>Payment Method</h4>
                        </div>
                        <fieldset class="w-commerce-commercecheckoutblockcontent" style="height: 120px;">
                            <div class="w-commerce-commercecheckoutorderitemslist">
                                <label
                                    class="w-commerce-commercecheckoutlabel">
                                    Select Payment
                                        Method</label><select class="w-commerce-commercecheckoutshippingcountryselector"
                                            name="address_country"
                                            style="margin-bottom: 16px;">
                                            <option value="points_only">Points Redemption</option>
                                            <option value="point_cash">Points + Cash</option>
                                            <option value="cash_only">Cash Only</option>
                                        </select>
                            </div>
                        </fieldset>
                        <fieldset class="w-commerce-commercecheckoutblockcontent"
                            style="margin-bottom: 16px; height: 90px;">
                            <div class="w-commerce-commercecheckoutorderitemslist">
                                <div style="display: flex; justify-content: space-between;">
                                    <div class="text-block-2">Your current points</div>
                                    <div>0</div>
                                </div>
                                <div style="display: flex; justify-content: space-between;">
                                    <div class="text-block-2">Point after use</div>
                                    <div>0</div>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </div>
                <div class="w-commerce-commercelayoutsidebar sticky-sidebar">
                    <div class="w-commerce-commercecheckoutordersummarywrapper">
                        <div class="w-commerce-commercecheckoutsummaryblockheader">
                            <h4>Order Summary</h4>
                        </div>
                        <fieldset class="w-commerce-commercecheckoutblockcontent">
                            <div style="display: flex; justify-content: space-between;">
                                <div class="text-block-2">Point used</div>
                                <div>0</div>
                            </div>
                        </fieldset>
                    </div>
                    <div class="w-commerce-commercecheckoutplaceorderbutton checkout-button">Place Order</div>
                    <div class="w-commerce-commercecheckouterrorstate"
                        data-node-type="commerce-checkout-error-state" style="display: none;">
                        <div class="w-checkout-error-msg"
                            data-w-billing-error="Your payment could not be completed with the payment information provided.  Please make sure that your card and billing address information is correct, or try a different payment card, to complete this order.  Contact us if you continue to have problems."
                            data-w-info-error="There was an error processing your customer info.  Please try again, or contact us if you continue to have problems."
                            data-w-payment-error="There was an error processing your payment.  Please try again, or contact us if you continue to have problems."
                            data-w-shipping-error="Sorry. We can’t ship your order to the address provided.">
                            There was an error processing your customer info. Please try again, or contact us if
                                    you continue to have problems.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</asp:Content>
