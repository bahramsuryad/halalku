﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SuciHalalSite.Master" AutoEventWireup="true" CodeBehind="ChangePassword.aspx.cs" Inherits="SuciHalalWebclient.ChangePassword" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="dashboard">
        <div class="w-row" style="margin-top: 40vh; margin-bottom: 10vh;">
            <div class="w-col w-col-3"></div>
            <div class="w-col w-col-6">
                <div class="w-row">
                    <div class="w-col w-col-12">
                        <div class="form-margin">
                            <input class="subscribe-field-2 full w-input" data-name="old-password" id="txtOldPassword" maxlength="256" name="Email" placeholder="Enter your old password" required="" type="password">
                        </div>
                        <div class="form-margin">
                            <input class="subscribe-field-2 full w-input" data-name="new-password" id="txtNewPassword" maxlength="256" name="Email" placeholder="Enter your new password" required="" type="password">
                        </div>
                        <div class="form-margin">
                            <input class="subscribe-field-2 full w-input" data-name="retype-password" id="txtConfirmPassword" maxlength="256" name="Email" placeholder="Re-type your new password" required="" type="password">
                        </div>
                    </div>
                </div>
                <div class="top-margin-2 medium">
                    <div>
                        <input class="checkout-button full-button w-button" data-wait="Please wait..." type="submit" value="Reset Password" onclick="var retvalue = ChangePassword(); event.returnValue = retvalue; return retvalue;">
                    </div>
                </div>
            </div>
            <div class="w-col w-col-3"></div>
        </div>
    </div>
    <div id="divLoader" class="divLoader" style="display: none;"></div>
    <script type="text/javascript">
        function ChangePassword() {
            var regularExpression = /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{6,16}$/;

            var lstrOldPassword = $("#txtOldPassword").val();
            var lstrNewPassword = $("#txtNewPassword").val();
            var lstrConfirmPassword = $("#txtConfirmPassword").val();

            if (lstrOldPassword == "") {
                $("#txtOldPassword").addClass('w-commerce-commercecheckouterrorstate');
            }
            else if (lstrNewPassword == "") {
                $("#txtNewPassword").addClass('w-commerce-commercecheckouterrorstate');
            }
            else if (!regularExpression.test(lstrNewPassword)) {
                swal(" ", "Password should be minimum 6 characters and should contain atleast 1 digit and 1 special character.", "warning");
            }
            else if (lstrConfirmPassword == "") {
                $("#txtConfirmPassword").addClass('w-commerce-commercecheckouterrorstate');
            }
            else if (lstrNewPassword != lstrConfirmPassword) {
                swal(" ", "New password and Confirm password must be same.", "warning");
            }
            else {

                $('#divLoader').show();

                $("#txtOldPassword").removeClass('w-commerce-commercecheckouterrorstate');
                $("#txtNewPassword").removeClass('w-commerce-commercecheckouterrorstate');
                $("#txtConfirmPassword").removeClass('w-commerce-commercecheckouterrorstate');

                $.ajax({
                    url: 'ChangePassword.aspx/ChangePwd',
                    type: 'POST',
                    contentType: 'application/json; charset =utf-8',
                    data: "{'pstrOldPassword':'" + lstrOldPassword.toString() + "'" + "," + "'pstrNewPassword':'" + lstrNewPassword.toString() + "'" + "," + "'pstrConfirmPassword':'" + lstrConfirmPassword.toString() + "'}",
                    dataType: 'json',
                    success: function (data) {

                        $('#divLoader').hide();
                        try {

                            var newData = data.d;

                            if ((newData != "") || newData == null) {
                                if (newData == "SUCCESS") {
                                    swal(" ", "Password changed successfully.", "success");
                                    setTimeout(function () {
                                        window.location.href = "Profile.aspx";
                                    }, 2000);
                                }
                                else {
                                    swal(" ", newData, "error");
                                }
                            }
                            else {
                                swal("Oops!", "Something went wrong", "error");
                            }
                        }
                        catch (e) {
                            swal("Oops!", "Something went wrong", "error");
                            return false;
                        }
                        return false;
                    },
                    error: function (errmsg) {
                        $('#divLoader').show();
                        swal("Oops!", "Something went wrong", "error");
                    }
                });
            }
        };
    </script>
</asp:Content>
