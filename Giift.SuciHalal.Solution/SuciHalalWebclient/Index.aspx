﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SuciHalalSite.Master" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="SuciHalalWebclient.Index" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="dashboard">
        <section>
            <div id="slick-slider" style="margin-bottom: 0px; margin-top: 0px;">
                <%--<div style="position: relative; display: block; overflow: hidden; width: 100vw !important; height: 100vh !important; padding-right: 20px; padding-left: 20px; justify-content: center; align-items: stretch;">
                    <video autoplay playsinline muted loop class="video-hero">
                        <source src="./videos/video-hero-6.mp4" type="video/mp4">
                        <source src="./videos/video-hero-6.webm" type="video/webm">
                        Your browser does not support the video tag.
                    </video>
                    <div class="hero-text-container">
                        <div style="position: absolute; left: 50%; top: 37%;">
                            <div class="above-overlay" style="position: relative; left: -50%; top: -50%; padding: 0px 20px;">
                                <h1 class="shop-text light" style="font-size: 50px !important; color: white !important;">WELCOME</h1>
                                <p class="shop-text light" style="font-size: 18px !important; line-height: 28px !important;">To your ultimate destination for all things halal</p>
                                <a class="btn go w-commerce-commercecartcheckoutbutton checkout-button white-button" style="width: 100%; margin-top: 20px; margin-left: auto; margin-right: auto;" href="./catalogue.aspx">SHOP NOW
                                </a>
                            </div>
                        </div>
                        <div class="instant-overlay"></div>
                    </div>
                </div>--%>
                <div class="home-banner" style="background-image: url('https://d2t62toc9frg03.cloudfront.net/assets/img/web/home_slider_1.jpeg');">
                    <div class="hero-text-container">
                        <div class="home-banner-overlay-container">
                            <div class="above-overlay home-banner-overlay">
                                <h1 class="shop-text light overlay-title">WELCOME</h1>
                                <p class="shop-text light overlay-content">To your ultimate destination for all things halal</p>
                                <a class="btn go w-commerce-commercecartcheckoutbutton checkout-button white-button overlay-button" href="./catalogue.aspx">SHOP NOW
                                </a>
                            </div>
                        </div>
                        <div class="instant-overlay"></div>
                    </div>
                </div>
                <div class="home-banner" style="background-image: url('https://d2t62toc9frg03.cloudfront.net/assets/img/web/home_slider_2.jpg');">
                    <div class="hero-text-container">
                        <div class="home-banner-overlay-container">
                            <div class="above-overlay home-banner-overlay">
                                <h1 class="shop-text light overlay-title">Donations</h1>
                                <p class="shop-text light overlay-content">Give your wealth to those who need it more</p>
                                <a class="btn go w-commerce-commercecartcheckoutbutton checkout-button white-button overlay-button" href="./donations.aspx">GIVE IT NOW
                                </a>
                            </div>
                        </div>
                        <div class="instant-overlay"></div>
                    </div>
                </div>
                <div class="home-banner" style="background-image: url('https://d2t62toc9frg03.cloudfront.net/assets/img/web/home_slider_3.jpg');">
                    <div class="hero-text-container">
                        <div class="home-banner-overlay-container">
                            <div class="above-overlay home-banner-overlay">
                                <h1 class="shop-text light overlay-title">TRAVEL</h1>
                                <p class="shop-text light overlay-content">To wherever destination you want to visit</p>
                                <a class="btn go w-commerce-commercecartcheckoutbutton checkout-button white-button overlay-button" href="./travel.aspx">TRAVEL NOW
                                </a>
                            </div>
                        </div>
                        <div class="instant-overlay"></div>
                    </div>
                </div>
                <div class="home-banner" style="background-image: url('https://d2t62toc9frg03.cloudfront.net/assets/img/web/home_slider_4.jpg');">
                    <div class="hero-text-container">
                        <div class="home-banner-overlay-container">
                            <div class="above-overlay home-banner-overlay">
                                <h1 class="shop-text light overlay-title">F&B Offers</h1>
                                <p class="shop-text light overlay-content">Find the best offer that we have</p>
                                <a class="btn go w-commerce-commercecartcheckoutbutton checkout-button white-button overlay-button" href="./offer.aspx">FIND NOW
                                </a>
                            </div>
                        </div>
                        <div class="instant-overlay"></div>
                    </div>
                </div>
            </div>
        </section>
        <section class="section">
            <div class="home-section-menu">
                   <%-- <div class="section-menu">
                        <img src="https://images.unsplash.com/photo-1522204523234-8729aa6e3d5f?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1050&q=80"
                            style="width: 100%; height: 100%; object-fit: cover;" />
                    </div>
                    <div class="section-menu" style="background: url('https://images.pexels.com/photos/1345082/pexels-photo-1345082.jpeg?auto=format%2Ccompress&cs=tinysrgb&dpr=2&h=650&w=940'); background-size: cover;">
                        <div class="section-content padding-24">
                            <p class="title-menu" style="color: #fff; line-height: 40px;">Your ultimate halal shopping destination</p>
                            <!--                                        <p class="content-menu" style="color: #fff;">Your ultimate halal shopping destination</p>-->
                            <a href="./catalogue.aspx" class="button-shop-now">Shop Now</a>
                        </div>
                    </div>--%>
                   <a href="./about.aspx" class="section-menu hover" style="justify-content: flex-start !important;">
                        <img src="https://images.unsplash.com/photo-1522204523234-8729aa6e3d5f?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1050&q=80"
                            style="width: 100%; height: 290px; object-fit: cover;" />
                        <p class="title-menu" style="margin-top: 15px;">About Us</p>
                      <%--  <p class="content-menu" style="margin-top: -10px; padding-left: 24px; padding-right: 24px;">Your ultimate halal shopping destination</p>--%>
                    </a>
                  <a href="./catalogue.aspx" class="section-menu hover" style="justify-content: flex-start !important;">
                        <img src="https://images.pexels.com/photos/1050244/pexels-photo-1050244.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260"
                            style="width: 100%; height: 290px; object-fit: cover;" />
                        <p class="title-menu" style="margin-top: 15px;">Shopping</p>
                        <p class="content-menu" style="margin-top: -10px; padding-left: 24px; padding-right: 24px;">Your ultimate halal shopping destination</p>
                    </a>
                    <a href="./travel.aspx" class="section-menu hover" style="justify-content: flex-start !important;">
                        <img src="https://images.unsplash.com/photo-1466719963251-9b175f096120?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=715&q=80"
                            style="width: 100%; height: 290px; object-fit: cover;" />
                        <p class="title-menu" style="margin-top: 15px;">Travel</p>
                        <p class="content-menu" style="margin-top: -10px; padding-left: 24px; padding-right: 24px;">Enjoy Halal Travel Experiences all over the world</p>
                    </a>
                    <a href="./tools.aspx" class="section-menu hover" style="justify-content: flex-start !important;">
                        <img src="https://images.unsplash.com/photo-1515911630632-0c8e17517110?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=654&q=80"
                            style="width: 100%; height: 290px; object-fit: cover;" />
                        <p class="title-menu" style="margin-top: 15px;">Tools</p>
                        <p class="content-menu" style="margin-top: -10px; padding-left: 24px; padding-right: 24px;">Find your nearest Mosque, Restaurant, Prayer Time..and more!</p>
                    </a>
                    <a href="./donations.aspx" class="section-menu hover" style="justify-content: flex-start !important;">
                        <img src="https://images.unsplash.com/photo-1558543933-42e54d59e14c?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1350&q=80"
                            style="width: 100%; height: 290px; object-fit: cover;" />
                        <p class="title-menu" style="margin-top: 15px;">Donation</p>
                        <p class="content-menu" style="margin-top: -10px; padding-left: 24px; padding-right: 24px;">Spread the joy and donate to a range of charities around the globe</p>
                    </a>
                    <a href="./offer.aspx" class="section-menu hover" style="justify-content: flex-start !important;">
                        <img src="./images/offers.jpeg"
                            style="width: 100%; height: 290px; object-fit: cover;" />
                        <p class="title-menu" style="margin-top: 15px;">F&B Offers</p>
                        <p class="content-menu" style="margin-top: -10px; padding-left: 24px; padding-right: 24px;">Use your VISA card and enjoy special discount offers</p>
                    </a>
                </div>
        </section>
    </div>
</asp:Content>
