﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SuciHalalSite.Master" AutoEventWireup="true" CodeBehind="ForgotPassword.aspx.cs" Inherits="SuciHalalWebclient.ForgotPassword" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="dashboard">
        <div class="w-row" style="margin-top: 40vh; margin-bottom: 10vh;">
            <div class="w-col w-col-3"></div>
            <div class="w-col w-col-6">
                <div class="w-row">
                    <div class="w-col w-col-12">
                        <div class="form-margin">
                            <input class="subscribe-field-2 full w-input" data-name="email"
                                id="txtEmail" maxlength="256" name="Email"
                                placeholder="Enter your email" required type="text">
                        </div>
                    </div>
                </div>
                <div class="top-margin-2 medium">
                    <div>
                        <input class="checkout-button full-button w-button" data-wait="Please wait..." type="submit" value="Reset Password" onclick="var retvalue = ForgotPassword(); event.returnValue = retvalue; return retvalue;">
                    </div>
                </div>
            </div>
            <div class="w-col w-col-3"></div>
        </div>
    </div>
    <div id="divLoader" class="divLoader" style="display: none;"></div>
    <script src="js/jquery-3.3.1.min.js"></script>
    <script type="text/javascript">

        $("#txtEmail").focusout(function () {
            if ($(this).val() == "") {
                $(this).addClass('w-commerce-commercecheckouterrorstate');
            }
            else {
                $(this).removeClass('w-commerce-commercecheckouterrorstate');
            }

        });

        function ForgotPassword() {
            var lstrMemberEmail = $("#txtEmail").val();

            if (lstrMemberEmail == "") {
                $("#txtEmail").addClass('w-commerce-commercecheckouterrorstate');
            }
            else {
                $("#txtEmail").removeClass('w-commerce-commercecheckouterrorstate');
                $('#divLoader').show();
                $.ajax({
                    url: 'ForgotPassword.aspx/ForgotPwd',
                    type: 'POST',
                    contentType: 'application/json; charset =utf-8',
                    data: "{'pstrMemberEmail':'" + lstrMemberEmail.toString() + "'}",
                    dataType: 'json',
                    success: function (data) {
                        $('#divLoader').hide();
                        try {
                            var newData = data.d;

                            if ((newData != "") || newData == null) {
                                if (newData == "SUCCESS") {
                                    swal("Success!", "A new temporary password has been sent to your registered mail id.", "success");
                                    setTimeout(function () {
                                        window.location.href = "Login.aspx";
                                    }, 2000);
                                }
                                else {
                                    swal("Oops!", "Something went wrong.", "error");
                                }
                            }
                            else {
                                swal("Oops!", "Something went wrong.", "error");
                            }
                        }
                        catch (e) {
                            swal("Oops!", "Something went wrong.", "error");
                            return false;
                        }
                        return false;
                    },
                    error: function (errmsg) {
                        $('#divLoader').hide();
                        swal("Oops!", "Something went wrong.", "error");
                    }
                });
            }
        };


    </script>
</asp:Content>
