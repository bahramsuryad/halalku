﻿using Core.Platform.Transactions.Entites;
using Framework.EnterpriseLibrary.Adapters;
using Framework.EnterpriseLibrary.Common.SerializationHelper;
using Giift.SuciHalal.Entities;
using Giift.SuciHalal.Helper;
using Newtonsoft.Json.Linq;
using Stripe;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;
using Core.Platform.Common.Entities;
using System.Text;

namespace SuciHalalWebclient
{
    public partial class PaymentStatus : System.Web.UI.Page
    {
        public static string lstrAccessToken = string.Empty;
        string lstrPayStatus = string.Empty;
        string lstrResponseURL = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                StringBuilder PostData = new StringBuilder();
                if (Request.Form != null && Request.Form.Count > 0)
                {
                    foreach (var item in Request.Form.AllKeys)
                    {
                        PostData.AppendLine(item + ": " + Request.Form[item]);
                    }
                }
                LoggingAdapter.WriteLog("PaymentStatus-Request.Form-" + PostData.ToString());

                if (HttpContext.Current.Session["AccessToken"] == null)
                {
                    lstrResponseURL = "Login.aspx";
                }
                else
                {
                    lstrAccessToken = Convert.ToString(HttpContext.Current.Session["AccessToken"]);
                }

                if (Request.Form != null && Request.Form.Count > 0)
                {
                    var customers = new CustomerService();
                    var charges = new ChargeService();
                    PaymentDetails lobjPaymentDetails = null;

                    try
                    {
                        List<object> lobjlist = CachingAdapter.Get(Request.Form[3].ToString()) as List<object>;
                        Session["PurchasePoints"] = lobjlist[0] as TransactionDetails;
                        Session["AccessToken"] = lobjlist[1] as string;
                        Session["URL"] = lobjlist[2] as string;

                        Response lobjResponse = new Response();
                        for (int i = 0; i < Request.Form.Count; i++)
                        {
                            if (Request.Form.Keys[i].StartsWith("PG_Response"))
                            {
                                lobjResponse = JSONSerialization.Deserialize<Response>(Request.Form[i]);
                            }
                        }

                        //Payment Post Details from Checkout page
                        if (HttpContext.Current.Session["PaymentDetails"] != null)
                        {
                            lobjPaymentDetails = HttpContext.Current.Session["PaymentDetails"] as PaymentDetails;
                        }
                        else { lstrResponseURL = "PaymentFailed.aspx"; }

                        if (lobjResponse.IsSucessful)
                        {
                            LoggingAdapter.WriteLog("lobjResponse.IsSucessful-" + DateTime.Now + "#" + lobjPaymentDetails.OrderId + "#" + lobjPaymentDetails.PaymentId);
                            var data = lobjResponse.ReturnObject.Split('-');

                            lobjPaymentDetails.PaymentToken = data[2];

                            //Payment Pay call 
                            string lstrDeliveryEmail = string.Empty;
                            lstrDeliveryEmail = Convert.ToString(HttpContext.Current.Session["DeliveryEmail"]);

                            LoggingAdapter.WriteLog("PaymentPay Request:" + lstrAccessToken + "#" + lobjPaymentDetails.PaymentId + "#" + lobjPaymentDetails.PaymentToken);
                            string lstrResponse = ApiHelper.PaymentPay(lstrAccessToken, lobjPaymentDetails.PaymentId, lobjPaymentDetails.PaymentToken, lstrDeliveryEmail);
                            LoggingAdapter.WriteLog("PaymentPay Response:" + lstrAccessToken + "#" + lstrResponse);

                            HttpContext.Current.Session["PaymentDetails"] = null;

                            JToken outer = JToken.Parse(lstrResponse);
                            if (outer["error"] != null)
                            {
                                JArray array = outer.Value<JArray>("error");
                                //lstrMessage = array[0].Value<string>("msg");
                            }
                            else if (outer["payment"] != null)
                            {
                                if (outer["payment"]["Status"] != null)
                                {
                                    lstrPayStatus = Convert.ToString(outer["payment"]["Status"]);

                                    if (lstrPayStatus == "completed")
                                    {
                                        lstrResponseURL = "PaymentSuccessful.aspx";
                                    }
                                    else
                                    {
                                        lstrResponseURL = "PaymentFailed.aspx";
                                    }
                                }
                                else
                                {
                                    lstrResponseURL = "PaymentFailed.aspx";
                                }
                            }
                        }
                        else
                        {
                            LoggingAdapter.WriteLog("lobjResponse.IsSucessful Failed-" + DateTime.Now + "#" + lstrAccessToken);
                            lstrResponseURL = "PaymentFailed.aspx";
                        }
                    }
                    catch (Exception ex)
                    {
                        LoggingAdapter.WriteLog("PaymentStatus Inner Ex: " + ex.Message + Environment.NewLine + ex.StackTrace + ex.InnerException);
                        HttpContext.Current.Session["PaymentDetails"] = null;
                        Response.Redirect("PaymentFailed.aspx");
                    }

                    //Console.WriteLine(charge);
                }
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("PaymentStatus Ex: " + ex.Message + Environment.NewLine + ex.StackTrace + ex.InnerException);
                HttpContext.Current.Session["PaymentDetails"] = null;
                lstrResponseURL = "PaymentFailed.aspx";
            }
            finally
            {
                Response.Redirect(lstrResponseURL);
                HttpContext.Current.Session["DeliveryEmail"] = null;
            }
        }
    }
}
