﻿using Giift.SuciHalal.Helper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Giift.SuciHalal.TestApplication
{
    public partial class ChangePassword : Form
    {
        public ChangePassword()
        {
            InitializeComponent();
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            txtResponse.Text = ApiHelper.ChangePassword(txtAccessToken.Text, txtCurrentPwd.Text, txtNewPwd.Text, txtConfirmPwd.Text);
        }
    }
}
