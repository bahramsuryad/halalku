﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Giift.SuciHalal.Helper;

namespace Giift.SuciHalal.TestApplication
{
    public partial class SetDeliveryGiftCard : Form
    {
        public SetDeliveryGiftCard()
        {
            InitializeComponent();
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            txtResponse.Text = ApiHelper.SetDeliveryGiftCard(txtAccessToken.Text, txtOrderItemId.Text, txtProviderKey.Text, txtName.Text, txtAddress.Text, txtCity.Text,
                txtCountry.Text, txtZip.Text, txtPhone.Text, txtDeliveryId.Text);
        }

        private void SetDeliveryGiftCard_Load(object sender, EventArgs e)
        {
            txtProviderKey.Text = "mail";
        }
    }
}
