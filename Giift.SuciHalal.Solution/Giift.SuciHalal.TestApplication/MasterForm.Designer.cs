﻿namespace Giift.SuciHalal.TestApplication
{
    partial class MasterForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.loginToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.signUpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.signInToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.resetPasswordToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.subscribeNewsletterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.updateProfileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.buyableToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.productListToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.productDetailsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.purchaseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.purchaseGiftCardToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.merchendisedPurchaseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newCartToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addItemToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.removeFromCartToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.getDelieveryProviderToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.setDeliveryProductToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.setDeliveryGiftToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.orderManagementToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.placeOrderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewOrdersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewOrderDetailsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.orderDeliveryToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.createPaymentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.paymentPayToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.locationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.getDiningOptionDetailsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listOfMosqueToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.retrieveContentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.getPaymentProviderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.getCartDetailsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.getLocationSettingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.loginToolStripMenuItem,
            this.buyableToolStripMenuItem,
            this.purchaseToolStripMenuItem,
            this.merchendisedPurchaseToolStripMenuItem,
            this.orderManagementToolStripMenuItem,
            this.toolStripMenuItem3,
            this.locationToolStripMenuItem,
            this.contentToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(800, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // loginToolStripMenuItem
            // 
            this.loginToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.signUpToolStripMenuItem,
            this.signInToolStripMenuItem,
            this.resetPasswordToolStripMenuItem,
            this.subscribeNewsletterToolStripMenuItem,
            this.updateProfileToolStripMenuItem});
            this.loginToolStripMenuItem.Name = "loginToolStripMenuItem";
            this.loginToolStripMenuItem.Size = new System.Drawing.Size(42, 20);
            this.loginToolStripMenuItem.Text = "User";
            // 
            // signUpToolStripMenuItem
            // 
            this.signUpToolStripMenuItem.Name = "signUpToolStripMenuItem";
            this.signUpToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
            this.signUpToolStripMenuItem.Text = "Sign Up";
            this.signUpToolStripMenuItem.Click += new System.EventHandler(this.signUpToolStripMenuItem_Click);
            // 
            // signInToolStripMenuItem
            // 
            this.signInToolStripMenuItem.Name = "signInToolStripMenuItem";
            this.signInToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
            this.signInToolStripMenuItem.Text = "Sign In";
            this.signInToolStripMenuItem.Click += new System.EventHandler(this.signInToolStripMenuItem_Click);
            // 
            // resetPasswordToolStripMenuItem
            // 
            this.resetPasswordToolStripMenuItem.Name = "resetPasswordToolStripMenuItem";
            this.resetPasswordToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
            this.resetPasswordToolStripMenuItem.Text = "Reset Password";
            this.resetPasswordToolStripMenuItem.Click += new System.EventHandler(this.resetPasswordToolStripMenuItem_Click);
            // 
            // subscribeNewsletterToolStripMenuItem
            // 
            this.subscribeNewsletterToolStripMenuItem.Name = "subscribeNewsletterToolStripMenuItem";
            this.subscribeNewsletterToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
            this.subscribeNewsletterToolStripMenuItem.Text = "Subscribe News Letter";
            this.subscribeNewsletterToolStripMenuItem.Click += new System.EventHandler(this.subscribeNewsletterToolStripMenuItem_Click);
            // 
            // updateProfileToolStripMenuItem
            // 
            this.updateProfileToolStripMenuItem.Name = "updateProfileToolStripMenuItem";
            this.updateProfileToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
            this.updateProfileToolStripMenuItem.Text = "Update Profile";
            this.updateProfileToolStripMenuItem.Click += new System.EventHandler(this.updateProfileToolStripMenuItem_Click);
            // 
            // buyableToolStripMenuItem
            // 
            this.buyableToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.productListToolStripMenuItem,
            this.productDetailsToolStripMenuItem});
            this.buyableToolStripMenuItem.Name = "buyableToolStripMenuItem";
            this.buyableToolStripMenuItem.Size = new System.Drawing.Size(61, 20);
            this.buyableToolStripMenuItem.Text = "Product";
            // 
            // productListToolStripMenuItem
            // 
            this.productListToolStripMenuItem.Name = "productListToolStripMenuItem";
            this.productListToolStripMenuItem.Size = new System.Drawing.Size(154, 22);
            this.productListToolStripMenuItem.Text = "Product List";
            this.productListToolStripMenuItem.Click += new System.EventHandler(this.productListToolStripMenuItem_Click);
            // 
            // productDetailsToolStripMenuItem
            // 
            this.productDetailsToolStripMenuItem.Name = "productDetailsToolStripMenuItem";
            this.productDetailsToolStripMenuItem.Size = new System.Drawing.Size(154, 22);
            this.productDetailsToolStripMenuItem.Text = "Product Details";
            this.productDetailsToolStripMenuItem.Click += new System.EventHandler(this.productDetailsToolStripMenuItem_Click);
            // 
            // purchaseToolStripMenuItem
            // 
            this.purchaseToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.purchaseGiftCardToolStripMenuItem});
            this.purchaseToolStripMenuItem.Name = "purchaseToolStripMenuItem";
            this.purchaseToolStripMenuItem.Size = new System.Drawing.Size(89, 20);
            this.purchaseToolStripMenuItem.Text = "Gift Purchase";
            // 
            // purchaseGiftCardToolStripMenuItem
            // 
            this.purchaseGiftCardToolStripMenuItem.Name = "purchaseGiftCardToolStripMenuItem";
            this.purchaseGiftCardToolStripMenuItem.Size = new System.Drawing.Size(172, 22);
            this.purchaseGiftCardToolStripMenuItem.Text = "Purchase Gift Card";
            this.purchaseGiftCardToolStripMenuItem.Click += new System.EventHandler(this.purchaseGiftCardToolStripMenuItem_Click);
            // 
            // merchendisedPurchaseToolStripMenuItem
            // 
            this.merchendisedPurchaseToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newCartToolStripMenuItem,
            this.addItemToolStripMenuItem,
            this.removeFromCartToolStripMenuItem,
            this.getDelieveryProviderToolStripMenuItem1,
            this.setDeliveryProductToolStripMenuItem,
            this.setDeliveryGiftToolStripMenuItem,
            this.getCartDetailsToolStripMenuItem});
            this.merchendisedPurchaseToolStripMenuItem.Name = "merchendisedPurchaseToolStripMenuItem";
            this.merchendisedPurchaseToolStripMenuItem.Size = new System.Drawing.Size(41, 20);
            this.merchendisedPurchaseToolStripMenuItem.Text = "Cart";
            // 
            // newCartToolStripMenuItem
            // 
            this.newCartToolStripMenuItem.Name = "newCartToolStripMenuItem";
            this.newCartToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
            this.newCartToolStripMenuItem.Text = "New Cart";
            this.newCartToolStripMenuItem.Click += new System.EventHandler(this.newCartToolStripMenuItem_Click);
            // 
            // addItemToolStripMenuItem
            // 
            this.addItemToolStripMenuItem.Name = "addItemToolStripMenuItem";
            this.addItemToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
            this.addItemToolStripMenuItem.Text = "Add to Cart";
            this.addItemToolStripMenuItem.Click += new System.EventHandler(this.addItemToolStripMenuItem_Click);
            // 
            // removeFromCartToolStripMenuItem
            // 
            this.removeFromCartToolStripMenuItem.Name = "removeFromCartToolStripMenuItem";
            this.removeFromCartToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
            this.removeFromCartToolStripMenuItem.Text = "Remove From Cart";
            this.removeFromCartToolStripMenuItem.Click += new System.EventHandler(this.removeFromCartToolStripMenuItem_Click);
            // 
            // getDelieveryProviderToolStripMenuItem1
            // 
            this.getDelieveryProviderToolStripMenuItem1.Name = "getDelieveryProviderToolStripMenuItem1";
            this.getDelieveryProviderToolStripMenuItem1.Size = new System.Drawing.Size(190, 22);
            this.getDelieveryProviderToolStripMenuItem1.Text = "Get Delievery Provider";
            this.getDelieveryProviderToolStripMenuItem1.Click += new System.EventHandler(this.getDelieveryProviderToolStripMenuItem_Click);
            // 
            // setDeliveryProductToolStripMenuItem
            // 
            this.setDeliveryProductToolStripMenuItem.Name = "setDeliveryProductToolStripMenuItem";
            this.setDeliveryProductToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
            this.setDeliveryProductToolStripMenuItem.Text = "Set Delivery Product";
            this.setDeliveryProductToolStripMenuItem.Click += new System.EventHandler(this.setDeliveryProductToolStripMenuItem_Click);
            // 
            // setDeliveryGiftToolStripMenuItem
            // 
            this.setDeliveryGiftToolStripMenuItem.Name = "setDeliveryGiftToolStripMenuItem";
            this.setDeliveryGiftToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
            this.setDeliveryGiftToolStripMenuItem.Text = "Set Delivery Gift Card";
            this.setDeliveryGiftToolStripMenuItem.Click += new System.EventHandler(this.setDeliveryGiftToolStripMenuItem_Click);
            // 
            // orderManagementToolStripMenuItem
            // 
            this.orderManagementToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.placeOrderToolStripMenuItem,
            this.viewOrdersToolStripMenuItem,
            this.viewOrderDetailsToolStripMenuItem,
            this.orderDeliveryToolStripMenuItem1});
            this.orderManagementToolStripMenuItem.Name = "orderManagementToolStripMenuItem";
            this.orderManagementToolStripMenuItem.Size = new System.Drawing.Size(54, 20);
            this.orderManagementToolStripMenuItem.Text = "Orders";
            // 
            // placeOrderToolStripMenuItem
            // 
            this.placeOrderToolStripMenuItem.Name = "placeOrderToolStripMenuItem";
            this.placeOrderToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.placeOrderToolStripMenuItem.Text = "Place Order";
            this.placeOrderToolStripMenuItem.Click += new System.EventHandler(this.placeOrderToolStripMenuItem_Click);
            // 
            // viewOrdersToolStripMenuItem
            // 
            this.viewOrdersToolStripMenuItem.Name = "viewOrdersToolStripMenuItem";
            this.viewOrdersToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.viewOrdersToolStripMenuItem.Text = "View Orders";
            this.viewOrdersToolStripMenuItem.Click += new System.EventHandler(this.viewOrdersToolStripMenuItem_Click);
            // 
            // viewOrderDetailsToolStripMenuItem
            // 
            this.viewOrderDetailsToolStripMenuItem.Name = "viewOrderDetailsToolStripMenuItem";
            this.viewOrderDetailsToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.viewOrderDetailsToolStripMenuItem.Text = "View Order Details";
            this.viewOrderDetailsToolStripMenuItem.Click += new System.EventHandler(this.viewOrderDetailsToolStripMenuItem_Click);
            // 
            // orderDeliveryToolStripMenuItem1
            // 
            this.orderDeliveryToolStripMenuItem1.Name = "orderDeliveryToolStripMenuItem1";
            this.orderDeliveryToolStripMenuItem1.Size = new System.Drawing.Size(180, 22);
            this.orderDeliveryToolStripMenuItem1.Text = "Order Delivery";
            this.orderDeliveryToolStripMenuItem1.Click += new System.EventHandler(this.orderDeliveryToolStripMenuItem1_Click);
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.createPaymentToolStripMenuItem,
            this.paymentPayToolStripMenuItem1,
            this.getPaymentProviderToolStripMenuItem});
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(66, 20);
            this.toolStripMenuItem3.Text = "Payment";
            // 
            // createPaymentToolStripMenuItem
            // 
            this.createPaymentToolStripMenuItem.Name = "createPaymentToolStripMenuItem";
            this.createPaymentToolStripMenuItem.Size = new System.Drawing.Size(158, 22);
            this.createPaymentToolStripMenuItem.Text = "Create Payment";
            this.createPaymentToolStripMenuItem.Click += new System.EventHandler(this.createPaymentToolStripMenuItem_Click);
            // 
            // paymentPayToolStripMenuItem1
            // 
            this.paymentPayToolStripMenuItem1.Name = "paymentPayToolStripMenuItem1";
            this.paymentPayToolStripMenuItem1.Size = new System.Drawing.Size(158, 22);
            this.paymentPayToolStripMenuItem1.Text = "Payment Pay";
            this.paymentPayToolStripMenuItem1.Click += new System.EventHandler(this.paymentPayToolStripMenuItem1_Click);
            // 
            // locationToolStripMenuItem
            // 
            this.locationToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.getDiningOptionDetailsToolStripMenuItem,
            this.listOfMosqueToolStripMenuItem,
            this.getLocationSettingsToolStripMenuItem});
            this.locationToolStripMenuItem.Name = "locationToolStripMenuItem";
            this.locationToolStripMenuItem.Size = new System.Drawing.Size(65, 20);
            this.locationToolStripMenuItem.Text = "Location";
            // 
            // getDiningOptionDetailsToolStripMenuItem
            // 
            this.getDiningOptionDetailsToolStripMenuItem.Name = "getDiningOptionDetailsToolStripMenuItem";
            this.getDiningOptionDetailsToolStripMenuItem.Size = new System.Drawing.Size(208, 22);
            this.getDiningOptionDetailsToolStripMenuItem.Text = "Get Dining Option Details";
            this.getDiningOptionDetailsToolStripMenuItem.Click += new System.EventHandler(this.getDiningOptionDetailsToolStripMenuItem_Click);
            // 
            // listOfMosqueToolStripMenuItem
            // 
            this.listOfMosqueToolStripMenuItem.Name = "listOfMosqueToolStripMenuItem";
            this.listOfMosqueToolStripMenuItem.Size = new System.Drawing.Size(208, 22);
            this.listOfMosqueToolStripMenuItem.Text = "List of Mosque";
            this.listOfMosqueToolStripMenuItem.Click += new System.EventHandler(this.listOfMosqueToolStripMenuItem_Click);
            // 
            // contentToolStripMenuItem
            // 
            this.contentToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.retrieveContentToolStripMenuItem});
            this.contentToolStripMenuItem.Name = "contentToolStripMenuItem";
            this.contentToolStripMenuItem.Size = new System.Drawing.Size(62, 20);
            this.contentToolStripMenuItem.Text = "Content";
            // 
            // retrieveContentToolStripMenuItem
            // 
            this.retrieveContentToolStripMenuItem.Name = "retrieveContentToolStripMenuItem";
            this.retrieveContentToolStripMenuItem.Size = new System.Drawing.Size(162, 22);
            this.retrieveContentToolStripMenuItem.Text = "Retrieve Content";
            this.retrieveContentToolStripMenuItem.Click += new System.EventHandler(this.retrieveContentToolStripMenuItem_Click);
            // 
            // getPaymentProviderToolStripMenuItem
            // 
            this.getPaymentProviderToolStripMenuItem.Name = "getPaymentProviderToolStripMenuItem";
            this.getPaymentProviderToolStripMenuItem.Size = new System.Drawing.Size(189, 22);
            this.getPaymentProviderToolStripMenuItem.Text = "Get Payment Provider";
            this.getPaymentProviderToolStripMenuItem.Click += new System.EventHandler(this.getPaymentProviderToolStripMenuItem_Click);
            // 
            // getCartDetailsToolStripMenuItem
            // 
            this.getCartDetailsToolStripMenuItem.Name = "getCartDetailsToolStripMenuItem";
            this.getCartDetailsToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
            this.getCartDetailsToolStripMenuItem.Text = "Get Cart Details";
            this.getCartDetailsToolStripMenuItem.Click += new System.EventHandler(this.getCartDetailsToolStripMenuItem_Click);
            // 
            // getLocationSettingsToolStripMenuItem
            // 
            this.getLocationSettingsToolStripMenuItem.Name = "getLocationSettingsToolStripMenuItem";
            this.getLocationSettingsToolStripMenuItem.Size = new System.Drawing.Size(208, 22);
            this.getLocationSettingsToolStripMenuItem.Text = "Get Location Settings";
            this.getLocationSettingsToolStripMenuItem.Click += new System.EventHandler(this.getLocationSettingsToolStripMenuItem_Click);
            // 
            // MasterForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.menuStrip1);
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MasterForm";
            this.Text = "Visa Halal";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem loginToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem signUpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem signInToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem resetPasswordToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem subscribeNewsletterToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem buyableToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem purchaseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem purchaseGiftCardToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem merchendisedPurchaseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newCartToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addItemToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem setDeliveryProductToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem setDeliveryGiftToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem orderManagementToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem viewOrdersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem viewOrderDetailsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem placeOrderToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem locationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem getDiningOptionDetailsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem listOfMosqueToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem contentToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem retrieveContentToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem createPaymentToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem paymentPayToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem orderDeliveryToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem updateProfileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem productListToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem productDetailsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem removeFromCartToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem getDelieveryProviderToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem getPaymentProviderToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem getCartDetailsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem getLocationSettingsToolStripMenuItem;
    }
}

