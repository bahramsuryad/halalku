﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Giift.SuciHalal.Helper;

namespace Giift.SuciHalal.TestApplication
{
    public partial class UpdateProfile : Form
    {
        public UpdateProfile()
        {
            InitializeComponent();
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            txtResponse.Text = ApiHelper.UpdateProfile(txtAccessToken.Text, txtFirstName.Text, txtLastName.Text, txtCity.Text, txtPhone.Text, txtDOB.Text, txtGender.Text,
                txtAddress.Text, txtZip.Text, txtLanguage.Text, txtNewsletter.Text, txtCountry.Text);
        }
    }
}
