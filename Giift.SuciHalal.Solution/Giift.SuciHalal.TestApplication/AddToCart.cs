﻿using Giift.SuciHalal.Helper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Giift.SuciHalal.TestApplication
{
    public partial class AddToCart : Form
    {
        public AddToCart()
        {
            InitializeComponent();
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            txtResponse.Text = ApiHelper.AddCartItem(txtAccessToken.Text, txtBuyableType.Text, txtProductId.Text, txtCCY.Text, Convert.ToInt32(txtQty.Text), txtCartId.Text);
        }
    }
}
