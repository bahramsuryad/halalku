﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Giift.SuciHalal.TestApplication
{
    public partial class MasterForm : Form
    {
        public MasterForm()
        {
            InitializeComponent();
        }

        private void signUpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SignUp lobjSignUpForm = new SignUp();
            lobjSignUpForm.MdiParent = this;
            lobjSignUpForm.Show();
        }

        private void signInToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SignIn lobjSignIn = new SignIn();
            lobjSignIn.MdiParent = this;
            lobjSignIn.Show();
        }

        private void resetPasswordToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ResetPassword lobjResetPassword = new ResetPassword();
            lobjResetPassword.MdiParent = this;
            lobjResetPassword.Show();
        }

        private void subscribeNewsletterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SubscribeNews lobjSubscribeNews = new SubscribeNews();
            lobjSubscribeNews.MdiParent = this;
            lobjSubscribeNews.Show();
        }

        private void updateProfileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UpdateProfile lobjUpdateProfile = new UpdateProfile();
            lobjUpdateProfile.MdiParent = this;
            lobjUpdateProfile.Show();
        }

        private void productListToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ProductList lobjProductList = new ProductList();
            lobjProductList.MdiParent = this;
            lobjProductList.Show();
        }

        private void purchaseGiftCardToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PurchaseGiftCard lobjPurchaseGiftCard = new PurchaseGiftCard();
            lobjPurchaseGiftCard.MdiParent = this;
            lobjPurchaseGiftCard.Show();
        }

        private void newCartToolStripMenuItem_Click(object sender, EventArgs e)
        {
            NewCart lobjNewCart = new NewCart();
            lobjNewCart.MdiParent = this;
            lobjNewCart.Show();
        }

        private void addItemToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddToCart lobjAddToCart = new AddToCart();
            lobjAddToCart.MdiParent = this;
            lobjAddToCart.Show();
        }

        private void setDeliveryProductToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SetDeliveryProduct lobjSetDeliveryProduct = new SetDeliveryProduct();
            lobjSetDeliveryProduct.MdiParent = this;
            lobjSetDeliveryProduct.Show();
        }

        private void setDeliveryGiftToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SetDeliveryGiftCard lobjSetDeliveryGiftCard = new SetDeliveryGiftCard();
            lobjSetDeliveryGiftCard.MdiParent = this;
            lobjSetDeliveryGiftCard.Show();
        }

        private void placeOrderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PlaceOrder lobjPlaceOrder = new PlaceOrder();
            lobjPlaceOrder.MdiParent = this;
            lobjPlaceOrder.Show();
        }

        private void viewOrdersToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ViewOrder lobjViewOrder = new ViewOrder();
            lobjViewOrder.MdiParent = this;
            lobjViewOrder.Show();
        }

        private void viewOrderDetailsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ViewOrderDetails lobjViewOrderDetails = new ViewOrderDetails();
            lobjViewOrderDetails.MdiParent = this;
            lobjViewOrderDetails.Show();
        }

        private void orderDeliveryToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            OrderDelivery lobjOrderDelivery = new OrderDelivery();
            lobjOrderDelivery.MdiParent = this;
            lobjOrderDelivery.Show();
        }

        private void createPaymentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CreatePayment lobjCreatePayment = new CreatePayment();
            lobjCreatePayment.MdiParent = this;
            lobjCreatePayment.Show();
        }

        private void paymentPayToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            PaymentPay lobjPaymentPay = new PaymentPay();
            lobjPaymentPay.MdiParent = this;
            lobjPaymentPay.Show();
        }

        private void getDiningOptionDetailsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DiningOptionDetails lobjDiningOptionDetails = new DiningOptionDetails();
            lobjDiningOptionDetails.MdiParent = this;
            lobjDiningOptionDetails.Show();
        }

        private void listOfMosqueToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MosqueList lobjMosqueList = new MosqueList();
            lobjMosqueList.MdiParent = this;
            lobjMosqueList.Show();
        }

        private void retrieveContentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RetrieveContent lobjRetrieveContent = new RetrieveContent();
            lobjRetrieveContent.MdiParent = this;
            lobjRetrieveContent.Show();
        }

        private void productDetailsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ProductDetails lobjProductDetails = new ProductDetails();
            lobjProductDetails.MdiParent = this;
            lobjProductDetails.Show();
        }

        private void removeFromCartToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RemoveCartItem lobjRemoveCartItem = new RemoveCartItem();
            lobjRemoveCartItem.MdiParent = this;
            lobjRemoveCartItem.Show();
        }

        private void getDelieveryProviderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            GetDelieveryProvider lobjGetDelieveryProvider = new GetDelieveryProvider();
            lobjGetDelieveryProvider.MdiParent = this;
            lobjGetDelieveryProvider.Show();
        }

        private void getPaymentProviderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            GetPaymentProvider lobjGetPaymentProvider = new GetPaymentProvider();
            lobjGetPaymentProvider.MdiParent = this;
            lobjGetPaymentProvider.Show();
        }

        private void getCartDetailsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            GetCartDetails lobjGetCartDetails = new GetCartDetails();
            lobjGetCartDetails.MdiParent = this;
            lobjGetCartDetails.Show();
        }

        private void getLocationSettingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            GetLocationSettings lobjGetLocationSettings = new GetLocationSettings();
            lobjGetLocationSettings.MdiParent = this;
            lobjGetLocationSettings.Show();
        }
    }
}
