﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Giift.SuciHalal.Helper;

namespace Giift.SuciHalal.TestApplication
{
    public partial class PurchaseGiftCard : Form
    {
        public PurchaseGiftCard()
        {
            InitializeComponent();
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            txtResponse.Text = ApiHelper.PurchaseGiftCard(txtAccessToken.Text, txtDmoId.Text, Convert.ToSingle(txtAmount.Text));
        }
    }
}
