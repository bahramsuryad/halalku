﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Framework.EnterpriseLibrary.Adapters;
using Framework.EnterpriseLibrary.Common.SerializationHelper;
using Giift.SuciHalal.CommonConstants;

namespace Giift.SuciHalal.Helper
{
    public class ApiHelper
    {
        public static string SignUp(string pstrEmailId, string pstrPassword)
        {
            string lstrResponse = string.Empty;
            try
            {
                NameValueCollection lobjNVCParameter = new NameValueCollection();
                lobjNVCParameter.Add("client_id", URLConstants.ClientId);
                lobjNVCParameter.Add("client_secret", URLConstants.ClientSecret);
                lobjNVCParameter.Add("grant_type", URLConstants.Grant_Signup);
                lobjNVCParameter.Add("username", pstrEmailId);
                lobjNVCParameter.Add("password", pstrPassword);
                lobjNVCParameter.Add("country", URLConstants.Country);
                lobjNVCParameter.Add("scope", URLConstants.Scope);
                string lstrPostData = ConstructQueryString(lobjNVCParameter);

                lstrResponse = PostUrlEncodedRequest(URLConstants.SignUp, HelperConstants.POST, null, lstrPostData);
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("ApiHelper SignUp Ex: " + ex.Message + Environment.NewLine + "StackTrace:" + ex.StackTrace + Environment.NewLine + "InnerException:" + ex.InnerException);

                lstrResponse = "ApiHelper SignUp Ex: " + ex.Message + Environment.NewLine + "StackTrace:" + ex.StackTrace + Environment.NewLine + "InnerException:" + ex.InnerException;
            }
            return lstrResponse;
        }

        public static string SignIn(string pstrEmailId, string pstrPassword)
        {
            string lstrResponse = string.Empty;
            try
            {
                NameValueCollection lobjNVCParameter = new NameValueCollection();
                lobjNVCParameter.Add("client_id", URLConstants.ClientId);
                lobjNVCParameter.Add("client_secret", URLConstants.ClientSecret);
                lobjNVCParameter.Add("grant_type", URLConstants.Grant_SignIn);
                lobjNVCParameter.Add("username", pstrEmailId);
                lobjNVCParameter.Add("password", pstrPassword);
                lobjNVCParameter.Add("scope", URLConstants.Scope);
                string lstrPostData = ConstructQueryString(lobjNVCParameter);
                lstrResponse = PostUrlEncodedRequest(URLConstants.SignIn, HelperConstants.POST, null, lstrPostData);

            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("ApiHelper SignIn Ex: " + ex.Message + Environment.NewLine + "StackTrace:" + ex.StackTrace + Environment.NewLine + "InnerException:" + ex.InnerException);
                lstrResponse = "ApiHelper SignIn Ex: " + ex.Message + Environment.NewLine + "StackTrace:" + ex.StackTrace + Environment.NewLine + "InnerException:" + ex.InnerException;
            }
            return lstrResponse;
        }

        public static string ForgotPassword(string pstrEmailId)
        {
            string lstrResponse = string.Empty;
            try
            {
                lstrResponse = PostUrlEncodedRequest(string.Format(URLConstants.ForgotPassword, pstrEmailId, URLConstants.ClientId), HelperConstants.GET, null, string.Empty);
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("ApiHelper ForgotPassword Ex: " + ex.Message + Environment.NewLine + "StackTrace:" + ex.StackTrace + Environment.NewLine + "InnerException:" + ex.InnerException);
                lstrResponse = "ApiHelper ForgotPassword Ex: " + ex.Message + Environment.NewLine + "StackTrace:" + ex.StackTrace + Environment.NewLine + "InnerException:" + ex.InnerException;
            }
            return lstrResponse;
        }

        public static string SubscribeNewsletter(string pstrAccesstoken, string pstrEmailId)
        {
            string lstrResponse = string.Empty;
            try
            {
                NameValueCollection lobjNVCHeader = new NameValueCollection();
                lobjNVCHeader.Add("Authorization", string.Format("Bearer {0}", pstrAccesstoken));

                NameValueCollection lobjNVCParameter = new NameValueCollection();
                lobjNVCParameter.Add("email", pstrEmailId);
                lobjNVCParameter.Add("country", URLConstants.Country);
                string lstrPostData = ConstructQueryString(lobjNVCParameter);

                lstrResponse = PostUrlEncodedRequest(URLConstants.SubscribeNewsletter, HelperConstants.POST, lobjNVCHeader, lstrPostData);
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("ApiHelper SubscribeNewsletter Ex: " + ex.Message + Environment.NewLine + "StackTrace:" + ex.StackTrace + Environment.NewLine + "InnerException:" + ex.InnerException);
                lstrResponse = "ApiHelper SubscribeNewsletter Ex: " + ex.Message + Environment.NewLine + "StackTrace:" + ex.StackTrace + Environment.NewLine + "InnerException:" + ex.InnerException;
            }
            return lstrResponse;
        }

        /// <summary>
        /// List of buyable items
        /// </summary>
        /// <param name="pstrAccesstoken">User token</param>
        /// <param name="pstrCountry">Country code eg.MY</param>
        /// <param name="pstrType">card,topup or product</param>
        /// <returns></returns>
        //public static string BuyableList(string pstrAccesstoken, string pstrCountry, string pstrType)
        //{
        //    string lstrResponse = string.Empty;
        //    try
        //    {
        //        NameValueCollection lobjNVCHeader = new NameValueCollection();
        //        lobjNVCHeader.Add("Authorization", string.Format("Bearer {0}", pstrAccesstoken));

        //        lstrResponse = PostUrlEncodedRequest(string.Format(URLConstants.BuyableList, pstrCountry, pstrType), HelperConstants.GET, lobjNVCHeader, string.Empty);
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggingAdapter.WriteLog("ApiHelper BuyableList Ex: " + ex.Message + Environment.NewLine + "StackTrace:" + ex.StackTrace + Environment.NewLine + "InnerException:" + ex.InnerException);
        //        lstrResponse = "ApiHelper BuyableList Ex: " + ex.Message + Environment.NewLine + "StackTrace:" + ex.StackTrace + Environment.NewLine + "InnerException:" + ex.InnerException;
        //    }
        //    return lstrResponse;
        //}


        public static string BuyableList(Dictionary<string, string> pstrDictionary)
        {
            string lstrResponse = string.Empty;
            string lstrDictionaryUrl = string.Empty;
            try
            {
                foreach (var item in pstrDictionary)
                {
                    lstrDictionaryUrl += "&" + item.Key + "=" + item.Value;
                }

                string lstrUrl = string.Format(URLConstants.BuyableList, URLConstants.ClientId, HttpUtility.UrlPathEncode(lstrDictionaryUrl));
                lstrResponse = PostUrlEncodedRequest(lstrUrl, HelperConstants.GET, null, string.Empty);
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("ApiHelper BuyableList Ex: " + ex.Message + Environment.NewLine + "StackTrace:" + ex.StackTrace + Environment.NewLine + "InnerException:" + ex.InnerException);
                lstrResponse = "ApiHelper BuyableList Ex: " + ex.Message + Environment.NewLine + "StackTrace:" + ex.StackTrace + Environment.NewLine + "InnerException:" + ex.InnerException;
            }

            return lstrResponse;
        }


        /// <summary>
        /// Details of buyable item
        /// </summary>
        /// <param name="pstrAccesstoken">User token</param>
        /// <param name="pstrType">card,topup or product</param>
        /// <param name="ProductId">underlying_id from the buyable list</param>
        /// <returns></returns>
        public static string BuyableDetails(string pstrAccesstoken, string pstrType, string ProductId, string pstrCurrency)
        {
            string lstrResponse = string.Empty;
            try
            {
                NameValueCollection lobjNVCHeader = new NameValueCollection();
                lobjNVCHeader.Add("Authorization", string.Format("Bearer {0}", pstrAccesstoken));

                lstrResponse = PostUrlEncodedRequest(string.Format(URLConstants.BuyableDetails, pstrType, ProductId, pstrCurrency), HelperConstants.GET, lobjNVCHeader, string.Empty);
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("ApiHelper BuyableDetails Ex: " + ex.Message + Environment.NewLine + "StackTrace:" + ex.StackTrace + Environment.NewLine + "InnerException:" + ex.InnerException);
                lstrResponse = "ApiHelper BuyableDetails Ex: " + ex.Message + Environment.NewLine + "StackTrace:" + ex.StackTrace + Environment.NewLine + "InnerException:" + ex.InnerException;
            }
            return lstrResponse;
        }

        //public static string BuyableListForCountry(string pstrAccesstoken, string pstrCountry)
        //{
        //    string lstrResponse = string.Empty;
        //    try
        //    {
        //        NameValueCollection lobjNVCHeader = new NameValueCollection();
        //        lobjNVCHeader.Add("Authorization", string.Format("Bearer {0}", pstrAccesstoken));

        //        lstrResponse = PostUrlEncodedRequest(string.Format(URLConstants.BuyableListForCountry, pstrCountry), HelperConstants.GET, lobjNVCHeader, string.Empty);
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggingAdapter.WriteLog("ApiHelper BuyableListForCountry Ex: " + ex.Message + Environment.NewLine + "StackTrace:" + ex.StackTrace + Environment.NewLine + "InnerException:" + ex.InnerException);
        //        lstrResponse = "ApiHelper SignUp Ex: " + ex.Message + Environment.NewLine + "StackTrace:" + ex.StackTrace + Environment.NewLine + "InnerException:" + ex.InnerException;
        //    }
        //    return lstrResponse;
        //}

        //public static string BuyableListForType(string pstrAccesstoken, string pstrType)
        //{
        //    string lstrResponse = string.Empty;
        //    try
        //    {
        //        NameValueCollection lobjNVCHeader = new NameValueCollection();
        //        lobjNVCHeader.Add("Authorization", string.Format("Bearer {0}", pstrAccesstoken));

        //        lstrResponse = PostUrlEncodedRequest(string.Format(URLConstants.BuyableListForType, pstrType), HelperConstants.GET, lobjNVCHeader, string.Empty);
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggingAdapter.WriteLog("ApiHelper BuyableListForType Ex: " + ex.Message + Environment.NewLine + "StackTrace:" + ex.StackTrace + Environment.NewLine + "InnerException:" + ex.InnerException);
        //        lstrResponse = "ApiHelper BuyableListForType Ex: " + ex.Message + Environment.NewLine + "StackTrace:" + ex.StackTrace + Environment.NewLine + "InnerException:" + ex.InnerException;
        //    }
        //    return lstrResponse;
        //}

        public static string PurchaseGiftCard(string pstrAccesstoken, string pstrDmoid, double pdblAmount)
        {
            string lstrResponse = string.Empty;
            try
            {
                NameValueCollection lobjNVCHeader = new NameValueCollection();
                lobjNVCHeader.Add("Authorization", string.Format("Bearer {0}", pstrAccesstoken));

                NameValueCollection lobjNVCParameter = new NameValueCollection();
                lobjNVCParameter.Add("dmo_id", pstrDmoid);
                lobjNVCParameter.Add("amount", Convert.ToString(pdblAmount));
                lobjNVCParameter.Add("delivery_type", "api");
                lobjNVCParameter.Add("delivery_data_notification_url", "");
                lobjNVCParameter.Add("metadatas[receipt_id]", "");//External Ref
                lobjNVCParameter.Add("metadatas[pos_id]", "");
                lobjNVCParameter.Add("metadatas[branch_id]", "");//RefererName
                lobjNVCParameter.Add("metadatas[operator_id]", "");
                string lstrPostData = ConstructQueryString(lobjNVCParameter);

                lstrResponse = PostUrlEncodedRequest(URLConstants.SubscribeNewsletter, HelperConstants.POST, lobjNVCHeader, lstrPostData);

            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("ApiHelper PurchaseGiftCard Ex: " + ex.Message + Environment.NewLine + "StackTrace:" + ex.StackTrace + Environment.NewLine + "InnerException:" + ex.InnerException);
                lstrResponse = "ApiHelper PurchaseGiftCard Ex: " + ex.Message + Environment.NewLine + "StackTrace:" + ex.StackTrace + Environment.NewLine + "InnerException:" + ex.InnerException;
            }
            return lstrResponse;
        }

        public static string RetrieveContent(string pstrRestaurantRetailer, string pstrContentKey)
        {
            string lstrResponse = string.Empty;
            try
            {
                //NameValueCollection lobjNVCHeader = new NameValueCollection();
                //lobjNVCHeader.Add("Authorization", string.Format("Bearer {0}", pstrAccesstoken));

                //NameValueCollection lobjNVCParameter = new NameValueCollection();
                //lobjNVCParameter.Add("retailer_content[where][0][]", "retailer");
                //lobjNVCParameter.Add("retailer_content[where][0][]", pstrRestaurantRetailer);
                //lobjNVCParameter.Add("retailer_content[where][1][]", "key");
                //lobjNVCParameter.Add("retailer_content[where][1][]", pstrContentKey);

                //string lstrPostData = ConstructQueryString(lobjNVCParameter);

                List<string> items = new List<string>();
                items.Add(string.Concat("retailer_content[where][0][]", "=", System.Web.HttpUtility.UrlPathEncode("retailer")));
                items.Add(string.Concat("retailer_content[where][0][]", "=", System.Web.HttpUtility.UrlPathEncode(pstrRestaurantRetailer)));
                items.Add(string.Concat("retailer_content[where][1][]", "=", System.Web.HttpUtility.UrlPathEncode("key")));
                items.Add(string.Concat("retailer_content[where][1][]", "=", System.Web.HttpUtility.UrlPathEncode(pstrContentKey)));

                string lstrPostData = string.Join("&", items.ToArray());
                

                lstrResponse = PostUrlEncodedRequest(URLConstants.RetrieveContent, HelperConstants.POST, null, lstrPostData);
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("ApiHelper RetrieveContent Ex: " + ex.Message + Environment.NewLine + "StackTrace:" + ex.StackTrace + Environment.NewLine + "InnerException:" + ex.InnerException);
                lstrResponse = "ApiHelper RetrieveContent Ex: " + ex.Message + Environment.NewLine + "StackTrace:" + ex.StackTrace + Environment.NewLine + "InnerException:" + ex.InnerException;
            }
            return lstrResponse;
        }

        /// <summary>
        /// UpdateProfile
        /// </summary>
        /// <param name="pstrAccesstoken">User token</param>
        /// <param name="pstrFirstname"></param>
        /// <param name="pstrLastname"></param>
        /// <param name="pstrCity"></param>
        /// <param name="pstrPhone"></param>
        /// <param name="pstrDOB">yyyy-mm-dd</param>
        /// <param name="pstrGender">male or female</param>
        /// <param name="pstrAddress"></param>
        /// <param name="pstrZip"></param>
        /// <param name="pstrLanguage">en</param>
        /// <param name="pstrNewsletter">bit value 1 or 0</param>
        /// <param name="pstrCountry">MY</param>
        /// <returns></returns>
        public static string UpdateProfile(string pstrAccesstoken, string pstrFirstname, string pstrLastname, string pstrCity, string pstrPhone, string pstrDOB, string pstrGender,
            string pstrAddress, string pstrZip, string pstrLanguage, string pstrNewsletter, string pstrCountry, string pstrState)
        {
            string lstrResponse = string.Empty;
            try
            {
                NameValueCollection lobjNVCHeader = new NameValueCollection();
                lobjNVCHeader.Add("Authorization", string.Format("Bearer {0}", pstrAccesstoken));

                NameValueCollection lobjNVCParameter = new NameValueCollection();
                lobjNVCParameter.Add("first_name", pstrFirstname);
                lobjNVCParameter.Add("last_name", pstrLastname);
                lobjNVCParameter.Add("phone", pstrPhone);
                lobjNVCParameter.Add("address", pstrAddress);
                lobjNVCParameter.Add("city", pstrCity);
                lobjNVCParameter.Add("zip", pstrZip);
                lobjNVCParameter.Add("country", pstrCountry);//MY
                if (!string.IsNullOrEmpty(pstrState))
                {
                    lobjNVCParameter.Add("state", pstrState);
                }
                if (!string.IsNullOrEmpty(pstrDOB))
                {
                    lobjNVCParameter.Add("birth_date", pstrDOB);
                }
                if (!string.IsNullOrEmpty(pstrGender))
                {
                    lobjNVCParameter.Add("gender", pstrGender);
                }
                
                if (!string.IsNullOrEmpty(pstrLanguage))
                {
                    lobjNVCParameter.Add("lang", pstrLanguage);//en
                }
                if (!string.IsNullOrEmpty(pstrNewsletter))
                {
                    lobjNVCParameter.Add("newsletter", pstrNewsletter);
                }

                
                //lobjNVCParameter.Add("undefined", string.Empty);

                string lstrPostData = ConstructQueryString(lobjNVCParameter);

                lstrResponse = PostUrlEncodedRequest(URLConstants.UpdateProfile, HelperConstants.PATCH, lobjNVCHeader, lstrPostData);
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("ApiHelper UpdateProfile Ex: " + ex.Message + Environment.NewLine + "StackTrace:" + ex.StackTrace + Environment.NewLine + "InnerException:" + ex.InnerException);
                lstrResponse = "ApiHelper UpdateProfile Ex: " + ex.Message + Environment.NewLine + "StackTrace:" + ex.StackTrace + Environment.NewLine + "InnerException:" + ex.InnerException;
            }
            return lstrResponse;
        }

        public static string GetAllCart(string pstrAccessToken, string pstrStatus)
        {
            string lstrResponse = string.Empty;
            try
            {
                NameValueCollection lobjNVCHeader = new NameValueCollection();
                lobjNVCHeader.Add("Authorization", string.Format("Bearer {0}", pstrAccessToken));

                string lstrUrl = string.Format(URLConstants.GetAllCart, pstrStatus);

                lstrResponse = PostUrlEncodedRequest(lstrUrl, HelperConstants.GET, lobjNVCHeader, string.Empty);
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("ApiHelper GetAllCart Ex: " + ex.Message + Environment.NewLine + "StackTrace:" + ex.StackTrace + Environment.NewLine + "InnerException:" + ex.InnerException);
                lstrResponse = "ApiHelper GetAllCart Ex: " + ex.Message + Environment.NewLine + "StackTrace:" + ex.StackTrace + Environment.NewLine + "InnerException:" + ex.InnerException;
            }
            return lstrResponse;
        }

        public static string NewCart(string pstrAccessToken)
        {
            string lstrResponse = string.Empty;
            try
            {
                NameValueCollection lobjNVCHeader = new NameValueCollection();
                lobjNVCHeader.Add("Authorization", string.Format("Bearer {0}", pstrAccessToken));

                lstrResponse = PostUrlEncodedRequest(URLConstants.NewCart, HelperConstants.GET, lobjNVCHeader, string.Empty);
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("ApiHelper NewCart Ex: " + ex.Message + Environment.NewLine + "StackTrace:" + ex.StackTrace + Environment.NewLine + "InnerException:" + ex.InnerException);
                lstrResponse = "ApiHelper NewCart Ex: " + ex.Message + Environment.NewLine + "StackTrace:" + ex.StackTrace + Environment.NewLine + "InnerException:" + ex.InnerException;
            }
            return lstrResponse;
        }

        /// <summary>
        /// Adding item to cart
        /// </summary>
        /// <param name="pstrAccesstoken">User token</param>
        /// <param name="pstrBuyableType">Type of item eg. product</param>
        /// <param name="pstrProductId"></param>
        /// <param name="pstrCCY"></param>
        /// <param name="pintQty"></param>
        /// <param name="pstrCartId">cart id</param>
        /// <returns></returns>
        public static string AddCartItem(string pstrAccesstoken, string pstrBuyableType, string pstrProductId, string pstrCCY, int pintQty, string pstrCartId)
        {
            string lstrResponse = string.Empty;
            try
            {
                NameValueCollection lobjNVCHeader = new NameValueCollection();
                lobjNVCHeader.Add("Authorization", string.Format("Bearer {0}", pstrAccesstoken));

                NameValueCollection lobjNVCParameter = new NameValueCollection();
                lobjNVCParameter.Add("buyable_type", pstrBuyableType);
                lobjNVCParameter.Add("buyable_id", pstrProductId);
                lobjNVCParameter.Add("ccy", pstrCCY);
                lobjNVCParameter.Add("qty", Convert.ToString(pintQty));
                lobjNVCParameter.Add("cart_id", pstrCartId);
                //lobjNVCParameter.Add("undefined", string.Empty);
                string lstrPostData = ConstructQueryString(lobjNVCParameter);

                lstrResponse = PostUrlEncodedRequest(URLConstants.AddCartItem, HelperConstants.POST, lobjNVCHeader, lstrPostData);
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("ApiHelper AddCartItem Ex: " + ex.Message + Environment.NewLine + "StackTrace:" + ex.StackTrace + Environment.NewLine + "InnerException:" + ex.InnerException);
                lstrResponse = "ApiHelper AddCartItem Ex: " + ex.Message + Environment.NewLine + "StackTrace:" + ex.StackTrace + Environment.NewLine + "InnerException:" + ex.InnerException;
            }
            return lstrResponse;
        }

        public static string CartCheckout(string pstrAccesstoken, string pstrCartId)
        {
            string lstrResponse = string.Empty;
            try
            {
                NameValueCollection lobjNVCHeader = new NameValueCollection();
                lobjNVCHeader.Add("Authorization", string.Format("Bearer {0}", pstrAccesstoken));

                lstrResponse = PostUrlEncodedRequest(string.Format(URLConstants.CartCheckout, pstrCartId), HelperConstants.GET, lobjNVCHeader, string.Empty);
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("ApiHelper CartCheckout Ex: " + ex.Message + Environment.NewLine + "StackTrace:" + ex.StackTrace + Environment.NewLine + "InnerException:" + ex.InnerException);
                lstrResponse = "ApiHelper CartCheckout Ex: " + ex.Message + Environment.NewLine + "StackTrace:" + ex.StackTrace + Environment.NewLine + "InnerException:" + ex.InnerException;
            }
            return lstrResponse;
        }

        public static string OrderDelivery(string pstrAccesstoken, string pstrOrderId, bool pstrCreateOnly, string pstrProvider)
        {
            string lstrResponse = string.Empty;
            try
            {
                NameValueCollection lobjNVCHeader = new NameValueCollection();
                lobjNVCHeader.Add("Authorization", string.Format("Bearer {0}", pstrAccesstoken));

                NameValueCollection lobjNVCParameter = new NameValueCollection();
                lobjNVCParameter.Add("provider", pstrProvider);
                string lstrPostData = ConstructQueryString(lobjNVCParameter);

                lstrResponse = PostUrlEncodedRequest(string.Format(URLConstants.OrderDelivery, pstrOrderId, pstrCreateOnly), HelperConstants.POST, lobjNVCHeader, lstrPostData);
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("ApiHelper CartCheckout Ex: " + ex.Message + Environment.NewLine + "StackTrace:" + ex.StackTrace + Environment.NewLine + "InnerException:" + ex.InnerException);
                lstrResponse = "ApiHelper CartCheckout Ex: " + ex.Message + Environment.NewLine + "StackTrace:" + ex.StackTrace + Environment.NewLine + "InnerException:" + ex.InnerException;
            }
            return lstrResponse;
        }

        public static string SetDeliveryProduct(string pstrAccesstoken, string pstrOrderItemId, string pstrProviderKey, string pstrName, string pstrAddress,
           string pstrCity, string pstrState, string pstrCountry, string pstrZip, string pstrPhone, string pstrDeliveryId)
        {
            string lstrResponse = string.Empty;
            try
            {
                NameValueCollection lobjNVCHeader = new NameValueCollection();
                lobjNVCHeader.Add("Authorization", string.Format("Bearer {0}", pstrAccesstoken));

                NameValueCollection lobjNVCParameter = new NameValueCollection();
                lobjNVCParameter.Add("provider_key", pstrProviderKey);
                lobjNVCParameter.Add("fields[name]", pstrName);
                lobjNVCParameter.Add("fields[address1]", pstrAddress);
                lobjNVCParameter.Add("fields[city]", pstrCity);
                lobjNVCParameter.Add("fields[state]", pstrState);
                lobjNVCParameter.Add("fields[zip]", pstrZip);
                lobjNVCParameter.Add("fields[country]", pstrCountry);
                lobjNVCParameter.Add("fields[phone]", pstrPhone);
                //lobjNVCParameter.Add("deliveryId", pstrDeliveryId);
                string lstrPostData = ConstructQueryString(lobjNVCParameter);

                lstrResponse = PostUrlEncodedRequest(string.Format(URLConstants.SetDeliveryProduct, pstrOrderItemId), HelperConstants.POST, lobjNVCHeader, lstrPostData);
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("ApiHelper SetDeliveryProduct Ex: " + ex.Message + Environment.NewLine + "StackTrace:" + ex.StackTrace + Environment.NewLine + "InnerException:" + ex.InnerException);
                lstrResponse = "ApiHelper SetDeliveryProduct Ex: " + ex.Message + Environment.NewLine + "StackTrace:" + ex.StackTrace + Environment.NewLine + "InnerException:" + ex.InnerException;
            }
            return lstrResponse;
        }

        public static string SetDeliveryGiftCard(string pstrAccesstoken, string pstrOrderItemId, string pstrProviderKey, string pstrName, string pstrAddress,
            string pstrCity, string pstrCountry, string pstrZip, string pstrPhone, string pstrDeliveryId)
        {
            string lstrResponse = string.Empty;
            try
            {
                NameValueCollection lobjNVCHeader = new NameValueCollection();
                lobjNVCHeader.Add("Authorization", string.Format("Bearer {0}", pstrAccesstoken));

                NameValueCollection lobjNVCParameter = new NameValueCollection();

                lobjNVCParameter.Add("provider_key", pstrProviderKey);
                lobjNVCParameter.Add("fields[name]", pstrName);
                lobjNVCParameter.Add("fields[address1]", pstrAddress);
                lobjNVCParameter.Add("fields[city]", pstrCity);
                lobjNVCParameter.Add("fields[country]", pstrCountry);
                lobjNVCParameter.Add("fields[zip]", pstrZip);
                lobjNVCParameter.Add("fields[phone]", pstrPhone);
                lobjNVCParameter.Add("deliveryId", pstrDeliveryId);
                string lstrPostData = ConstructQueryString(lobjNVCParameter);

                lstrResponse = PostUrlEncodedRequest(string.Format(URLConstants.SetDeliveryGiftCard, pstrOrderItemId), HelperConstants.POST, lobjNVCHeader, lstrPostData);
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("ApiHelper SetDeliveryGiftCard Ex: " + ex.Message + Environment.NewLine + "StackTrace:" + ex.StackTrace + Environment.NewLine + "InnerException:" + ex.InnerException);
                lstrResponse = "ApiHelper SetDeliveryGiftCard Ex: " + ex.Message + Environment.NewLine + "StackTrace:" + ex.StackTrace + Environment.NewLine + "InnerException:" + ex.InnerException;
            }
            return lstrResponse;
        }

        public static string CreatePayment(string pstrAccesstoken, string pstrOrderId, string pstrProvider)
        {
            string lstrResponse = string.Empty;
            try
            {
                NameValueCollection lobjlobjNVCHeader = new NameValueCollection();
                lobjlobjNVCHeader.Add("Authorization", string.Format("Bearer {0}", pstrAccesstoken));

                NameValueCollection lobjNVCParameter = new NameValueCollection();
                lobjNVCParameter.Add("provider", pstrProvider);
                string lstrPostData = ConstructQueryString(lobjNVCParameter);

                lstrResponse = PostUrlEncodedRequest(string.Format(URLConstants.CreatePayment, pstrOrderId), HelperConstants.POST, lobjlobjNVCHeader, lstrPostData);
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("ApiHelper CreatePayment Ex: " + ex.Message + Environment.NewLine + "StackTrace:" + ex.StackTrace + Environment.NewLine + "InnerException:" + ex.InnerException);
                lstrResponse = "ApiHelper CreatePayment Ex: " + ex.Message + Environment.NewLine + "StackTrace:" + ex.StackTrace + Environment.NewLine + "InnerException:" + ex.InnerException;
            }
            return lstrResponse;
        }

        public static string PaymentPay(string pstrAccesstoken, string pstrPaymentId, string pstrToken, string pstrBillingEmail)
        {
            string lstrResponse = string.Empty;
            try
            {
                NameValueCollection lobjlobjNVCHeader = new NameValueCollection();
                lobjlobjNVCHeader.Add("Authorization", string.Format("Bearer {0}", pstrAccesstoken));

                NameValueCollection lobjNVCParameter = new NameValueCollection();
                lobjNVCParameter.Add("token", pstrToken);
                lobjNVCParameter.Add("billing_email", pstrBillingEmail);
                string lstrPostData = ConstructQueryString(lobjNVCParameter);


                lstrResponse = PostUrlEncodedRequest(string.Format(URLConstants.PaymentPay, pstrPaymentId), HelperConstants.POST, lobjlobjNVCHeader, lstrPostData);
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("ApiHelper PaymentPay Ex: " + ex.Message + Environment.NewLine + "StackTrace:" + ex.StackTrace + Environment.NewLine + "InnerException:" + ex.InnerException);
                lstrResponse = "ApiHelper PaymentPay Ex: " + ex.Message + Environment.NewLine + "StackTrace:" + ex.StackTrace + Environment.NewLine + "InnerException:" + ex.InnerException;
            }
            return lstrResponse;
        }

        public static string GetAllOrders(string pstrAccessToken)
        {
            string lstrResponse = string.Empty;
            try
            {
                NameValueCollection lobjNVCHeader = new NameValueCollection();
                lobjNVCHeader.Add("Authorization", string.Format("Bearer {0}", pstrAccessToken));

                lstrResponse = PostUrlEncodedRequest(URLConstants.GetAllOrders, HelperConstants.GET, lobjNVCHeader, string.Empty);
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("ApiHelper GetAllOrders Ex: " + ex.Message + Environment.NewLine + "StackTrace:" + ex.StackTrace + Environment.NewLine + "InnerException:" + ex.InnerException);
                lstrResponse = "ApiHelper GetAllOrders Ex: " + ex.Message + Environment.NewLine + "StackTrace:" + ex.StackTrace + Environment.NewLine + "InnerException:" + ex.InnerException;
            }
            return lstrResponse;
        }

        public static string GetOrderDetails(string pstrAccessToken, string pstrOrderId)
        {
            string lstrResponse = string.Empty;
            try
            {
                NameValueCollection lobjNVCHeader = new NameValueCollection();
                lobjNVCHeader.Add("Authorization", string.Format("Bearer {0}", pstrAccessToken));

                lstrResponse = PostUrlEncodedRequest(string.Format(URLConstants.GetAllOrdersCopy, pstrOrderId), HelperConstants.GET, lobjNVCHeader, string.Empty);
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("ApiHelper GetAllOrdersCopy Ex: " + ex.Message + Environment.NewLine + "StackTrace:" + ex.StackTrace + Environment.NewLine + "InnerException:" + ex.InnerException);
                lstrResponse = "ApiHelper GetAllOrdersCopy Ex: " + ex.Message + Environment.NewLine + "StackTrace:" + ex.StackTrace + Environment.NewLine + "InnerException:" + ex.InnerException;
            }
            return lstrResponse;
        }

        //public static string GetRestaurantLocations(string pstrRestaurantRetailerId)
        //{
        //    string lstrResponse = string.Empty;
        //    try
        //    {
        //        //NameValueCollection lobjNVCParameter = new NameValueCollection();
        //        //lobjNVCParameter.Add("retailer_location[where][0][]", "retailer_id");
        //        //lobjNVCParameter.Add("retailer_location[where][0][]", pstrRestaurantRetailerId);

        //        //string lstrPostData = ConstructQueryString(lobjNVCParameter);

        //        List<string> items = new List<string>();
        //        items.Add(string.Concat("retailer_location[where][0][]", "=", System.Web.HttpUtility.UrlPathEncode("retailer_id")));
        //        items.Add(string.Concat("retailer_location[where][0][]", "=", System.Web.HttpUtility.UrlPathEncode(pstrRestaurantRetailerId)));

        //        string lstrPostData = string.Join("&", items.ToArray());

        //        lstrResponse = PostUrlEncodedRequest(URLConstants.GetRestaurantLocations, HelperConstants.POST, null, lstrPostData);
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggingAdapter.WriteLog("ApiHelper GetRestaurantLocations Ex: " + ex.Message + Environment.NewLine + "StackTrace:" + ex.StackTrace + Environment.NewLine + "InnerException:" + ex.InnerException);
        //        lstrResponse = "ApiHelper GetRestaurantLocations Ex: " + ex.Message + Environment.NewLine + "StackTrace:" + ex.StackTrace + Environment.NewLine + "InnerException:" + ex.InnerException;
        //    }
        //    return lstrResponse;
        //}

        public static string GetRestaurantLocations(string pstrRestaurantRetailerId, string pstrLatitude, string pstrLongitude, int pintPageNo, int pintRecordsPerPage)
        {
            string lstrResponse = string.Empty;
            string lstrDictionaryUrl = string.Empty;
            try
            {
                string lstrUrl = string.Format(URLConstants.GetRestaurantLocations,pstrLatitude,pstrLongitude,pstrRestaurantRetailerId,pintPageNo, pintRecordsPerPage);
                lstrResponse = PostUrlEncodedRequest(lstrUrl, HelperConstants.GET, null, string.Empty);
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("ApiHelper GetRestaurantLocations Ex: " + ex.Message + Environment.NewLine + "StackTrace:" + ex.StackTrace + Environment.NewLine + "InnerException:" + ex.InnerException);
                lstrResponse = "ApiHelper GetRestaurantLocations Ex: " + ex.Message + Environment.NewLine + "StackTrace:" + ex.StackTrace + Environment.NewLine + "InnerException:" + ex.InnerException;
            }

            return lstrResponse;
        }

        public static string GetOffers(string pstrAccessToken)
        {
            string lstrResponse = string.Empty;
            try
            {
                NameValueCollection lobjNVCHeader = new NameValueCollection();
                lobjNVCHeader.Add("Authorization", string.Format("Bearer {0}", pstrAccessToken));

                List<string> items = new List<string>();
                items.Add(string.Concat("retailer_location_setting[where][0][]", "=", System.Web.HttpUtility.UrlPathEncode("name")));
                items.Add(string.Concat("retailer_location_setting[where][0][]", "=", System.Web.HttpUtility.UrlPathEncode("visa_halal_deal")));

                string lstrPostData = string.Join("&", items.ToArray());

                lstrResponse = PostUrlEncodedRequest(URLConstants.LocationSettings, HelperConstants.POST, lobjNVCHeader, lstrPostData);


            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("ApiHelper GetOffers Ex: " + ex.Message + Environment.NewLine + "StackTrace:" + ex.StackTrace + Environment.NewLine + "InnerException:" + ex.InnerException);
                lstrResponse = "ApiHelper GetOffers Ex: " + ex.Message + Environment.NewLine + "StackTrace:" + ex.StackTrace + Environment.NewLine + "InnerException:" + ex.InnerException;
            }
            return lstrResponse;
        }

        //public static string GetMosqueLocations(string pstrMosqueRetailerId)
        //{
        //    string lstrResponse = string.Empty;
        //    try
        //    {
        //        //NameValueCollection lobjNVCParameter = new NameValueCollection();
        //        //lobjNVCParameter.Add("retailer_location[where][0][]", "retailer_id");
        //        //lobjNVCParameter.Add("retailer_location[where][0][]", pstrMosqueRetailerId);

        //        //string lstrPostData = ConstructQueryString(lobjNVCParameter);

        //        List<string> items = new List<string>();
        //        items.Add(string.Concat("retailer_location[where][0][]", "=", System.Web.HttpUtility.UrlPathEncode("retailer_id")));
        //        items.Add(string.Concat("retailer_location[where][0][]", "=", System.Web.HttpUtility.UrlPathEncode(pstrMosqueRetailerId)));

        //        string lstrPostData = string.Join("&", items.ToArray());

        //        lstrResponse = PostUrlEncodedRequest(URLConstants.GetMosqueLocations, HelperConstants.POST, null, lstrPostData);
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggingAdapter.WriteLog("ApiHelper GetMosqueLocations Ex: " + ex.Message + Environment.NewLine + "StackTrace:" + ex.StackTrace + Environment.NewLine + "InnerException:" + ex.InnerException);
        //        lstrResponse = "ApiHelper GetMosqueLocations Ex: " + ex.Message + Environment.NewLine + "StackTrace:" + ex.StackTrace + Environment.NewLine + "InnerException:" + ex.InnerException;
        //    }
        //    return lstrResponse;
        //}

        public static string GetMosqueLocations(string pstrMosqueRetailerId, string pstrLatitude, string pstrLongitude, int pintPageNo, int pintRecordsPerPage)
        {
            string lstrResponse = string.Empty;
            string lstrDictionaryUrl = string.Empty;
            try
            {
                string lstrUrl = string.Format(URLConstants.GetMosqueLocations, pstrLatitude, pstrLongitude, pstrMosqueRetailerId, pintPageNo, pintRecordsPerPage);
                lstrResponse = PostUrlEncodedRequest(lstrUrl, HelperConstants.GET, null, string.Empty);
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("ApiHelper GetMosqueLocations Ex: " + ex.Message + Environment.NewLine + "StackTrace:" + ex.StackTrace + Environment.NewLine + "InnerException:" + ex.InnerException);
                lstrResponse = "ApiHelper GetMosqueLocations Ex: " + ex.Message + Environment.NewLine + "StackTrace:" + ex.StackTrace + Environment.NewLine + "InnerException:" + ex.InnerException;
            }

            return lstrResponse;
        }

        public static string RemoveCartItem(string pstrAccesstoken, string pstrCartItemId)
        {
            string lstrResponse = string.Empty;
            try
            {
                NameValueCollection lobjNVCHeader = new NameValueCollection();
                lobjNVCHeader.Add("Authorization", string.Format("Bearer {0}", pstrAccesstoken));

                lstrResponse = PostUrlEncodedRequest(string.Format(URLConstants.RemoveCartItem, pstrCartItemId), HelperConstants.DELETE, lobjNVCHeader, string.Empty);
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("ApiHelper RemoveCartItem Ex: " + ex.Message + Environment.NewLine + "StackTrace:" + ex.StackTrace + Environment.NewLine + "InnerException:" + ex.InnerException);
                lstrResponse = "ApiHelper RemoveCartItem Ex: " + ex.Message + Environment.NewLine + "StackTrace:" + ex.StackTrace + Environment.NewLine + "InnerException:" + ex.InnerException;
            }
            return lstrResponse;
        }

        public static string GetDeliveryProvider(string pstrAccesstoken, string pstrCartOrderItemId)
        {
            string lstrResponse = string.Empty;
            try
            {
                NameValueCollection lobjNVCHeader = new NameValueCollection();
                lobjNVCHeader.Add("Authorization", string.Format("Bearer {0}", pstrAccesstoken));

                lstrResponse = PostUrlEncodedRequest(string.Format(URLConstants.GetDeliveryProvider, pstrCartOrderItemId), HelperConstants.GET, lobjNVCHeader, string.Empty);
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("ApiHelper GetDeliveryProvider Ex: " + ex.Message + Environment.NewLine + "StackTrace:" + ex.StackTrace + Environment.NewLine + "InnerException:" + ex.InnerException);
                lstrResponse = "ApiHelper GetDeliveryProvider Ex: " + ex.Message + Environment.NewLine + "StackTrace:" + ex.StackTrace + Environment.NewLine + "InnerException:" + ex.InnerException;
            }
            return lstrResponse;
        }

        public static string GetPaymentProvider(string pstrAccesstoken, string pstrOrderId)
        {
            string lstrResponse = string.Empty;
            try
            {
                NameValueCollection lobjNVCHeader = new NameValueCollection();
                lobjNVCHeader.Add("Authorization", string.Format("Bearer {0}", pstrAccesstoken));

                lstrResponse = PostUrlEncodedRequest(string.Format(URLConstants.GetPaymentProvider, pstrOrderId), HelperConstants.GET, lobjNVCHeader, string.Empty);
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("ApiHelper GetPaymentProvider Ex: " + ex.Message + Environment.NewLine + "StackTrace:" + ex.StackTrace + Environment.NewLine + "InnerException:" + ex.InnerException);
                lstrResponse = "ApiHelper GetPaymentProvider Ex: " + ex.Message + Environment.NewLine + "StackTrace:" + ex.StackTrace + Environment.NewLine + "InnerException:" + ex.InnerException;
            }
            return lstrResponse;
        }

        public static string GetCartDetails(string pstrAccesstoken, string pstrCartId)
        {
            string lstrResponse = string.Empty;
            try
            {
                NameValueCollection lobjNVCHeader = new NameValueCollection();
                lobjNVCHeader.Add("Authorization", string.Format("Bearer {0}", pstrAccesstoken));

                lstrResponse = PostUrlEncodedRequest(string.Format(URLConstants.CartDetails, pstrCartId), HelperConstants.GET, lobjNVCHeader, string.Empty);
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("ApiHelper GetCartDetails Ex: " + ex.Message + Environment.NewLine + "StackTrace:" + ex.StackTrace + Environment.NewLine + "InnerException:" + ex.InnerException);
                lstrResponse = "ApiHelper GetCartDetails Ex: " + ex.Message + Environment.NewLine + "StackTrace:" + ex.StackTrace + Environment.NewLine + "InnerException:" + ex.InnerException;
            }
            return lstrResponse;
        }

        public static string GetLocationSettings(string pstrAccesstoken, string pstrLocationId)
        {
            string lstrResponse = string.Empty;
            try
            {
                NameValueCollection lobjNVCHeader = new NameValueCollection();
                lobjNVCHeader.Add("Authorization", string.Format("Bearer {0}", pstrAccesstoken));

                //NameValueCollection lobjNVCParameter = new NameValueCollection();
                ////lobjNVCParameter.Add("retailer_location_setting[where][name]", pstrKey);
                //lobjNVCParameter.Add("retailer_location_setting[where][location]", pstrLocation);
                //string lstrPostData = ConstructQueryString(lobjNVCParameter);

                List<string> items = new List<string>();
                items.Add(string.Concat("retailer_location_setting[where][0][]", "=", System.Web.HttpUtility.UrlPathEncode("location")));
                items.Add(string.Concat("retailer_location_setting[where][0][]", "=", System.Web.HttpUtility.UrlPathEncode(pstrLocationId)));

                string lstrPostData = string.Join("&", items.ToArray());

                lstrResponse = PostUrlEncodedRequest(URLConstants.LocationSettings, HelperConstants.POST, lobjNVCHeader, lstrPostData);
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("ApiHelper GetLocationSettings Ex: " + ex.Message + Environment.NewLine + "StackTrace:" + ex.StackTrace + Environment.NewLine + "InnerException:" + ex.InnerException);
                lstrResponse = "ApiHelper GetLocationSettings Ex: " + ex.Message + Environment.NewLine + "StackTrace:" + ex.StackTrace + Environment.NewLine + "InnerException:" + ex.InnerException;
            }
            return lstrResponse;
        }

        public static string GetMultipleLocationSettings(string pstrAccesstoken, List<string> pobjLocationIds)
        {
            string lstrResponse = string.Empty;
            try
            {
                NameValueCollection lobjNVCHeader = new NameValueCollection();
                lobjNVCHeader.Add("Authorization", string.Format("Bearer {0}", pstrAccesstoken));

                List<string> items = new List<string>();
                items.Add(string.Concat("retailer_location_setting[where][0][0]", "=", System.Web.HttpUtility.UrlPathEncode("location")));
                items.Add(string.Concat("retailer_location_setting[where][0][1]", "=", System.Web.HttpUtility.UrlPathEncode("in")));

                for (int i = 0; i < pobjLocationIds.Count; i++)
                {
                    items.Add(string.Concat("retailer_location_setting[where][0][2][", i, "]=", System.Web.HttpUtility.UrlPathEncode(pobjLocationIds[i])));
                }

                string lstrPostData = string.Join("&", items.ToArray());

                lstrResponse = PostUrlEncodedRequest(URLConstants.LocationSettings, HelperConstants.POST, lobjNVCHeader, lstrPostData);
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("ApiHelper GetMultipleLocationSettings Ex: " + ex.Message + Environment.NewLine + "StackTrace:" + ex.StackTrace + Environment.NewLine + "InnerException:" + ex.InnerException);
                lstrResponse = "ApiHelper GetMultipleLocationSettings Ex: " + ex.Message + Environment.NewLine + "StackTrace:" + ex.StackTrace + Environment.NewLine + "InnerException:" + ex.InnerException;
            }
            return lstrResponse;
        }

        public static string ChangePassword(string pstrAccesstoken, string pstrCurrentPassword, string pstrNewPassword, string pstrConfirmPassword)
        {
            string lstrResponse = string.Empty;
            try
            {
                NameValueCollection lobjNVCHeader = new NameValueCollection();
                lobjNVCHeader.Add("Authorization", string.Format("Bearer {0}", pstrAccesstoken));

                NameValueCollection lobjNVCParameter = new NameValueCollection();
                lobjNVCParameter.Add("current_password", pstrCurrentPassword);
                lobjNVCParameter.Add("confirm_password", pstrConfirmPassword);
                lobjNVCParameter.Add("new_password2", pstrNewPassword);

                string lstrPostData = ConstructQueryString(lobjNVCParameter);

                lstrResponse = PostUrlEncodedRequest(URLConstants.ChangePassword, HelperConstants.POST, lobjNVCHeader, lstrPostData);
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("ApiHelper ChangePassword Ex: " + ex.Message + Environment.NewLine + "StackTrace:" + ex.StackTrace + Environment.NewLine + "InnerException:" + ex.InnerException);
                lstrResponse = "ApiHelper ChangePassword Ex: " + ex.Message + Environment.NewLine + "StackTrace:" + ex.StackTrace + Environment.NewLine + "InnerException:" + ex.InnerException;
            }
            return lstrResponse;
        }

        public static string GetUserProfile(string pstrAccesstoken)
        {
            string lstrResponse = string.Empty;
            try
            {
                NameValueCollection lobjNVCHeader = new NameValueCollection();
                lobjNVCHeader.Add("Authorization", string.Format("Bearer {0}", pstrAccesstoken));

                lstrResponse = PostUrlEncodedRequest(URLConstants.GetUserProfile, HelperConstants.GET, lobjNVCHeader, string.Empty);
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("ApiHelper GetUserProfile Ex: " + ex.Message + Environment.NewLine + "StackTrace:" + ex.StackTrace + Environment.NewLine + "InnerException:" + ex.InnerException);
                lstrResponse = "ApiHelper GetUserProfile Ex: " + ex.Message + Environment.NewLine + "StackTrace:" + ex.StackTrace + Environment.NewLine + "InnerException:" + ex.InnerException;
            }
            return lstrResponse;
        }

        public static string GetCategories(string pstrType, string pstrBuyingWith)
        {
            string lstrResponse = string.Empty;
            try
            {
                //NameValueCollection lobjNVCHeader = new NameValueCollection();
                //lobjNVCHeader.Add("Authorization", string.Format("Bearer {0}", pstrType));

                lstrResponse = PostUrlEncodedRequest(string.Format(URLConstants.GetCategories, pstrType, URLConstants.ClientId, pstrBuyingWith), HelperConstants.GET, null, string.Empty);
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("ApiHelper GetCategories Ex: " + ex.Message + Environment.NewLine + "StackTrace:" + ex.StackTrace + Environment.NewLine + "InnerException:" + ex.InnerException);
                lstrResponse = "ApiHelper GetCategories Ex: " + ex.Message + Environment.NewLine + "StackTrace:" + ex.StackTrace + Environment.NewLine + "InnerException:" + ex.InnerException;
            }
            return lstrResponse;
        }

        public static string GetMetas(string pstrType, string pstrCategory)
        {
            string lstrResponse = string.Empty;
            try
            {
                //NameValueCollection lobjNVCHeader = new NameValueCollection();
                //lobjNVCHeader.Add("Authorization", string.Format("Bearer {0}", pstrCategory));

                lstrResponse = PostUrlEncodedRequest(string.Format(URLConstants.GetMetas, pstrCategory, pstrType, URLConstants.ClientId), HelperConstants.GET, null, string.Empty);
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("ApiHelper GetMetas Ex: " + ex.Message + Environment.NewLine + "StackTrace:" + ex.StackTrace + Environment.NewLine + "InnerException:" + ex.InnerException);
                lstrResponse = "ApiHelper GetMetas Ex: " + ex.Message + Environment.NewLine + "StackTrace:" + ex.StackTrace + Environment.NewLine + "InnerException:" + ex.InnerException;
            }
            return lstrResponse;
        }

        public static string GetMetas(Dictionary<string, string> pstrDictionary)
        {
            string lstrResponse = string.Empty;
            string lstrDictionaryUrl = string.Empty;
            try
            {
                foreach (var item in pstrDictionary)
                {
                    lstrDictionaryUrl += "&" + item.Key + "=" + item.Value;
                }

                string lstrUrl = string.Format(URLConstants.GetMetas, URLConstants.ClientId, HttpUtility.UrlPathEncode(lstrDictionaryUrl));
                lstrResponse = PostUrlEncodedRequest(lstrUrl, HelperConstants.GET, null, string.Empty);
                //lstrResponse = "[{\"name\":\"Color\",\"options\":[\"Red\",\"Green\",\"Yellow\"]},{\"name\":\"Size\",\"options\":[\"S\",\"M\",\"L\"]}]";
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("ApiHelper GetMetas Ex: " + ex.Message + Environment.NewLine + "StackTrace:" + ex.StackTrace + Environment.NewLine + "InnerException:" + ex.InnerException);
                lstrResponse = "ApiHelper GetMetas Ex: " + ex.Message + Environment.NewLine + "StackTrace:" + ex.StackTrace + Environment.NewLine + "InnerException:" + ex.InnerException;
            }

            return lstrResponse;
        }

        public static string SetDeliveryEmail(string pstrAccesstoken, string pstrOrderItemId, string pstrProviderKey, string pstrName, string pstrEmailId, string pstrSenderEmail)
        {
            string lstrResponse = string.Empty;
            try
            {
                NameValueCollection lobjNVCHeader = new NameValueCollection();
                lobjNVCHeader.Add("Authorization", string.Format("Bearer {0}", pstrAccesstoken));

                NameValueCollection lobjNVCParameter = new NameValueCollection();

                lobjNVCParameter.Add("provider_key", pstrProviderKey);
                lobjNVCParameter.Add("fields[from]", URLConstants.Email_From);
                lobjNVCParameter.Add("fields[s_email]", pstrSenderEmail);
                lobjNVCParameter.Add("fields[name]", pstrName);
                lobjNVCParameter.Add("fields[r_email]", pstrEmailId);
                lobjNVCParameter.Add("fields[message]", "Here are your orders.");
                string lstrPostData = ConstructQueryString(lobjNVCParameter);

                lstrResponse = PostUrlEncodedRequest(string.Format(URLConstants.SetDeliveryEmail, pstrOrderItemId), HelperConstants.POST, lobjNVCHeader, lstrPostData);
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("ApiHelper SetDeliveryGiftCard Ex: " + ex.Message + Environment.NewLine + "StackTrace:" + ex.StackTrace + Environment.NewLine + "InnerException:" + ex.InnerException);
                lstrResponse = "ApiHelper SetDeliveryGiftCard Ex: " + ex.Message + Environment.NewLine + "StackTrace:" + ex.StackTrace + Environment.NewLine + "InnerException:" + ex.InnerException;
            }
            return lstrResponse;
        }


        #region Common Methods

        public static string ConstructQueryString(NameValueCollection parameters)
        {
            List<string> items = new List<string>();

            foreach (string name in parameters)
                items.Add(string.Concat(name, "=", System.Web.HttpUtility.UrlPathEncode(parameters[name])));

            return string.Join("&", items.ToArray());
        }

        public static string PostUrlEncodedRequest(string pstrURL, string pstrMethod, NameValueCollection namevalueCollection, string pstrPostData)
        {
            string lstrResponse = string.Empty;
            HttpWebResponse lobjWebResponse = null;
            try
            {
                HttpWebRequest lobjWebRequest = (HttpWebRequest)WebRequest.Create(pstrURL);

                lobjWebRequest.KeepAlive = false;
                lobjWebRequest.Method = pstrMethod;
                lobjWebRequest.Accept = "application/json";
                lobjWebRequest.ContentType = "application/x-www-form-urlencoded";
                lobjWebRequest.Headers.Add("cache-control", "no-cache");
                lobjWebRequest.Timeout = 120000;

                if (namevalueCollection != null && namevalueCollection.Count > 0)
                {
                    lobjWebRequest.Headers.Add(namevalueCollection);
                }

                LoggingAdapter.WriteLog("PostUrlEncodedRequest URL:" + pstrURL + Environment.NewLine + "POST Data :" + pstrPostData);

                if (pstrMethod.Equals(HelperConstants.POST) || pstrMethod.Equals(HelperConstants.PATCH))
                {
                    byte[] byteArray = Encoding.UTF8.GetBytes(pstrPostData);
                    lobjWebRequest.ContentLength = byteArray.Length;
                    Stream dataStream = lobjWebRequest.GetRequestStream();
                    dataStream.Write(byteArray, 0, byteArray.Length);
                    dataStream.Close();
                }

                //if (pstrMethod.Equals(HelperConstants.POST))
                //{
                //    StreamWriter writer = new StreamWriter(lobjWebRequest.GetRequestStream());
                //    writer.WriteLine(pstrPostData);
                //    writer.Close();
                //}
                //else if (pstrMethod.Equals(HelperConstants.PATCH))
                //{
                //    byte[] byteArray = Encoding.UTF8.GetBytes(pstrPostData);
                //    lobjWebRequest.ContentLength = byteArray.Length;
                //    Stream dataStream = lobjWebRequest.GetRequestStream();
                //    dataStream.Write(byteArray, 0, byteArray.Length);
                //    dataStream.Close();
                //}

                lobjWebResponse = (HttpWebResponse)lobjWebRequest.GetResponse();
                lobjWebRequest.ServicePoint.CloseConnectionGroup(lobjWebRequest.ConnectionGroupName);

                if (lobjWebResponse.StatusCode == HttpStatusCode.OK || lobjWebResponse.StatusCode == HttpStatusCode.Created)
                {
                    lstrResponse = ReadStream(lobjWebResponse);
                }
                else if (lobjWebResponse.StatusCode == HttpStatusCode.Unauthorized)
                {
                    throw new ApplicationException("INVALID_CREDENTIALS");
                }

            }
            catch (WebException wex)
            {
                LoggingAdapter.WriteLog("PostUrlEncodedRequest WebEx: " + wex.Message + Environment.NewLine + wex.StackTrace + Environment.NewLine + wex.InnerException);
                lstrResponse = ReadStream(wex.Response as HttpWebResponse);
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("PostUrlEncodedRequest Ex: " + ex.Message + Environment.NewLine + ex.StackTrace + Environment.NewLine + ex.InnerException);
                lstrResponse = "PostUrlEncodedRequest Ex: " + ex.Message + Environment.NewLine + ex.StackTrace + Environment.NewLine + ex.InnerException;
            }
            finally
            {
                if (lobjWebResponse != null)
                {
                    lobjWebResponse.Close();
                }
            }
            LoggingAdapter.WriteLog("PostUrlEncodedRequest Response:" + lstrResponse);
            return lstrResponse;
        }

        public static T PostUrlEncodedRequest<T>(string pstrURL, string pstrMethod, NameValueCollection namevalueCollection, string pstrPostData)
        {
            string lstrResponse = string.Empty;
            T result = default(T);
            HttpWebResponse lobjWebResponse = null;
            try
            {
                HttpWebRequest lobjWebRequest = (HttpWebRequest)WebRequest.Create(pstrURL);
                lobjWebRequest.KeepAlive = false;
                lobjWebRequest.Method = pstrMethod;
                lobjWebRequest.Accept = "application/json";
                lobjWebRequest.ContentType = "application/x-www-form-urlencoded";
                lobjWebRequest.Timeout = 120000;

                if (namevalueCollection != null && namevalueCollection.Count > 0)
                {
                    lobjWebRequest.Headers.Add(namevalueCollection);
                }

                LoggingAdapter.WriteLog("PostUrlEncodedRequest URL:" + pstrURL + Environment.NewLine + "POST Data :" + pstrPostData);

                if (pstrMethod.Equals(HelperConstants.POST) || pstrMethod.Equals(HelperConstants.PATCH))
                {
                    byte[] byteArray = Encoding.UTF8.GetBytes(pstrPostData);
                    lobjWebRequest.ContentLength = byteArray.Length;
                    Stream dataStream = lobjWebRequest.GetRequestStream();
                    dataStream.Write(byteArray, 0, byteArray.Length);
                    dataStream.Close();
                }

                //if (pstrMethod.Equals("POST"))
                //{
                //    StreamWriter writer = new StreamWriter(lobjWebRequest.GetRequestStream());
                //    writer.WriteLine(pstrPostData);
                //    writer.Close();
                //}

                lobjWebResponse = (HttpWebResponse)lobjWebRequest.GetResponse();
                lobjWebRequest.ServicePoint.CloseConnectionGroup(lobjWebRequest.ConnectionGroupName);

                if (lobjWebResponse.StatusCode == HttpStatusCode.OK || lobjWebResponse.StatusCode == HttpStatusCode.Created)
                {
                    lstrResponse = ReadStream(lobjWebResponse);
                    result = JSONSerialization.Deserialize<T>(lstrResponse);
                    LoggingAdapter.WriteLog("PostUrlEncodedRequest Response:" + lstrResponse);
                }
                else if (lobjWebResponse.StatusCode == HttpStatusCode.Unauthorized)
                {
                    throw new ApplicationException("INVALID_CREDENTIALS");
                }

            }
            catch (WebException wex)
            {
                LoggingAdapter.WriteLog("PostUrlEncodedRequest WebEx: " + wex.Message + Environment.NewLine + wex.StackTrace + Environment.NewLine + wex.InnerException);
                lstrResponse = ReadStream(wex.Response as HttpWebResponse);
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("PostUrlEncodedRequest Ex: " + ex.Message + Environment.NewLine + ex.StackTrace + Environment.NewLine + ex.InnerException);
            }
            finally
            {
                if (lobjWebResponse != null)
                {
                    lobjWebResponse.Close();
                }
            }

            return result;
        }

        private static string ReadStream(HttpWebResponse response)
        {
            string lstrReturnString = string.Empty;
            StreamReader lobjStreamReader = null;
            try
            {
                lobjStreamReader = new StreamReader(response.GetResponseStream());
                lstrReturnString = lobjStreamReader.ReadToEnd();
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("ReadStream Exception: " + ex.Message + Environment.NewLine + ex.StackTrace + Environment.NewLine + ex.InnerException);
            }
            finally
            {
                lobjStreamReader.Dispose();
            }
            return lstrReturnString;
        }

        public static string GuestToken()
        {
            string lstrResponse = string.Empty;
            try
            {
                NameValueCollection lobjNVCParameter = new NameValueCollection();
                lobjNVCParameter.Add("client_id", URLConstants.ClientId);
                lobjNVCParameter.Add("client_secret", URLConstants.ClientSecret);
                lobjNVCParameter.Add("grant_type", URLConstants.Grant_Guest);
                lobjNVCParameter.Add("scope", URLConstants.Scope_Guest);
                string lstrPostData = ConstructQueryString(lobjNVCParameter);
                lstrResponse = PostUrlEncodedRequest(URLConstants.SignIn, HelperConstants.POST, null, lstrPostData);

            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("GuestToken SignIn Ex: " + ex.Message + Environment.NewLine + "StackTrace:" + ex.StackTrace + Environment.NewLine + "InnerException:" + ex.InnerException);
                lstrResponse = "ApiHelper GuestToken Ex: " + ex.Message + Environment.NewLine + "StackTrace:" + ex.StackTrace + Environment.NewLine + "InnerException:" + ex.InnerException;
            }
            return lstrResponse;
        }

        #endregion
    }
}
