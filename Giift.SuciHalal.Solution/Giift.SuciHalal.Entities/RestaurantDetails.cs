﻿namespace Giift.SuciHalal.Entities
{
    public class RestaurantDetails
    {
        public string Id { get; set; } = string.Empty;
        public string Name { get; set; } = string.Empty;
        public string Address { get; set; } = string.Empty;
        public string ImageUrl { get; set; } = "images/restaurant.jpg";
        public double Latitude { get; set; } = 0.0F;
        public double Longitude { get; set; } = 0.0F;
        public double Distance { get; set; } = 0.0F;
        public string Website { get; set; } = string.Empty;
        public string Locality { get; set; } = string.Empty;
        public string PhoneNo { get; set; } = string.Empty;
        public string Deal { get; set; } = string.Empty;
        public string DealTeaser { get; set; } = string.Empty;
        public string DealMore { get; set; } = string.Empty;
        public string Description { get; set; } = string.Empty;
        public string Cuisine { get; set; } = string.Empty;
        public string AveragePrice { get; set; } = string.Empty;
        public string HalalType { get; set; } = string.Empty;
    }
}