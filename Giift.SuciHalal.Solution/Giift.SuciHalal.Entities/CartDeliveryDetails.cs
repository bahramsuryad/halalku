﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Giift.SuciHalal.Entities
{
    public class CartDeliveryDetails : CartBaseObject
    {
        public string Provider { get; set; } = string.Empty;
        public string OrderId { get; set; } = string.Empty;
        public string CartOrder { get; set; } = string.Empty;
        public List<OrderItemDetails> CartOrderItems { get; set; }
        //public CartDeliveryInfo Infos { get; set; }
    }
}
