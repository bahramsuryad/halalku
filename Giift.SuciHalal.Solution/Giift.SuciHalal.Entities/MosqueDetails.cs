﻿namespace Giift.SuciHalal.Entities
{
    public class MosqueDetails
    {
        public string Id { get; set; } = string.Empty;
        public string Name { get; set; } = string.Empty;
        public string Address { get; set; } = string.Empty;
        public string ImageUrl { get; set; } = "images/mosque.jpg";
        public double Latitude { get; set; } = 0.0F;
        public double Longitude { get; set; } = 0.0F;
        public double Distance { get; set; } = 0.0F;
        public string PhoneNo { get; set; } = string.Empty;
        public string Description { get; set; } = string.Empty;
        public string ServiceLanguage { get; set; } = string.Empty;
        public string QuickFacts { get; set; } = string.Empty;
    }
}