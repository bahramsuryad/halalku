﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Giift.SuciHalal.Entities
{
    public class CartDetails : CartBaseObject
    {
        public List<CartItemDetails> CartItems { get; set; }
        public double value { get; set; } = 0;
        public double shipping_fee { get; set; } = 0;
        public string UserId { get; set; } = string.Empty;
        public string Currency { get; set; } = string.Empty;
    }
}
