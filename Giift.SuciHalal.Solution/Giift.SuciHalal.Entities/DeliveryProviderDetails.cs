﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Giift.SuciHalal.Entities
{
    public class DeliveryProviderDetails
    {
        public string Id { get; set; } = string.Empty;
        public string Status { get; set; } = string.Empty;
        public string CartId { get; set; } = string.Empty;
        public string Currency { get; set; } = string.Empty;
        public DateTime CreatedAt { get; set; } = DateTime.MinValue.ToUniversalTime();
        public DateTime UpdatedAt { get; set; } = DateTime.MinValue.ToUniversalTime();
        public CartDetails Cart { get; set; }
        public List<OrderItemDetails> CartOrderItems { get; set; }
        public List<CartDeliveryDetails> CartDeliveries { get; set; }         
        public string ResponseMessage { get; set; } = string.Empty;
    }
}
