﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Giift.SuciHalal.Entities
{
    public class BuyableDetail
    {
        public string underlying_id { get; set; }

        public string type { get; set; }

        public string name { get; set; }

        public List<string> imgs { get; set; }

        public List<UnitDetail> units { get; set; }

        public object bonus { get; set; }

        public object fees { get; set; }

        public object tc { get; set; }

        public object eta { get; set; }

        public string description { get; set; }

        public List<string> categories { get; set; }

        public List<CategoriesTree> categories_tree { get; set; }

        public string country { get; set; }

        public object metas { get; set; }
    }
}
