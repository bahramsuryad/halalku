﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Giift.SuciHalal.Entities
{
    public class OrderBaseObject
    {
        public string Id { get; set; } = string.Empty;
        public string Status { get; set; } = string.Empty;
        public double Value { get; set; } = 0;
        public DateTime CreatedAt { get; set; } = DateTime.MinValue.ToUniversalTime();
        public DateTime UpdatedAt { get; set; } = DateTime.MinValue.ToUniversalTime();
        public string ResponseMessage { get; set; } = string.Empty;
    }
}
