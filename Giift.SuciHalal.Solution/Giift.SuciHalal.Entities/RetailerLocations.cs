﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Giift.SuciHalal.Entities
{
    public class RetailerLocations
    {
        public Metadata metadata { get; set; }
        public List<Data> data { get; set; }
    }

    public class Metadata
    {
        public int items { get; set; }
        public int page { get; set; }
        public int per_page { get; set; }
        public int total_pages { get; set; }
    }

    public class Data
    {
        public string id { get; set; }
        public string retailer_id { get; set; }
        public string name { get; set; }
        public string address { get; set; }
        public string phone { get; set; }
        public double longitude { get; set; }
        public double latitude { get; set; }
        public string created_by { get; set; }
        //public int created_at { get; set; }
        //public int updated_at { get; set; }
        public List<Setting> settings { get; set; }
    }

    public class Setting
    {
        public string id { get; set; }
        public string location { get; set; }
        public string lang { get; set; }
        public string name { get; set; }
        public string value { get; set; }
        //public int created_at { get; set; }
        //public int updated_at { get; set; }
    }
}
