﻿using System.Collections.Generic;

namespace Giift.SuciHalal.Entities
{
    public class CategoriesTree
    {
        public string name { get; set; }

        public List<ChildCategories> children { get; set; }
    }
}