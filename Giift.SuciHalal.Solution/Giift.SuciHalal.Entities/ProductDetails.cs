﻿namespace Giift.SuciHalal.Entities
{
    public class ProductDetails
    {
        public string Id { get; set; } = string.Empty;
        public string Name { get; set; } = string.Empty;
        public string ImageUrl { get; set; } = string.Empty;
        public string Price { get; set; } = string.Empty;
        public string Category { get; set; } = string.Empty;
        public string Currency { get; set; } = string.Empty;
    }
}