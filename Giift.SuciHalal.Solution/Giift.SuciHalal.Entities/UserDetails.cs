﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Giift.SuciHalal.Entities
{
    public class UserDetails
    {
        public string id { get; set; }
        public string email { get; set; }
        public string lang { get; set; }
        public string status { get; set; }
        public string givenName { get; set; }
        public string familyName { get; set; }
        public string nickname { get; set; }
        public IList<Adr> adr { get; set; }
        public IList<Tel> tel { get; set; }
        public string bday { get; set; }
        public string gender { get; set; }
        public string currency { get; set; }
        public int created_at { get; set; }
        public int updated_at { get; set; }
    }

    public class Adr
    {
        [JsonProperty(PropertyName = "street-address")]
        public string streetaddress { get; set; }

        [JsonProperty(PropertyName = "postal-code")]
        public string postalcode { get; set; }
        public string locality { get; set; }
        public object region { get; set; }

        [JsonProperty(PropertyName = "country-name")]
        public string countryname { get; set; }

    }

    public class Tel
    {
        public string value { get; set; }
        public string prefix { get; set; }
    }
}
