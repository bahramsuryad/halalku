﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Giift.SuciHalal.Entities
{
    public class OrderHistoryDetails : OrderBaseObject
    {
        public List<OrderItemDetails> CartOrderItems { get; set; }
        public string UserId { get; set; } = string.Empty;
        public string CartId { get; set; } = string.Empty;
        public double Value { get; set; } = 0;
        public List<CartDeliveryDetails> CartDeliveries { get; set; }
    }
}
