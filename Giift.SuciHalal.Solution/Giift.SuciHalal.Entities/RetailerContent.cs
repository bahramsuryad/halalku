﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Giift.SuciHalal.Entities
{
    public class RetailerContent
    {
        public string id { get; set; }
        public string retailer { get; set; }
        public string key { get; set; }
        public string lang { get; set; }
        public string value { get; set; }
        public int created_at { get; set; }
        public int updated_at { get; set; }
    }
}
