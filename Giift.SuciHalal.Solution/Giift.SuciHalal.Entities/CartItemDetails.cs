﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Giift.SuciHalal.Entities
{
    public class CartItemDetails : CartBaseObject
    {
        public int Quantity { get; set; } = 0;
        public string Currency { get; set; } = string.Empty;
        public string BuyableType { get; set; } = string.Empty;
        public string BuyableId { get; set; } = string.Empty;
        public string CartId { get; set; } = string.Empty;        
        public string Name { get; set; } = string.Empty;
        public string Img { get; set; } = string.Empty;
    }
}
