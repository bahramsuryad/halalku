﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Giift.SuciHalal.Entities
{
    public class OrderItemDetails : OrderBaseObject
    {
        public string CartItemId { get; set; } = string.Empty;
        public string OrderId { get; set; } = string.Empty;
        public string DeliveryId { get; set; } = string.Empty;
        public string Currency { get; set; } = string.Empty;
        public string Name { get; set; } = string.Empty;
        public string Img { get; set; } = string.Empty;
        public int Qty { get; set; } = 0;        
    }
}
