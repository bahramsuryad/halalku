﻿namespace Giift.SuciHalal.Entities
{
    public class UnitDetail
    {
        public string unit { get; set; }

        public string type { get; set; }

        public int? value { get; set; }

        public double? price { get; set; }

        public double? unitPrice { get; set; }

        public int? step { get; set; }

        public int? min { get; set; }

        public int? max { get; set; }
    }
}