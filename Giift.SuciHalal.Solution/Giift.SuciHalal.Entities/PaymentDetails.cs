﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Giift.SuciHalal.Entities
{
    public class PaymentDetails
    {
        public string OrderId { get; set; } = string.Empty;
        public double Amount { get; set; } = 0.0F;
        public string PaymentId { get; set; } = string.Empty;
        public string PaymentToken { get; set; } = string.Empty;
        public double ShippingFee { get; set; } = 0.0F;
    }
}
